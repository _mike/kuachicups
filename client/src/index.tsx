import "@/injectLogLevel"; // @ import so tsfmt doesn't put it on the bottom
import { render } from "solid-js/web";
import App from "./components/App";
import { Provider as Auth } from "./context/Auth";
import { Provider as Size } from "./context/Size";
import { Provider as Router } from "./router";
import "./serviceWorker";
import { ToastProvider } from "./shared/ui/Toast";
import "./theme/globals.scss";

log.setLevel(0);
log.debug("Starting kuachi.gg");

function Main() {
  return (
    <Size>
      <Router>
        <Auth>
          <ToastProvider>
            <App />
          </ToastProvider>
        </Auth>
      </Router>
    </Size>
  );
}

render(Main, document.getElementById("app")!);
