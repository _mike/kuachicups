import createRouter, { State as RouteState, Router as Router5 } from "router5";
import browserPluginFactory from "router5-plugin-browser";
import createSolidRouter, { ReadRoutes } from "solid-typefu-router5";
import { RSM } from "solid-typefu-router5/dist/components/Router";
import routes from "./definition";
import { O, U } from "ts-toolbelt";
// import Analytics from "analytics";
// // @ts-ignore
// import GoogleAnalytics from "@analytics/google-analytics";

export { useIsActive, useRoute } from "solid-typefu-router5";
export type { RouteState };

export const LOGIN_DEST_KEY = "login_dest";

export type Routes = ReadRoutes<typeof routes>;

export type RenderAllRoutes = RSM<Routes>;

export type RenderRoutes<Path extends string[]> = O.Path<RenderAllRoutes, Path>;

export type RouteName = Routes[number]["name"];

type RouteLike = { name: string };
export type RoutesByName = U.IntersectOf<
  Routes[number] extends infer Route_
  ? Route_ extends infer Route
  ? Record<Extract<Route, RouteLike>["name"], Route>
  : never
  : never
>;

/** Routes that have a page param */
export type PagedLink =
  | "cups"
  | "cups.matches"
  | "news"
  | "players"
  | "teams"
  | "teams.mine"
  | "media";

// export const tracker = Analytics({
//   app: "kuachi.gg",
//   plugins: [
//     GoogleAnalytics({
//       measurementIds: ["G-FGNJ1M9YJY"],
//     }),
//   ],
// });
//
export const { Router, Provider, Link, router, navigate } = createSolidRouter<
  typeof routes,
  Routes
>({
  routes,

  createRouter5: (routes) => {
    const router = createRouter(routes, { allowNotFound: true });
    router.usePlugin(browserPluginFactory({ useHash: false }));
    router.forward("home", "news");
    // initAnalytics(router);
    return router;
  },

  onStart: (router) => {
    // Perform login redirect
    const here = router.getState();
    switch (here.name) {
      case "logged_in": {
        const dest = localStorage.getItem(LOGIN_DEST_KEY);
        const state = here.params.state;
        log.debug("logged_in:", { dest, state });
        if (dest !== null) {
          const redirect: { state: string; dest: RouteState } = JSON.parse(dest);
          if (redirect.state === state) {
            router.navigate(redirect.dest.name, redirect.dest.params);
          } else {
            log.warn("invalid login dest", { dest, state });
            router.navigate("home");
          }
          localStorage.removeItem(LOGIN_DEST_KEY);
        }
        break;
      }
    }
  },
});

router.subscribe((state) => {
  log.debug("router update", state);
});

// function initAnalytics(router: Router5) {
//   if ("gtag" in window) {
//     // if google analytics is on
//     router.subscribe((state) => {
//       const location = window.location;
//       try {
//         window.gtag("event", "page_view", {
//           page_path: location.pathname + location.search + location.hash,
//           page_title: document.title,
//           page_search: location.search,
//           page_hash: location.hash,
//         });
//       } catch (e) {
//         log.error("gtag", e);
//       }
//     });
//   } else {
//     router.subscribe((_state) => {
//       try {
//         tracker.page();
//       } catch (e) {
//         log.error("ga4mp", e);
//       }
//     });
//   }
// }
//
(window as any).router = router;

////////////////////////////////////////////////////////////////////////////////
// Param parsing functions
//

export function intParam(param: string | undefined, def: number = 0): number {
  if (typeof param === "string") {
    const n = Number(param);
    if (Number.isSafeInteger(n)) return n;
  }
  log.warn("intParam failed for", param);
  return def;
}

export function stringParam(param: string | undefined, def: string = ""): string {
  if (typeof param === "string") return param;
  return def;
}

export function arrayParam<T>(
  parse: (param: string | undefined) => T,
  param: string | undefined,
  def: T[] = []
) {
  if (param === undefined) return def;
  if (param === "") return def;
  return param.split(",").map(parse);
}

export function boolParam(param: string | boolean | undefined, def: boolean = false) {
  if (param === undefined) return def;
  if (param === "") return def;
  if (typeof param === "boolean") return param;
  if (typeof param === "string") return param.startsWith("t");
  return def;
}
