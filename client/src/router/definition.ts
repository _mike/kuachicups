const routes = [
  { name: "home", path: "/" },

  { name: "reset_csrf", path: "/reset_csrf" },

  {
    name: "news",
    path: "/ann?page",
    children: [
      { name: "unpublished", path: "/unpublished" },
      { name: "new", path: "/new" },
      {
        name: "item",
        path: "/:id",
        children: [{ name: "edit", path: "/edit" }],
      },
    ],
  },

  { name: "logged_in", path: "/logged_in" },

  { name: "privacy_policy", path: "/privacy_policy" },

  { name: "media", path: "/media?page" },

  { name: "donate", path: "/donate" },

  { name: "streaming", path: "/streaming" },

  {
    name: "games",
    path: "/games",
    children: [
      {
        name: "rankings",
        path: "/:gameId/:modeId",
      },
      {
        name: "pickbans",
        path: "/pickbans"
      }
    ]
  },

  {
    name: "teams",
    path: "/teams?page&query",
    children: [
      { name: "invites", path: "/invites" },
      { name: "mine", path: "/mine" },
      { name: "new", path: "/new" },
      {
        name: "profile",
        path: "/:id",
        children: [
          {
            name: "members",
            path: "/members",
          },
          {
            name: "your-membership",
            path: "/membership",
          },
          {
            name: "manage",
            path: "/manage",
          },
        ],
      },
    ],
  },

  {
    name: "cups",
    path: "/cups?page&active",
    children: [
      { name: "new", path: "/new" },
      { name: "matches", path: "/matches?page&is_scored&player_ids&team_ids&cup_id&stage_id" },
      {
        name: "cup",
        path: "/:id",
        children: [
          {
            name: "manage",
            path: "/manage",
            children: [{ name: "stage", path: "/stage/:stageNo" }],
          },
          { name: "edit", path: "/edit" },
          {
            name: "matches",
            path: "/matches",
            children: [
              { name: "match", path: "/match/:matchId" },
              { name: "own", path: "/own" },
              { name: "recent", path: "/recent" },
              { name: "upcoming", path: "/upcoming" },
            ],
          },
          {
            name: "stage",
            path: "/stage/:stageNo",
            children: [
              { name: "placements", path: "/placements" },
              { name: "bracket", path: "/bracket" },
              { name: "play", path: "/play" },
            ],
          },
        ],
      },
    ],
  },

  {
    name: "players",
    path: "/players?page",
    children: [{ name: "profile", path: "/:id" }],
  },

  {
    name: "stream",
    path: "/stream?cfg&secret",
    children: [
      { name: "docs", path: "/docs" },
      { name: "overlay", path: "/overlay" },
      { name: "remote", path: "/remote" },
    ],
  },

  {
    name: "overlay_dev", // dev only
    path: "/stream/overlay/index.html?cfg&secret",
  },

  {
    name: "overlay-remote_dev", // dev only
    path: "/stream/overlay/remote/index.html?secret",
  },
] as const;

export default routes;
