import log from "loglevel";
(window as any).log = log;

/** Add 'loglevel' to the global scope */
declare global {
  const log: typeof import("loglevel");
}
