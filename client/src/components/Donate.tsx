import dazza from "@/resources/dazza256.png";
import redbubble from "@/resources/redbubble.png?width=32";
import { styled } from "solid-styled-components";

const DonateLink = styled("a")`
  margin-right: var(--spacing);
  margin-bottom: var(--spacing);
`;

export default function Donate() {
  return (
    <div class="container">
      <img style="float:right" src={dazza} />
      <h1>Support kuachi.gg!</h1>
      <p>
        kuachi.gg is run by a team of volunteers who donate their time (and money) to run AFPS
        tournaments and events.
      </p>
      <p>
        <a href="https://quasimal.com">Mike</a> built, maintains and serves the
        website since around 2020. If you like it, send me a coffee at the
        Ko-fi link below.
      </p>
      <p>
        If you want to contribute to prize pools in current competitions,
        please message an admin in the Discord server.
      </p>
      <DonateLink class="contrast" role="button" href="https://feetwind.redbubble.com">
        <img style="height:24px" src={redbubble} /> <b>Merch</b>, including AQL shirts, mugs, and
        more
      </DonateLink>
      <DonateLink href="https://ko-fi.com/C0C5GJXC3">
        <img style="height:32px" src="https://ko-fi.com/img/githubbutton_sm.svg" />
      </DonateLink>
    </div>
  );
}
