import { api } from "@/api";
import { createSignal, ErrorBoundary, For, Show } from 'solid-js'
import { useAsync } from "@/shared/async/Awaitable";
import * as F from "@/shared/ui/Form2";
import { createStore } from "solid-js/store";

export default () => {
  const [store, setStore] = createStore<{
    splitNumber: number,
    enabled: Record<string, boolean>
  }>({ splitNumber: 2, enabled: {...defaultQCMapPool} });
  const [formState, setFormState] = createSignal<FormState>({ tag: FormStateTag.Init })
  const { Fld, optic: sf } = F.createField(store, setStore);
  return <div>
    <h1>KDC Split Pickban Generator</h1>
    <Fld label="Split number" optic={sf.prop("splitNumber")}>
      {(value, onChange) =>
        <F.StrictNumberField value={value()} onChange={v => onChange(v ?? 0)} />
      }
    </Fld>
    <label>Maps</label>
    {Object.keys(defaultQCMapPool).map(map =>
      <Fld optic={sf.prop("enabled").prop(map)}>
        {(value, onChange) =>
          <F.CheckBoxField label={map} onChange={onChange} value={value()} />
        }
      </Fld>
    )}
    <br/>
    <button
      disabled={formState().tag !== FormStateTag.Init}
      onClick={() => {
        const { splitNumber, enabled: enabledMaps } = store;
        const maps = Object.entries(enabledMaps).filter(v => v[1]).map(v => ({
          title: v[0],
          slug: qcMapSlugs[v[0]]
        }));
        setFormState({ tag: FormStateTag.Submitted });
        api.gameKdcSplitPickbanGenerate({ maps, split_number: splitNumber }).then(() => {
          setFormState({ tag: FormStateTag.Done });
          setTimeout(() => {
            setFormState({ tag: FormStateTag.Init });
          }, 5000)
        }, (error) => {
          setFormState({ tag: FormStateTag.Error, error })
        });
      }}
    >
      Create
    </button>
    <Show when={formState().tag === FormStateTag.Done}>
      <p>Done</p>
    </Show>
  </div>
}


type FormState = {
  tag: FormStateTag.Init | FormStateTag.Submitted | FormStateTag.Done;
} | {
  tag: FormStateTag.Error;
  error: any;
}

enum FormStateTag {
  Init,
  Submitted,
  Done,
  Error
}

const defaultQCMapPool: Record<string, boolean> = {
  Awoken: false,
  "Blood Covenant": false,
  "Blood Run": false,
  "Burial Chamber": false,
  "Church of Azathoth": false,
  "Corrupted Keep": false,
  Crucible: false,
  "Deep Embrace": false,
  Exile: false,
  Insomnia: false,
  Lockbox: false,
  "Longest Yard": false,
  "Molten Falls": false,
  "Ruins of Sarnath": false,
  "Tempest Shrine": false,
  "Tower of Koth": false,
  "Vale of Pnath": false,
} as const;

const qcMapSlugs: Record<string, string> = {
  Awoken: "awoken",
  "Blood Covenant": "bc",
  "Blood Run": "br",
  "Burial Chamber": "burial",
  "Church of Azathoth": "azathoth",
  "Corrupted Keep": "ck",
  Crucible: "crucible",
  "Deep Embrace": "de",
  Exile: "exile",
  Insomnia: "insomnia",
  Lockbox: "lockbox",
  "Longest Yard": "longestyard",
  "Molten Falls": "mf",
  "Ruins of Sarnath": "ruins",
  "Tempest Shrine": "tempest",
  "Tower of Koth": "koth",
  "Vale of Pnath": "vale",
} as const;

