import { api } from "@/api";
import { router } from "@/router";
import { useAsync } from "@/shared/async/Awaitable"
import Container from "@/shared/ui/Container"
import Menu from "@/shared/ui/Menu"
import { For, JSX } from "solid-js"
import { useRoute } from "solid-typefu-router5";

export function GameList(props: {
  children?: JSX.Element,
}) {
  const gameModes = useAsync(api.games.allGamesAndModes);
  const route = useRoute();
  return (
    <Container>
      <Menu.Layout menu={
        <Menu.Menus>
          <Menu>
            <Menu.Title>Rankings for...</Menu.Title>
            <For each={gameModes()}>
              {m =>
                <Menu.Link
                  to="games.rankings"
                  params={{ modeId: m.mode.id, gameId: m.game.id }}
                  classList={{ "is-active":
                    ({ ...route().params }, router.isActive("games.rankings", { modeId: m.mode.id, gameId: m.game.id }))
                  }}
                >
                  {m.game.name} / {m.mode.name}
                </Menu.Link>
              }
            </For>
          </Menu>
          <Menu.Admin>
            <Menu.Title>Administration</Menu.Title>
            <Menu.Link to="games.pickbans">KDC Pickban Pools</Menu.Link>
          </Menu.Admin>
        </Menu.Menus>
      }>
        {props.children}
      </Menu.Layout>
    </Container>
  );
}

