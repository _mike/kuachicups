import { api, PostMedia } from "@/api";
import useAuth from "@/context/Auth";
import { navigate } from "@/router";
import Resource from "@/shared/async/Resource";
import { Lazily, LazyProvider } from "@/shared/Lazy";
import Container from "@/shared/ui/Container";
import { DiscordAvatarImg } from "@/shared/ui/Discord/Avatar";
import Icon from "@/shared/ui/Icon";
import Paged from "@/shared/ui/Paginate";
import toast from "@/shared/ui/Toast";
import clipboardCopy from "clipboard-copy";
import { format } from "date-fns";
import { createSignal, Show } from "solid-js";
import { useRoute } from "solid-typefu-router5";
import theme from "./theme.module.scss";

export default function Medias() {
  const route = useRoute();
  return (
    <Container>
      <Paged<() => Promise<PostMedia>>
        onClickPage={(page) => navigate({ to: "media", params: { page: page + "" } })}
        getPage={() => route().params.page}
        source={(pg) => api.media.page(pg)}
        renderItem={(item) => (
          <Lazily>
            <Resource data={item} fallback={MediaLoading}>
              {(item) => <MediaView item={item} showCopyID />}
            </Resource>
            <hr />
            <br />
          </Lazily>
        )}
        renderItems={(p) => (
          <LazyProvider length={() => p.items.length} monotonic>
            <div>{p.children}</div>
          </LazyProvider>
        )}
        renderContent={(c) => c}
      />
    </Container>
  );
}

export function MediaView(p: { item: PostMedia; showCopyID?: boolean }) {
  const auth = useAuth();
  return (
    <div class={theme.mediaView}>
      <div class={theme.mediaAvatar}>
        <MediaAvatar userId={p.item.discord_user_id} avatarId={p.item.discord_avatar} />
      </div>
      <div class={theme.mediaContent}>
        <div class="flex-row">
          <strong class={theme.username}>{p.item.discord_name}</strong>
          <small class={theme.date}>{format(new Date(p.item.discord_timestamp), "PPpp")}</small>
        </div>
        <small class={theme.messageContent} innerHTML={p.item.discord_content_html ?? ""} />
        <Show when={typeof p.item.embed_kind === "string"}>
          <EmbedView {...p.item} />
        </Show>
        <Show when={p.showCopyID && auth().admin}>
          <button
            class="secondary"
            style="margin-top:var(--spacing)"
            onClick={() => {
              clipboardCopy(p.item.id).then(
                () => {
                  toast.success(() => (
                    <>
                      Copied <code>{p.item.id}</code>
                    </>
                  ));
                },
                () => {
                  toast.failure(() => (
                    <>
                      Could not copy ID <code>{p.item.id}</code>!
                    </>
                  ));
                }
              );
            }}
          >
            <Icon type="copy" /> Copy ID
          </button>
        </Show>
      </div>
    </div>
  );
}

function MediaAvatar(p: { userId: string; avatarId: string | undefined | null }) {
  return () =>
    typeof p.avatarId === "string" ? (
      <DiscordAvatarImg discordId={p.userId} discordAvatar={p.avatarId} size="64" />
    ) : (
      <Icon type="person" />
    );
}

export function EmbedView(p: PostMedia) {
  if (p.embed_kind === "video") {
    if (typeof p.embed_thumbnail_url === "string" && typeof p.embed_video_url === "string") {
      return (
        <EmbedVideoView videoUrl={p.embed_video_url} thumbnailUrl={p.embed_thumbnail_url} {...p} />
      );
    }
  } else if (p.embed_kind === "link") {
    return <EmbedLink {...p} />;
  }
  return (
    <pre>
      <code>{JSON.stringify(p, null, 2)}</code>
    </pre>
  );
}

function EmbedLink(props: PostMedia) {
  return (
    <>
      <a href={props.embed_url ?? "#"}>{props.embed_title}</a>
      <p>{props.embed_description}</p>
    </>
  );
}

function EmbedVideoView(
  p: PostMedia & {
    thumbnailUrl: string;
    videoUrl: string;
  }
) {
  const [showEmbed, setShowEmbed] = createSignal(false);
  return (
    <article class={theme.embedVideoView}>
      <Show when={typeof p.embed_author_name === "string"}>
        <h3>
          {typeof p.embed_author_url === "string" ? (
            <a href={p.embed_author_url}>{p.embed_author_name}</a>
          ) : (
            p.embed_author_name
          )}
        </h3>
      </Show>
      <Show when={typeof p.embed_title === "string"}>
        <h3>
          {typeof p.embed_video_url === "string" ? (
            <a href={p.embed_video_url}>{p.embed_title}</a>
          ) : (
            p.embed_title
          )}
        </h3>
      </Show>
      {showEmbed() ? (
        <EmbedVideo
          provider={p.embed_provider_name ?? undefined}
          video={p.videoUrl}
          href={p.embed_video_url ?? undefined}
        />
      ) : (
        <a
          class={theme.embedThumbnail}
          href={p.embed_url ?? undefined}
          onClick={(ev) => {
            ev.preventDefault();
            setShowEmbed(true);
          }}
        >
          <Icon class={theme.playIcon} type="play" />
          <img src={p.thumbnailUrl} />
        </a>
      )}
    </article>
  );
}

function EmbedVideo(props: {
  provider: string | undefined;
  video: string;
  href: string | undefined;
}) {
  switch (props.provider) {
    case "Twitch":
      return (
        <iframe
          class={theme.embedVideo}
          src={props.video.replace("&parent=meta.tag", "&parent=kuachi.gg")}
        />
      );
    default:
      return <iframe class={theme.embedVideo} src={props.video} />;
  }
}

function MediaLoading() {
  return <div aria-busy="true" />;
}
