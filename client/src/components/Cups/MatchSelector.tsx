import { CupMatch2p } from "@/api";
import { navigate } from "@/router";
import Icon from "@/shared/ui/Icon";
import Menu from "@/shared/ui/Menu";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import { selection } from "d3";
import { createContext, For, Show, useContext } from "solid-js";
import { createMatchOverlay, encodeConfig } from "../Overlay/Config/config";
import { MatchTitle } from "./Cup/Match/MatchTitle";

export const MatchSelectorContext = createContext<{
  selection: Record<number, CupMatch2p>;
  size: number;
}>();

export function MatchSelectorProvider() {}

export default function MatchSelectorMenu() {
  const ctx = useContext(MatchSelectorContext)!;
  return (
    <Menu.MenuPortal side="left">
      <Menu>
        <Menu.Title>Selected matches</Menu.Title>
        <For
          each={Object.keys(ctx.selection).sort()}
          fallback={() => <Menu.Item>No matches selected</Menu.Item>}
        >
          {(matchId) => (
            <Menu.Item>
              <MatchTitle match={ctx.selection[Number(matchId)]} longFormat={false} />
            </Menu.Item>
          )}
        </For>
        <Menu.Title>Stream Overlay</Menu.Title>
        <Menu.ItemLink
          onClick={() => {
            (async () => {
              const overlay = await createMatchOverlay(Object.values(ctx.selection));
              const cfg = await encodeConfig(overlay);
              navigate({ to: "stream", params: { cfg } });
            })().catch((err) => {
              toast.failure(() => (
                <>
                  Cannot create overlay config!
                  <FormatResponseError error={err} />
                </>
              ));
            });
          }}
          disabled={ctx.size === 0}
        >
          <Icon type="tv" /> Create stream overlay
        </Menu.ItemLink>
        <Show when={ctx.size === 0}>
          <Menu.Item>
            <small>Select matches in order to create an overlay</small>
          </Menu.Item>
        </Show>
      </Menu>
    </Menu.MenuPortal>
  );
}
