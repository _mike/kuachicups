import { format } from "date-fns";
import { Show } from "solid-js";
import { api, Cup, CupStage, Game, GameMode, Paged, PagedForUuid, Team } from "@/api";
import { intParam, Link, navigate, useRoute } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import { formatCheckinDuration } from "../Cup/index";
import Pages from "@/shared/ui/Paginate";
import { unpaged } from "@/shared/ui/Dropdown";
import Icon from "@/shared/ui/Icon";
import theme from "./theme.module.scss";
import Menu from "@/shared/ui/Menu";
import { parseDate } from "@/shared/date";
import Container from "@/shared/ui/Container";

export default function CupList(props: { type: "active" | "inactive" }) {
  const route = useRoute();
  return (
    <Container>
      <Menu.Layout
        menu={
          <>
            <Menu.Admin>
              <Menu.Title>Administration</Menu.Title>
              <Menu.Link
                to="cups"
                params={{ page: "0", active: "inactive" }}
                classList={{ "is-active": route().params?.active === "inactive" }}
              >
                Unpublished cups
              </Menu.Link>
              <Menu.Link to="cups.new">Create</Menu.Link>
              <Menu.Link to="cups.matches" params={{ page: "0", is_scored: "false" }}>
                Unconfirmed matches
              </Menu.Link>
            </Menu.Admin>
          </>
        }
      >
        <CupPages type={props.type} />
      </Menu.Layout>
    </Container>
  );
}

function CupPages(props: { type: "active" | "inactive" }) {
  const route = useRoute();

  async function inactiveCups(): Promise<[Paged, Cup[]]> {
    const pageNo = intParam(route().params?.page, 0);
    const page = await api.cupsCupsUnpublishedPage(pageNo);
    const cups = await api.cups.byIds(page.items.map((i) => i.id));
    return Promise.resolve([page, cups]);
  }

  function getCups(page: number) {
    if (props.type === "active") return api.cups.page(page);
    return inactiveCups();
  }

  return (
    <Pages
      onClickPage={(page) => {
        const ps = route().params;
        navigate({ to: "cups", params: { ...ps, page: page + "" } });
      }}
      source={getCups}
      renderContent={(c) => c}
      renderItem={(cup) => {
        const info = useAsync(async (ref) => {
          const mode = await api.games.modes.byId(cup.game_mode_id, ref);
          const game = await api.games.byId(mode.game_id, ref);
          const host = await api.teams.byId(cup.owner_team_id, ref);
          const stage0 = await api.cups.stages.byId(cup.id, 0, ref);
          return { cup, game, mode, host, stage0 };
        });
        return <info.Await>{CupListItem}</info.Await>;
      }}
      renderItems={(p) => <div class={theme.list}>{p.children}</div>}
    />
  );
}

function CupListItem(p: { cup: Cup; mode: GameMode; game: Game; host: Team; stage0?: CupStage }) {
  return (
    <div class={theme.item}>
      <h1 class={theme.title}>
        {/*
        // <img
        //   class={theme.itembg}
        //   src="https://images.ctfassets.net/rporu91m20dc/6ydEF5mcnuYKEooWi4aMu4/1c1d64a93145ad5fb9a1f568c00eecf9/QC_Parch_Logo_HERO_1920x870.jpg"
        // />
        */}
        {p.cup.is_finished ? [<Icon type="trophy" />, " "] : undefined}
        <Link to="cups.cup" params={{ id: p.cup.id }}>
          {p.cup.title}
        </Link>
        <div class="subtitle">
          {p.game?.name} / {p.mode?.name}
        </div>
      </h1>
      <div class={theme.info}>
        <span>
          <Icon type="calendar" /> {format(parseDate(p.stage0!.starttime)!, "PPp")}
        </span>
        <Show when={formatCheckinDuration(p.stage0!)}>
          {(dur) => (
            <span>
              <Icon type="clock" /> {dur} check-in time
            </span>
          )}
        </Show>
      </div>
    </div>
  );
}
