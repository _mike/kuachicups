import { sub } from "date-fns";
import {
  union,
  array,
  boolean,
  create,
  date,
  enums,
  intersection,
  masked,
  object,
  optional,
  refine,
  size,
  string,
  StructError,
  trimmed,
} from "superstruct";
import {
  CupCreateForm,
  CupStageCreateForm,
  CupStageUpdateForm,
  CupUpdateForm,
} from "../../../../api";
import { datetime, optionalS, selected } from "../../../../superstruct/form_util";
import { Nat, Odd, Pos } from "../../../../superstruct/numbers";
import { Uuid } from "../../../../superstruct/uuid";
import { CupForm, CupStageForm } from "./types";

/*
 * Validating/parsing a [[CupForm]] (the UI state) into a [[CupCreateForm]]
 * (what the backend wants)
 */
export function parseCupCreateForm(form: CupForm): StructError | CupCreateForm {
  try {
    const f = { ...form, stages: form.stages.map(prepareStageForm) };
    return create(f, cupCreateForm) as CupCreateForm;
  } catch (e: any) {
    return e;
  }
}

export interface CupUpdateForm_ {
  cup: CupUpdateForm;
  stages: CupStageUpdateOrCreate[];
}

export type CupStageUpdateOrCreate =
  | ({ id: string } & CupStageUpdateForm)
  | ({ id: undefined } & CupStageCreateForm);

/**
 * Validating cup updates
 */
export function parseCupUpdateForm(form: CupForm): StructError | CupUpdateForm_ {
  try {
    const cup = create(form, cupUpdateForm);
    const preparedStages = form.stages.map(prepareStageForm);
    const stages = create({ stages: preparedStages }, cupStageUpdateForm);
    return { cup, stages: stages.stages };
  } catch (e: any) {
    return e;
  }
}

const cupStageScoringForm = masked(
  object({
    bestof: intersection([Pos, Odd]),
    groups: boolean(),
    elim_round: optionalS(Nat),
    is_lb: optional(boolean()),
    pickbans: optionalS(Nat),
    is_point_score: optional(boolean()),
    is_time_score: optional(boolean()),
    is_time_score_race: optional(boolean()),
    point_score_greatest_is_winner: optional(boolean()),
  })
);

const cupStageCreateForm = refine(
  masked(
    object({
      id: optional(Uuid), // actually only used for updates, yolo
      // superstruct seems to break coercians with assign/intersection so that
      // is why
      format: selected(enums(["groups", "single_elim", "double_elim"])),
      group_size: optionalS(Pos),
      group_rematches: optionalS(Nat),
      max_participants: optionalS(Pos),
      scorings: size(array(cupStageScoringForm), 1, 10),
      start_immediately: boolean(),
      starttime: datetime(optional(date())),
      checkintime: datetime(optional(date())),
      title: size(trimmed(string()), 1, 80),
    })
  ),
  "CupStageCreateForm.scorings.size",
  (s) => {
    switch (s.format) {
      case "double_elim":
        return s.scorings.length > 1;
    }
    return true;
  }
);

const cupCreateForm = masked(
  object({
    description: size(trimmed(string()), 1, 2000),
    game_mode_id: selected(Uuid),
    overlay_theme: optional(string()),
    is_published: boolean(),
    is_signups_closed: boolean(),
    name: size(trimmed(string()), 1, 80),
    owner_team_id: selected(Uuid),
    stages: size(array(cupStageCreateForm), 1, 100),
  })
);

const cupUpdateForm = masked(
  object({
    description: size(trimmed(string()), 1, 2000),
    game_mode_id: selected(Uuid),
    overlay_theme: optional(string()),
    is_published: boolean(),
    is_signups_closed: boolean(),
    name: size(trimmed(string()), 1, 80),
    owner_team_id: selected(Uuid),
  })
);

const cupStageUpdateForm = masked(
  object({
    stages: size(array(cupStageCreateForm), 1, 100),
  })
);

function prepareStageForm(s: CupStageForm) {
  return {
    ...s,
    index: undefined,
    starttime: s.starttime.parsed as Date,
    checkinduration: undefined,
    checkintime: s.start_immediately
      ? undefined
      : sub(s.starttime.parsed as Date, {
          minutes: s.checkinduration as number,
        }),
  };
}
