import { api } from "@/api";
import { themes } from "@/components/Overlay/themes";
import useAuth from "@/context/Auth";
import { intParam } from "@/router";
import Dropdown, { simpleItems, unpaged } from "@/shared/ui/Dropdown";
import {
  CheckBoxField,
  createField,
  DateTimeField,
  DurationField,
  MarkdownField,
  NumberField,
  RadioField,
  StrictNumberField,
  TextField,
} from "@/shared/ui/Form2";
import Icon from "@/shared/ui/Icon";
import ShowStructError from "@/superstruct/StructError";
import { Field } from "fp-ts/lib/number";
import { cloneDeep, debounce } from "lodash";
import * as O from "optics-ts";
import {
  createComputed,
  createEffect,
  createMemo,
  createSignal,
  JSX,
  Show,
  untrack,
} from "solid-js";
import { createStore, SetStoreFunction, unwrap } from "solid-js/store";
import { StructError } from "superstruct";
import { useCup, useCupMaybe } from "../Context";
import {
  createCupForm,
  createCupStageForm,
  createCupStageScoringForm,
  CupForm,
  initialiseScorings,
} from "./types";

export type CupFormData = Parameters<typeof createCupForm>[0];

export default function Form2<T>(props: {
  data?: CupFormData;
  updateMode?: boolean;
  submitLabel: JSX.Element;
  validator(cf: CupForm): StructError | T;
  onSubmit(v: T): void;
}): JSX.Element {
  const [store, _setStore] = createStore(untrack(() => createCupForm(props.data)));
  const setStore: SetStoreFunction<CupForm> = function (...args: any[]): any {
    doValidation();
    return (_setStore as any)(...args);
  };
  const { optic: cf, Fld, ArrayFld } = createField(store, setStore);
  const self = useAuth();

  const [validation, setValidation] = createSignal<StructError | T | undefined>(undefined);
  const doValidation = debounce(
    () => {
      const cf = cloneDeep(unwrap(store) as CupForm);
      setValidation(() => props.validator(cf));
    },
    250,
    { leading: false, trailing: true }
  );

  // initial validation
  createEffect(() => {
    doValidation();
  });

  return (
    <div>
      <div class="grid">
        {/* Cup title */}
        <Fld optic={cf.prop("name")} label="Title">
          {(value, onChange) => <TextField value={value()} onChange={onChange} />}
        </Fld>

        {/* Cup team owner */}
        <Fld optic={cf.prop("owner_team_id")} label="Team">
          {(value, onChange) => (
            <Dropdown
              class="is-w100"
              buttonClass="is-w100"
              value={value()}
              onChange={(opt) => onChange(opt.key + "")}
              kind="fetch"
              deps={self().login}
              options={async () => {
                const ownId = self().login?.id;
                if (ownId === undefined) return simpleItems([]);
                const roles = await api.players.teamRoles.byParentId(ownId);
                const teams = await api.teams.byIds(roles.array.map((r) => r.team_id));
                return simpleItems(
                  teams.map((t) => ({
                    id: t.id,
                    name: t.name,
                  }))
                );
              }}
            >
              Select a team
            </Dropdown>
          )}
        </Fld>
      </div>

      {/* Game and mode. Note that only the mode id is actually used in a cup as
       * that determines the game... that may change however as it does add some
       * overhead */}
      <div class="flex-row">
        {/* Game */}
        <Fld
          optic={cf.prop("game_id")}
          label="Game"
          beforeChangeHook={() => {
            setStore("game_mode_id", undefined);
          }}
        >
          {(value, onChange) => (
            <Dropdown
              kind="fetch"
              options={async (pg) => {
                const [paged, games] = await api.games.page(pg);
                return [paged, games.map((g) => ({ title: g.name, key: g.id }))];
              }}
              onChange={(opt) => onChange(opt.key)}
              value={value()}
            >
              Select game
            </Dropdown>
          )}
        </Fld>

        {/* Game mode */}
        <Fld optic={cf.prop("game_mode_id")} label="Mode">
          {(value, onChange) => (
            <Dropdown
              disabled={store.game_id === undefined}
              kind="fetch"
              deps={store.game_id}
              options={async (_page) => {
                const gameId = store.game_id;
                if (gameId !== undefined) {
                  const modes = await api.games.modes.byParentId(gameId);
                  return [
                    unpaged,
                    modes.array.map((m) => ({
                      title: m.name,
                      key: m.id,
                    })),
                  ];
                }
                return [unpaged, []];
              }}
              onChange={(opt) => onChange(opt.key)}
              value={value()}
            >
              Select mode
            </Dropdown>
          )}
        </Fld>
      </div>

      {/* Cup theme
       *
       * Just a string recognised by the overlay creator to theme an overlay
       */}
      <Fld optic={cf.prop("overlay_theme")} label="Theme" description="Used for overlays">
        {(value, onChange) => (
          <select
            value={value()}
            onChange={(ev) => {
              log.debug("hello", ev);
              onChange((ev.target as HTMLSelectElement).value ?? undefined);
            }}
          >
            {Object.keys(themes).map((theme) => (
              <option value={theme}>{themes[theme as keyof typeof themes].name}</option>
            ))}
          </select>
        )}
      </Fld>

      <br />

      {/* Cup description */}
      <Fld optic={cf.prop("description")} label="Description">
        {(value, onChange) => <MarkdownField onChange={onChange} value={value()} />}
      </Fld>

      {/* Publish immediately */}
      <Fld optic={cf.prop("is_published")}>
        {(value, onChange) => (
          <CheckBoxField label="Publish immediately" value={value()} onChange={onChange} />
        )}
      </Fld>

      {/* Signups kind */}
      <Fld optic={cf.prop("is_signups_closed")} defaultValue={false}>
        {(value, onChange) => (
          <CheckBoxField label="Open signups" value={!value()} onChange={onChange} />
        )}
      </Fld>

      {/* Stages */}
      <ArrayFld optic={cf.prop("stages")} create={createCupStageForm}>
        {({ Each, onAdd, onDelete }) => (
          <div>
            <Each>
              {(stage, i, sf) => (
                <fieldset class="box">
                  {/* Title */}
                  <Fld
                    label="Title"
                    optic={sf.prop("title")}
                    description="Leave this as default, probably."
                  >
                    {(value, onChange) => <TextField onChange={onChange} value={value()} />}
                  </Fld>

                  <Show when={i() > 0}>
                    <Fld optic={sf.prop("start_immediately")}>
                      {(value, onChange) => (
                        <CheckBoxField
                          label="Start immediately"
                          onChange={onChange}
                          value={value()}
                        />
                      )}
                    </Fld>
                  </Show>

                  <Show when={i() === 0 || !O.preview(sf.prop("start_immediately"))(store)}>
                    {/* Start time and duration */}
                    <div class="grid">
                      <Fld optic={sf.prop("starttime")} label="Start date and time">
                        {(value, onChange) => <DateTimeField value={value()} onChange={onChange} />}
                      </Fld>
                      <Fld optic={sf.prop("checkinduration")} label="Check-in duration (minutes)">
                        {(value, onChange) => (
                          <DurationField precision="m" value={value()} onChange={onChange} />
                        )}
                      </Fld>
                    </div>
                  </Show>

                  {/* Max participants */}
                  <Fld
                    optic={sf.prop("max_participants")}
                    label="Maximum participants"
                    description="Optional cut-off for the maximum number of entrants."
                  >
                    {(value, onChange) => <NumberField value={value()} onChange={onChange} />}
                  </Fld>

                  {/* Stage format selector */}
                  <Fld
                    label="Format"
                    optic={sf.prop("format")}
                    beforeChangeHook={(next) => {
                      const s = initialiseScorings(next);
                      setStore("stages", i(), "scorings", s);
                    }}
                  >
                    {(value, onChange) => (
                      <Dropdown
                        value={value()}
                        onChange={(opt) => onChange(opt.key)}
                        options={[
                          {
                            key: "double_elim",
                            title: "Double elim",
                          },
                          {
                            key: "single_elim",
                            title: "Single elim",
                          },
                          { key: "groups", title: "Groups" },
                        ]}
                      >
                        Format
                      </Dropdown>
                    )}
                  </Fld>

                  {/* Group size */}
                  <Show when={stage.format === "groups"}>
                    <Fld
                      optic={sf.prop("group_size")}
                      label="Group size"
                      description="Optional override for group sizes"
                    >
                      {(value, onChange) => <NumberField value={value()} onChange={onChange} />}
                    </Fld>
                    <Fld
                      optic={sf.prop("group_rematches")}
                      label="Rematches"
                      description="Optional number of rematches per group"
                    >
                      {(value, onChange) => <NumberField value={value()} onChange={onChange} />}
                    </Fld>
                  </Show>

                  {/* Elimination scoring config */}
                  <Show when={stage.format === "single_elim" || stage.format === "double_elim"}>
                    <ArrayFld
                      optic={sf.prop("scorings")}
                      create={() => createCupStageScoringForm()}
                    >
                      {({ Each: EachScoring, onAdd: onAddScoring, onDelete: onDeleteScoring }) => (
                        <>
                          <div class="content">
                            <ul>
                              <li>At least one scoring config must be added.</li>
                              <li>
                                There must be at least one scoring config starting at round 0.
                              </li>
                              <Show when={stage.format === "double_elim"}>
                                <li>
                                  There must be at least one scoring config starting at lower
                                  bracket round 0.
                                </li>
                              </Show>
                            </ul>
                          </div>
                          <div class="flex-row wrap">
                            <EachScoring>
                              {(scoring, j, s) => (
                                <fieldset class="box" style="width:320px">
                                  {/* Round */}
                                  <Fld
                                    optic={s.prop("elim_round")}
                                    label="Round"
                                    description={
                                      <RoundHelp
                                        max_participants={
                                          typeof stage.max_participants === "number"
                                            ? stage.max_participants
                                            : undefined
                                        }
                                        format={stage.format as any}
                                        is_lb={!!scoring.is_lb}
                                        round={
                                          typeof scoring.elim_round === "number"
                                            ? scoring.elim_round
                                            : 0
                                        }
                                      />
                                    }
                                  >
                                    {(value, onChange) => (
                                      <NumberField onChange={onChange} value={value()} />
                                    )}
                                  </Fld>

                                  {/* Bracket (lower or upper) selection */}
                                  <Show when={stage.format === "double_elim"}>
                                    <Fld optic={s.prop("is_lb")} label="Bracket">
                                      {(value, onChange) => (
                                        <RadioField
                                          values={[
                                            { key: false, name: "Upper bracket" },
                                            { key: true, name: "Lower bracket" },
                                          ]}
                                          onChange={onChange}
                                          value={value()}
                                        />
                                      )}
                                    </Fld>
                                  </Show>

                                  {/* Best of X */}
                                  <Fld optic={s.prop("bestof")} label="Best of">
                                    {(value, onChange) => (
                                      <NumberField onChange={onChange} value={value()} />
                                    )}
                                  </Fld>

                                  {/* Pickbans -- sets scoring */}
                                  <Fld optic={s.prop("pickbans")} label="Pickbans">
                                    {(value, onChange) => (
                                      <Dropdown
                                        value={value() as number | undefined}
                                        onChange={(opt) => onChange(opt.key as number)}
                                        kind="fetch"
                                        deps={[store.game_mode_id, O.preview(s)(store)?.bestof]}
                                        options={async () => [
                                          unpaged,
                                          (await api.cupsPickbans())
                                            .filter((p) => {
                                              const m = O.preview(s)(store);
                                              const g = store.game_mode_id;
                                              return (
                                                p.bestof === m?.bestof &&
                                                (p.game_mode == null || p.game_mode === g)
                                              );
                                            })
                                            .map((p) => ({ title: p.name, key: p.id })),
                                        ]}
                                      >
                                        Pickban ruleset
                                      </Dropdown>
                                    )}
                                  </Fld>

                                  {/* Remove this scoring */}
                                  <button
                                    class="button is-light is-pulled-right"
                                    onClick={() => onDeleteScoring(j())}
                                  >
                                    <Icon type="close" />
                                    <span>Remove this scoring</span>
                                  </button>
                                  <div class="is-clearfix" />
                                </fieldset>
                              )}
                            </EachScoring>
                          </div>
                          <button class="button" onClick={onAddScoring}>
                            Add scoring config
                          </button>
                          <div class="is-clearfix" />
                        </>
                      )}
                    </ArrayFld>
                  </Show>

                  {/* Groups scoring config -- much simpler, only the first element in scorings is used */}
                  <Show when={stage.format === "groups"}>
                    {/* Best of X */}
                    <Fld optic={sf.prop("scorings").at(0).prop("bestof")} label="Best of">
                      {(value, onChange) => <NumberField onChange={onChange} value={value()} />}
                    </Fld>

                    {/* Pickbans -- sets scoring */}
                    <Fld optic={sf.prop("scorings").at(0).prop("pickbans")} label="Pickbans">
                      {(value, onChange) => (
                        <Dropdown
                          value={value() as number | undefined}
                          onChange={(opt) => onChange(opt.key as number)}
                          kind="fetch"
                          deps={[store.game_mode_id, O.preview(sf)(store)?.scorings?.[0]?.bestof]}
                          options={async () => [
                            unpaged,
                            (await api.cupsPickbans())
                              .filter((p) => {
                                const m = O.preview(sf)(store)?.scorings?.[0];
                                const g = store.game_mode_id;
                                return (
                                  p.bestof === m?.bestof &&
                                  (p.game_mode == null || p.game_mode === g)
                                );
                              })
                              .map((p) => ({ title: p.name, key: p.id })),
                          ]}
                        >
                          Pickban ruleset
                        </Dropdown>
                      )}
                    </Fld>
                  </Show>

                  {/* Remove this stage */}
                  <button class="button is-light is-pulled-right" onClick={() => onDelete(i())}>
                    <Icon type="close" />
                    <span>Remove {stage.title}</span>
                  </button>
                  <div class="is-clearfix" />
                </fieldset>
              )}
            </Each>
            <button class="button" onClick={onAdd}>
              Add stage
            </button>
          </div>
        )}
      </ArrayFld>

      <br />
      <ShowStructError value={validation()} />
      <br />
      <button
        class="button is-primary"
        disabled={validation() === undefined || validation() instanceof StructError}
        onClick={() => {
          const v = validation();
          if (v === undefined) return;
          if (v instanceof StructError) return;
          return props.onSubmit(v);
        }}
      >
        {props.submitLabel}
      </button>
    </div>
  );
}

function RoundHelp(props: {
  max_participants: number | undefined;
  format: "single_elim" | "double_elim";
  is_lb: boolean;
  round: number;
}) {
  const c = useCupMaybe();
  const participants = createMemo(() => {
    const mp = props.max_participants;
    let s: number | undefined;
    if (c !== undefined) {
      s = c.signups().array.length;
    }
    if (s === undefined) return mp;
    if (mp === undefined) return s;
    const min = Math.min(s, mp);
    return min === 0 ? undefined : min;
  });
  const label = createMemo(() => {
    const { is_lb, format } = props;
    let round = props.round;
    const p = participants();
    if (p === undefined) return "";
    const size = Math.pow(2, Math.ceil(Math.log2(p)));
    const rounds = format === "single_elim" ? Math.log2(size) : 2 * Math.log2(size);
    function cmp() {
      if (round > rounds) {
        return `Error: max round is ${rounds}`;
      }
      if (format === "single_elim") {
        if (round === rounds - 1) return "Grand Final";
        if (round === rounds - 2) return "≥ Semi-Final";
        if (round === rounds - 3) return "≥ Quarter-Final";
        return `≥ Round ${round + 1}`;
      } else {
        if (!is_lb) {
          const upperBracketRounds = rounds / 2;
          const lowerBracketRounds = rounds - 2;
          const upperBracketRoundStart = lowerBracketRounds - upperBracketRounds;
          round -= upperBracketRoundStart - 1;
          if (round > upperBracketRounds + 1)
            return `Error: max UB round is ${upperBracketRounds + 1}`;
          if (round === upperBracketRounds + 1) return "Grand Final (2)";
          if (round === upperBracketRounds - 0) return "≥ Grand Final (1)";
          if (round === upperBracketRounds - 1) return "≥ WB Final";
          if (round === upperBracketRounds - 2) return "≥ WB Semi-Final";
          if (round === upperBracketRounds - 3) return "≥ WB Quarter-Final";
          if (round <= 0) return `≥ WB Round ${round} (!)`;
          return `≥ WB Round ${round}`;
        } else {
          const lowerBracketRounds = rounds - 2;
          if (round > lowerBracketRounds - 1) return `Error: max LB round is ${rounds}`;
          if (round === lowerBracketRounds - 1) return "LB Final (2)";
          if (round === lowerBracketRounds - 2) return "≥ LB Final (1)";
          return `≥ LB Round ${round + 1}`;
        }
      }
    }
    return `${cmp()} (with size ${p}=${size} tournament)`;
  });
  return (
    <span>
      <Show
        when={participants() !== undefined}
        fallback={"no signups or max participants for help message"}
      >
        {label()}
      </Show>
    </span>
  );
}
