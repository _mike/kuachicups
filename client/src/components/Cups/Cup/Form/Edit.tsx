import { api, CupStageScoring } from "@/api";
import { navigate } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import Load from "@/shared/async/Load";
import { useCup } from "../Context";
import CupForm, { CupFormData } from "./Form2";
import { CupUpdateForm_, parseCupUpdateForm } from "./validate";

export default function CupEditForm(props: { cupId: string }) {
  const ctx = useCup();

  const cupFormData = useAsync<CupFormData>(async () => {
    ctx.cup();
    ctx.stages();
    const cup = await ctx.cup.then();
    const stages = (await ctx.stages.then()).array;
    const pp = [];
    const scorings: { [stageId: string]: CupStageScoring[] } = {};
    for (const stage of stages) {
      pp.push(
        api.cupsCupStageScorings(stage.id).then((these) => {
          scorings[stage.id] = these;
        })
      );
    }
    await Promise.all(pp);
    const gameMode = await api.games.modes.byId(cup.game_mode_id);
    const gameId = gameMode.game_id;
    return {
      cup,
      scorings,
      stages,
      gameId,
    };
  });

  async function onUpdateCup(cf: CupUpdateForm_) {
    await api.cupsCupUpdate(props.cupId, {
      game_mode_id: cf.cup.game_mode_id,
      is_published: cf.cup.is_published,
      is_signups_closed: cf.cup.is_signups_closed,
      name: cf.cup.name,
      description: cf.cup.description,
    });
    await Promise.all(
      cf.stages.map((stage, stage_no) =>
        stage.id === undefined
          ? api.cupsCupStageCreate(props.cupId, stage_no, stage)
          : api.cupsCupStageUpdate(stage.id, stage)
      )
    );
    delete api.cups.cache[props.cupId];
    delete api.cups.stages.cache[props.cupId];
    navigate({ to: "cups.cup", params: { id: props.cupId } });
  }

  return (
    <div>
      <Load
        component={CupForm}
        onSubmit={onUpdateCup}
        submitLabel="Update"
        validator={parseCupUpdateForm}
        data={Load.memo(cupFormData)}
        updateMode
      />
    </div>
  );
}
