import { api, CupCreateForm as ICupCreateForm } from "@/api";
import { navigate } from "@/router";
import { toastFailure } from "@/shared/ui/Toast";
import Form2 from "./Form2";
import { parseCupCreateForm } from "./validate";

export default function CupCreateForm() {
  function onCreateCup(cupForm: ICupCreateForm) {
    api.cups.create(cupForm).then(
      (cupId) => {
        navigate({ to: "cups.cup", params: { id: cupId } });
      },
      (err) => {
        toastFailure(String(err));
      }
    );
  }
  return (
    <Form2
      updateMode={false}
      onSubmit={onCreateCup}
      submitLabel="Create"
      validator={parseCupCreateForm}
    />
  );
}
