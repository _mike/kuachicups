import { format } from "date-fns";
import {
  Cup,
  CupStage,
  CupStageCreateForm,
  CupStageFormat,
  CupStageScoring,
  CupStageUpdateForm,
  CupUpdateForm,
} from "@/api";
import { asIDT, IDateTimeStr } from "@/shared/ui/Form2/Field/DateTime";
import { parseDate } from "@/shared/date";
import { CupStageUpdateOrCreate } from "./validate";

export interface CupForm {
  name: string;
  game_id: undefined | string;
  game_mode_id: undefined | string;
  owner_team_id: undefined | string;
  description: string;
  is_published: boolean;
  is_signups_closed: boolean;
  stages: CupStageForm[];
  overlay_theme: undefined | string;
  is_new?: boolean;
}

export interface CupStageForm {
  cupId?: string;
  id?: string;
  index: number;
  title: string;
  // gameMatchControlId: { active: undefined | string },
  start_immediately: boolean;
  starttime: IDateTimeStr;
  checkinduration: number | string;
  max_participants: number | string;
  format: undefined | CupStageFormat;
  group_size: string | number;
  group_rematches: string | number;
  //  scoringGroup: StageScoringForm;
  scorings: CupStageScoringForm[];
}

export interface CupStageScoringForm {
  bestof: number | string;
  elim_round: number | string;
  is_lb: undefined | boolean;
  pickbans: number | string;
  groups: boolean;
  is_point_score: boolean;
  point_score_greatest_is_winner: boolean;
  is_time_score: boolean;
  is_time_score_race: boolean;
}

export function createCupForm(h?: {
  cup: Cup;
  game_id?: string;
  stages: readonly CupStage[];
  scorings: { readonly [stageId: string]: readonly CupStageScoring[] };
}): CupForm {
  return {
    game_id: h?.game_id,
    game_mode_id: h?.cup.game_mode_id,
    overlay_theme: h?.cup?.overlay_theme ?? "generic",
    name: h?.cup.title ?? "",
    description: h?.cup.description_md ?? "",
    owner_team_id: h?.cup.owner_team_id ?? undefined,
    is_signups_closed: h?.cup.is_signups_closed ?? false,
    is_published: h?.cup.is_published ?? true,
    stages:
      h?.stages.map(
        (s, index): CupStageForm => ({
          cupId: h.cup.id,
          id: s.id,
          index,
          title: s.title,
          format: s.format,
          group_size: s.group_size ?? "",
          group_rematches: s.group_rematches ?? 0,
          starttime: createStartTime(s.starttime),
          checkinduration: getCheckinDuration(s.starttime, s.checkintime),
          max_participants: s.max_participants ?? "",
          start_immediately: s.start_immediately,
          scorings: (h?.scorings?.[s.id] ?? []).map(
            (sc): CupStageScoringForm => ({
              bestof: sc.bestof,
              elim_round: sc.elim_round ?? "",
              is_lb: sc.is_lb ?? undefined,
              pickbans: sc.pickbans ?? "",
              groups: sc.groups,
              is_time_score: sc.is_time_score ?? false,
              is_time_score_race: sc.is_time_score_race ?? false,
              is_point_score: sc.is_point_score ?? false,
              point_score_greatest_is_winner: sc.point_score_greatest_is_winner ?? false,
            })
          ),
        })
      ) ?? [],
  };
}

function createStartTime(starttime: undefined | null | string): IDateTimeStr {
  const p = parseDate(starttime);
  if (p instanceof Date) return asIDT(p);
  return {
    time: "",
    date: "",
    parsed: undefined,
  };
}

function getCheckinDuration(
  starttime: string | null | undefined,
  checkintime: string | null | undefined
) {
  const start = parseDate(starttime);
  const checkin = parseDate(checkintime);
  if (start instanceof Date && checkin instanceof Date) {
    return (start.getTime() - checkin.getTime()) / 60000;
  }
  return "";
}

export function createCupStageForm(index: number): CupStageForm {
  return {
    index,
    title: `Stage ${index + 1}`,
    starttime: { time: "", date: "", parsed: undefined },
    start_immediately: index > 0,
    // gameMatchControlId: { active: last?.gameModeId?.active??undefined },
    max_participants: "",
    group_size: "",
    group_rematches: 0,
    checkinduration: "",
    format: undefined,
    scorings: [],
  };
}

export function createCupStageScoringForm(groups: boolean = false): CupStageScoringForm {
  return {
    bestof: 3,
    groups,
    elim_round: 0,
    is_lb: false,
    is_time_score: false,
    is_time_score_race: false,
    is_point_score: true,
    point_score_greatest_is_winner: true,
    pickbans: "",
  };
}

export function initialiseScorings(format: undefined | CupStageFormat): CupStageScoringForm[] {
  switch (format) {
    case "groups":
      return [createCupStageScoringForm(true)];
    case "single_elim":
      return [createCupStageScoringForm(false)];
    case "double_elim":
      const ub = createCupStageScoringForm(true);
      const lb = createCupStageScoringForm(true);
      lb.is_lb = true;
      return [ub, lb];
    default:
      return [];
  }
}
