import { createComputed, createSignal, For } from "solid-js";
import { CupSignup, Ranking, Rankings } from "../../../api";
import { formatPlus } from "../../Player/CupSignupMatchResultsTable";
import { SignupLoader } from "./CupSignup";

export default function CupStandings(props: {
  rankings?: {
    rankings: Rankings["rankings"];
    isComplete: boolean;
    stageId: string;
  };
  signups: CupSignup[];
  cupId: string;
  tab?: "signups" | "standings";
  disableTabs?: boolean;
}) {
  const [tab, setTab] = createSignal<"signups" | "standings">("signups");
  createComputed(() => {
    if (props.tab !== undefined) setTab(props.tab);
  });

  return (
    <div class="box p-0">
      {!props.disableTabs && (
        <div class="tabs is-small is-centered mb-1">
          <ul>
            <li classList={{ "is-active": tab() === "signups" }}>
              <a onClick={() => setTab("signups")}>Signups ({props.signups.length})</a>
            </li>
            {props.rankings === undefined || props.rankings.rankings.length === 0 ? undefined : (
              <li classList={{ "is-active": tab() === "standings" }}>
                <a onClick={() => setTab("standings")}>
                  {props.rankings.isComplete ? "Final standings" : "Latest standings"}
                </a>
              </li>
            )}
          </ul>
        </div>
      )}
      {tab() === "signups" ? (
        <div class="cup-signups p-3">
          <For each={props.signups} fallback={() => "Nothing yet"}>
            {(signup) => <SignupLoader signup={signup} showCheckin />}
          </For>
        </div>
      ) : (
        <div class="cup-standings p-3">
          <For each={props.rankings?.rankings ?? []}>
            {(rank: Ranking) => (
              <>
                <SignupLoader signup={rank.signup_id} />
                <span
                  classList={{
                    "has-text-success-dark": rank.wins > rank.losses,
                    "has-text-danger": rank.losses > rank.wins,
                  }}
                >
                  {formatPlus(rank.wins - rank.losses)}
                </span>
              </>
            )}
          </For>
        </div>
      )}
    </div>
  );
}
