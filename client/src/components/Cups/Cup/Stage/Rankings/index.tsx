import { CupStage, Ranking, Rankings } from "@/api";
import PlayerLink from "@/components/Player/Link";
import { SignupLoader } from "../../CupSignup";
import { For, Show } from "solid-js";
import Ordinal from "@/shared/Ordinal";
import theme from "./theme.module.scss";
import StageTitle from "../Title";
import classNames from "classnames";
import { nonEmptyArray } from "fp-ts";
import { Eq } from "fp-ts/lib/Eq";

export default function StageRankings(props: { stage: CupStage; rankings: Rankings }) {
  return (
    <table class={theme.table}>
      <thead>
        <tr>
          <th class={theme.title} colspan="2">
            <StageTitle stage={props.stage} />{" "}
            <span class={theme.rankings}> &ndash; Placements</span>
          </th>
          <th style="text-align:right">Wins</th>
          <th>Losses</th>
          <th style="text-align:right">Map Wins</th>
          <th>Map Losses</th>
        </tr>
      </thead>
      <tbody>
        <For each={offsetRankings(props.rankings.rankings)}>
          {(rank) => (
            <tr class={theme.row} classList={{ [theme.top3]: rank.start <= 2 }}>
              <td class={theme.placement}>
                {rank.start !== rank.end ? (
                  <>
                    <Ordinal placement={rank.start} /> &mdash; <Ordinal placement={rank.end} />
                  </>
                ) : (
                  <Ordinal placement={rank.start} />
                )}
              </td>
              <td>
                <SignupLoader signup={rank.signup_id} showSeed />
              </td>
              <td
                class={classNames(theme.score, theme.wins)}
                data-netpositive={rank.wins - rank.losses > 0}
                style="text-align:right"
              >
                {rank.wins}
              </td>
              <td
                class={classNames(theme.score, theme.losses)}
                data-netpositive={rank.wins - rank.losses > 0}
              >
                {rank.losses}
              </td>
              <td
                class={classNames(theme.score, theme.wins)}
                data-netpositive={rank.map_wins! - rank.map_losses! > 0}
                style="text-align:right"
              >
                {rank.map_wins}
              </td>
              <td
                class={classNames(theme.score, theme.losses)}
                data-netpositive={rank.map_wins! - rank.map_losses! > 0}
              >
                {rank.map_losses}
              </td>
            </tr>
          )}
        </For>
      </tbody>
    </table>
  );
}

export interface OffsetRanking extends Ranking {
  start: number;
  end: number;
}

export function offsetRankings(rankings: Ranking[]): OffsetRanking[] {
  const rankGrouped = nonEmptyArray.group(eqPlacement)(rankings);
  const out = Array(rankings.length);
  let placement = 0;
  let i = 0;
  for (const group of rankGrouped) {
    const size = group.length - 1;
    for (const rank of group) {
      out[i++] = {
        ...rank,
        start: placement,
        end: placement + size,
      };
    }
    placement += group.length;
  }
  return out;
}

const eqPlacement: Eq<Ranking> = {
  equals(a: Ranking, b: Ranking): boolean {
    return a.placement === b.placement;
  },
};
