import { CupMatch2p, ElimMatchType } from "@/api";
import memoized from "micro-memoize";
import theme from "./theme.module.scss";

function makeAlphabetise(a: number) {
  return (i: number) => {
    const r: number[] = [];
    i++;
    let rem: number;
    while (i > 0) {
      i--;
      rem = i % 26;
      i = (i / 26) | 0;
      r.push(a + rem);
    }
    r.reverse();
    return String.fromCharCode(...r);
  };
}

const asAlpha = memoized(makeAlphabetise("a".charCodeAt(0)));
const asALPHA = memoized(makeAlphabetise("A".charCodeAt(0)));

export function matchName(
  m: undefined | CupMatch2p,
  options?: {
    showRound?: boolean;
    longFormat?: boolean;
  }
): string {
  if (typeof m !== "object" || m === undefined) return "";
  let prefix = "";
  if (options?.showRound && typeof m.group_round === "number") {
    if (options?.longFormat) {
      prefix = `Round ${m.group_round + 1} — `;
    } else {
      prefix = `R${m.group_round + 1}.`;
    }
  }
  if (options?.showRound && typeof m.elim_round === "number") {
    if (options?.longFormat) {
      prefix = `Round ${m.elim_round + 1}, `;
    } else {
      prefix = `R${m.elim_round + 1}.`;
    }
  }
  const match = m.elim_type === "LB" || m.elim_type == null ? asAlpha(m.lid) : asALPHA(m.lid);
  if (options?.longFormat) return prefix + " Match " + m.lid;
  return prefix + match;
}

export function MatchName(p: {
  round?: number;
  showRound?: boolean;
  longFormat?: boolean;
  match: undefined | CupMatch2p;
}) {
  return <span class={theme.matchId}>{matchName(p.match, p)}</span>;
}
