import { CupStage, CupStageFormat } from "@/api";

const stageFormats: Record<CupStageFormat, string> = {
  //  [CupStageFormat.RoundRobin]: "RobinRobin",
  groups: "Groups",
  double_elim: "Double Elim",
  single_elim: "Single Elim",
  ladder: "Ladder",
};

export function stageFormatName(stage: CupStage | undefined) {
  if (stage === undefined) {
    return "Unknown";
  }
  return stageFormats[stage.format];
}
