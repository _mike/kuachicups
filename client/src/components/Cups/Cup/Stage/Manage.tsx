import { api, CupMatch2p, CupMatchReportForm2p, CupStage } from "@/api";
import { useAsync, useAsyncI } from "@/shared/async/Awaitable";
import { EnumFromTo } from "@/shared/Case";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import Icon from "@/shared/ui/Icon";
import { createComputed, createSignal, For, Show } from "solid-js";
import { createMutable } from "solid-js/store";
import { SignupLoader } from "../CupSignup";
import { MatchName } from "./MatchName";

export default function CupStageManage(props: { stage: CupStage }) {
  const matches = useAsyncI(
    async (ref) => (await api.cups.matches.byParentId(props.stage.id, ref)).array,
    []
  );

  return (
    <div>
      <For each={matches()}>{(m) => <MatchControl match={m} />}</For>
    </div>
  );
}

export function MatchControl(props: { match: CupMatch2p; onUpdate?: () => void }) {
  const [isOpen, setOpen] = createSignal(false);
  const scores = createMutable<CupMatchReportForm2p>({
    high_report: [],
    low_report: [],
    match_id: props.match.id,
  });
  createComputed(() => {
    scores.match_id = props.match.id;
  });
  const Signup = (p: { who: "high_id" | "low_id" }) => (
    <span
      classList={{
        "has-background-success-light": props.match.winner_id === props.match[p.who],
        "has-background-danger-light": props.match.loser_id === props.match[p.who],
      }}
    >
      <SignupLoader signup={props.match[p.who] ?? undefined} />
    </span>
  );
  async function onOverride() {
    await api.cupsMatchOverrideReport2P(props.match.id, scores);
    props.onUpdate?.();
  }
  async function onReset() {
    await api.cupsMatchResetReport2P(props.match.id);
    props.onUpdate?.();
  }
  return (
    <article data-is-open={isOpen()}>
      <header>
        <span style="flex-grow:1">
          <MatchName showRound match={props.match} /> &mdash; &nbsp;
          <Signup who="high_id" />
          {" / "}
          <Signup who="low_id" />
        </span>
        <button
          class="button is-small is-light"
          style="margin:0"
          onClick={() => setOpen(!isOpen())}
        >
          <Icon type={isOpen() ? "arrow-up" : "arrow-down"} />
        </button>
      </header>
      <Show when={isOpen()}>
        <div style="margin: calc(var(--block-spacing-vertical) * -1) calc(var(--block-spacing-horizontal) * -1)">
          <MatchControlTable match={props.match} scores={scores} />
        </div>
        <footer>
          <div class="grid">
            <ConfirmButton class="is-danger" onConfirm={onReset}>
              Reset
            </ConfirmButton>
            <ConfirmButton class="is-danger" onConfirm={onOverride}>
              Override
            </ConfirmButton>
          </div>
        </footer>
      </Show>
    </article>
  );
}

export function MatchControlTable(props: {
  class?: string;
  match: CupMatch2p;
  scores: Pick<CupMatchReportForm2p, "high_report" | "low_report">;
}) {
  const Signup = (p: { who: "high_id" | "low_id" }) => (
    <span
      classList={{
        "has-background-success-light": props.match.winner_id === props.match[p.who],
        "has-background-danger-light": props.match.loser_id === props.match[p.who],
      }}
    >
      <SignupLoader signup={props.match[p.who] ?? undefined} />
    </span>
  );
  const scoring = useAsync((ref) => api.cups.scorings.byId(props.match.scoring_id, ref));
  return (
    <table class={props.class}>
      <thead>
        <tr>
          <th colSpan={2}>Low report</th>
          <th colSpan={2}>High report</th>
          <th>
            High &mdash; <Signup who="high_id" />
          </th>
          <th>
            Low &mdash; <Signup who="low_id" />
          </th>
        </tr>
      </thead>
      <tbody>
        <EnumFromTo from={0} to={scoring()?.bestof ?? 0}>
          {(matchNo) => (
            <tr>
              <td>{props.match.low_report?.[matchNo]?.high}</td>
              <td>{props.match.low_report?.[matchNo]?.low}</td>
              <td>{props.match.high_report?.[matchNo]?.high}</td>
              <td>{props.match.high_report?.[matchNo]?.low}</td>
              <td>
                <input
                  class="input"
                  type="number"
                  value={props.scores.high_report[matchNo]}
                  onInput={(ev) => {
                    props.scores.high_report[matchNo] = ev.currentTarget.valueAsNumber;
                  }}
                />
              </td>
              <td>
                <input
                  class="input"
                  type="number"
                  value={props.scores.low_report[matchNo]}
                  onInput={(ev) => {
                    props.scores.low_report[matchNo] = ev.currentTarget.valueAsNumber;
                  }}
                />
              </td>
            </tr>
          )}
        </EnumFromTo>
      </tbody>
    </table>
  );
}
