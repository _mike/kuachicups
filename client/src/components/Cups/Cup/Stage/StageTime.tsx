import { CupStage } from "@/api";
import { Countdown } from "@/shared/ui/Time";
import { format } from "date-fns";
import { createMemo } from "solid-js";

export default function StageTime(props: { stage: CupStage }) {
  return createMemo(() => {
    const { start_immediately, starttime, checkintime } = props.stage;
    if (start_immediately) {
      return (
        <dl>
          <dt>Starting time</dt>
          <dd>Immediately following previous stage</dd>
        </dl>
      );
    }
    if (starttime == null || checkintime == null) return undefined;
    const start = new Date(starttime);
    const checkin = new Date(checkintime);
    return (
      <dl>
        <dt>Starting time</dt>
        <dd>
          {format(start, "PPPPpppp")} (<Countdown tick="s" target={start} />)
        </dd>
        <dt>Check-in time</dt>
        <dd>
          {format(checkin, "PPPPpppp")} (<Countdown tick="s" target={checkin} />)
        </dd>
      </dl>
    );
  });
}
