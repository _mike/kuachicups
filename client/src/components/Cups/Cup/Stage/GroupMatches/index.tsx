import { CupMatch2p } from "@/api";
import { Link } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import classNames from "classnames";
import { cloneDeep } from "lodash";
import log from "loglevel";
import { For, Show } from "solid-js";
import { unwrap } from "solid-js/store";
import { useCup } from "../../Context";
import { SignupProvider } from "../../CupSignup";
import { hasBye, winnerOf } from "../../Match/Context";
import { useCupStage } from "../Context";
import { MatchName } from "../MatchName";
import theme from "./theme.module.scss";

export default function GroupTables() {
  const ctx = useCupStage();
  const p = useAsync(async () => {
    const matches = ctx.matches();
    if (matches === undefined) return undefined;
    return groupMatches(cloneDeep(unwrap(matches?.array)));
  }, []);
  return (
    <div>
      <Show
        when={p().length > 1}
        fallback={() => (
          <div class={theme.groupRoundTables}>
            <For each={p()[0]}>
              {(round, index) => <GroupRoundTable matches={round} roundNo={index() + 1} />}
            </For>
          </div>
        )}
      >
        <div
          class={theme.groupTables}
          classList={{ [theme.groupGrid]: p()?.length > 1, [theme.roundRobin]: p()?.length === 1 }}
        >
          <For each={p()}>
            {(group, groupNo) => <GroupTable group={group} groupNo={groupNo()} />}
          </For>
        </div>
      </Show>
    </div>
  );
}

export function groupMatches(matches: readonly CupMatch2p[]): CupMatch2p[][][] {
  if (!matches.every((m) => typeof m.group_no === "number" && typeof m.group_round === "number")) {
    log.warn(
      "malformed group matches, no groupNo or groupRound",
      matches.filter((m) => typeof m.group_no !== "number" || typeof m.group_round !== "number")
    );
    return [];
  }
  const r = [];
  for (const m of matches) {
    const { group_no: groupNo, group_round: groupRound } = m;
    const group: CupMatch2p[][] = r[groupNo!] ?? (r[groupNo!] = []);
    const round: CupMatch2p[] = group[groupRound!] ?? (group[groupRound!] = []);
    round.push(m);
  }
  return r;
}

export function GroupTable(props: { group: CupMatch2p[][]; groupNo: number }) {
  return (
    <article style="margin:0">
      <header>Group {props.groupNo + 1}</header>
      <div class={theme.groupRoundTables}>
        <For each={props.group}>
          {(round, index) => <GroupRoundTable matches={round} roundNo={index() + 1} />}
        </For>
      </div>
    </article>
  );
}

export function GroupRoundTable(props: {
  groupNo?: number;
  roundNo: number;
  matches: CupMatch2p[];
}) {
  return (
    <article class={theme.groupRound}>
      <header>
        {typeof props.groupNo === "number" ? `Group ${props.groupNo + 1} - ` : ""}Round{" "}
        {props.roundNo}
      </header>
      <div class="flush">
        <div class={theme.groupRoundTable}>
          <For each={props.matches}>{(m: CupMatch2p) => <GroupMatchRow match={m} />}</For>
        </div>
      </div>
    </article>
  );
}

function GroupMatchRow(props: { match: CupMatch2p }) {
  const cup = useCup();
  const contents = (
    <>
      <div class={theme.matchName}>
        <MatchName match={props.match} />
      </div>
      {(["high", "low"] as const).map((who) => (
        <>
          <SignupProvider signup={props.match[`${who}_id`]}>
            {typeof props.match[`${who}_id`] !== "string" ? (
              <div class={classNames(theme.signup, theme.bye, theme[who])}>
                <small>bye</small>
              </div>
            ) : (
              <>
                <SignupProvider.Name class={classNames(theme.signup, theme[who])} />
                <SignupProvider.Img class={classNames(theme.avatar, theme[who])} />
              </>
            )}
          </SignupProvider>
          <div class={classNames(theme.score, theme[who])}>
            {winsFor(who, props.match.high_report ?? props.match.low_report)}
          </div>
        </>
      ))}
    </>
  );
  return (
    <Show
      when={!hasBye(props.match)}
      fallback={() => <div class={theme.groupMatchRow}>{contents}</div>}
    >
      <Link
        to="cups.cup.matches.match"
        params={{ id: cup.cupId, matchId: props.match.id + "" }}
        class={theme.groupMatchRow}
        data-winner={winnerOf(props.match)}
        data-bye={typeof props.match.low_id !== "string" || typeof props.match.high_id !== "string"}
      >
        {contents}
      </Link>
    </Show>
  );
}

export function winsFor(
  self: "high" | "low",
  report: { high: undefined | number; low: undefined | number }[] | null | undefined
): number | undefined {
  const highWin = self === "high" ? 1 : 0;
  const lowWin = self === "low" ? 1 : 0;
  if (Array.isArray(report)) {
    let w = 0;
    for (const { high, low } of report) {
      if (high === undefined || low === undefined) continue;
      w += high > low ? highWin : lowWin;
    }
    return w;
  }
  return undefined;
}
