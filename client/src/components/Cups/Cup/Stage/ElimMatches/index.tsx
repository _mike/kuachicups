import { ArrayMap, CupMatch2p, ElimMatchType } from "@/api";
import { useScrollable } from "@/components/App/ScrollableContext";
import { useMobile } from "@/context/Size";
import { navigate, router } from "@/router";
import Await from "@/shared/async/Await";
import { Case, EnumFromTo } from "@/shared/Case";
import { useDiscordAvatar } from "@/shared/ui/Discord/Avatar";
import Icon from "@/shared/ui/Icon";
import Menu from "@/shared/ui/Menu";
import { HierarchyNode, HierarchyPointNode, stratify, tree } from "d3-hierarchy";
import { curveLinear, curveStep, line } from "d3-shape";
import { cloneDeep, sortBy } from "lodash";
import {
  batch,
  createEffect,
  createMemo,
  createSignal,
  For,
  onCleanup,
  Show,
  untrack,
} from "solid-js";
import { createMutable, createStore, unwrap } from "solid-js/store";
import { useCup } from "../../Context";
import { CupSignupProps, SignupLoader } from "../../CupSignup";
import { useCupStage } from "../Context";
import { matchName } from "../MatchName";
import defaultTheme from "./theme.module.scss";

type N = CupMatch2p & { x?: number; y?: number };

type HN = HierarchyNode<N>;

const matchVGap = 10;
const matchHeight = 64;
const matchHalfHeight = matchHeight * 0.5;
const matchWidth = 240;
const matchHGap = 28;
const lbUbGap = 64;

declare module "solid-js" {
  namespace JSX {
    export interface IntrinsicElements {
      feDropShadow: {
        dx?: string | number;
        dy?: string | number;
        stdDeviation?: string | number;
        "flood-color"?: string;
      };
    }
  }
}

export interface Props {
  panZoom?: boolean;
  theme?: Record<string, string>;
  onClickMatch(m: number): void;
}

export default function ElimMatches(props: Props) {
  Menu.useFullscreenLayout();

  const ctx = useCupStage();

  const mobile = useMobile();

  if (!props.panZoom) {
    return createMemo(() => {
      const m = ctx.matches();
      if (m !== undefined) {
        return <ElimMatchesSvg onClickMatch={props.onClickMatch} theme={props.theme} matches={m} />;
      }
      return undefined;
    });
  }

  const scrollableCtx = useScrollable();
  let lastScrollableVal = true;
  scrollableCtx.setScrollable((s) => {
    lastScrollableVal = s;
    return false;
  });
  onCleanup(() => {
    scrollableCtx.setScrollable(lastScrollableVal);
  });

  const theme = props.theme ?? defaultTheme;

  const [zoom, setZoom] = createStore({
    panzoom: false,
    x: 0,
    y: 0,
    scale: 1,
  });

  function onZoom(ev: WheelEvent) {
    ev.preventDefault();
    setZoom((z) => ({
      scale: z.scale - ev.deltaY * 0.001 * z.scale,
    }));
  }

  function onPanMove(ev: { movementX: number; movementY: number; preventDefault(): void }) {
    ev.preventDefault();
    setZoom((z) => ({
      x: z.x + ev.movementX / z.scale,
      y: z.y + ev.movementY / z.scale,
    }));
  }

  function onPanStart(ev: PointerEvent) {
    ev.preventDefault();
    addEventListener("pointermove", onPanMove);
    addEventListener("pointerup", onPanEnd);
    setZoom({ panzoom: true });
  }

  function onPanEnd() {
    removeEventListener("pointermove", onPanMove);
    removeEventListener("pointerup", onPanEnd);
    setZoom({ panzoom: false });
  }

  let lastTouches: TouchList | undefined;
  let zoomStart: number | undefined;

  function onTouchMove(ev: TouchEvent) {
    log.debug("onTouchMove");
    ev.preventDefault();
    if (lastTouches !== undefined) {
      const touch = ev.touches[0];
      const movementX = touch.pageX - lastTouches[0].pageX;
      const movementY = touch.pageY - lastTouches[0].pageY;
      let scale: number = zoom.scale;
      log.debug(ev.touches);
      if (ev.touches.length > 1) {
        const zoomX = ev.touches[0].pageX - ev.touches[1].pageX;
        const zoomY = ev.touches[0].pageY - ev.touches[1].pageY;
        if (zoomStart === undefined) {
          zoomStart = Math.sqrt(zoomX ** 2 + zoomY ** 2) / scale;
        } else {
          scale = Math.sqrt(zoomX ** 2 + zoomY ** 2) / zoomStart;
        }
      }
      log.debug({ movementX, movementY, scale });
      setZoom((z) => ({
        x: z.x + movementX / scale,
        y: z.y + movementY / scale,
        scale,
      }));
    }
    lastTouches = ev.touches;
  }

  function onTouchStart(ev: TouchEvent) {
    ev.preventDefault();
    lastTouches = undefined;
    zoomStart = undefined;
    addEventListener("touchmove", onTouchMove);
    setZoom({ panzoom: true });
  }

  function onTouchEnd() {
    removeEventListener("touchmove", onTouchMove);
    setZoom({ panzoom: false });
  }

  return (
    <div
      class={theme.elimBracket}
      ref={(div) => {
        setTimeout(() => {
          log.debug("rect", div.offsetWidth, div.offsetHeight);
        }, 100);
      }}
      style={{
        "--zoom-x": zoom.x + "px",
        "--zoom-y": zoom.y + "px",
        "--zoom-scale": zoom.scale,
        "--cursor": zoom.panzoom ? "grabbing" : "grab",
      }}
      onPointerDown={!mobile() ? onPanStart : undefined}
      onPointerUp={!mobile() ? onPanEnd : undefined}
      onTouchStart={mobile() ? onTouchStart : undefined}
      onTouchEnd={mobile() ? onTouchEnd : undefined}
      onBlur={onPanEnd}
      onWheel={onZoom}
    >
      <Show when={ctx.matches()}>
        {(matches) => (
          <ElimMatchesSvg onClickMatch={props.onClickMatch} theme={props.theme} matches={matches} />
        )}
      </Show>
      <div class={theme.buttons}>
        <button onClick={() => setZoom("scale", (s) => s - 0.5 * s)}>
          <Icon type="remove-circle-outline" />
        </button>
        <button onClick={() => setZoom("scale", (s) => s + 0.5 * s)}>
          <Icon type="add-circle-outline" />
        </button>
      </div>
    </div>
  );
}

const elbow = line().curve(curveStep);
const lin = line().curve(curveLinear);

function createElimBracketTree(props: { matches: ArrayMap<CupMatch2p, number> }) {
  interface ElimBracketTree {
    name: "ub" | "lb";
    width: number;
    height: number;
    root: HierarchyPointNode<N>;
  }

  const matches = untrack(() => cloneDeep(unwrap(props.matches.array)));
  const nonLbMatches = [];
  const lbGfMatches = [];

  let hasLB = false;
  for (const m of matches) {
    if (m.elim_type !== "LB") nonLbMatches.push(m);
    if (m.elim_type === "LB") hasLB = true;
    lbGfMatches.push(m);
  }

  function strat(name: "ub" | "lb", matches: CupMatch2p[]): ElimBracketTree | undefined {
    if (matches.length === 0) return undefined;
    // assign names to every match
    // gives LB matches lowercase names a-z and UB ones A-Z
    const sorted: N[] = sortBy(cloneDeep(unwrap(matches)), (a) => [a.elim_round, a.elim_index]);
    const stratifier = stratify<CupMatch2p>()
      .parentId((m) => (m.elim_winner_match_id ?? "") + "")
      .id((m) => m.id + "");
    const stra = stratifier(sorted);
    const width = stra.height * (matchWidth + matchHGap); // width to the last origin point of a match
    const leaves = stra.leaves();
    const height = (matchHeight + matchVGap) * leaves.length;
    const localHeight =
      name === "ub"
        ? height
        : (matchHeight + matchVGap) * leaves.filter((a) => a.data.elim_type === "LB").length; // height to the last match
    const root = tree<N>()
      .size([height, width])
      .separation(() => 1)(stra);
    log.debug({ stra, name, height });
    return { name, width, height: localHeight, root };
  }
  const ub = strat("ub", nonLbMatches)!;
  const lb = hasLB ? strat("lb", lbGfMatches) : undefined;
  const width = Math.max(ub.width, lb?.width ?? 0);
  const height = ub.height + (lb?.height !== undefined ? lb.height + lbUbGap : 0);
  const nodeById: { [id: number]: HN } = {};
  const rounds: {
    ub: { x: number; y: number; round: number; type: ElimMatchType }[];
    lb: { x: number; y: number; round: number; type: ElimMatchType }[];
  } = {
    lb: [],
    ub: [],
  };
  const mysteriousYOffset = (matchHeight + matchVGap) * -0.5;
  const brackets = [ub];
  if (lb !== undefined) brackets.push(lb);
  for (const bracket of brackets) {
    bracket.root.each((m) => {
      if (bracket.name === "ub" && m.data.elim_type === "GF2") return;
      if (bracket.name === "lb" && !(m.data.elim_type === "LB" || m.data.elim_type === "GF2"))
        return;
      const x = width - m.y;
      // Assign round label if not existing already
      if (rounds[bracket.name][height - m.depth] === undefined) {
        const round = m.data?.elim_logical_round ?? m.data?.elim_round ?? 0;
        rounds[bracket.name][height - m.depth] = {
          x,
          y: bracket.name === "ub" || m.data.elim_type === "GF2" ? 0 : ub.height + lbUbGap * 0.5,
          round: round + 1,
          type: m.data?.elim_type ?? "WB",
        };
      }
      m.data.x = x;
      m.data.y = m.x + mysteriousYOffset; // + height * 0.5;
      if (lb !== undefined && (m.data.elim_type === "GF1" || m.data.elim_type === "GF2")) {
        m.data.y = 0.5 * (lb !== undefined ? ub.height + lb.height : ub.height);
      } else if (bracket.name === "lb") {
        m.data.y += lbUbGap;
      }
      nodeById[m.data.id] = m;
    });
  }

  const descendants = ub.root
    .descendants()
    .filter((a) => a.data.elim_type === "WB" || a.data.elim_type === "GF1")
    .concat(
      lb === undefined
        ? []
        : lb.root
            .descendants()
            .filter((a) => a.data.elim_type === "LB" || a.data.elim_type === "GF2")
    );

  return { width, height, nodeById, rounds, descendants };
}

export function ElimMatchesSvg(props: {
  matches: ArrayMap<CupMatch2p, number>;
  onClickMatch(m: number): void;
  theme?: Record<string, string>;
}) {
  const theme = props.theme ?? defaultTheme;
  const { width, height, rounds, nodeById, descendants } = createElimBracketTree(props);

  const viewBox = {
    minX: -matchHGap, // Math.round(-pad),
    minY: 0, // Math.round(-pad),
    width: width + (matchWidth + matchHGap) + matchHGap,
    height: height + matchHalfHeight, // Math.round(height + pad)
  };

  const highlights = createMutable<{
    matches: Record<number, boolean>;
    paths: Record<number, boolean>;
  }>({ matches: {}, paths: {} });

  function setHighlights(k: "matches" | "paths", id: number, highlight: boolean) {
    if (highlight) {
      highlights[k][id] = true;
    } else {
      delete highlights[k][id];
    }
  }

  function onHoverMatch(hn: HN, hover: boolean): () => void {
    return () => {
      const data = hn.data;
      if (data === undefined) return;
      const { id, elim_loser_match_id: elimLoserMatchId, elim_type: elimType } = data;
      batch(() => {
        setHighlights("paths", id, hover);
        setHighlights("matches", id, hover);
        if (typeof elimLoserMatchId === "number") setHighlights("matches", elimLoserMatchId, hover);
        if (elimType === "LB") {
          for (const k of ["elim_high_match_id", "elim_low_match_id"] as const) {
            const id = data[k];
            if (typeof id === "number") {
              const src = nodeById[id]?.data;
              if (src !== undefined && src.elim_type !== "LB") {
                setHighlights("matches", src.id, hover);
              }
            }
          }
        }
      });
    };
  }

  const BracketSvg = () => (
    <svg
      class={theme.elimBracketSvg}
      style={{
        "--viewbox-width": viewBox.width - viewBox.minX + "px",
        "--viewbox-height": viewBox.height - viewBox.minY + "px",
      }}
      viewBox={`${viewBox.minX} ${viewBox.minY} ${viewBox.width} ${viewBox.height}`}
      preserveAspectRatio="xMidYMid meet"
    >
      <defs>
        <clipPath id="avatar-clip">
          <circle r={avatarWidth * 0.5} cx={avatarWidth * 0.5} cy={avatarWidth * 0.5} />
        </clipPath>
        <linearGradient id="sudden-dropoff">
          <stop offset="0%" stop-color="#FFF" />
          <stop offset="95%" stop-color="#FFF" />
          <stop offset="100%" stop-color="#000" />
        </linearGradient>
        <EnumFromTo from={0} to={5}>
          {(i) => [
            <mask id={"signup-mask-" + i}>
              <rect
                fill="url(#sudden-dropoff)"
                width={matchWidth * (1 - matchScoresWidthPropByScores[i]) - 6}
                height={matchHeight}
              />
            </mask>,
            <clipPath id={"signup-clip-" + i}>
              <rect
                width={matchWidth * (1 - matchScoresWidthPropByScores[i]) - 6}
                height={matchHeight}
              />
            </clipPath>,
          ]}
        </EnumFromTo>
        <filter id="shadow-intense">
          <feDropShadow dx="0" dy="2" stdDeviation="3" flood-color="rgba(0,0,0,0.5)" />
        </filter>
        <filter id="shadow">
          <feDropShadow dx="0" dy="4" stdDeviation="6" flood-color="rgba(0,0,0,0.04)" />
        </filter>
        <filter id="shadow-big">
          <feDropShadow dx="0" dy="4" stdDeviation="3" flood-color="rgba(0,0,0,0.09)" />
        </filter>
      </defs>
      <g class={theme.elimRoundLabels}>
        {[rounds.ub, rounds.lb].map((rs) =>
          rs.map((r) => (
            <g transform={`translate(${r.x}, ${r.y})`}>
              <text x={matchWidth * 0.5}>
                <Case
                  key={r.type}
                  children={{
                    GF1: "Grand Final",
                    GF2: "Grand Final (LB)",
                    LB: "LB." + r.round,
                    WB: "WB." + r.round,
                  }}
                />
              </text>
            </g>
          ))
        )}
      </g>
      <g transform={`translate(0, ${matchVGap * 2})`}>
        <g class={theme.elimDropPaths}>
          {descendants.map((hn) => {
            const {
              data: { id, elim_loser_match_id: elimLoserMatchId, x, y },
            } = hn;
            if (typeof elimLoserMatchId !== "number") return "";
            const drop = nodeById[elimLoserMatchId]?.data;
            if (drop === undefined) return undefined;
            const path = lin([
              [x! + matchWidth, y! + matchHeight],
              [drop.x! + matchWidth, drop.y! + matchHeight],
            ])!;
            return (
              <path
                data-highlight={highlights.paths[id] || highlights.paths[elimLoserMatchId]}
                d={path}
              />
            );
          })}
        </g>
        <g class={theme.elimMatchPaths}>
          {descendants.map((hn) => {
            const { parent: parentNode, data: child } = hn;
            if (parentNode === null) return "";
            const parent = parentNode.data;
            const path = elbow([
              [parent.x!, parent.y! + matchHalfHeight],
              [child.x! + matchWidth, child.y! + matchHalfHeight],
            ])!;
            return <path class={theme.elimMatchPath} data-filled={child.is_scored} d={path} />;
          })}
        </g>
        <g class={theme.elimMatches}>
          {descendants.map((hn) => (
            <g
              class={theme.elimMatchGroup}
              data-highlight={highlights.matches?.[hn.data.id]}
              data-scored={hn.data.is_scored}
              data-pending={
                !hn.data.is_scored &&
                typeof hn.data.low_id === "string" &&
                typeof hn.data.high_id === "string"
              }
              transform={`translate(${hn.data.x!}, ${hn.data.y!})`}
              onMouseEnter={onHoverMatch(hn, true)}
              onMouseLeave={onHoverMatch(hn, false)}
              onClick={() => props.onClickMatch(hn.data.id)}
            >
              <rect class={theme.elimMatchRect} width={matchWidth} height={matchHeight} />
              <text class={theme.elimMatchId} dx={matchWidth + 2} dy={matchHalfHeight + 12}>
                {matchName(hn.data)}
              </text>
              <Await data={() => props.matches.map[hn.data.lid]}>
                {(match) => <ElimMatch theme={theme} match={match} nodeById={nodeById} />}
              </Await>
              <rect class={theme.elimMatchOutlineRect} width={matchWidth} height={matchHeight} />
            </g>
          ))}
        </g>
      </g>
    </svg>
  );

  return <BracketSvg />;
}

const matchScoresWidthPropByScores: Record<number, number> = {
  0: 0,
  1: 0.12,
  2: 0.24,
  3: 0.3333,
  4: 0.4,
  5: 0.5,
};

const mh12 = matchHeight * 0.5;
const mh14 = matchHeight * 0.25;
const mh34 = matchHeight * 0.75;
const avatarWidth = 20;
const avatarWidth_2 = avatarWidth * 0.5;

function RoundedImage(props: { href: string; dx: number; dy: number }) {
  const href = createMemo(() => encodeURI(props.href));
  return (
    <g
      innerHTML={`
        <image
          x="0"
          y="0"
          transform="translate(${props.dx}, ${props.dy})"
          width="${avatarWidth}"
          height="${avatarWidth}"
          href="${href()}"
          clip-path="url(#avatar-clip)"
        />
      `}
    />
  );
}

function SignupView(
  props: CupSignupProps & {
    theme: Record<string, string>;
    lenClamped: number;
    dy: number;
  }
) {
  const avatar = useDiscordAvatar(() => ({
    discordAvatar: props.player?.discord_avatar!,
    discordId: props.player?.discord_id!,
    size: 24,
  }));
  return (
    <g class={props.theme.elimSignup}>
      <a
        href={router.buildPath("players.profile", { id: props.player?.id })}
        target="_blank"
        onClick={(ev) => {
          ev.preventDefault();
          navigate({
            to: "players.profile",
            params: { id: props.player?.id ?? "_unknown" },
          });
        }}
      >
        <Await data={avatar} fallback={undefined}>
          {(avatar) => <RoundedImage href={avatar} dx={6} dy={props.dy - avatarWidth_2} />}
        </Await>
        <text
          dx={10 + (avatar() !== undefined ? avatarWidth : 0)}
          dy={props.dy}
          mask={`url(#signup-mask-${props.lenClamped})`}
          clip-path={`url(#signup-clip-${props.lenClamped})`}
          dominant-baseline="middle"
          text-anchor="start"
        >
          {props.team?.name ?? props.player?.discord_username}
        </text>
      </a>
    </g>
  );
}

function ElimMatch(props: {
  match: CupMatch2p;
  nodeById: { [id: number]: HN };
  onHover?: (inside: boolean) => void;
  theme?: Record<string, string>;
}) {
  const [hovered, setHovered] = createSignal(false);
  const theme = props.theme ?? defaultTheme;
  const cupCtx = useCup();

  createEffect(() => {
    const { onHover } = props;
    if (typeof onHover === "function") {
      onHover(hovered());
    }
  });

  function MatchRow(self: "high" | "low") {
    const other = self === "high" ? "low" : "high";
    const id = props.match?.[(self + "_id") as "high_id" | "low_id"];
    const fromId = props.match[self === "high" ? "elim_high_match_id" : "elim_low_match_id"];
    const report = () => props.match.low_report ?? props.match.high_report;
    const len = createMemo(() => report()?.length ?? 0);
    const lenClamped = createMemo(() => Math.min(5, len()));
    const matchScoresWidth = createMemo(
      () => matchWidth * (matchScoresWidthPropByScores?.[len()] ?? 0.36)
    );
    // const matchScoreWidth = () => matchScoresWidth() / len();
    const matchSignupsWidth = createMemo(() => matchWidth - matchScoresWidth());
    const offs = createMemo(() => matchScoresWidth() / len());
    const dy = createMemo(() => (self === "high" ? mh14 : mh34));
    const amMatchWinner = createMemo(() =>
      typeof props.match.winner_id === "string"
        ? props.match[`${self}_id`] === props.match.winner_id
        : undefined
    );
    return (
      <g class={theme.elimMatchRow} data-winner={amMatchWinner()}>
        {typeof id === "string" ? (
          <SignupLoader
            signup={id}
            render={(s) => <SignupView theme={theme} lenClamped={lenClamped()} dy={dy()} {...s} />}
          />
        ) : (
          <text class={typeof fromId === "number" ? theme.elimMatchId : ""} dx={10} dy={dy()}>
            {typeof fromId === "number" ? matchName(props.nodeById[fromId]?.data!) : "Bye"}
          </text>
        )}
        <For each={report()}>
          {(m, i) => (
            <g transform={`translate(${matchSignupsWidth() + (1 + i()) * offs() - 6}, 0)`}>
              <text
                class={theme.elimScore}
                dy={dy()}
                data-self={self}
                data-winner-loss={amMatchWinner() ? m[other] > m[self] : undefined}
                data-loser-win={!amMatchWinner() ? m[self] > m[other] : undefined}
              >
                {m[self]}
              </text>
            </g>
          )}
        </For>
      </g>
    );
  }

  return (
    <g
      class={theme.elimMatchContent}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
    >
      {props.match.is_scored && (
        <rect
          class={theme.elimWinnerBg}
          y={props.match.high_id === props.match.winner_id ? 0 : mh12}
          height={mh12}
          width={matchWidth}
        />
      )}
      {props.match.high_id === props.match.winner_id
        ? [MatchRow("high"), MatchRow("low")]
        : [MatchRow("low"), MatchRow("high")]}
    </g>
  );
}
