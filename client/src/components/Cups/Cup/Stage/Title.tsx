import { CupStage } from "@/api";
import { createMemo } from "solid-js";
import { stageFormatName } from "./stageFormats";

export default function StageTitle(props: { stage: undefined | CupStage; longFormat?: boolean }) {
  return <>{props.stage?.title}</>;
}
