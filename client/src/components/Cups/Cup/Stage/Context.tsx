import { api, CupStage } from "@/api";
import { useAsync } from "@/shared/async/Awaitable";
import { createContext, JSX, useContext } from "solid-js";
import { useCup } from "../Context";

const Ctx = createContext<ReturnType<typeof useCupStageData>>();

export type CupStageProviderProps =
  | {
      stageId?: undefined;
      cupId: string;
      stageNo: number;
    }
  | {
      stageId: undefined | null | string;
      stageNo?: undefined;
    };

export function CupStageProvider(
  props: {
    children: JSX.Element;
  } & CupStageProviderProps
) {
  const value = useCupStageData(props);
  return <Ctx.Provider value={value}>{props.children}</Ctx.Provider>;
}

export function useCupStage() {
  return useContext(Ctx)!;
}

function useCupStageData(props: CupStageProviderProps) {
  const cup = useCup();

  function checkStageNo(stage: CupStage | undefined): stage is CupStage {
    if (stage === undefined) return false;
    if ("stageId" in props) {
      return props.stageId === stage?.id;
    } else if (typeof props.stageNo === "number") {
      return props.stageNo === stage?.stage_no;
    }
    log.warn("invalid stage number, waiting...");
    return false;
  }

  const data = useAsync(async (ref) => {
    const { stageId, stageNo } = props;
    const stage_ =
      typeof stageId === "string"
        ? await api.cups.stagesById.byId(stageId, ref)
        : typeof stageNo === "number"
        ? cup.stages()?.array?.[stageNo]
        : undefined;
    if (!checkStageNo(stage_)) return undefined;
    const matches = await api.cups.matches.byParentId(stage_.id, ref);
    const rankings = await api.cups.stages.rankings.byId(stage_.id, ref);
    return { stage: stage_, matches, rankings };
  });

  return {
    stage: () => data()?.stage,
    matches: () => data()?.matches,
    rankings: () => data()?.rankings,
  };
}
