import { ArrayMap, CupMatch2p, CupStage, CupStageFormat } from "@/api";
import { Link, navigate } from "@/router";
import { Match, Switch } from "solid-js";
import { useCup } from "../Context";
import { useCupStage } from "./Context";
import ElimMatches from "./ElimMatches";
import GroupMatches from "./GroupMatches";
import StageTime from "./StageTime";
import theme from "./theme.module.scss";
import StageTitle from "./Title";

export interface CupStageHomeProps {
  stage: CupStage;
  matches: ArrayMap<CupMatch2p, number>;
}

export function CupStageHome(p: CupStageHomeProps) {
  const c = useCup();
  function onClickMatch(matchId: number) {
    log.debug("onCLickMatch", matchId);
    navigate({ to: "cups.cup.matches.match", params: { id: c.cupId, matchId: "" + matchId } });
  }
  return (
    <div class={theme.cupStageHome}>
      <h2>
        <Link to="cups.cup.stage" params={{ id: p.stage.cup_id, stageNo: p.stage.stage_no + "" }}>
          <StageTitle stage={p.stage} />
        </Link>
      </h2>
      {p.stage.is_started ? (
        <StageMatches onClickMatch={onClickMatch} />
      ) : (
        <StageTime stage={p.stage} />
      )}
    </div>
  );
}

export function StageMatches(p: { onClickMatch: (id: number) => void }) {
  const ctx = useCupStage();
  return (
    <Switch>
      <Match when={ctx.stage()?.format === "groups"}>
        <GroupMatches />
      </Match>
      <Match when={isElim(ctx.stage()?.format)}>
        <ElimMatches onClickMatch={p.onClickMatch} panZoom />
      </Match>
    </Switch>
  );
}

function isElim(format?: CupStageFormat) {
  return format === "single_elim" || format === "double_elim";
}
