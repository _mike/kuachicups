import { api, CupStage } from "@/api";
import { Link, navigate } from "@/router";
import { parseDate } from "@/shared/date";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import Icon from "@/shared/ui/Icon";
import FormatResponseError from "@/shared/ui/ResponseError";
import { createTicker } from "@/shared/ui/Time";
import toast from "@/shared/ui/Toast";
import log from "loglevel";
import { For } from "solid-js";
import { useCup } from "../Context";
import StageTime from "../Stage/StageTime";
import StageTitle from "../Stage/Title";

export default function () {
  const ctx = useCup();
  return <For each={ctx.stages()?.array}>{(stage) => <CupStageControls stage={stage} />}</For>;
}

function CupStageControls(props: { stage: CupStage }) {
  const now = createTicker("m");

  const ctx = useCup();

  const CupStageButtons = () => (
    <div class="grid">
      <button
        disabled={
          props.stage.is_started ||
          (typeof props.stage.starttime === "string" &&
            !(now() >= parseDate(props.stage.starttime!)!))
        }
        onClick={() => {
          api.cupsCupStageInitialise(props.stage.id).then(
            () => {
              delete api.cups.cache[ctx.cupId];
              delete api.cups.stages.cache[ctx.cupId];
              navigate({
                to: "cups.cup.stage",
                params: {
                  id: ctx.cupId,
                  stageNo: props.stage.stage_no + "",
                },
              });
            },
            (err) => {
              log.error("could not initialise cup!", JSON.stringify(props, null, 2));
              toast.failure(() => (
                <>
                  <p>Could not initialise cup</p>
                  <FormatResponseError error={err} />
                </>
              ));
            }
          );
        }}
        class="is-primary"
      >
        {props.stage.is_started ? "Already started" : "Start"}
      </button>
      <ConfirmButton
        class="is-warning"
        onConfirm={() => {
          api.cupsCupStageRestart(props.stage.id).then(
            () => {
              delete api.cups.cache[ctx.cupId];
              delete api.cups.stages.cache[ctx.cupId];
              navigate({
                to: "cups.cup.stage",
                params: {
                  id: ctx.cupId,
                  stageNo: props.stage.stage_no + "",
                },
              });
            },
            (err) => {
              toast.failure(() => (
                <>
                  <p>Could not restart stage</p>
                  <FormatResponseError error={err} />
                </>
              ));
            }
          );
        }}
      >
        Restart
      </ConfirmButton>
      <ConfirmButton
        class="is-warning"
        onConfirm={() => {
          api.cupsCupStageAdvance(props.stage.id).then(
            () => {
              navigate({
                to: "cups.cup.stage",
                params: {
                  id: ctx.cupId,
                  stageNo: props.stage.stage_no + "",
                },
              });
            },
            (err) => {
              toast.failure(() => (
                <>
                  <p>Could not advance stage</p>
                  <FormatResponseError error={err} />
                </>
              ));
            }
          );
        }}
      >
        Advance
      </ConfirmButton>
    </div>
  );

  return (
    <div class="box" style={{ "margin-bottom": "var(--typography-spacing-vertical)" }}>
      <h4>
        <Link
          to="cups.cup.manage.stage"
          params={{ id: ctx.cupId, stageNo: props.stage.stage_no + "" }}
        >
          <Icon type="options" />
          <StageTitle stage={props.stage} />
        </Link>
      </h4>
      <StageTime stage={props.stage} />
      <CupStageButtons />
    </div>
  );
}
