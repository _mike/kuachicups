import { api, Cup, CupSignup, CupStage, SetSignup, UpdateSignup } from "@/api";
import PlayerLink from "@/components/Player/Link";
import { useAsync } from "@/shared/async/Awaitable";
import Dropdown, { unpaged } from "@/shared/ui/Dropdown";
import { CheckBoxField, createField, NumberField, StrictNumberField } from "@/shared/ui/Form2";
import Icon from "@/shared/ui/Icon";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import { createEffect, createMemo, For, JSX } from "solid-js";
import { createStore } from "solid-js/store";
import { useCup } from "../Context";
import StageTitle from "../Stage/Title";

interface SignupState extends SetSignup {
  drop: boolean;
  forfeit: boolean;
  origin: CupSignup;
  changed(): boolean;
}

interface State {
  signups: SignupState[];
  extra: {
    playerId: string;
    teamId: string | undefined;
    seedValue: number | undefined;
    addToStageNo: number | undefined;
    status: "signup" | "checkin" | "drop";
  }[];
}

export default function SignupsControls(): JSX.Element {
  const ctx = useCup();

  const isEditable = useAsync(async () => {
    return true;
    /*
    if (
      typeof props.cup.currentStage === "number" &&
      props.cup.currentStage > 0
    ) {
      log.info(
        "cannot edit signups since already at stage ",
        props.cup.currentStage
      );
      return false;
    }
    const stages = await api.cups.stages.byParentId(props.cup.id);
    if (stages.length > 0) {
      const stage0 = stages[0];
      if (stage0.isStarted) return false;
    }
    return true;
    */
  });

  const [state, setState] = createStore<State>({
    signups: [],
    extra: [],
  });

  const dropOrForfeit = createMemo(() =>
    ctx.stages().array[ctx.currentStageNo()!]?.is_started ? "forfeit" : "drop"
  );

  function dropOrForfeitLabel(df: "drop" | "forfeit", value: boolean) {
    if (df === "drop") return value ? "Will drop" : "Not dropped";
    if (df === "forfeit") return value ? "Will forfeit" : "Not forfeited";
    return "unknown";
  }

  const S = createField(state, setState);

  const update = () => {
    setState({ signups: [], extra: [] });
    ctx.update();
  };

  createEffect(async () => {
    const signups = ctx.signups()?.array;
    if (signups === undefined) return;
    const init = [];
    for (const signup of signups) {
      const startState: Omit<SignupState, "changed"> = {
        origin: signup,
        signup_id: signup.id,
        drop: false,
        forfeit: typeof signup.forfeit_stage_no === "number" ? true : false,
        checkin: signup.checked_in,
        player_id: signup.player_id,
        team_id: signup.team_id ?? undefined,
        seed_value: signup.seed_value ?? undefined,
      };
      Object.freeze(startState);
      init.push({
        ...startState,
        changed() {
          for (const k in startState) {
            if ((startState as any)[k] !== this[k]) return true;
          }
          return false;
        },
      });
    }
    setState("signups", init);
  });

  async function onSaveChanges() {
    const updates: UpdateSignup[] = [];
    for (const s of state.signups) {
      if (!s.changed()) {
        log.debug("skipping update of", JSON.parse(JSON.stringify(s)));
        continue;
      }
      if (s.drop && dropOrForfeit() === "drop") {
        updates.push({ tag: "DropSignup", content: s.signup_id });
      } else if (s.forfeit && dropOrForfeit() === "forfeit") {
        updates.push({ tag: "ForfeitSignup", content: s.signup_id });
      } else {
        updates.push({ tag: "SetSignup", content: s });
      }
    }
    for (const ex of state.extra) {
      if (ex.status === "drop") {
        log.warn("unsupported drop on state.extra");
        continue;
      } else {
        updates.push({
          tag: "AddSignup",
          content: {
            player_id: ex.playerId,
            seed_value: ex.seedValue,
            team_id: ex.teamId,
            checkin: ex.status === "checkin",
          },
        });
      }
    }
    try {
      await api.cupsCupUpdateSignups(ctx.cupId, updates);
      toast.success("Saved changes");
    } catch (err) {
      toast.failure(() => (
        <>
          <p class="content">Could not set signups</p>
          <FormatResponseError error={err} />
        </>
      ));
    } finally {
      update();
    }
  }

  function SignupAdderRow(props: { nextStageNo: number }) {
    const initialState = {
      seedValue: undefined,
      checkin: false,
      teamId: undefined,
      playerId: undefined,
      stageNo: props.nextStageNo,
    } as {
      seedValue: undefined | number;
      teamId: undefined | string;
      playerId: undefined | string;
      checkin: boolean;
      stageNo: undefined | number;
    };
    const [another, setAnother] = createStore({ ...initialState });
    const S = createField(another, setAnother);
    return (
      <tr>
        <td>
          <S.Fld optic={S.optic.prop("seedValue")}>
            {(value, onChange) => (
              <StrictNumberField step={1} value={value() ?? undefined} onChange={onChange} />
            )}
          </S.Fld>
        </td>

        <td>
          <PlayerTeamSelector
            playerId={another.playerId}
            teamId={another.teamId}
            onChange={setAnother}
          />
        </td>

        <td>
          <S.Fld optic={S.optic.prop("checkin")}>
            {(value, onChange) => (
              <CheckBoxField label="Check-in" value={value()} onChange={onChange} />
            )}
          </S.Fld>
        </td>

        <td>
          <S.Fld optic={S.optic.prop("stageNo")}>
            {(value, onChange) => (
              <Dropdown
                kind="fetch"
                options={api.cups.stages.dropdownOptions(ctx.cupId)}
                value={ctx.stages()?.array?.[value()!]?.id}
                onChange={(opt) => {
                  const ix = ctx.stages()?.array?.findIndex?.((v) => v.id === opt.key);
                  if (typeof ix === "number" && ix >= 0) {
                    log.debug("stage = ", ix);
                    onChange(ix);
                  }
                }}
              >
                Stage
              </Dropdown>
            )}
          </S.Fld>
        </td>

        <td>
          <button
            class="button is-pulled-right"
            onClick={async () => {
              const { playerId, teamId, seedValue, checkin, stageNo } = another;
              if (typeof playerId !== "string") return;
              setState("extra", (es) => [
                ...es,
                {
                  status: checkin ? "checkin" : "signup",
                  seedValue,
                  playerId,
                  teamId: teamId ?? undefined,
                  addToStageNo: stageNo,
                },
              ]);
              setAnother({ ...initialState });
            }}
          >
            <Icon type="add" />
            <span>Add</span>
          </button>
        </td>
      </tr>
    );
  }

  return (
    <div>
      <table class="table is-fullwidth">
        <colgroup>
          <col span="1" width="120px" />
          <col span="1" width="120px" />
          <col span="1" />
          <col span="1" />
          <col span="1" />
        </colgroup>
        <thead>
          <tr>
            <th>Origin stage</th>
            <th>
              Seed <Icon type="arrow-down" />
            </th>
            <th>Participant</th>
            <th>Checked in?</th>
            <th>{dropOrForfeit() === "forfeit" ? "Forfeit?" : "Drop?"}</th>
          </tr>
        </thead>
        <tbody>
          <S.ArrayFld
            optic={S.optic.prop("signups")}
            create={() => {
              throw new Error("cannot create SignupChange");
            }}
          >
            {(AS) => (
              <AS.Each>
                {(signup, _ix, o) => (
                  <tr>
                    <td>
                      <S.Fld optic={o.prop("origin").prop("add_to_stage_no")}>
                        {(value, _onChange) => (
                          <StageTitle stage={ctx.stages()?.array?.[value()!]} />
                        )}
                      </S.Fld>
                    </td>

                    <td>
                      <S.Fld optic={o.prop("seed_value")}>
                        {(value, onChange) => (
                          <StrictNumberField
                            step={1}
                            value={value() ?? undefined}
                            onChange={onChange}
                          />
                        )}
                      </S.Fld>
                    </td>
                    <td>
                      <PlayerLink
                        playerId={signup.player_id}
                        teamId={signup.team_id ?? undefined}
                      />
                    </td>
                    <td>
                      <S.Fld optic={o.prop("checkin")}>
                        {(value, onChange) => (
                          <CheckBoxField
                            label={value() ? "Checked in" : "Checked out"}
                            value={value() ?? undefined}
                            onChange={onChange}
                          />
                        )}
                      </S.Fld>
                    </td>

                    <td>
                      {() => {
                        const df = dropOrForfeit();
                        return (
                          <S.Fld optic={o.prop(df)}>
                            {(value, onChange) => (
                              <CheckBoxField
                                label={dropOrForfeitLabel(df, value())}
                                value={value() ?? undefined}
                                onChange={onChange}
                              />
                            )}
                          </S.Fld>
                        );
                      }}
                    </td>
                  </tr>
                )}
              </AS.Each>
            )}
          </S.ArrayFld>
          <For each={state.extra}>
            {(extra) => (
              <tr>
                <td>
                  <input
                    class="input is-small"
                    disabled
                    placeholder="Seed"
                    value={extra.seedValue ?? ""}
                  />
                </td>
                <td>
                  <PlayerLink playerId={extra.playerId} teamId={extra.teamId} />
                </td>
                <td colSpan={2}>
                  <CheckBoxField
                    label="Check-in"
                    value={extra.status === "checkin"}
                    innerProps={{ disabled: true }}
                  />
                </td>
                <td>
                  <NumberField value={extra.addToStageNo} innerProps={{ disabled: true }} />
                </td>
              </tr>
            )}
          </For>
        </tbody>

        <tfoot>
          <tr>
            <th colSpan={6}>
              <button
                class="button is-link is-pulled-right"
                onClick={onSaveChanges}
                disabled={!isEditable()}
              >
                Save changes
              </button>
            </th>
          </tr>
        </tfoot>
      </table>
      <hr />
      <h3 class="heading">New signups</h3>
      <table class="table is-fullwidth">
        <colgroup>
          <col span="1" style="width:120px" />
          <col />
          <col />
        </colgroup>
        <thead>
          <tr>
            <th>Seed</th>
            <th>Participant</th>
            <th>Checking in</th>
            <th>Stage</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <SignupAdderRow nextStageNo={nextStageNo(ctx.cup(), ctx.stages()?.array)} />
        </tbody>
      </table>
      <hr />
    </div>
  );
}

function PlayerTeamSelector(props: {
  teamId?: string;
  playerId?: string;
  onChange: (val: { teamId: string | undefined; playerId: string | undefined }) => void;
}) {
  return (
    <>
      <Dropdown
        renderOption={(p) => (typeof p.option.key === "string" ? p.option.title : "Team")}
        onChange={(option) => {
          const teamId = option.key;
          if (typeof teamId === "string" && teamId !== "_empty") {
            props.onChange({ teamId, playerId: undefined });
          }
        }}
        value={props.teamId ?? "_empty"}
        kind="search"
        options={async (pg: number, terms) => {
          const noTeamOpt = {
            key: "_empty",
            title: "No team",
            onClick: () => props.onChange({ teamId: undefined, playerId: props.playerId }),
          };
          if (terms === "") {
            return [unpaged, [noTeamOpt]];
          }
          const [p, ts] = await api.teams.page(pg, terms);
          return [
            p,
            [
              noTeamOpt,
              ...ts.map((t) => ({
                key: t.id,
                title: t.name,
              })),
            ],
          ];
        }}
      >
        Team
      </Dropdown>
      <Dropdown
        onChange={(option) =>
          props.onChange({
            teamId: props.teamId,
            playerId: option.key as string,
          })
        }
        value={props.playerId}
        kind="search"
        textSearchRequireTerms
        options={(page, terms) =>
          api.players.page(page, terms).then((ps) => [
            ps[0],
            ps[1].map((p) => ({
              key: p.id,
              title: p.discord_tag,
            })),
          ])
        }
      >
        Player
      </Dropdown>
    </>
  );
}

function nextStageNo(cup: undefined | Cup, stages: undefined | readonly CupStage[]): number {
  if (typeof cup?.current_stage === "number") {
    const stage = stages?.[cup.current_stage];
    return cup.current_stage + (stage?.is_started ? 1 : 0);
  }
  return 0;
}
