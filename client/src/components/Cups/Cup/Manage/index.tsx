import { api } from "@/api";
import { navigate } from "@/router";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import { createMemo, JSX } from "solid-js";
import { createStore } from "solid-js/store";
import { useCup } from "../Context";
import SignupsControls from "./SignupsControls";
import StagesControls from "./StagesControls";

export default function CupStageManage(): JSX.Element {
  return (
    <>
      <h2>Signups</h2>
      <SignupsControls />
      <h2>Stages</h2>
      <StagesControls />
      <h2>Delete this cup</h2>
      <CupDeletion />
    </>
  );
}

function CupDeletion() {
  const ctx = useCup();
  const [state, setState] = createStore({
    doubleChecked: false,
    understandUnrecoverable: false,
  });
  const canDelete = createMemo(() => state.doubleChecked && state.understandUnrecoverable);
  function onDeleteCup() {
    api.cupsCupDelete(ctx.cupId).then(
      () => {
        navigate({ to: "cups" });
        location.reload();
      },
      (err) => {
        toast.failure(() => (
          <>
            <p>Could not delete cup</p>
            <FormatResponseError error={err} />
          </>
        ));
      }
    );
  }
  return (
    <div>
      <label>
        <input
          type="checkbox"
          onClick={(ev) => setState("doubleChecked", ev.currentTarget.checked)}
        />
        I intend to delete <i>{ctx.cup()?.title}</i>
      </label>
      <label>
        <input
          type="checkbox"
          onClick={(ev) => setState("understandUnrecoverable", ev.currentTarget.checked)}
        />
        I understand this operation is unrecoverable
      </label>
      <button class="contrast" disabled={!canDelete()} onClick={onDeleteCup}>
        Delete
      </button>
    </div>
  );
}
