import {
  api,
  CupMatch2p,
  CupSignup,
  CupStage,
  CupStageScoring,
  PagedForUuid,
  PostMedia,
} from "@/api";
import useAuth from "@/context/Auth";
import { useAsync, useAsyncI, usePromise } from "@/shared/async/Awaitable";
import { createCounter, createCounterSignal } from "@/shared/async/Counter";
import { unpaged } from "@/shared/ui/Dropdown";
import log from "loglevel";
import { createContext, createMemo, JSX, useContext } from "solid-js";
import createSignupState from "./signupState";

export interface PendingMatch {
  match: CupMatch2p;
  scoring: CupStageScoring;
  signup: CupSignup | undefined;
  admin: boolean;
}

function useCupData(props: { cupId: string }) {
  const getAuth = useAuth();

  const cup = useAsync((ref) => api.cups.byId(props.cupId, ref));

  const ownerTeam = usePromise(async () => {
    const { owner_team_id: ownerTeamId } = await cup.then();
    return api.teams.byId(ownerTeamId);
  });

  const gameMode = usePromise(async () => {
    const { game_mode_id: gameModeId } = await cup.then();
    const mode = await api.games.modes.byId(gameModeId);
    const game = await api.games.byId(mode.game_id);
    return { game, mode };
  });

  const loginIsOwner = useAsync(async (ref) => {
    const ownerTeamId = (await cup.then()).owner_team_id;
    const auth = await getAuth().then;
    if (auth.loggedIn !== true) {
      return false;
    }
    return (await api.players.roleWithin(auth.login.id, ownerTeamId, ref)) === "admin";
  }, false);

  const currentStageNo = useAsync(async () => (await cup.then())?.current_stage ?? undefined);

  const stages = useAsync(
    (ref) => api.cups.stages.byParentId(props.cupId, ref),
    Object.freeze({ array: [], map: {} })
  );

  const currentStage = useAsync<CupStage | undefined>(async () => {
    stages();
    currentStageNo();
    const sts = await stages.then();
    const idx = await currentStageNo.then();
    return sts?.array?.[idx ?? -1];
  });

  const finalStage = useAsync<CupStage | undefined>(async () => {
    stages();
    const sts = await stages.then();
    return sts?.array?.[(sts?.array?.length ?? 0) - 1];
  });

  const signups = useAsync((ref) => api.cups.signups.byParentId(props.cupId, ref), {
    array: [],
    object: {},
  });

  const isComplete = createMemo(() => cup()?.is_finished);

  const ownPendingMatches = useAsync(async () => {
    const ms = await api.cupsCupOwnPendingMatches(props.cupId);
    log.debug("ownPendingMatches", ms);
    return ms;
  }, []);

  const matches = useAsync(() => api.cupsCupMatches(props.cupId));

  const signupCounter = createCounter(createCounterSignal("signupState"));
  const ownCupSignup = useAsync(
    async (_ref) => {
      const cupId = props.cupId;
      const auth = getAuth();
      if (auth.login === undefined) return undefined;
      const ownSignups = await api.cupsCupSignupsSelf(cupId);
      return ownSignups?.[0];
    },
    undefined,
    signupCounter
  );
  const signupState = createSignupState(ownCupSignup, cup, signupCounter);

  // XXX only loads the first something or rather
  const mediaLinks = useAsyncI<[PagedForUuid, PostMedia[]]>(async () => {
    const page = await api.postsMediasQuery(0, props.cupId);
    const items = await api.media.byIds(page.items.map((i) => i.id));
    return [page, items];
  }, [unpaged, []]);

  const isLoading = createMemo(() =>
    [
      cup.loading(),
      ownerTeam() === undefined,
      gameMode() === undefined,
      stages.loading(),
      signups.loading(),
      ownPendingMatches.loading(),
      signupState.loading(),
      mediaLinks.loading(),
    ].some((v) => v)
  );

  function update() {
    log.info("doing cup context update");
    cup.update();
    stages.update(); // ^^ these two unlikely to update, remove? XXX
    signups.update();
    ownPendingMatches.update();
    signupCounter.signal.increment();
  }

  return {
    get cupId() {
      return props.cupId;
    },
    cup,
    ownerTeam,
    gameMode,
    loginIsOwner,
    stages,
    currentStage,
    finalStage,
    currentStageNo,
    ownCupSignup,
    ownPendingMatches,
    matches,
    signupState,
    signups,
    mediaLinks,
    isComplete,
    isLoading,
    update,
  };
}

const Ctx = createContext<ReturnType<typeof useCupData>>();

export function CupProvider(props: { cupId: string; children: JSX.Element }) {
  const value = useCupData(props);
  return <Ctx.Provider value={value}>{props.children}</Ctx.Provider>;
}

export function useCup() {
  return useContext(Ctx)!;
}

export function useCupMaybe() {
  return useContext(Ctx);
}

