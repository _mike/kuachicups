import { api, Cup, CupSignup } from "@/api";
import { AwaitableI, useAsyncI } from "@/shared/async/Awaitable";
import { Counter } from "@/shared/async/Counter";
import { parseDate } from "@/shared/date";

export type SignupState =
  | { tag: "canSignup"; mustCheckinOnSignup: boolean; requiresTeam: boolean }
  | ({ tag: "signedUp" } & SignedUpState)
  | { tag: "cannotSignup"; reason: CannotSignupReason }
  | { tag: "noStages" }
  | { tag: "loading" };

export type SignedUpState =
  | { checkin: "cannotCheckinTooEarly"; checkintime: Date }
  | { checkin: "checkedIn"; begun: boolean }
  | { checkin: "canCheckin" | "cannotCheckinTooLate" };

export type CannotSignupReason = "closed" | "missed" | "finished";

export default function createSignupState(
  getOwnSignup: () => CupSignup | undefined,
  getCup: () => Cup | undefined,
  counter: Counter
): AwaitableI<SignupState> {
  async function getRequiresTeamSignup(cup: Cup, refresh?: boolean) {
    const mode = await api.games.modes.byId(cup.game_mode_id, refresh);
    return mode.entrant_type === "team";
  }

  async function getFirstStage(cupId: string, refresh?: boolean) {
    return api.cups.stages.byId(cupId, 0, refresh);
  }

  return useAsyncI<SignupState>(
    async (refresh): Promise<SignupState> => {
      const cup = getCup();
      if (cup === undefined) return { tag: "loading" };
      // destructure these first (before any await) in order to react to changes
      const { is_finished, is_signups_closed, game_mode_id: _gm, id: cupId } = cup;
      if (is_finished) {
        return { tag: "cannotSignup", reason: "finished" };
      }
      const now = new Date();
      const mySignup = getOwnSignup();
      // NOTE reactivity ends here
      const first = await getFirstStage(cupId, refresh);
      if (first === undefined) return { tag: "noStages" };
      if (first.starttime == null || first.checkintime == null) {
        return { tag: "cannotSignup", reason: "closed" };
      }
      const starttime = parseDate(first.starttime);
      const checkintime = parseDate(first.checkintime);
      const begun = now >= starttime!;
      const checkinBegun = now >= checkintime!;
      // not signed up
      if (mySignup === undefined) {
        // can sign up
        if (!begun) {
          return {
            tag: "canSignup",
            mustCheckinOnSignup: checkinBegun,
            requiresTeam: await getRequiresTeamSignup(cup, refresh),
          };
        }
        // too late
        return { tag: "cannotSignup", reason: "missed" };
      }
      // if we have a signup and signups are closed, then we were one of the
      // invited players, and we can still manage our signup
      if (is_signups_closed && mySignup === undefined) {
        return { tag: "cannotSignup", reason: "closed" };
      }
      // allow checking out? nah that defeats the purpose
      // have a "withdraw" button maybe
      if (mySignup.checked_in) {
        return { tag: "signedUp", checkin: "checkedIn", begun };
      }
      if (!begun && checkintime !== undefined) {
        if (checkinBegun) {
          return { tag: "signedUp", checkin: "canCheckin" };
        }
        return {
          tag: "signedUp",
          checkin: "cannotCheckinTooEarly",
          checkintime: checkintime,
        };
      }
      return { tag: "signedUp", checkin: "cannotCheckinTooLate" };
    },
    { tag: "loading" },
    counter
  );
}
