import { PoolItemState } from "@/api";
import { createMemo, For, Show } from "solid-js";
import { getItemNameFromStep, useMatch } from "../Context";
import PoolCard from ".";
import theme from "./theme.module.scss";

export default function PickbanSelect(props: {
  poolName: string;
  filter: undefined | Record<string, PoolItemState | undefined>;
  mode: "pick" | "ban";
  selection?: undefined | { poolName: string; itemName: undefined | string };
  onSelect?: (props: { poolName: string; itemName: undefined | string }) => void;
}) {
  const m = useMatch();
  const parent = createMemo(() => {
    const rules = m.pickbanRules();
    const state = m.pickbanState();
    const rule = rules?.steps[state?.next_step?.pickban_index!];
    if (rule === undefined) return undefined;
    if (typeof rule.parent_index !== "number") return undefined;
    return state?.steps[rule.parent_index];
  });
  return (
    <details class={theme.select} role="list" open={true}>
      <summary onClick={(ev) => ev.preventDefault()}>
        You must {props.mode} a {props.poolName.toLowerCase() ?? ""}
        <Show when={parent()}>{(parent) => <> for {getItemNameFromStep(parent)}</>}</Show>
      </summary>
      <div role="listbox" style="position:initial">
        <div class={theme.selectList} data-mode={props.mode}>
          <For each={m.pickbanRules()?.pool?.find((v) => v.name === props.poolName)?.items}>
            {(item) => (
              <PoolCard
                available={props?.filter?.[item.name] === undefined}
                itemName={item.name}
                poolName={props.poolName}
                onClick={props.onSelect}
                selected={
                  props.selection !== undefined &&
                  props.selection?.itemName === item.name &&
                  props.selection?.poolName === props.poolName
                }
              />
            )}
          </For>
        </div>
      </div>
    </details>
  );
}
