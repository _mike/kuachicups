import { Pickbans } from "@/api";
import { usePromise } from "@/shared/async/Awaitable";
import magicalExplodingObject from "@/shared/explosion";
import { createContext, JSX, splitProps, useContext } from "solid-js";
import theme from "./theme.module.scss";

/** Lazily perform a promise, once */
function once<T>(importer: () => Promise<{ default: T }>): () => Promise<T> {
  let cached: undefined | Promise<T>;
  return () => {
    if (cached !== undefined) return cached;
    return (cached = importer().then((t) => t.default));
  };
}

////////////////////////////////////////////////////////////////////////////////
// Quake Champions pools

const fallback = once(() => import("@/resources/dazza256.png"));

const qcChamps: Record<string, () => Promise<string>> = {
  Anarki: once(() => import("@/resources/qc/champs/Anarki.jpg")),
  Athena: once(() => import("@/resources/qc/champs/Athena.jpg")),
  "B.J. Blazkowicz": once(
    () => import("@/resources/qc/champs/B.J. Blazkowicz.jpg")
  ),
  Clutch: once(() => import("@/resources/qc/champs/Clutch.jpg")),
  "Death Knight": once(() => import("@/resources/qc/champs/Death Knight.jpg")),
  "Doom Slayer": once(() => import("@/resources/qc/champs/Doom Slayer.jpg")),
  Eisen: once(() => import("@/resources/qc/champs/Eisen.jpg")),
  Galena: once(() => import("@/resources/qc/champs/Galena.jpg")),
  Keel: once(() => import("@/resources/qc/champs/Keel.jpg")),
  Nyx: once(() => import("@/resources/qc/champs/Nyx.jpg")),
  Ranger: once(() => import("@/resources/qc/champs/Ranger.jpg")),
  Scalebearer: once(() => import("@/resources/qc/champs/Scalebearer.jpg")),
  Slash: once(() => import("@/resources/qc/champs/Slash.jpg")),
  Sorlag: once(() => import("@/resources/qc/champs/Sorlag.jpg")),
  "Strogg & Peeker": once(
    () => import("@/resources/qc/champs/Strogg & Peeker.jpg")
  ),
  Visor: once(() => import("@/resources/qc/champs/Visor.jpg")),
};

const qcMaps: Record<string, () => Promise<string>> = {
  Awoken: once(() => import("@/resources/qc/maps/Awoken.jpg")),
  "Blood Covenant": once(
    () => import("@/resources/qc/maps/Blood Covenant.jpg")
  ),
  "Blood Run": once(() => import("@/resources/qc/maps/Blood Run.jpg")),
  "Burial Chamber": once(
    () => import("@/resources/qc/maps/Burial Chamber.jpg")
  ),
  "Church of Azathoth": once(
    () => import("@/resources/qc/maps/Church of Azathoth.jpg")
  ),
  "Corrupted Keep": once(
    () => import("@/resources/qc/maps/Corrupted Keep.jpg")
  ),
  Crucible: once(() => import("@/resources/qc/maps/Crucible.jpg")),
  "Deep Embrace": once(() => import("@/resources/qc/maps/Deep Embrace.jpg")),
  Exile: once(() => import("@/resources/qc/maps/Exile.jpg")),
  Insomnia: once(() => import("@/resources/qc/maps/Insomnia.jpg")),
  Lockbox: once(() => import("@/resources/qc/maps/Lockbox.jpg")),
  "Longest Yard": once(() => import("@/resources/qc/maps/Longest Yard.jpg")),
  "Molten Falls": once(() => import("@/resources/qc/maps/Molten Falls.jpg")),
  "Ruins of Sarnath": once(
    () => import("@/resources/qc/maps/Ruins of Sarnath.jpg")
  ),
  "Tempest Shrine": once(
    () => import("@/resources/qc/maps/Tempest Shrine.jpg")
  ),
  "Tower of Koth": once(() => import("@/resources/qc/maps/Tower of Koth.jpg")),
  "Vale of Pnath": once(() => import("@/resources/qc/maps/Vale of Pnath.jpg")),
};

const qlMaps: Record<string, () => Promise<string>> = {
  Aerowalk: once(() => import("@/resources/ql/aerowalk.webp")),
  Battleforged: once(() => import("@/resources/ql/battleforged.webp")),
  Campgrounds: once(() => import("@/resources/ql/campgrounds.webp")),
  Cure: once(() => import("@/resources/ql/cure.webp")),
  Dismemberment: once(() => import("@/resources/ql/dismemberment.webp")),
  Elder: once(() => import("@/resources/ql/elder.webp")),
  Hektik: once(() => import("@/resources/ql/hektik.webp")),
  "House of Decay": once(() => import("@/resources/ql/houseofdecay.webp")),
  "Lost World": once(() => import("@/resources/ql/lostworld.webp")),
  Sinister: once(() => import("@/resources/ql/sinister.webp")),
  "Furious Heights": once(() => import("@/resources/ql/t7.webp")),
  Toxicity: once(() => import("@/resources/ql/tox.webp")),
  "Blood Run": once(() => import("@/resources/ql/ztn.webp")),
} as const;

const qlPool: Record<string, Record<string, () => Promise<string>>> = {
  Map: qlMaps,
};

const qcPool: Record<string, Record<string, () => Promise<string>>> = {
  Map: qcMaps,
  Champion: qcChamps,
} as const;

/** Known pickbans by slug -> pool slug -> name -> image */
const pickbans: Record<
  string,
  Record<string, Record<string, () => Promise<string>>>
> = {
  "qc-tld-s1-bo1": qcPool,
  "qc-tld-s1-bo3": qcPool,
  "qc-tld-s1-bo5": qcPool,
  "qc-tld-s3-bo1": qcPool,
  "qc-tld-s3-bo3": qcPool,
  "qc-tld-s3-bo5": qcPool,
  "aql3-bo1": qcPool,
  "aql3-bo3": qcPool,
  "aql3-bo5": qcPool,
  "aql6-bo1": qcPool,
  "aql6-bo3": qcPool,
  "aql6-bo5": qcPool,
  "aql6-bo7-gf": qcPool,
  "kuachicon-bo1": qcPool,
  "kuachicon-bo3": qcPool,
  "kuachicon-bo5": qcPool,
  "kuachicon-bo7-gf": qcPool,
  "allmaps-bo1": qcPool,
  "allmaps-bo3": qcPool,
  "allmaps-bo5": qcPool,
  "qpl2023-s1-bo1": qcPool,
  "qpl2023-s1-bo3": qcPool,
  "qpl2023-s1-bo5": qcPool,
  'kdc-y1-s1-bo1': qcPool,
  'kdc-y1-s1-bo3': qcPool,
  'kdc-y1-s1-bo5': qcPool,
  'kdc-y1-s1-bo7': qcPool,
  "aql4-bo1": qcPool,
  "aql4-bo3": qcPool,
  "aql4-bo5": qcPool,
  "apacql1-bo1": qlPool,
  "apacql1-bo3": qlPool,
  "apacql1-bo5": qlPool,
};

////////////////////////////////////////////////////////////////////////////////

export function lookupPoolImage(
  pickbansSlug: string,
  poolName: string,
  itemName: string
) {
  log.debug("lookupPoolImage", pickbansSlug, poolName, itemName);
  return pickbans?.[pickbansSlug]?.[poolName]?.[itemName];
}

export function lookupPoolImageByIndex(
  pickbans: Pickbans,
  poolIndex: number,
  itemIndex: number
) {
  const pbPool = pickbans.pool[poolIndex];
  const pbItem = pbPool.items[itemIndex];
  return lookupPoolImage(pickbans.slug, pbPool.name, pbItem.name)?.();
}

const PickbanImageContext = createContext<{
  pickbans: undefined | Pickbans;
}>(magicalExplodingObject("PickbanImageContext"));

export function PickbanImageProvider(props: {
  pickbans: Pickbans | undefined;
  children: JSX.Element;
}) {
  return (
    <PickbanImageContext.Provider
      value={{
        get pickbans() {
          return props.pickbans;
        },
      }}
    >
      {props.children}
    </PickbanImageContext.Provider>
  );
}

export function usePoolImage(
  props: () => { poolName: string; itemName: undefined | string }
) {
  const ctx = useContext(PickbanImageContext);
  return usePromise(async () => {
    const { poolName, itemName } = props();
    const pickbanSlug = ctx.pickbans?.slug;
    if (pickbanSlug === undefined) return undefined;
    if (itemName === undefined) return undefined;
    const img = lookupPoolImage(pickbanSlug, poolName, itemName);
    if (img === undefined) return fallback();
    return img();
  });
}

export function PoolImage(
  props: Omit<JSX.IntrinsicElements["img"], "pool" | "item" | "img"> & {
    poolName: string;
    itemName: undefined | string;
  }
) {
  const [innerProps, imgProps] = splitProps(props, [
    "poolName",
    "itemName",
    "src",
  ]);
  const src = usePoolImage(() => innerProps);
  return (
    <img
      class={`${theme.poolImage} ${props.class}`}
      src={src()}
      {...imgProps}
    />
  );
}
