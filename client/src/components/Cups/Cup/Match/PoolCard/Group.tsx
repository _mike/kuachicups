import { JSX } from "solid-js";
import theme from "./theme.module.scss";

export default function PoolCardGroup(props: {
  isActive: undefined | boolean;
  primary: JSX.Element;
  secondary: JSX.Element;
  scores?: JSX.Element;
}) {
  return (
    <div
      class={theme.group}
      data-active={props.isActive}
      data-has-score={props.scores !== undefined}
    >
      {props.primary}
      {props.secondary}
      {props.scores}
    </div>
  );
}
