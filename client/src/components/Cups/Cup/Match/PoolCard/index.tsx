import classNames from "classnames";
import { createSignal, JSX, Show, untrack } from "solid-js";
import { SignupLoader } from "../../CupSignup";
import { usePoolImage } from "./Image";
import theme_ from "./theme.module.scss";

export interface IPoolCardProps {
  class?: string;
  available?: boolean;
  signupId?: string;
  poolName: string;
  pickIndex?: number;
  itemName: undefined | string;
  ban?: boolean;
  high?: boolean;
  low?: boolean;
  selected?: boolean;
  isNext?: boolean;
  hideItemName?: boolean;
  onClick?: (props: { poolName: string; itemName: undefined | string }) => void;
  children?: JSX.Element;
  winner?: boolean;
  theme?: Record<string, string>
}

export default function PoolCard(props: IPoolCardProps) {
  const img = usePoolImage(() => props);
  const theme = untrack(() => props.theme ?? theme_);
  return (
    <div
      class={classNames(theme.card, props.class)}
      data-ban={props.ban}
      data-available={props.available}
      data-has-pick={props.itemName !== undefined}
      data-has-signup={props.signupId !== undefined}
      data-high={props.high}
      data-low={props.low}
      data-winner={props.winner}
      data-next={props.isNext}
      data-selected={props.selected}
      onClick={() => {
        log.debug("click pool card", props);
        props.onClick?.(props);
      }}
    >
      <div class={theme.inner}>
        <Show when={props.signupId}>
          {(signupId) => (
            <span class={theme.signupWrapper}>
              <SignupLoader tabbable={false} size="24" class={theme.signup} signup={signupId} />
            </span>
          )}
        </Show>
        <div class={theme.img} style={{ "background-image": `url(${JSON.stringify(img())})` }} />
        <span class={theme.itemNameWrapper}>
          <span class={theme.itemName}>
            {props.itemName ?? addIndex(props.poolName, props.pickIndex)}
          </span>
        </span>
      </div>
      {props.children !== undefined || props.isNext === true ? (
        <div class={theme.children}>
          {props.children ?? (props.isNext === true ? <div aria-busy="true" /> : undefined)}
        </div>
      ) : undefined}
    </div>
  );
}

function addIndex(name: string, index?: number) {
  if (index !== undefined) {
    return `${name} #${index + 1}`;
  }
  return name;
}
