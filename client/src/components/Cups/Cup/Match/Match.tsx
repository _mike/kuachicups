import { api } from "@/api";
import { EmbedView } from "@/components/Media";
import useAuth from "@/context/Auth";
import Resource from "@/shared/async/Resource";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import isUuid from "is-uuid";
import { createSignal, For, Show } from "solid-js";
import { useCup } from "../Context";
import { MatchScoreEditorProvider, useMatch } from "./Context";
import { _MatchCard } from "./MatchCard";
import { MatchTitle } from "./MatchTitle";
import Pickbans from "./Pickbans";

const pickbanItemHeight = 52;

export default function Match() {
  const auth = useAuth();
  const match = useMatch();
  const cup = useCup();
  return (
    <div>
      <h2>
        <MatchTitle cupId={cup.cupId} longFormat match={match.match()!} showUpdateTime />
      </h2>
      <Show
        when={match.pickbanRules()?.isInteresting}
        fallback={() => <_MatchCard selection={{}} />}
      >
        <MatchScoreEditorProvider>
          <Pickbans />
        </MatchScoreEditorProvider>
      </Show>
      <Show when={match.mediaLinks()?.total_count! > 0}>
        <MediaLinks />
      </Show>
      <Show when={auth().admin}>
        <MediaLinker />
      </Show>
    </div>
  );
}

function MediaLinks() {
  const match = useMatch();
  return (
    <div class="flex-row" style="margin-bottom:var(--spacing)">
      <For each={match.mediaLinks()?.items}>
        {(pageItem) => (
          <Resource data={() => api.media.byId(pageItem.id)}>
            {(media) => <EmbedView {...media} />}
          </Resource>
        )}
      </For>
    </div>
  );
}

function MediaLinker() {
  const match = useMatch();
  const cup = useCup();
  const [input, setInput] = createSignal("");
  return (
    <article>
      <header>Media linking</header>
      <p>In the media page, copy the ID of a relevant media item here to link it to this match</p>
      <label>
        Media ID
        <input
          style="font-family:monospace"
          value={input()}
          onInput={(ev) => setInput(ev.currentTarget.value)}
          type="text"
        />
      </label>
      <button
        disabled={!isUuid.v4(input())}
        onClick={async () => {
          const { cup_stage_id: cupStageId } = await match.match.then();
          try {
            await api.postsLinkMedia(input(), {
              match_id: match.matchId,
              cup_id: cup.cupId,
              stage_id: cupStageId,
            });
            toast.success("Added media link!");
          } catch (err) {
            toast.failure(() => (
              <>
                <p>Could not add media link</p>
                <FormatResponseError error={err} />
              </>
            ));
          }
        }}
      >
        Set media ID
      </button>
    </article>
  );
}
