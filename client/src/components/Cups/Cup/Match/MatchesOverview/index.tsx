import { CupMatch2p } from "@/api";
import { createMatchOverlay, encodeConfig } from "@/components/Overlay/Config/config";
import IntersectionRoot from "@/context/IntersectionObserver";
import { navigate } from "@/router";
import { Lazily, LazyProvider } from "@/shared/Lazy";
import Icon from "@/shared/ui/Icon";
import Menu from "@/shared/ui/Menu";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import { createMemo, For, Show } from "solid-js";
import { createMutable } from "solid-js/store";
import { useCup } from "../../Context";
import { MatchCard, MatchCardSkeleton } from "../MatchCard";
import { MatchTitle } from "../MatchTitle";
import theme from "./theme.module.scss";

export default function MatchesOverview(props: { source: "all" | "self" }) {
  const ctx = useCup();
  const selection = createMutable({} as Record<number, CupMatch2p>);
  return (
    <>
      <MatchesGrid
        selection={selection}
        matches={
          props.source === "all" ? ctx.matches()?.filter?.(hasSignups) : ctx.ownPendingMatches()
        }
      />
      <Menu.MenuPortal side="left">
        <MatchesSelectionMenu selection={selection} />
      </Menu.MenuPortal>
    </>
  );
}

export function MatchesSelectionMenu(props: { selection: Record<number, CupMatch2p> }) {
  const selectionSize = createMemo(() => Object.keys(props.selection).length);
  return (
    <Menu>
      <Menu.Title>Selected matches</Menu.Title>
      <For
        each={Object.keys(props.selection).sort()}
        fallback={() => <Menu.Item>No matches selected</Menu.Item>}
      >
        {(matchId) => (
          <Menu.Item>
            <MatchTitle match={props.selection[Number(matchId)]} longFormat={false} />
          </Menu.Item>
        )}
      </For>
      <Menu.Title>Stream Overlay</Menu.Title>
      <Menu.ItemLink
        onClick={() => {
          (async () => {
            const overlay = await createMatchOverlay(Object.values(props.selection));
            const cfg = await encodeConfig(overlay);
            navigate({ to: "stream", params: { cfg } });
          })().catch((err) => {
            toast.failure(() => (
              <>
                Cannot create overlay config!
                <FormatResponseError error={err} />
              </>
            ));
          });
        }}
        disabled={selectionSize() === 0}
      >
        <Icon type="tv" /> Create stream overlay
      </Menu.ItemLink>
      <Show when={selectionSize() === 0}>
        <Menu.Item>
          <small>Select matches in order to create an overlay</small>
        </Menu.Item>
      </Show>
    </Menu>
  );
}

export function MatchesGrid(props: {
  selection: Record<number, CupMatch2p>;
  matches: CupMatch2p[] | undefined;
}) {
  return (
    <IntersectionRoot component="div" class={theme.matchesGrid}>
      <LazyProvider>
        <For each={props.matches}>
          {(m) => (
            <Lazily fallback={MatchCardSkeleton}>
              <MatchCard match={m} selection={props.selection} />
            </Lazily>
          )}
        </For>
      </LazyProvider>
    </IntersectionRoot>
  );
}

function hasSignups(m: CupMatch2p) {
  return typeof m.high_id === "string" && typeof m.low_id === "string";
}
