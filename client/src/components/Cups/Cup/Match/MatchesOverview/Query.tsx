import { api, CupMatch2p } from "@/api";
import IntersectionRoot from "@/context/IntersectionObserver";
import { arrayParam, boolParam, intParam, navigate, stringParam, useRoute } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import { Lazily, LazyProvider } from "@/shared/Lazy";
import Dropdown2 from "@/shared/ui/Dropdown2";
import Menu from "@/shared/ui/Menu";
import { PageLinks, SimplePaged } from "@/shared/ui/Paginate";
import { createEffect, createMemo, For, JSX } from "solid-js";
import { createMutable } from "solid-js/store";
import { MatchesSelectionMenu } from ".";
import { CupProvider } from "../../Context";
import { MatchCard, MatchCardSkeleton } from "../MatchCard";
import theme from "./theme.module.scss";

function MatchesGrid(props: { matches: CupMatch2p[]; selection: Record<number, CupMatch2p> }) {
  return (
    <IntersectionRoot component="div" class={theme.matchesGrid}>
      <LazyProvider>
        <For each={props.matches}>
          {(m) => (
            <Lazily fallback={MatchCardSkeleton}>
              <CupFromStageProvider stageId={m.cup_stage_id!}>
                <MatchCard showCupName match={m} selection={props.selection} />
              </CupFromStageProvider>
            </Lazily>
          )}
        </For>
      </LazyProvider>
    </IntersectionRoot>
  );
}

function CupFromStageProvider(props: { stageId: string; children: JSX.Element }) {
  const cupId = useAsync(async (ref) => {
    const stage = await api.cups.stagesById.byId(props.stageId, ref);
    return stage.cup_id;
  });
  return (
    <cupId.Await>
      {(cupId) => <CupProvider cupId={cupId}>{props.children}</CupProvider>}
    </cupId.Await>
  );
}

export default function MatchesQuery() {
  const route = useRoute();
  const params = createMemo(() => {
    const p = route().params;
    return {
      page: intParam(p.page, 0),
      cupId: p.cup_id,
      stageId: p.stage_id,
      isScored:
        p.is_scored !== "" && p.is_scored !== null && p.is_scored !== undefined
          ? boolParam(p.is_scored)
          : undefined,
      playerIds: arrayParam(stringParam, p.player_ids),
      teamIds: arrayParam(stringParam, p.team_ids),
    };
  });
  const selection = createMutable({} as Record<number, CupMatch2p>);
  return (
    <Menu.Layout
      menu={() => (
        <>
          <Menu>
            <Menu.Title>Filter matches</Menu.Title>
            <Menu.Item>
              <label>
                <input
                  type="checkbox"
                  ref={(inp) => {
                    createEffect(() => {
                      const scored = params().isScored;
                      if (scored === undefined) {
                        inp.checked = false;
                        inp.indeterminate = true;
                      } else {
                        inp.indeterminate = false;
                        inp.checked = scored;
                      }
                    });
                  }}
                  onChange={() => {
                    const cur = params().isScored;
                    let next: boolean | undefined;
                    if (cur === undefined) {
                      next = false;
                    } else if (cur === false) {
                      next = true;
                    }
                    navigate({
                      to: "cups.matches",
                      params: { ...(route().params as any), is_scored: next },
                    });
                  }}
                />
                {" Scored"}
              </label>
            </Menu.Item>
          </Menu>
          <MatchesSelectionMenu selection={selection} />
        </>
      )}
    >
      <SimplePaged<CupMatch2p>
        source={async (page) => {
          const p = params();
          const ids = await api.cupsMatchesQuery(
            p.playerIds ?? [],
            p.teamIds ?? [],
            p.isScored ?? null,
            p.cupId ?? null,
            p.stageId ?? null,
            page
          );
          const ms = await api.cupsMatches(ids.items.map((i) => i.id));
          return [ids, ms];
        }}
        getPage={() => params().page ?? 0}
        renderItems={(p) => <MatchesGrid matches={p.items} selection={selection} />}
        renderItem={() => null}
        renderContent={(c) => c}
        renderPageButtons={(p) => <PageLinks to="cups.matches" {...p} />}
      />
    </Menu.Layout>
  );
}
