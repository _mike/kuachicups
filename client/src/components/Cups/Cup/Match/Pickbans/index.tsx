import {
  api,
  CupMatch2pPickbanStepAbbrev,
  NextPickbanStep,
  PickbanStep,
  PickbanStepKind,
} from "@/api";
import { Case, CaseOf } from "@/shared/Case";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import Icon from "@/shared/ui/Icon";
import toast from "@/shared/ui/Toast";
import clipboardCopy from "clipboard-copy";
import { createEffect, createMemo, For, Index, onCleanup, Show } from "solid-js";
import { createStore } from "solid-js/store";
import { Dynamic } from "solid-js/web";
import { SignupLoader, SignupName } from "../../CupSignup";
import {
  getActorFromStepKind,
  getItemNameFromStep,
  isWinner,
  NicePickbanStep,
  PickbanStepRule,
  subPickItemName,
  useMatch,
  useMatchScoreEditor,
} from "../Context";
import PoolCard from "../PoolCard";
import PoolCardGroup from "../PoolCard/Group";
import { PickbanImageProvider } from "../PoolCard/Image";
import PickbanSelect from "../PoolCard/Select";
import ScoreInput from "../ScoreInput";
import theme from "./theme.module.scss";
import onChangeAudio from "@/resources/EriMotionArrayDotCom.ogg?url";

const pickbanItemHeight = 52;

export const pickbansAudioId = "pickbansAudio";

export default function Pickbans() {
  const match = useMatch();
  const edit = useMatchScoreEditor();
  match.startInterval();

  function setupAudio(audioEl?: HTMLAudioElement) {
    createEffect((lastIx?: number) => {
      const state = match.pickbanState();
      const done = state?.is_done;
      if (done && typeof lastIx !== "number") return;
      const now = state?.next_step?.pickban_index;
      if (now !== lastIx && now !== 0) {
        if (audioEl != null && audioEl instanceof HTMLAudioElement) {
          audioEl.volume = 0.3;
          audioEl.play();
        }
      }
      return now;
    });
  }

  return (
    <div class={theme.match}>
      <audio id={pickbansAudioId} ref={setupAudio}>
        <source src={onChangeAudio} type="audio/ogg" />
      </audio>
      <PickbanImageProvider pickbans={match.pickbanRules()}>
        <Show when={match.pickbanState() !== undefined} fallback="pickbans not available">
          <div class={theme.pickbansArea}>
            <div class={theme.cards}>
              <For each={match.pickbanRules()?.pickSteps}>
                {(rule, index) => (
                  <PickbanStepCard
                    index={index()}
                    rule={rule}
                    step={match.pickbanState()?.steps?.[rule.index]}
                    showScore={
                      match.winner() !== undefined
                        ? "result"
                        : typeof match.canReportScoresAs() === "string"
                          ? "input"
                          : undefined
                    }
                  />
                )}
              </For>
              <button
                disabled={match.winner() !== undefined || edit?.winner === undefined}
                onClick={() => edit?.onSubmitScores()}
              >
                Submit scores &mdash;{" "}
                {edit?.winner === "high" ? <SignupName signup={match.match()?.high_id} /> : null}
                {edit?.winner === "low" ? <SignupName signup={match.match()?.low_id} /> : null} wins
              </button>
              <PickbanNextStep />
            </div>
            <PickbanSteps />
          </div>
        </Show>
      </PickbanImageProvider>
    </div>
  );
}

function PickbanNextStep() {
  const match = useMatch();
  const state = createMemo(() => {
    const reporter = match.canReportScoresAs();
    const step = match.pickbanState()?.next_step;
    const rule = match.pickbanRules()?.steps[step?.pickban_index ?? -1];
    if (step === undefined || step === null) return undefined;
    if (rule === undefined) return undefined;
    const canEdit = stepSignup(rule.kind) === reporter;
    return { canEdit, step, rule };
  });
  return (
    <Show when={state()} fallback={() => <PickbanString />}>
      {(state) =>
        !state.canEdit ? (
          <div class="box" aria-busy="true">
            Waiting for next action
          </div>
        ) : (
          <NextPickbanStepEditor nextStep={state.step} stepRule={state.rule} />
        )
      }
    </Show>
  );
}

function PickbanString() {
  const match = useMatch();

  const picksArray = createMemo<{ pick: string; subs: string[] }[] | undefined>(() => {
    const state = match.pickbanState();
    const rules = match.pickbanRules()?.pickSteps;
    const m = match.match();
    const r = [];
    if (m === undefined || state === undefined || rules === undefined) return undefined;
    if (!state.is_done) return undefined;
    for (const rule of rules) {
      const step = state.steps[rule.index]!;
      const subPicks = rule.sub_picks.map((p) => state.steps[p.index]);
      if (step.tag !== "done") continue;
      r.push({
        pick: step.item_name!,
        subs: subPicks.map((p) => (p.tag === "done" ? p.item_name! : "unknown")),
      });
    }
    return r;
  });

  let stringRef: HTMLElement | undefined;

  function onCopyPickbanString() {
    if (stringRef === undefined) {
      log.error("no pickban string element to select");
      return;
    }
    const text = stringRef.textContent;
    if (typeof text !== "string") {
      log.error("no pickban string textContent within element");
      return;
    }
    const range = document.createRange();
    const sel = getSelection();
    range.selectNodeContents(stringRef);
    sel?.removeAllRanges();
    sel?.addRange(range);
    clipboardCopy(text).then(
      () => {
        toast.success(`copied ${text}!`);
      },
      () => {
        toast.failure("could not copy string!");
      }
    );
  }

  return (
    <Show when={match.pickbanState()?.is_done}>
      <div class="box flex-row">
        <small ref={(e) => (stringRef = e)}>
          [
          <SignupName signup={match.match()?.high_id} />
          /
          <SignupName signup={match.match()?.low_id} />]{" "}
          <For each={picksArray()}>
            {(step) => (
              <>
                [{step.pick}] {step.subs.join("/")}{" "}
              </>
            )}
          </For>
        </small>
        <button style="float:right" onClick={onCopyPickbanString}>
          <Icon type="copy" /> Copy
        </button>
      </div>
    </Show>
  );
}

function PickbanStepCard(props: {
  index: number;
  step: NicePickbanStep | undefined;
  rule: PickbanStepRule;
  showScore?: "input" | "result";
}) {
  const matchCtx = useMatch();
  const editCtx = useMatchScoreEditor();
  function isActive() {
    const state = matchCtx.pickbanState();
    const rules = matchCtx.pickbanRules();
    if (state === undefined || state.is_done || state.next_step == null) return undefined;
    const nextIx = state.next_step.pickban_index;
    const nextRule = rules?.steps[nextIx];
    return props.rule.index === nextIx || props.rule.index === nextRule?.parent_index;
  }

  const reportWinner = createMemo(() => {
    const realWinner = matchCtx.winner();
    const m = matchCtx.match();
    const { pick_index: pickIndex } = props.rule;
    if (realWinner !== undefined) {
      const scores = (m?.high_report ?? m?.low_report)?.[pickIndex!];
      const high = scores?.high ?? 0;
      const low = scores?.low ?? 0;
      if (high > low) return "high";
      if (low > high) return "low";
    }
    if (editCtx === undefined) return undefined;
    const { scores } = editCtx;
    const high = scores?.[pickIndex!]?.high ?? 0;
    const low = scores?.[pickIndex!]?.low ?? 0;
    if (high > low) return "high";
    if (low > high) return "low";
  });

  function scoreCell(index: number, who: "high" | "low" | undefined) {
    const m = matchCtx.match();
    if (who === undefined) return undefined;
    if (m === undefined) return undefined;
    const report = m.high_report?.[index] ?? m.low_report?.[index];
    return report?.[who] ?? "";
  }

  return (
    <PoolCardGroup
      isActive={isActive()}
      primary={
        <PoolCard
          pickIndex={props.rule.pick_index}
          signupId={getActorFromStepKind(props.step?.kind ?? undefined, matchCtx.match())}
          poolName={props.rule.pool_name}
          itemName={getItemNameFromStep(props.step)!}
        />
      }
      secondary={
        <For each={props.rule.sub_picks}>
          {(sub, subIndex) => (
            <PoolCard
              pickIndex={sub.sub_pick_index}
              signupId={getActorFromStepKind(sub.kind, matchCtx.match())}
              poolName={sub.pool_name}
              itemName={subPickItemName(sub, matchCtx)!}
              winner={isWinner(reportWinner(), stepSignup(sub.kind))}
            >
              <Case key={props.showScore ?? "none"}>
                {{
                  input: () => (
                    <ScoreInput
                      who={stepSignup(sub.kind)!}
                      game={props.index}
                      tabIndex={1 + props.index * 2 + subIndex()}
                      tooltipPlacement="right"
                      disableScroll
                    />
                  ),
                  result: () => (
                    <Show when={scoreCell(props.index, stepSignup(sub.kind))}>
                      {(score) => <input type="text" readonly value={score} />}
                    </Show>
                  ),
                }}
              </Case>
            </PoolCard>
          )}
        </For>
      }
    />
  );
}

function PickbanSteps() {
  const match = useMatch();
  return (
    <div
      ref={(div) => {
        createEffect(() => {
          const index = match.pickbanState()?.next_step?.pickban_index;
          if (index === undefined) return;
          div.scrollTo({ top: index * pickbanItemHeight });
        });
      }}
      class={theme.pickbanSteps}
    >
      <Index each={match.pickbanState()?.steps}>
        {(step) => (
          <div class={theme.pickbanStep} data-tag={step().tag}>
            <CaseOf data={step()} key="tag">
              {{
                done: (step) => <StepTitle step={step} tense="past" />,
                pending: (step) => <StepTitle step={step} tense="future" />,
                next: (step) => <StepTitle step={step} tense="future" />,
              }}
            </CaseOf>
          </div>
        )}
      </Index>
    </div>
  );
}

function NextPickbanStepEditor(props: { nextStep: NextPickbanStep; stepRule: PickbanStep }) {
  const match = useMatch();
  const [state, setState] = createStore({
    selection: undefined as undefined | { itemName: string | undefined; poolName: string },
  });
  return (
    <>
      <PickbanSelect
        poolName={props.stepRule.pool_name}
        filter={props.nextStep.pool_filter?.[props.stepRule.pool_name]}
        mode={stepMode(props.stepRule.kind)}
        selection={state.selection}
        onSelect={(p) => {
          setState({ selection: p });
        }}
      />
      <button
        class="bright"
        disabled={state.selection === undefined}
        onClick={() => {
          const p = state.selection;
          if (p === undefined) {
            log.warn("User was able to click pickban confirm without a selection");
            return;
          }
          if (p.itemName === undefined) {
            log.warn("tried to choose no item");
            return;
          }
          api
            .cupsCupPickbanAction(match.matchId, {
              pool_name: p.poolName,
              item_name: p.itemName,
            })
            .then(
              () => {
                setTimeout(() => {
                  match.pickbanState.update();
                }, 400);
              },
              () => { }
            );
          log.debug("select", p);
        }}
      >
        <Show when={state.selection} fallback="Confirm selection">
          {(sel) => (
            <>
              {stepKindTitle(props.stepRule.kind)} {sel.itemName}
            </>
          )}
        </Show>
      </button>
    </>
  );
}

function StepTitle(
  props:
    | { tense: "future"; step: PickbanStep }
    | { tense: "past"; step: CupMatch2pPickbanStepAbbrev }
) {
  const m = useMatch();
  return (
    <span>
      <SignupLoader signup={m.match()?.[`${stepSignup(props.step.kind)}_id`]} />{" "}
      {stepVerbPhrase(props.step.kind!, props.tense)}{" "}
      {props.tense === "future" ? (
        attachPoolNameDeterminer(props.step.pool_name)
      ) : (
        <Dynamic component={props.step.kind?.endsWith("pick") ? "ins" : "del"}>
          {props.step.item_name}
        </Dynamic>
      )}
      {props.step.kind !== "low_lose" ? null : " as a map in hand"}
    </span>
  );
}
function stepSignup(kind: PickbanStepKind | null | undefined): "high" | "low" {
  switch (kind) {
    case undefined:
    case null:
    case "reset":
      throw new Error("encounted null PickbanStepKind");
    case "low_ban":
    case "low_pick":
      return "low"
    case "low_lose":
    case "high_ban":
    case "high_pick":
      return "high"
  }
}

function stepMode(kind: PickbanStepKind | null | undefined): "pick" | "ban" {
  if (typeof kind !== "string") throw new Error("encountered null PickbanStepKind");
  if (kind === "low_lose") return "ban";
  return kind.endsWith("ban") ? "ban" : "pick";
}

function stepVerbPhrase(kind: PickbanStepKind, tense: "future" | "past"): string {
  const baseVerb: "pick" | "ban" = stepMode(kind); // kind.split("_")[1] as any;
  switch (baseVerb) {
    case "pick":
      return tense === "future" ? "will pick" : "picked";
    case "ban":
      return tense === "future" ? "will ban" : "banned";
  }
}

function stepKindTitle(kind: PickbanStepKind): string {
  switch (kind) {
    case "low_lose":
      return "(Lose)";
    case "high_ban":
    case "low_ban":
      return "Ban";
    case "high_pick":
    case "low_pick":
      return "Pick";
    case "reset":
      return "Reset";
  }
}

function attachPoolNameDeterminer(poolName: string): string {
  switch (poolName) {
    case "Map":
      return "a map";
    case "Champion":
      return "a champion";
    default:
      return "a " + poolName.toLowerCase();
  }
}
