import { api, CupMatch2p } from "@/api";
import { Link } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import Resource from "@/shared/async/Resource";
import { parseDate } from "@/shared/date";
import format from "date-fns/format/index";
import { createResource, Show } from "solid-js";
import { useCup } from "../Context";
import { MatchName } from "../Stage/MatchName";
import StageTitle from "../Stage/Title";

export function MatchTitle(props: {
  cupId?: string;
  match: CupMatch2p | undefined;
  longFormat?: boolean;
  showUpdateTime?: boolean;
  showCupName?: boolean;
}) {
  const ctx = useCup();
  const [getStage] = createResource(
    () => props.match?.cup_stage_id,
    (stageId) => {
      if (typeof stageId === "string") return api.cups.stagesById.byId(stageId);
      return undefined;
    }
  );
  return (
    <span>
      <Link
        class="bright"
        to="cups.cup.matches.match"
        params={{ id: ctx.cupId, matchId: "" + props.match?.id }}
      >
        <Show when={props.showCupName}>{ctx.cup()?.title} / </Show>
        <Show when={getStage()}>
          {(stage) => (
            <span>
              <StageTitle stage={stage} longFormat={props.longFormat ?? true} />
              {" ­ "}
            </span>
          )}
        </Show>
        {" / "}
        <b>
          <MatchName match={props.match} longFormat={props.longFormat ?? true} showRound />
        </b>
      </Link>
      <Show when={props.showUpdateTime}>
        <small>&nbsp; {format(parseDate(props.match?.updated_at) ?? 0, "PPpp")}</small>
      </Show>
    </span>
  );
}
