import {
  api,
  ApiError,
  CupMatch2p,
  CupMatch2pPickbanState,
  CupMatch2pPickbanStepAbbrev,
  CupMatchReportForm2p,
  NextPickbanStep,
  Paged_for_Uuid,
  PickbanPool,
  PickbanStep,
  PickbanStepKind,
} from "@/api";
import useAuth from "@/context/Auth";
import {
  Awaitable,
  createLazyMemo,
  useAsync,
  UsePromise,
  usePromise,
} from "@/shared/async/Awaitable";
import {
  combineCounterSignals,
  createCounter,
  createCounterSignal,
  useCounter,
} from "@/shared/async/Counter";
import { LoadingCounterProvider } from "@/shared/async/LoadingCounter";
import magicalExplodingObject from "@/shared/explosion";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import * as S from "fp-ts";
import {
  batch,
  createComputed,
  createContext,
  createEffect,
  createMemo,
  JSX,
  onCleanup,
  untrack,
  useContext,
} from "solid-js";
import { createMutable } from "solid-js/store";
import { useCup } from "../Context";
import { CupStageProvider } from "../Stage/Context";

////////////////////////////////////////////////////////////////////////////////

export interface IMatchContext {
  matchId: number;
  match: Awaitable<CupMatch2p>;
  canReportScoresAs(): "high" | "low" | false | undefined;
  pickbanRules: UsePromise<{
    id: number;
    name: string;
    slug: string;
    game_mode?: string | null;
    bestof: number;
    pool: PickbanPool[];
    isInteresting: boolean;
    steps: PickbanStepRule[];
    pickSteps: (PickbanStepRule & {
      kind: "low_pick" | "high_pick";
      parent_index: undefined | null;
    })[];
  }>;
  pickbanState: {
    (): undefined | NicePickbanState;
    update(): void;
  };
  bestof(): number;
  winner(): "high" | "low" | undefined;
  winInfo():
    | {
        highWins: number;
        lowWins: number;
      }
    | undefined;
  mediaLinks: Awaitable<Paged_for_Uuid>;
  startInterval(): void;
}

export type NicePickbanState = Omit<CupMatch2pPickbanState, "steps"> & {
  steps: NicePickbanStep[];
  picks: NicePickbanPickStep[];
};

export type NicePickbanPickStep = {
  tag: "done";
  kind: `${"high" | "low"}_pick`;
} & CupMatch2pPickbanStepAbbrev;

export type NicePickbanStep =
  | ({ tag: "done" } & CupMatch2pPickbanStepAbbrev)
  | ({ tag: "next"; item_name?: undefined } & NextPickbanStep & PickbanStep)
  | ({ tag: "pending"; item_name?: undefined } & PickbanStep);

export interface PickbanStepRule extends PickbanStep {
  index: number;
  pick_index: undefined | number;
  sub_pick_index: undefined | number;
  sub_picks: PickbanStepRule[];
}

const MatchContext = createContext<IMatchContext>(magicalExplodingObject("MatchContext"));

export function createMatchState(props: {
  match: number | CupMatch2p;
  interval?: number | undefined;
  playAudioOnChange?: boolean;
}): IMatchContext {
  const cupCtx = useCup();

  function matchId() {
    const m = props.match;
    if (typeof m === "number") return m;
    return m.id;
  }

  const match = useAsync(() => api.cupsMatchResult2P(matchId()));

  const canReportScoresAs = createMemo(() => {
    const m = match();
    if (m === undefined) return false;
    if (m.is_scored) return false;
    const signup = cupCtx.ownCupSignup();
    if (signup !== undefined && (m.low_id === signup.id || m.high_id === signup.id)) {
      return m.low_id === signup.id ? "low" : "high";
    }
    return undefined;
  });

  const pickbanRules = usePromise(async () => {
    const pbs = await api.cupsCupMatchPickbans(matchId());
    const grouped: PickbanStepRule[] = [];
    let pickIndex = 0;
    for (let i = 0; i < pbs.steps.length; i++) {
      const baseStep = pbs.steps[i];
      const step: PickbanStepRule = {
        ...baseStep,
        index: i,
        pick_index: baseStep.kind.endsWith("_pick") ? pickIndex++ : undefined,
        sub_pick_index: undefined,
        sub_picks: [],
      };
      grouped.push(step);
      if (typeof step.parent_index === "number" && step.kind.endsWith("_pick")) {
        step.sub_pick_index = grouped[step.parent_index].sub_picks.length;
        grouped[step.parent_index].sub_picks.push(step);
      }
    }
    for (const step of grouped) {
      step.sub_picks.sort((a, b) => pickbanStepKindOrd.compare(a.kind, b.kind));
      // for (let i = 0; i < step.subPicks.length; i++) {
      //   step.subPicks[i].subPickIndex = i;
      // }
      // Uncomment to enable sorted indices on subpicks
    }
    const r = {
      ...pbs,
      isInteresting: !pbs.slug.startsWith("generic"),
      steps: grouped,
      pickSteps: grouped.filter(
        (
          t
        ): t is typeof t & {
          kind: "low_pick" | "high_pick";
          parent_index: null | undefined;
        } => t.kind.endsWith("_pick") && typeof t.parent_index !== "number"
      ),
    };
    log.debug("pickbanRules", r);
    return r;
  });

  const pickbansSignal = createCounterSignal("pickbans");
  const pickbansNextCounter = createCounter(
    combineCounterSignals([pickbansSignal, useCounter().signal])
  );

  const pickbanStateRaw = useAsync(
    (_ref) => api.cupsCupMatchPickbanState(matchId()),
    undefined,
    pickbansNextCounter
  );

  const pickbanState = createLazyMemo<NicePickbanState | undefined>(() => {
    const next_ = pickbanStateRaw();
    const edit = canReportScoresAs();
    const pendingSteps = pickbanRules()?.steps;
    if (pendingSteps === undefined || next_ === undefined) return undefined;
    const { next_step: nextStep, is_done: isDone, steps: doneSteps } = next_;
    if (isDone || nextStep == null) {
      const r = doneSteps.map(doneStep);
      return {
        ...next_,
        steps: r,
        picks: r.filter(guardIsPick),
      };
    }
    const r: NicePickbanStep[] = [];
    r.push(...doneSteps.map(doneStep));
    if (typeof edit === "string") {
      r.push({ tag: "next", ...nextStep, ...pendingSteps[nextStep.pickban_index] });
      r.push(...pendingSteps.slice(1 + doneSteps.length).map(pendingStep));
    } else {
      r.push(...pendingSteps.slice(doneSteps.length).map(pendingStep));
    }
    return {
      ...next_,
      steps: r,
      picks: r.filter(guardIsPick) as NicePickbanPickStep[],
    };
  });

  const bestof = useAsync(async (ref) => {
    const m = match();
    if (m === undefined) return undefined;
    const scoring = await api.cups.scorings.byId(m.scoring_id, ref);
    return scoring.bestof;
  }, 0);

  const winner = createMemo(() => winnerOf(match()));
  const winInfo = createLazyMemo(() => {
    const m = match();
    const w = winner();
    if (w !== undefined) {
      const scores = m?.high_report ?? m?.low_report;
      if (!Array.isArray(scores)) return undefined;
      let highWins = 0;
      let lowWins = 0;
      for (let i = 0; i < scores.length; i++) {
        const high = scores?.[i].high ?? 0;
        const low = scores?.[i].low ?? 0;
        if (high > low) highWins++;
        if (low > high) lowWins++;
      }
      return { highWins, lowWins };
    }
  });

  const mediaLinks = useAsync(() => api.postsMediasQuery(matchId()));

  function startInterval() {
    createEffect(() => {
      // start interval if it's editable (whether or not _we_ can edit it)
      if (typeof canReportScoresAs() !== undefined && !pickbanStateRaw()?.is_done) {
        pickbansSignal.startInterval(5);
      } else {
        pickbansSignal.stopInterval();
      }
    });
    onCleanup(() => {
      pickbansSignal.stopInterval();
    });
  }

  // If props.interval is set, start the interval now
  createEffect(() => {
    const interval = props.interval;
    if (typeof interval === "number") {
      startInterval();
    }
  });

  return {
    get matchId() {
      return matchId();
    },
    mediaLinks,
    match,
    winner,
    winInfo,
    bestof,
    canReportScoresAs,
    pickbanState: Object.assign(pickbanState, {
      update() {
        pickbansSignal.increment();
      },
    }),
    pickbanRules,
    startInterval,
  };
}

export function MatchProvider(props: {
  match: number | CupMatch2p;
  interval?: number | undefined;
  children: JSX.Element;
}) {
  return (
    <LoadingCounterProvider>
      <MatchContext.Provider value={createMatchState(props)}>
        {props.children}
      </MatchContext.Provider>
    </LoadingCounterProvider>
  );
}

export function useMatch() {
  return useContext(MatchContext);
}

export function MatchStageProvider(props: { children: JSX.Element }) {
  const ctx = useMatch();
  return <CupStageProvider stageId={ctx.match()?.cup_stage_id}>{props.children}</CupStageProvider>;
}

////////////////////////////////////////////////////////////////////////////////
// Match results/score editing context
//

export interface IMatchScoreEditor {
  admin: boolean;
  self: "admin" | "high" | "low" | undefined;
  scores: {
    low: number | undefined;
    high: number | undefined;
  }[];
  winner: "high" | "low" | undefined;
  wins: {
    high: number;
    low: number;
  };
  onSubmitScores(): void;
  onAdminOverrideScores(): void;
}

const MatchScoreEditorContext = createContext<IMatchScoreEditor>();

export function MatchScoreEditorProvider(props: { admin?: boolean; children: JSX.Element }) {
  const m = useMatch();
  const _scores: Record<"low" | "high", number | undefined>[] = [];
  for (let i = 0; i < m.bestof(); i++) {
    _scores[i] = { low: undefined, high: undefined };
  }
  const scores = createMutable<Record<"low" | "high", number | undefined>[]>(_scores);

  function self() {
    const self = m.canReportScoresAs();
    if (typeof self === "string") return self;
    if (auth().admin) return "admin";
    return undefined;
  }

  createComputed(() => {
    const admin = props.admin;
    const self = m.canReportScoresAs();
    const bestof = m.bestof();
    const match = m.match();
    if (bestof === 0 || typeof bestof !== "number" || match === undefined) return;
    const { low_report: _lr, high_report: _hr } = match;
    untrack(() => {
      batch(() => {
        if (admin) {
          for (let i = 0; i < bestof; i++) {
            const low_low = match.low_report?.[i]?.low;
            const low_high = match.low_report?.[i]?.high;
            const high_low = match.high_report?.[i]?.low;
            const high_high = match.high_report?.[i]?.high;
            scores[i] = {
              high: high_high ?? low_high,
              low: high_low ?? low_low,
            };
          }
        } else if (typeof self === "string") {
          for (let i = 0; i < bestof; i++) {
            scores[i] = {
              high: match[`${self}_report`]?.[i]?.high,
              low: match[`${self}_report`]?.[i]?.low,
            };
          }
        }
      });
    });
  });

  const auth = useAuth();
  const results = createMemo(() => {
    let highWins = 0;
    let lowWins = 0;
    for (let i = 0; i < scores.length; i++) {
      const high = scores[i].high ?? 0;
      const low = scores[i].low ?? 0;
      if (high > low) highWins++;
      if (low > high) lowWins++;
    }
    let winner: "high" | "low" | undefined;
    if (Math.max(highWins, lowWins) > m.bestof() / 2) {
      if (highWins > lowWins) {
        winner = "high";
      } else if (lowWins > highWins) {
        winner = "low";
      }
    }
    return { winner, wins: { high: highWins, low: lowWins } };
  });

  function scoresToForm() {
    const form: CupMatchReportForm2p = {
      high_report: [],
      low_report: [],
      match_id: m.matchId,
    };
    if (scores === undefined || scores.length === 0) {
      return undefined;
    }
    for (let i = 0; i < scores.length; i++) {
      if (scores[i].high === undefined && scores[i].low === undefined) break;
      form.high_report.push(scores[i].high ?? 0);
      form.low_report.push(scores[i].low ?? 0);
    }
    debugger;
    return form;
  }

  return (
    <MatchScoreEditorContext.Provider
      value={{
        get admin() {
          return auth().admin === true && props.admin === true;
        },
        get self() {
          return self();
        },
        get winner() {
          return results().winner;
        },
        get wins() {
          return results().wins;
        },
        scores,
        onSubmitScores() {
          if (self() === undefined) {
            toast.failure("Cannot submit scores, not an admin or reporter on this match");
            return;
          }
          const form = scoresToForm();
          if (form === undefined) {
            toast.failure("No scores to submit");
            return;
          }
          api.cupsMatchReport2P(m.matchId, form).then(
            () => {
              toast.success("Submitted match score!");
              m.match.update();
            },
            (err) => {
              toast.failure(() => (
                <>
                  <h1>Match submission failure</h1>
                  <FormatResponseError error={err} />
                </>
              ));
              m.match.update();
            }
          );
        },
        onAdminOverrideScores() {
          const form = scoresToForm();
          if (form === undefined) {
            toast.failure("No scores to submit");
            return;
          }
          api.cupsMatchOverrideReport2P(m.matchId, form).then(
            () => {
              toast.success("Overrided match scores!");
              m.match.update();
            },
            (err) => {
              if (err instanceof ApiError) {
                toast.failure(() => <FormatResponseError error={err} />);
              } else {
                toast.failure(() => (
                  <>
                    <b>Error</b>
                    <pre>{JSON.stringify(err, null, 2)}</pre>
                  </>
                ));
              }
            }
          );
        },
      }}
    >
      {props.children}
    </MatchScoreEditorContext.Provider>
  );
}

export function useMatchScoreEditor(): undefined | IMatchScoreEditor {
  return useContext(MatchScoreEditorContext);
}

////////////////////////////////////////////////////////////////////////////////

function doneStep(step: CupMatch2pPickbanStepAbbrev): NicePickbanStep {
  return { tag: "done", ...step } as const;
}

function pendingStep(step: PickbanStep): NicePickbanStep {
  return { tag: "pending", ...step } as const;
}

export function getActorFromStepKind(
  stepKind: PickbanStepKind | undefined | null,
  m: undefined | CupMatch2p
) {
  const p = stepKind;
  if (typeof p !== "string" || m === undefined) return undefined;
  if (p.startsWith("high")) return m.high_id ?? undefined;
  return m.low_id ?? undefined;
}

export function getItemNameFromStep(step: undefined | NicePickbanStep) {
  if (step?.tag === "done") return step.item_name;
  return undefined;
}

function guardIsPick(step: NicePickbanStep): step is NicePickbanPickStep {
  return step.tag === "done" && step.kind !== null && step.kind.endsWith("_pick");
}

export function isWinner(winner: undefined | "high" | "low", query: "high" | "low") {
  return winner === undefined ? undefined : query === winner;
}

export function winnerOf(m: undefined | CupMatch2p) {
  if (m === undefined) return undefined;
  const { low_id: lowId, high_id: highId, winner_id: winnerId } = m;
  if (winnerId === highId) return "high";
  if (winnerId === lowId) return "low";
}

export function hasBye(m: undefined | CupMatch2p): undefined | boolean {
  if (m === undefined) return undefined;
  const { low_id: lowId, high_id: highId } = m;
  return typeof lowId !== "string" || typeof highId !== "string";
}

export const pickbanStepKindOrd: S.ord.Ord<PickbanStepKind> = {
  compare(a, b) {
    if (a === b) return 0;
    if (a.startsWith("high") && b.startsWith("low")) return -1;
    if (a.startsWith("low") && b.startsWith("high")) return 1;
    log.warn("PickbanStepKind encountered impossible case");
    return 0;
  },
  equals(a, b) {
    return a === b;
  },
};

export function subPickItemName(sub: PickbanStepRule, m: IMatchContext) {
  const step = m.pickbanState()?.steps?.[sub.index];
  if (step?.tag === "done") return step.item_name;
  return undefined;
}
