import Tooltip, { useSetTooltipActive } from "@/shared/ui/Tooltip";
import { Placement } from "@floating-ui/dom";
import { createMemo } from "solid-js";
import { useMatch, useMatchScoreEditor } from "./Context";

export default function ScoreInput(props: {
  who: "high" | "low";
  game: number;
  tabIndex?: number;
  zIndex?: number;
  tooltipPlacement?: Placement;
  disableScroll?: boolean;
}) {
  const m = useMatch();
  const e = useMatchScoreEditor()!;

  const state = createMemo(() => {
    const { who, game: j } = props;
    const self = e.self;
    const other =
      self === "admin" || self === undefined ? undefined : self === "high" ? "low" : "high";
    return {
      pick: m.pickbanState()?.picks?.[j],
      submittedValues: {
        self:
          self === "admin" || self === undefined
            ? undefined
            : m.match()?.[`${self}_report`]?.[j]?.[who],
        other: other === undefined ? undefined : m.match()?.[`${other}_report`]?.[j]?.[who],
        high: m.match()?.high_report?.[j]?.[who],
        low: m.match()?.low_report?.[j]?.[who],
      },
      value: e.scores?.[j]?.[who],
    } as const;
  });

  function onChange(value: number | undefined) {
    const scores = e.scores;
    const { who, game: j } = props;
    if (scores !== undefined) {
      (scores[j] ?? (scores[j] = { high: undefined, low: undefined }))[who] = value;
    } else {
      log.error(`scores[${who}][${j}] doesnt exist`);
    }
  }

  const placeholder = createMemo(() => {
    const self = e.self;
    const { game, who } = props;
    if (self === "admin") {
      const score = e?.scores?.[game]?.[who];
      if (typeof score === "number") return "" + score;
    } else {
      const otherReport = state().submittedValues.other;
      if (typeof otherReport === "number") return "" + otherReport;
    }
    return "";
  });

  function wrapValue(value: unknown) {
    switch (typeof value) {
      case "number":
      case "string":
        return value;
      default:
        return "";
    }
  }

  function Input() {
    const setTooltipActive = useSetTooltipActive();
    return (
      <input
        style="margin:0"
        data-who={props.who}
        tabIndex={props.tabIndex}
        type="text"
        pattern="[0-9]*"
        placeholder={placeholder()}
        value={wrapValue(e.scores?.[props.game]?.[props.who])}
        onFocus={() => setTooltipActive(true)}
        onFocusOut={() => setTooltipActive(false)}
        onPointerEnter={() => setTooltipActive(true)}
        onPointerLeave={() => setTooltipActive(false)}
        onBlur={() => setTooltipActive(false)}
        onInput={(ev) => {
          const el = ev.target as HTMLInputElement;
          const val = el.value;
          if (val === "") {
            onChange(undefined);
          }
          const n = Number.parseInt(val);
          if (Number.isSafeInteger(n) && val.match(/^-?[0-9]+$/)) {
            onChange(n);
          } else {
            // immediately override the user input
            // el.value = "" + (state().value ?? "");
            // actually-allow it; just don't store the value
            onChange(undefined);
          }
        }}
        onWheel={(ev) => {
          ev.preventDefault();
          const el = ev.target as HTMLInputElement;
          const val = Number.parseInt(el.value);
          const offset = ev.deltaY > 0 ? -1 : 1;
          const next = (Number.isSafeInteger(val) ? val : 0) + offset;
          onChange(next);
        }}
      />
    );
  }

  return (
    <Tooltip
      controlled
      placement={props.tooltipPlacement}
      zIndex={props.zIndex}
      tooltip={() => (
        <>
          <p>
            <b>{e.self === "admin" ? "High" : "Your"} submission</b>:{" "}
            {e.self === "admin" ? state().submittedValues.high : state().submittedValues.self}
          </p>
          <p>
            <b>{e.self === "admin" ? "Low" : "Their"} submission</b>:{" "}
            {e.self === "admin" ? state().submittedValues.low : state().submittedValues.other}
          </p>
        </>
      )}
    >
      <Input />
    </Tooltip>
  );
}
