import { api, ApiError, CupMatch2p, CupMatchReportForm2p } from "@/api";
import useAuth from "@/context/Auth";
import { Link } from "@/router";
import { EnumFromTo } from "@/shared/Case";
import { parseDate } from "@/shared/date";
import { LazyFallbackProps } from "@/shared/Lazy";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import { FallbackAvatarImg } from "@/shared/ui/Discord/Avatar";
import Icon from "@/shared/ui/Icon";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import classNames from "classnames";
import format from "date-fns/format";
import { createMemo, createSignal, Show, Suspense, JSX } from "solid-js";
import { createMutable } from "solid-js/store";
import { useCup } from "../Context";
import { SignupLoader } from "../CupSignup";
// import { MatchControlTable as AdminMatchControlTable } from "../Stage/Manage";
import {
  isWinner,
  MatchProvider,
  MatchScoreEditorProvider,
  useMatch,
  useMatchScoreEditor,
} from "./Context";
import { MatchTitle } from "./MatchTitle";
import ScoreInput from "./ScoreInput";
import theme from "./theme.module.scss";

export function MatchCard(props: {
  class?: string;
  match: number | CupMatch2p;
  selection?: Record<number, CupMatch2p>;
  streamFriendly?: boolean;
  showCupName?: boolean;
}) {
  return (
    <MatchProvider match={props.match}>
      <Suspense fallback={() => <MatchCardSkeleton />}>
        <_MatchCard
          class={props.class}
          selection={props.selection}
          showCupName={props.showCupName}
        />
      </Suspense>
    </MatchProvider>
  );
}

export function MatchCardSkeleton(
  props: Partial<LazyFallbackProps> & { streamFriendly?: boolean }
) {
  const match = useMatch();
  const ctx = useCup();
  return (
    <article
      ref={props.setup}
      data-in-viewport={props.inViewport}
      class={theme.matchCard + " " + (props.class ?? "")}
    >
      <header aria-busy="true">
        <label>Match</label>
      </header>
      <div class={theme.matchCardContent}>
        <table class={theme.matchScores}>
          <colgroup>
            <col />
          </colgroup>
          <tbody>
            <tr>
              <td>
                <FallbackAvatarImg />
              </td>
            </tr>
            <tr>
              <td>
                <FallbackAvatarImg />
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <Show when={!props.streamFriendly}>
        <footer>
          <Link to="cups.cup.matches.match" params={{ id: ctx.cupId, matchId: match.matchId + "" }}>
            <Icon type="arrow-forward" /> View match pickbans and details
          </Link>
        </footer>
      </Show>
    </article>
  );
}

export function _MatchCard(props: {
  class?: string;
  selection?: Record<number, CupMatch2p>;
  streamFriendly?: boolean;
  showCupName?: boolean;
}) {
  const [adminOpen, setAdminOpen] = createSignal(false);
  const auth = useAuth();
  const m = useMatch();

  function ContentFooter(p: { children?: JSX.Element }) {
    return (
      <>
        <div class={theme.matchCardContent}>{p.children}</div>
        <Show when={!props.streamFriendly}>
          <footer class="flex-row" style="justify-content:space-between">
            <MatchControlButtons />
          </footer>
        </Show>
      </>
    );
  }

  return (
    <article class={theme.matchCard}>
      <header class="flex-row" style="justify-content:space-between">
        <label>
          <Show when={props.selection !== undefined}>
            <input
              type="checkbox"
              checked={props.selection?.[m.matchId] !== undefined}
              onChange={(ev) => {
                const sel = props.selection;
                if (sel === undefined) throw new Error("impossible");
                if (ev.currentTarget.checked) {
                  sel[m.matchId] = m.match()!;
                } else {
                  delete sel[m.matchId];
                }
              }}
            />
          </Show>
          <m.match.Await>
            {(match) => <MatchTitle match={match} showUpdateTime showCupName={props.showCupName} />}
          </m.match.Await>
        </label>
        <Show when={auth().admin}>
          <div class="flex-row">
            <button class="outline small" style="margin:0" onClick={() => setAdminOpen((s) => !s)}>
              {adminOpen() ? <Icon type="close" /> : "admin"}
            </button>
          </div>
        </Show>
      </header>
      <Show
        when={typeof m.canReportScoresAs() === "string" || adminOpen()}
        fallback={() => (
          <ContentFooter>
            <MatchResultTable />
          </ContentFooter>
        )}
      >
        <MatchScoreEditorProvider admin={adminOpen()}>
          <ContentFooter>
            <EditableMatchResultTable />
          </ContentFooter>
        </MatchScoreEditorProvider>
      </Show>
    </article>
  );
}

export function MatchResultTable() {
  const m = useMatch();
  function eitherScore(j: number, who: "high" | "low") {
    const m_ = m?.match();
    return m_?.high_report?.[j]?.[who] ?? m_?.low_report?.[j]?.[who];
  }
  const matchNumScores = createMemo(() => {
    const m_ = m?.match();
    return Math.max(m_?.high_report?.length ?? 0, m_?.low_report?.length ?? 0);
  });
  return (
    <table class={theme.matchScores}>
      <colgroup>
        <col />
        <EnumFromTo from={0} to={matchNumScores()}>
          {(_i) => <col width="40px" />}
        </EnumFromTo>
      </colgroup>
      <tbody>
        {(["high", "low"] as const).map((who) => (
          <tr class={theme.matchRow} data-winner={isWinner(m.winner(), who) ?? "asdf"}>
            <td class={theme.signup}>
              <SignupLoader signup={m?.match()?.[`${who}_id`]} />
            </td>
            <EnumFromTo from={0} to={matchNumScores()}>
              {(j) => <td class={theme.score}>{eitherScore(j, who)}</td>}
            </EnumFromTo>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

////////////////////////////////////////////////////////////////////////////////
// Editing results

export function EditableMatchResultTable() {
  const m = useMatch();
  const e = useMatchScoreEditor()!;
  return (
    <table class={theme.matchScores}>
      <colgroup>
        <col />
        <EnumFromTo from={0} to={m.bestof()}>
          {(_i) => <col width="60px" />}
        </EnumFromTo>
      </colgroup>
      <tbody>
        {(["high", "low"] as const).map((who, row) => (
          <tr class={theme.matchRow} data-winner={e.winner === who}>
            <td class={theme.signup}>
              <SignupLoader signup={m?.match()?.[`${who}_id`]} />
            </td>
            <EnumFromTo from={0} to={m.bestof()}>
              {(j) => (
                <td class={classNames(theme.score, theme.scoreEdit)} style={{ "z-index": 2 - row }}>
                  <ScoreInput tooltipPlacement="bottom" who={who} game={j} zIndex={100 - row} />
                </td>
              )}
            </EnumFromTo>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export function MatchControlButtons() {
  const ctx = useCup();
  const match = useMatch();
  const e = useMatchScoreEditor();
  return (
    <div class="grid" style="flex:1">
      <Show when={match.pickbanRules()?.isInteresting}>
        <Link to="cups.cup.matches.match" params={{ id: ctx.cupId, matchId: match.matchId + "" }}>
          <Icon type="arrow-forward" /> View match pickbans and details
        </Link>
      </Show>
      <Show when={typeof match.canReportScoresAs() === "string" && e !== undefined}>
        <ConfirmButton
          onConfirm={() => e?.onSubmitScores()}
          confirming="Really submit scores?"
          disabled={match.winner() !== undefined || e?.winner === undefined}
        >
          Submit scores
        </ConfirmButton>
      </Show>
      <Show when={e?.admin}>
        <ConfirmButton
          onConfirm={() => e?.onAdminOverrideScores()}
          confirming="Really override scores?"
          disabled={e?.winner === undefined}
        >
          Override scores
        </ConfirmButton>
      </Show>
    </div>
  );
}
