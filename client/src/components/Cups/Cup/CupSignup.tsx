import { api, CupSignup, Player, Team } from "@/api";
import PlayerLink, { PlayerTeamView } from "@/components/Player/Link";
import { Awaitable, useAsync } from "@/shared/async/Awaitable";
import magicalExplodingObject from "@/shared/explosion";
import { DiscordAvatarImg, Size } from "@/shared/ui/Discord/Avatar";
import Icon from "@/shared/ui/Icon";
import { createContext, JSX, useContext } from "solid-js";

export interface CupSignupProps {
  signup: undefined | CupSignup;
  player?: Player;
  team?: Team;
  showSeed?: boolean;
  showCheckin?: boolean;
  tabbable?: boolean;
  align?: "right" | "left";
  nameOnly?: boolean;
  size?: Size;
  class?: string;
}

export function SignupView(props: CupSignupProps): JSX.Element {
  return (
    <span class={props.class}>
      <PlayerLink
        size={props.size}
        player={props.player}
        team={props.team}
        nameOnly={props.nameOnly}
        seedValue={props.showSeed ? props.signup?.seed_value ?? undefined : undefined}
        align={props.align}
        tabbable={props.tabbable}
      />
      {props.showCheckin && props.signup?.checked_in ? <Icon type="done-all" /> : undefined}
    </span>
  );
}

const SignupContext = createContext<
  Awaitable<{
    signup: CupSignup;
    player: undefined | Player;
    team: undefined | Team;
  }>
>(magicalExplodingObject("SignupContext"));

function SignupProvider(props: {
  signup: CupSignup | string | undefined | null;
  children: JSX.Element;
}) {
  const data = useAsync(async () => {
    const ps = props.signup;
    if (ps === undefined || ps === null) return undefined;
    const signup = typeof ps === "string" ? await api.cups.signupsByIdOnly.byId(ps) : ps;
    const [player, team] = await Promise.all([
      signup?.player_id != null ? api.players.byId(signup?.player_id) : undefined,
      signup?.team_id != null ? api.teams.byId(signup?.team_id) : undefined,
    ]);
    return { signup, player, team };
  });
  return <SignupContext.Provider value={data}>{props.children}</SignupContext.Provider>;
}

namespace SignupProvider {
  export function Name(props: { class?: string }) {
    const ctx = useContext(SignupContext)!;
    return (
      <span class={props.class}>
        <PlayerTeamView player={ctx()?.player} team={ctx()?.team} />
      </span>
    );
  }
  export function Seed() {
    const ctx = useContext(SignupContext);
    return <>{ctx()?.signup?.seed_value}</>;
  }
  export function Img(props: { class?: string; size?: string }) {
    const ctx = useContext(SignupContext);
    return (
      <DiscordAvatarImg
        class={props.class}
        discordId={ctx()?.player?.discord_id}
        discordAvatar={ctx()?.player?.discord_avatar}
        size={props.size}
      />
    );
  }
  export function Await(props: {
    children: (props: {
      signup: CupSignup;
      player: undefined | Player;
      team: undefined | Team;
    }) => JSX.Element;
  }) {
    const ctx = useContext(SignupContext);
    return <ctx.Await>{props.children}</ctx.Await>;
  }
  export function use() {
    return useContext(SignupContext);
  }
}

export { SignupProvider };

export function SignupName(props: { signup: CupSignup | string | undefined | null }) {
  const name = useAsync(async () => {
    const ps = props.signup;
    if (ps === undefined || ps === null) return undefined;
    const signup = typeof ps === "string" ? await api.cups.signupsByIdOnly.byId(ps) : ps;
    const [player, team] = await Promise.all([
      signup?.player_id != null ? api.players.byId(signup?.player_id) : undefined,
      signup?.team_id != null ? api.teams.byId(signup?.team_id) : undefined,
    ]);
    return team?.name ?? player?.discord_username;
  });
  return name;
}

export function SignupImg(props: { signup: CupSignup | string | undefined | null }) {}

export function SignupLoader(props: {
  class?: string;
  signup: CupSignup | string | undefined | null;
  showSeed?: boolean;
  showCheckin?: boolean;
  tabbable?: boolean;
  align?: "left" | "right";
  nameOnly?: boolean;
  size?: Size;
  render?: (props: CupSignupProps) => JSX.Element;
}) {
  const get = useAsync(async () => {
    const ps = props.signup;
    if (ps === undefined || ps === null) return undefined;
    const signup = typeof ps === "string" ? await api.cups.signupsByIdOnly.byId(ps) : ps;
    const [player, team] = await Promise.all([
      signup?.player_id != null ? api.players.byId(signup?.player_id) : undefined,
      signup?.team_id != null ? api.teams.byId(signup?.team_id) : undefined,
    ]);
    return {
      signup,
      team,
      player,
      showSeed: props.showSeed,
      showCheckin: props.showCheckin,
      nameOnly: props.nameOnly,
      size: props.size,
      tabbable: props.tabbable,
      class: props.class,
      align: props.align,
    };
  });
  return (
    <get.Await>
      {(p) => {
        if (p.signup === undefined) return undefined;
        if (props.render !== undefined) return props.render(p);
        return <SignupView {...p} />;
      }}
    </get.Await>
  );
}
