import { CupStage } from "@/api";
import { EmbedView } from "@/components/Media";
import PlayerLink from "@/components/Player/Link";
import useAuth from "@/context/Auth";
import { useMobile } from "@/context/Size";
import { Link, RouteName, useRoute } from "@/router";
import { useCounter } from "@/shared/async/Counter";
import { parseDate } from "@/shared/date";
import Icon from "@/shared/ui/Icon";
import { UnsafeMarkdownContent } from "@/shared/ui/Markdown/Content";
import Menu from "@/shared/ui/Menu";
import Pages from "@/shared/ui/Paginate";
import RefreshButton from "@/shared/ui/RefreshButton";
import { format, formatDuration, intervalToDuration } from "date-fns";
import { createMemo, For, JSX, Show } from "solid-js";
import { useCup } from "./Context";
import SignupControls from "./SignupControls";
import StageTitle from "./Stage/Title";
import theme from "./theme.module.scss";

export default function Cup(p: { children: JSX.Element }) {
  const auth = useAuth();
  const c = useCup();
  const counter = useCounter();
  const route = useRoute();
  const mobile = useMobile();
  return (
    <Menu.Layout
      class={theme.cupLayout}
      contentClass={theme.cupContent}
      menu={() => (
        <>
          <Menu>
            <Menu.Link to="cups.cup" params={{ id: c.cupId }}>
              Overview
            </Menu.Link>
            <Menu.Link to="cups.cup.matches" params={{ id: c.cupId }}>
              Matches
            </Menu.Link>
            <Show when={c.signupState()?.tag === "signedUp"}>
              <Menu.SubMenu>
                <Menu.Link to="cups.cup.matches.own" params={{ id: c.cupId }}>
                  Your Matches
                </Menu.Link>
              </Menu.SubMenu>
            </Show>
            <Menu.Title>Stages</Menu.Title>
            {c.stages().array.map((s) => (
              <>
                <Menu.Link
                  to="cups.cup.stage"
                  params={{ id: c.cupId, stageNo: s.stage_no + "" }}
                >
                  <StageTitle stage={s} />
                </Menu.Link>
                <Menu.SubMenu>
                  <Menu.Link
                    to="cups.cup.stage.placements"
                    params={{ id: c.cupId, stageNo: s.stage_no + "" }}
                  >
                    Placements
                  </Menu.Link>
                </Menu.SubMenu>
              </>
            ))}
          </Menu>
          <Show when={auth().admin}>
            <Menu>
              <Menu.Title>Administration</Menu.Title>
              <Menu.Link to="cups.cup.manage" params={{ id: c.cupId }}>
                Manage
              </Menu.Link>
              <Menu.SubMenu>
                {c.stages().array.map((_, stageIx) => (
                  <Menu.Link
                    to="cups.cup.manage.stage"
                    params={{ id: c.cupId, stageNo: stageIx + "" }}
                  >
                    Stage {stageIx + 1}
                  </Menu.Link>
                ))}
              </Menu.SubMenu>
              <Menu.Link to="cups.cup.edit" params={{ id: c.cupId }}>
                Edit
              </Menu.Link>
            </Menu>
          </Show>
        </>
      )}
    >
      <h1 class={theme.title}>
        <span class={theme.left}>
          <Link to="cups.cup" params={{ id: c.cupId }}>
            {c.cup()?.title ?? "Unknown"}
          </Link>
          <span class="subtitle">
            &nbsp; &mdash; &nbsp;
            {c.gameMode()?.game?.name} / {c.gameMode()?.mode?.name}
          </span>
        </span>
        <RefreshButton
          update={() => {
            counter.signal.increment();
          }}
          isLoading={c.isLoading}
        />
      </h1>
      {p.children}
    </Menu.Layout>
  );
}

export function formatCheckinDuration(stage: CupStage): string | null {
  const { checkintime: _checkintime, starttime: _starttime } = stage;
  const checkintime = parseDate(_checkintime);
  const starttime = parseDate(_starttime);
  if (!(checkintime instanceof Date && starttime instanceof Date)) return null;
  const duration = intervalToDuration({
    start: starttime,
    end: checkintime,
  });
  return formatDuration(duration, { zero: false });
}

export function CupHome() {
  const c = useCup();

  const first = createMemo(() => c.stages()?.array?.[0]);

  const checkinDuration = createMemo(() => {
    const stage = first();
    if (stage === undefined) return null;
    return formatCheckinDuration(stage);
  });

  return (
    <div class={theme.cupHome}>
      <article class={theme.cupSignupControls}>
        <div>
          <Show when={first()?.starttime}>
            {(starttime) => (
              <div>
                <Icon type="calendar" />
                &nbsp;
                {format(parseDate(starttime) ?? 0, "PPp")}
              </div>
            )}
          </Show>
          <Show when={checkinDuration()}>
            {(duration) => (
              <div>
                <Icon type="clock" />
                &nbsp;
                {duration} checkin time
              </div>
            )}
          </Show>
          <hr />
          <br />
        </div>
        <div>
          <SignupControls />
        </div>
      </article>

      <c.cup.Await>
        {(cup) => (
          <UnsafeMarkdownContent
            class={theme.description}
            html={cup.description_html}
          />
        )}
      </c.cup.Await>

      <div>
        <h3>
          {c.gameMode()?.mode?.entrant_type === "player" ? "Players" : "Teams"}
          <Show when={c.signups()?.array}>
            {(arr) => (arr.length > 0 ? ` (${arr.length})` : undefined)}
          </Show>
        </h3>
        <div class={theme.players}>
          <For each={c.signups()?.array}>
            {(t) => <PlayerLink size="24" playerId={t.player_id} />}
          </For>
        </div>
      </div>

      <Show when={c.mediaLinks()?.[0]?.total_count! > 0}>
        <div>
          <h3>Media</h3>
          <Pages
            source={() => c.mediaLinks()}
            render={(item) => <EmbedView {...item} />}
            renderItems={(items) => (
              <figure>
                <div class="flex-row">{items.children}</div>
              </figure>
            )}
          />
        </div>
      </Show>
    </div>
  );
}
