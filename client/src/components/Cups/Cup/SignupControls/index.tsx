import { api } from "@/api";
import { loginRedirBack } from "@/api/oauth2";
import useAuth from "@/context/Auth";
import ConfirmButton from "@/shared//ui/ConfirmButton";
import { usePromise } from "@/shared/async/Awaitable";
import Await from "@/shared/async/Await";
import { CaseOf, EnumFromTo } from "@/shared/Case";
import * as Dropdown from "@/shared/ui/Dropdown";
import FormatResponseError from "@/shared/ui/ResponseError";
import { Countdown } from "@/shared/ui/Time";
import toast from "@/shared/ui/Toast";
import log from "loglevel";
import { createMemo, createSignal, JSX, Show } from "solid-js";
import { useCup } from "../Context";
import { CannotSignupReason, SignupState } from "../Context/signupState";
import theme from './theme.module.scss';
import { empty } from "fp-ts/lib/ReadonlyArray";
import { parseIDT } from "@/shared/ui/Form2/Field/DateTime";
import Icon from "@/shared/ui/Icon";
import { Loading } from "@/shared/ui/Loading";
import { clamp } from "lodash";

export default function SignupControls(): JSX.Element {
  const ctx = useCup();
  // wrap some async effect so that it causes a signupState update on completion
  function handler<T, Args extends any[]>(
    okMessage: string,
    errorMessage: string,
    up: (...args: Args) => Promise<T>
  ): (...args: Args) => Promise<void> {
    return (...args) =>
      up(...args)
        .then(
          () => {
            toast.success(okMessage);
          },
          (err) => {
            toast.failure(() => (
              <>
                <h1>{errorMessage}</h1>
                <FormatResponseError error={err} />
              </>
            ));
          }
        )
        .finally(() => {
          setTimeout(() => {
            ctx.signupState.update();
          }, 500);
        });
  }
  const onCheckin = handler("Checked in!", "Could not check in", () =>
    api.cupsCupSignupCheckin(ctx.cupId)
  );
  const onCheckout = handler("Checked out!", "Could not check out", () =>
    api.cupsCupSignupCheckout(ctx.cupId)
  );
  const onLeaveCup = handler("Left cup", "Could not leave cup", () =>
    api.cupsCupSignupLeave(ctx.cupId)
  );
  const onSignup = handler("Signed up!", "Could not sign up", async (teamId?: string) => {
    const state = ctx.signupState();
    if (state.tag === "loading") return;
    if (state.tag === "signedUp") {
      log.error("tried to solo signup but already signed up");
      return;
    }
    if (state.tag === "cannotSignup") {
      log.error("tried to solo signup but cannot sign up");
      return;
    }
    if (state.tag === "canSignup") {
      await (state.requiresTeam
        ? api.cupsCupSignupTeam(ctx.cupId, teamId)
        : api.cupsCupSignupSolo(ctx.cupId, teamId));
      if (state.mustCheckinOnSignup) await onCheckin();
    }
  });
  const onForfeitCup = handler("Forfeited", "Could not forfeit", () =>
    api.cupsCupSignupForfeit(ctx.cupId)
  );
  return (
    <CaseOf
      data={ctx.signupState()}
      key="tag"
      fallback={() => "Signup not available"}
      children={{
        canSignup: (s) => (
          <CanSignup
            mustCheckinOnSignup={s.mustCheckinOnSignup}
            requiresTeam={s.requiresTeam}
            onSignupTeam={onSignup}
            onSignup={onSignup}
          />
        ),
        cannotSignup: (s) => <CannotSignup reason={s.reason} />,
        signedUp: (s) => (
          <SignedUp
            state={s}
            onCheckin={onCheckin}
            onCheckout={onCheckout}
            onLeaveCup={onLeaveCup}
            onForfeitCup={onForfeitCup}
          />
        ),
      }}
    />
  );
}

function CanSignup(props: {
  requiresTeam: boolean;
  mustCheckinOnSignup: boolean;
  onSignup: (teamId?: string) => void;
  onSignupTeam: (teamId: string) => void;
}): JSX.Element {
  const getAuth = useAuth();

  return (
    <div class="grid small" style="align-items:center">
      <span>Signup available</span>
      <Show
        when={getAuth().loggedIn === true}
        fallback={() => <button onClick={() => loginRedirBack()}>Login</button>}
      >
        <Dropdown.default
          isRight
          style="margin:0"
          kind="fetch"
          options={async () => {
            const auth = getAuth();
            const requiresTeam = props.requiresTeam;
            if (auth.loggedIn !== true) return undefined;
            const {
              login: { id: authId },
            } = auth;
            const roles = (await api.players.teamRoles.byParentId(authId)).array;
            const teams = await api.teams.byIds(roles.map((r) => r.team_id));
            const options: Dropdown.Options<string> = [];
            if (!requiresTeam)
              options.push({
                title: () => (
                  <>
                    Signup as <b>{auth.login.discord_username}</b>
                  </>
                ),
                isActive: false,
                onClick: () => props.onSignup(),
              });
            for (const team of teams) {
              options.push({
                title: () => (
                  <>
                    Signup under <b>{team.name}</b>
                  </>
                ),
                isActive: false,
                onClick: requiresTeam
                  ? () => props.onSignupTeam(team.id)
                  : () => props.onSignup(team.id),
              });
            }
            return [Dropdown.unpaged, options];
          }}
        >
          Join
        </Dropdown.default>
      </Show>
    </div>
  );
}

enum ForfeitButtonState {
  Unclicked,
  Clicked,
  SudokuSolved,
}


function SignedUp(props: {
  state: SignupState & { tag: "signedUp" };
  onCheckout: () => void;
  onCheckin: () => void;
  onLeaveCup: () => void;
  onForfeitCup: () => void;
}): JSX.Element {
  function isLeaveAvailable(k: SignupState & { tag: "signedUp" }) {
    return (
      (k.checkin === "checkedIn" && !k.begun) ||
      k.checkin === "canCheckin" ||
      k.checkin === "cannotCheckinTooEarly"
    );
  }

  function SecondaryForfeitButton() {
    const sudokuImport = usePromise(() => import("sudoku-gen"));
    return (
      <div>
        <Await data={sudokuImport}>
          {Sudoku => {
            const initialState = Sudoku.getSudoku("medium")
            const [sudokuState, setSudokuState] = createSignal(initialState);
            const inputs: HTMLInputElement[] = Array(81);

            function sudokuValue(i: number) {
              const ch = sudokuState().puzzle[i];
              if (ch === '-') return null;
              return Number.parseInt(ch);
            }

            function setSudokuValue(i: number, value: number | null) {
              if (!(value === null || Number.isSafeInteger(value) && value >= 1 && value <= 9)) {
                log.error("setSudokuValue", value);
                value = null;
              }
              setSudokuState(s => ({
                ...s,
                puzzle: s.puzzle.substring(0, i) + (value === null ? "-" : ("" + value)) + s.puzzle.substring(i + 1)
              }))
            }

            function isSolved() {
              return sudokuState().solution === sudokuState().puzzle;
            }

            return (
              <div class="box">
                <div class={theme.sudokuGrid}>
                  <EnumFromTo from={0} to={9 * 9}>
                    {i => {
                      const x = i % 9;
                      const y = Math.floor(i / 9);
                      return <input
                        ref={el => {
                          inputs[i] = el;
                        }}
                        class={theme.sudokuValue}
                        data-left-border={(x > 0 && x % 3 === 0) ? 2 : 0}
                        data-right-border={x !== 8 ? (((x + 1) % 3 === 0) ? 2 : 1) : 0}
                        data-top-border={(y > 0 && y % 3 === 0) ? 2 : 0}
                        data-bottom-border={y !== 8 ? (((y + 1) % 3 === 0) ? 2 : 1) : 0}
                        value={sudokuValue(i) ?? ""}
                        disabled={initialState.puzzle[i] !== '-'}
                        onKeyDown={(ev) => {
                          let x1 = 0, y1 = 0;
                          switch (ev.code) {
                            case "0": ev.preventDefault();
                            case "ArrowDown": y1 += 1; break;
                            case "ArrowUp": y1 -= 1; break;
                            case "ArrowLeft": x1 -= 1; break;
                            case "ArrowRight": x1 += 1; break;
                            case "Backspace":
                            case "Delete":
                              setSudokuValue(i, null);
                              ev.preventDefault();
                              return;
                          }
                          if (x1 !== 0 || y1 !== 0) {
                            for (let offset = 1; offset < 8; offset++) {
                              const j = (y + y1 * offset) * 9 + (x + x1 * offset);
                              const el = inputs[j];
                              if (el === undefined) break;
                              if (el.disabled) continue;
                              if (el !== undefined) el.focus();
                              ev.preventDefault();
                              break;
                            }
                          }
                        }}
                        onInput={(ev) => {
                          let next = Number.parseInt(ev.currentTarget.value);
                          const valid = Number.isFinite(next) && Number.isSafeInteger(next) && next >= 1;
                          if (valid && (next > 9)) {
                            next = next % 10;
                          }
                          setSudokuValue(i, clamp(next, 1, 9));
                        }}
                      />
                    }}
                  </EnumFromTo>
                </div>
                <div class={theme.sudokuControls}>
                  <p>To forfeit, solve the sudoku puzzle.</p>
                  <progress
                    value={81 - (sudokuState().puzzle.match(/-/g) ?? []).length}
                    max={81}
                  />
                  <button
                    class="contrast"
                    disabled={!isSolved()}
                  >
                    Forfeit
                  </button>
                </div>
              </div>
            );
          }}
        </Await>
      </div>
    );
  }

  function ForfeitButton() {
    const [state, setState] = createSignal(ForfeitButtonState.Unclicked);
    function InitialForfeitButton() {
      return (
        <ConfirmButton
          class="is-danger"
          onConfirm={() => {
            setState(ForfeitButtonState.Clicked);
          }}
          confirmingClickDelay={1000}
          confirming={
            <div
              class="small"
              style="max-width:160px;white-space:break-spaces;overflow:hidden;text-align:left"
            >
              <p>Most players regret clicking this.</p>
              <b>Continue?</b>
            </div>
          }
        >
          Forfeit the cup
        </ConfirmButton>
      );
    }

    return (
      <div>
        {state() === ForfeitButtonState.Unclicked ?
          <InitialForfeitButton /> :
          <SecondaryForfeitButton />
        }
      </div>
    );
  }

  return (
    <div class="flex-row" style="justify-content:space-between">
      <CaseOf data={props.state} key="checkin">
        {{
          checkedIn: (s) => () =>
            s.begun === true ? (
              <>
                <label>The cup has started. Good luck!</label>
                <ForfeitButton />
              </>
            ) : (
              <>
                <label>Checked in!</label>
                <input type="checkbox" checked={true} onChange={props.onCheckout}>
                  Check-out
                </input>
              </>
            ),
          canCheckin: () => (
            <>
              <label>Check-in</label>
              <input type="checkbox" checked={false} onChange={props.onCheckin} />
            </>
          ),
          cannotCheckinTooEarly: (s) => (
            <label>
              Check-in available <Countdown tick="s" target={s.checkintime} />
            </label>
          ),
          cannotCheckinTooLate: () => "Missed checkin",
        }}
      </CaseOf>
      <Show when={isLeaveAvailable(props.state)}>
        <ConfirmButton onConfirm={props.onLeaveCup} confirming="Really leave this cup?">
          Leave cup
        </ConfirmButton>
      </Show>
    </div>
  );
}

function CannotSignup(props: { reason: CannotSignupReason }): JSX.Element {
  return createMemo(() => cannotSignupMessage[props.reason]);
}

const cannotSignupMessage: Record<CannotSignupReason, string> = {
  closed: "Cannot signup",
  missed: "Missed signup period",
  finished: "Cup is finished",
};
