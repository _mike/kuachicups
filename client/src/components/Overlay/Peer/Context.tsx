import { createContext, createMemo, JSX, onCleanup, untrack, useContext } from "solid-js";
import { createStore } from "solid-js/store";
import { useOverlaySecret } from "../Secret";
import Peer from "./Peer";
import { MessageType, PeerType } from "./types";

export interface IPeerContext<PT extends PeerType = any> {
  readonly peer: Peer<PT>;
  readonly status: Peer.Status;
  readonly send: this["peer"]["send"];
}

const PeerContext = createContext<IPeerContext<any>>();

export function PeerProvider<PT extends PeerType = PeerType>(props: {
  type: PT;
  onData(data: MessageType[PT]["recv"], self: Peer<PT>): void;
  onConnect?: (self: Peer<PT>) => void;
  children: JSX.Element;
}) {
  const os = useOverlaySecret();
  const [status, setStatus] = createStore<Peer.Status>({
    died: false,
    conn: undefined,
    peer: false,
    value: 0,
  });

  const peer = createMemo<Peer<PT>>(() => {
    const ptype = props.type;
    const secret = os.secret();
    const p = untrack(
      () =>
        new Peer(ptype, secret, {
          onData: props.onData,
          onConnect: props.onConnect,
          onStatusUpdate: setStatus,
        })
    );
    onCleanup(() => p.dispose());
    return p;
  });

  const interval = setInterval(() => {
    const status = peer()?.status();
    if (status !== undefined) {
      setStatus(status);
    } else {
      setStatus({ conn: undefined, peer: false, value: 0 });
    }
  }, 1000);

  onCleanup(() => {
    log.debug("clearing old peer interval");
    clearInterval(interval);
  });

  return (
    <PeerContext.Provider
      value={{
        get peer() {
          return peer();
        },
        send(msg) {
          return peer().send(msg);
        },
        status,
      }}
    >
      {props.children}
    </PeerContext.Provider>
  );
}

export function usePeer<PT extends PeerType = any>(): IPeerContext<PT> {
  return useContext(PeerContext)!;
}
