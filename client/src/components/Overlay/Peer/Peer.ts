import * as P from "peerjs";
import { onCleanup } from "solid-js";
import { MessageType, otherPeer, PeerType } from "./types";

class Peer<P extends PeerType = PeerType> {
  constructor(
    public readonly type: P,
    public readonly secret: string,
    public readonly handlers: {
      readonly onData: (data: MessageType[P]["recv"], self: Peer<P>) => void;
      readonly onStatusUpdate: (status: Peer.Status) => void;
      readonly onConnect?: (self: Peer<P>) => void;
    }
  ) {
    [this.selfId, this.destId] = Peer.createId(type, secret);
    this.peer = this.connect()!;
  }

  public connect() {
    const peer = new P.Peer(this.selfId, { debug: 3 });
    log.debug("Peer::connect", this);

    const setupConn = (conn: P.DataConnection) =>
      conn
        .on("data", (data) => this.onMessage(data as any))
        .on("error", () => this.updateStatus())
        .on("close", () => this.updateStatus())
        .on("open", () => this.onConnect())
        .on("iceStateChanged", () => this.updateStatus());

    peer.on("connection", (conn) => {
      if (conn.peer !== this.destId) return;
      log.debug("connection", conn);
      this.conn = setupConn(conn);
      this.updateStatus();
    });

    peer.on("disconnected", () => this.updateStatus());
    peer.on("close", () => this.updateStatus());
    peer.on("error", () => this.updateStatus());
    peer.on("open", () => {
      this.conn = setupConn(peer.connect(this.destId, { serialization: "json" }));
      this.updateStatus();
    });

    this.peer = peer;
    return peer;
  }

  public readonly selfId: string;
  public readonly destId: string;
  public died: boolean = false;
  private peer: P.Peer;
  private conn: P.DataConnection | undefined;

  private onConnect() {
    this.updateStatus();
    this.handlers.onConnect?.(this);
    for (const msg of this.sendQueue.splice(0)) {
      this.send(msg);
    }
  }

  private onMessage(data: MessageType[P]["recv"]) {
    this.handlers.onData(data, this);
    this.listeners?.[data.tag]?.(data, this);
  }

  public dispose() {
    if (this.died) return;
    log.debug("Peer::dispose", this);
    this.died = true;
    this.conn?.close?.();
    this.peer.destroy();
    this.handlers.onStatusUpdate(this.status());
  }

  public status(): Peer.Status {
    return {
      died: this.died,
      peer: this.peer.open,
      conn: this.conn?.open,
      value: (this.peer.open ? 1 : 0) + (this.conn === undefined ? 0 : this.conn.open ? 2 : 1),
    };
  }

  public updateStatus() {
    this.handlers.onStatusUpdate(this.status());
  }

  public send(data: MessageType[P]["send"], canQueue: boolean = false) {
    if (this.conn?.open === true) {
      this.conn.send(data);
    } else if (canQueue) {
      this.sendQueue.push(data);
    }
  }

  public sendQueue: MessageType[P]["send"][] = [];

  public partner(): PeerType {
    return otherPeer[this.type];
  }

  private listeners: Record<string, (msg: MessageType[P]["recv"], self: this) => void> = {};
  public listen<T extends MessageType[P]["recv"]["tag"]>(
    tag: T,
    fn: (msg: MessageType[P]["recv"] & { tag: T }) => void
  ): () => void {
    this.listeners[tag] = fn as any;
    return () => {
      delete this.listeners[tag];
    };
  }

  public listenWithKey<T extends MessageType[P]["recv"]["tag"]>(
    tag: T,
    key: string,
    fn: (msg: MessageType[P]["recv"] & { tag: T }) => void
  ): () => void {
    const listenerKey = `${tag}.${key}`;
    this.listeners[listenerKey] = fn as any;
    return () => {
      delete this.listeners[listenerKey];
    };
  }

  public createListener<T extends MessageType[P]["recv"]["tag"]>(
    tag: T,
    fn: (msg: MessageType[P]["recv"] & { tag: T }) => void
  ) {
    const dispose = this.listen(tag, fn);
    onCleanup(dispose);
  }

  public createListenerWithKey<T extends MessageType[P]["recv"]["tag"]>(
    tag: T,
    key: string,
    fn: (msg: MessageType[P]["recv"] & { tag: T }) => void
  ) {
    const dispose = this.listenWithKey(tag, key, fn);
    onCleanup(dispose);
  }

  static createId(whom: PeerType, secret: string = ""): [selfId: string, destId: string] {
    const prefix = secret.length === 0 ? "" : secret + "-";
    return [prefix + whom, prefix + otherPeer[whom]];
  }
}

namespace Peer {
  export type Type = PeerType;
  export interface Status {
    died: boolean;
    peer: boolean;
    conn: boolean | undefined;
    value: number;
  }
}

export default Peer;
