import { RemoteCmd } from "../Remote/command";
import * as P from "peerjs";

export type PeerType = "remote" | "view" | "view-cfg" | "cfg";

export interface ActiveSceneMsg {
  tag: "scene-info";
  key: string;
  scene: [key: string, index: number];
}

export type ViewMsg = ActiveSceneMsg | SetConfig;

export interface SetConfig {
  tag: "set-config";
  config: string;
}

export interface GotConfig {
  tag: "got-config";
  config: string;
}

export interface MessageType {
  remote: {
    send: RemoteCmd;
    recv: ViewMsg;
  };
  view: {
    send: ViewMsg;
    recv: RemoteCmd;
  };
  cfg: {
    send: SetConfig;
    recv: GotConfig;
  };
  "view-cfg": {
    send: GotConfig;
    recv: SetConfig;
  };
}

export interface Peer<PT extends PeerType = PeerType> {
  prefix(): string;
  setPrefix(pfix: string): void;
  send(msg: MessageType[PT]["send"]): void;
  listen(cb: (msg: MessageType[PT]["recv"]) => void): void;
  connected(): boolean;
  status(): number;
  cleanup(cb?: () => void): void;
  reconnect(secret?: string, cb?: () => void): void;
  state: Conn;
  type: PT;
}

export interface Conn {
  rconn: undefined | P.DataConnection;
  sconn: undefined | P.DataConnection;
  peer: undefined | P.Peer;
}

export const otherPeer: Record<PeerType, PeerType> = {
  remote: "view",
  view: "remote",
  cfg: "view-cfg",
  "view-cfg": "cfg",
};
