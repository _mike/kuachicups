import Icon from "@/shared/ui/Icon";
import { createMemo, Show } from "solid-js";
import { useOverlayForm } from "../Config/Form/Context";
import { usePeer } from "./Context";

enum Status {
  None,
  Ok,
  Error,
}

namespace Status {
  export function from(b: Status | boolean | undefined): Status {
    if (b === undefined) return Status.None;
    if (typeof b === "boolean") return b ? Status.Ok : Status.None;
    return b;
  }
}

export function ConnectProgressBar(props: { otherConfigStr?: string }) {
  const peer = usePeer()!;
  const f = useOverlayForm();
  function configStrStatus() {
    const { otherConfigStr } = props;
    const ownConfigStr = f?.configStr();
    if (otherConfigStr !== undefined) {
      return ownConfigStr === otherConfigStr ? Status.Ok : Status.Error;
    }
    return Status.None;
  }
  return (
    <div style="min-width:200px">
      <div class="flex-row" style="font-size:14px;margin-bottom:2px;">
        {(["peer", "conn"] as const).map((s) => (
          <Dot label={s} status={peer.status?.[s] ? Status.Ok : Status.None} />
        ))}
        <Dot label="config" status={configStrStatus()} />
      </div>
      <progress value={peer.status?.value ?? 0} max={3} />
    </div>
  );
}

function Dot(props: { label: string; status: Status }) {
  return (
    <span style={{ flex: 0.5, color: statusColour(props.status) }}>
      <Icon type={statusIcon(props.status)} /> {props.label}
    </span>
  );
}

function statusColour(status: Status) {
  switch (status) {
    case Status.None:
      return "var(--muted-color)";
    case Status.Ok:
      return "var(--primary)";
    case Status.Error:
      return "var(--button)";
  }
}

function statusIcon(status: Status) {
  switch (status) {
    case Status.None:
      return "radio-button-off";
    case Status.Ok:
      return "radio-button-on";
    case Status.Error:
      return "close-circle-outline";
  }
}
