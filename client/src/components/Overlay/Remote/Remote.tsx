import { api } from "@/api";
import { SignupLoader } from "@/components/Cups/Cup/CupSignup";
import { useAsync } from "@/shared/async/Awaitable";
import { CaseOf } from "@/shared/Case";
import { createField, MultilineTextField, StrictNumberField, TextField } from "@/shared/ui/Form2";
import Icon from "@/shared/ui/Icon";
import { NiceTabs } from "@/shared/ui/Tabs";
import "@/theme/globals.scss";
import { createComputed, createEffect, createMemo, For, untrack } from "solid-js";
import { createStore } from "solid-js/store";
import { decodeConfig } from "../Config/config";
import { lines } from "../Config/Form";
import { ArrayFld, useOverlayForm } from "../Config/Form/Context";
import { ConnectProgressBar, usePeer } from "../Peer";
import { OverlaySecretInput } from "../Secret";
import { MatchOverlayMode as MM } from "../View/Cup/Match";
import * as OC from "../View/types";
import ScenesControl from "./SceneControl";
import theme from "./theme.module.scss";

export function OverlayRemoteStandalone() {
  const p = usePeer<"remote">()!;
  const f = useOverlayForm()!;
  const [activeScenes, setActiveScenes] = createStore<Record<string, string>>({});
  p.peer.createListener("set-config", (msg) => {
    decodeConfig(msg.config).then(f.setStore);
  });
  p.peer.createListener("scene-info", (msg) => {
    setActiveScenes(msg.key, msg.scene[0]);
  });
  return (
    <div class={theme.remote}>
      <OverlayRemoteControls activeScenes={activeScenes} />
    </div>
  );
}

export function OverlayRemoteHelp() {
  return <div class="container-fluid" style="padding-top:var(--spacing)"></div>;
}

export function OverlayRemoteControls(props: { activeScenes: Record<string, string> }) {
  const f = useOverlayForm()!;

  // All overlay objects by key
  const cmds = createMemo(() => {
    const map: Record<string, [OC.Overlay, { cupId?: string; stageId?: string }, number]> = {};
    const order: string[] = [];
    OC.forOverlay_(f.store.cmd, (c, ctx) => {
      if (!implementedTags[c.tag as keyof typeof implementedTags]) return;
      if (typeof c.key !== "string") return;
      map[c.key!] = [c, ctx, order.length];
      order.push(c.key);
    });
    return { map, order };
  });

  let setTab: (tab: undefined | number) => void = () => {};

  function jumpToTab(key: string) {
    const cmd = cmds().map[key];
    if (cmd !== undefined) {
      setTab(1 + cmd[2]);
    }
  }

  return (
    <>
      <NiceTabs
        class={theme.remoteControlsTabs}
        childClass={theme.remoteButtons}
        display="tabs"
        tabRef={(_tab, _setTab) => (setTab = _setTab)}
        defaultTab={0}
        tabs={[
          { data: "CONFIG", title: "Remote Settings" },
          ...cmds().order.map((key) => ({ data: key, title: key })),
        ]}
      >
        {(tab) => {
          const { data: key } = tab;
          if (key === "CONFIG") return <OverlayRemoteConfig />;
          const [cmd] = cmds().map[key];
          return (
            <CaseOf data={cmd} key="tag">
              {{
                chat: (p) => <ChatControl {...p} />,
                "cup-match": (p) => <CupMatchGameOverlayControl {...p} />,
                "cup-bracket": (p) => <CupBracketControl {...p} />,
                "cup-backdrop": (p) => <CupBackdropControl {...p} />,
              }}
            </CaseOf>
          );
        }}
      </NiceTabs>
      <ScenesControl activeScenes={props.activeScenes} setTab={jumpToTab} />
    </>
  );
}

const implementedTags = {
  img: true,
  "cup-match": true,
  "cup-bracket": true,
  "cup-backdrop": true,
  chat: true,
} as const;

function OverlayRemoteConfig() {
  const f = useOverlayForm();
  return (
    <fieldset>
      <label>
        Secret key
        <OverlaySecretInput />
      </label>
      <br />
      <ConnectProgressBar otherConfigStr={f.configStr()} />
      <br />
      <p>
        If you use this overlay, consider making a donation to{" "}
        <a href="https://paypal.me/feetwind">help fund its development</a>.
      </p>
    </fieldset>
  );
}

function CupMatchGameOverlayControl(props: OC.CupMatch) {
  const { peer } = usePeer<"remote">()!;
  const match = useAsync(() => api.cupsMatchResult2P(props.matchId));
  let lowInput: HTMLInputElement | undefined;
  let highInput: HTMLInputElement | undefined;
  function updateScores() {
    const low = lowInput?.valueAsNumber;
    const high = highInput?.valueAsNumber;
    if (
      typeof low === "number" &&
      typeof high === "number" &&
      Number.isSafeInteger(low) &&
      Number.isSafeInteger(high)
    ) {
      peer.send({ tag: "match-set-score", key: props.key!, high, low });
    }
  }
  return (
    <div>
      <aside>
        <nav>
          <ul>
            {(
              [
                MM.Repr.BeforeGames,
                MM.Repr.Pickbans,
                MM.Repr.BetweenGames,
                MM.Repr.AfterGames,
                MM.Repr.IngamePlayer,
                MM.Repr.IngameSpectator,
              ] as const
            ).map((mode) => (
              <li>
                <a
                  href="#"
                  onClick={() => {
                    peer.send({
                      tag: "match-set-view-mode",
                      key: props.key!,
                      mode,
                    });
                  }}
                >
                  <Icon type="arrow-round-forward" />
                  <span>{MM.label(mode)}</span>
                </a>
              </li>
            ))}
          </ul>
        </nav>
      </aside>
      <br />
      <fieldset>
        <div class={theme.scores}>
          <SignupLoader signup={match()?.high_id ?? undefined} />
          <input ref={highInput} class="input" type="number" value={0} />
          <SignupLoader signup={match()?.low_id ?? undefined} />
          <input ref={lowInput} class="input" type="number" value={0} />
        </div>
        <button class="button is-primary is-fullwidth" onClick={updateScores}>
          Update overlay scores
        </button>
      </fieldset>
    </div>
  );
}

function CupBracketControl(props: OC.CupBracket) {
  const p = usePeer<"remote">();
  return (
    <button
      class="button"
      onClick={() => {
        if (props.key !== undefined) {
          p.send({ tag: "cup-bracket-refresh", key: props.key });
        }
      }}
    >
      Refresh
    </button>
  );
}

function CupBackdropControl(props: OC.CupBackdrop) {
  const { peer } = usePeer<"remote">();
  const [store, setStore] = createStore({
    iframes: props.iframes.map((value) => ({ value })),
  });
  const f = createField(store, setStore);
  function updateBackdrop() {
    peer.send({
      tag: "set-cup-backdrop-iframes",
      key: props.key!,
      iframes: store.iframes.map((i) => i.value),
    });
  }
  return (
    <>
      <fieldset>
        <f.Fld
          optic={f.optic.prop("iframes")}
          label="Webcam URIs"
          description="Each line shall be interpretted as its own URI"
        >
          {(value, onChange) => (
            <MultilineTextField
              innerProps={{
                style: "--spacing: 0.5rem; font-family: monospace; font-size: 14px",
              }}
              value={
                value()
                  .map((v) => v.value)
                  .join("\n") ?? "\n"
              }
              onChange={(v) => {
                onChange(lines(v).map((value) => ({ value })));
              }}
            />
          )}
        </f.Fld>
      </fieldset>
      <button onClick={updateBackdrop}>Update backdrop</button>
    </>
  );
}

function ChatControl(props: OC.Chat) {
  const { peer } = usePeer<"remote">();
  function clearChat() {
    peer.send({ tag: "chat-clear", key: props.key! });
  }
  return (
    <button class="button is-warning" onClick={clearChat}>
      Clear chat
    </button>
  );
}
