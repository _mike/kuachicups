import { MatchOverlayMode } from "../View/Cup/Match";

export type RemoteCmd =
  | RemoteSceneAdvCmd
  | RemoteSceneSetCmd
  | RemoteMatchSetScoreCmd
  | RemoteMatchSetModeCmd
  | RemoteChatClearCmd
  | RemoteCupBracketRefreshCmd
  | RemoteSetCupBackdropIframes;

export interface BaseRemoteCmd {
  key: string;
}

export interface RemoteSceneAdvCmd extends BaseRemoteCmd {
  tag: "scene-next" | "scene-prev" | "scene-pause" | "scene-play";
}

export interface RemoteSceneSetCmd extends BaseRemoteCmd {
  tag: "scene-set";
  sceneKey: string;
}

export interface RemoteMatchSetScoreCmd extends BaseRemoteCmd {
  tag: "match-set-score";
  high: number;
  low: number;
}

export interface RemoteMatchSetModeCmd extends BaseRemoteCmd {
  tag: "match-set-view-mode";
  mode: MatchOverlayMode.Repr;
}

export interface RemoteChatClearCmd extends BaseRemoteCmd {
  tag: "chat-clear";
}

export interface RemoteCupBracketRefreshCmd extends BaseRemoteCmd {
  tag: "cup-bracket-refresh";
}

export interface RemoteSetCupBackdropIframes extends BaseRemoteCmd {
  tag: "set-cup-backdrop-iframes";
  iframes: string[];
}
