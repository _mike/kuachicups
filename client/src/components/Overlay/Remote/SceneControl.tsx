import Menu from "@/shared/ui/Menu";
import { For, Show } from "solid-js";
import { FormConfig } from "../Config/config";
import { useOverlayForm } from "../Config/Form/Context";
import { usePeer } from "../Peer";
import * as OC from "../View/types";
import theme from "./theme.module.scss";

export default function ScenesControl(props: {
  activeScenes: Record<string, string>;
  setTab(key: string): void;
}) {
  const { peer } = usePeer<"remote">()!;
  const form = useOverlayForm()!;
  function jumpToScene(hierarchy: SceneInfo[], tabKey: string) {
    function go(i: number) {
      if (i >= 0) {
        // HACK this is to try ensure the scenes at the deepest level of the
        // hierarchy switch first so that we dont see the "inner" scene transitions
        setTimeout(() => {
          const { parentSceneKey, key } = hierarchy[i];
          peer!.send({ tag: "scene-set", key: parentSceneKey, sceneKey: key });
          go(i - 1);
        }, 300);
      }
    }
    go(hierarchy.length - 1);
    props.setTab(tabKey);
  }

  function mergeActive(outer: undefined | SceneActive, inner: boolean): SceneActive {
    return SceneActive.merge(outer, inner);
  }

  function isActive(hierarchy: SceneInfo[]): SceneActive {
    let active: SceneActive | undefined = undefined;
    for (const { parentSceneKey, key } of hierarchy) {
      active = mergeActive(active, props.activeScenes[parentSceneKey] === key);
    }
    return active ?? SceneActive.Inactive;
  }

  function SceneMenu(p: { scenes: Scenes[] }) {
    return (
      <For each={p.scenes}>
        {(s) => (
          <>
            <Menu.ItemLink
              class={theme.sceneLink}
              data-active={SceneActive.name(isActive(s.hierarchy))}
              onClick={() => jumpToScene(s.hierarchy, s.tabKey)}
            >
              {s.displayName}
            </Menu.ItemLink>
            <Show when={s.children.length > 0}>
              <Menu.SubMenu>
                <SceneMenu scenes={s.children} />
              </Menu.SubMenu>
            </Show>
          </>
        )}
      </For>
    );
  }

  return (
    <Menu class={theme.menu}>
      <Menu.Title>Scene Select</Menu.Title>
      <SceneMenu scenes={getScenes(form.store).scenes} />
    </Menu>
  );
}

interface ScenesRoot {
  scenes: Scenes[];
}

interface SceneInfo {
  parentSceneKey: string;
  key: string;
}

interface Scenes extends SceneInfo {
  hierarchy: SceneInfo[];
  displayName: string;
  tabKey: string;
  children: Scenes[];
}

enum SceneActive {
  Inactive,
  ActiveHidden,
  ActiveDisplayed,
}

namespace SceneActive {
  export function name(self: SceneActive) {
    return SceneActive[self];
  }

  export const mergeMap: Record<SceneActive, Record<0 | 1, SceneActive>> = {
    [SceneActive.Inactive]: {
      [0]: SceneActive.Inactive,
      [1]: SceneActive.ActiveHidden,
    },
    [SceneActive.ActiveDisplayed]: {
      [0]: SceneActive.Inactive,
      [1]: SceneActive.ActiveDisplayed,
    },
    [SceneActive.ActiveHidden]: {
      [0]: SceneActive.Inactive,
      [1]: SceneActive.ActiveHidden,
    },
  };

  export function merge(outer: SceneActive | undefined, inner: boolean): SceneActive {
    if (outer === undefined) return inner ? SceneActive.ActiveDisplayed : SceneActive.Inactive;
    return mergeMap[outer][inner ? 1 : 0];
  }
}

function getScenes(cfg: FormConfig): ScenesRoot {
  function go(cmds: OC.Overlay[], hierarchy: SceneInfo[]): Scenes[] {
    let r: Scenes[] = [];
    for (const cmd of cmds) {
      if (cmd.tag === "scenes") {
        if (cmd.key === undefined) continue;
        for (const scene of cmd.scenes) {
          if (scene.scene.key === undefined) continue;
          let tabKey = scene.scene.key;
          let displayName = tabKey;
          if (scene.scene.display !== undefined && scene.scene.display.key !== undefined) {
            displayName += "/" + scene.scene.display.key;
            tabKey = scene.scene.display.key;
          }
          const parentSceneKey = cmd.key;
          const key = scene.scene.key;
          if (parentSceneKey === undefined) continue;
          const hierarchy1 = [...hierarchy, { parentSceneKey, key }];
          r.push({
            key,
            parentSceneKey,
            hierarchy: hierarchy1,
            displayName,
            tabKey,
            children: go([scene.scene], hierarchy1),
          });
        }
        // } else if (cmd.tag === "cup-match") {
        //   if (cmd.key === undefined) continue;
        //   for (const t of [
        //     MM.Repr.BeforeGames,
        //     MM.Repr.BetweenGames,
        //     MM.Repr.AfterGames,
        //     MM.Repr.IngamePlayer,
        //     MM.Repr.IngameSpectator,
        //   ]) {
        //     const hierarchy1 = [...hierarchy, { parentSceneKey: cmd.key, key: MM.Repr[t] }];
        //     r.push({
        //       parentSceneKey: cmd.key,
        //       tabKey: cmd.key,
        //       displayName: MM.label(t),
        //       key: cmd.key,
        //       hierarchy: hierarchy1,
        //       children: [],
        //     });
        //   }
        //   r = [
        //     {
        //       parentSceneKey: cmd.key,
        //       tabKey: cmd.key,
        //       displayName: cmd.key,
        //       key: cmd.key,
        //       hierarchy,
        //       children: r,
        //     },
        //   ];
      } else if (cmd.display !== undefined) {
        r.push(...go([cmd.display], hierarchy));
      }
    }
    return r;
  }
  const scenes = go(cfg.cmd, []);
  return { scenes };
}
