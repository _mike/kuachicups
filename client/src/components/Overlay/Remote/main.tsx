import { Provider as AuthProvider } from "@/context/Auth";
import "@/injectLogLevel"; // @ import so tsfmt doesn't put it on the bottom
import { Provider as RouterProvider } from "@/router";
import "@/serviceWorker";
import { reconcile } from "solid-js/store";
import { render } from "solid-js/web";
import { decodeConfig } from "../Config/config";
import { OverlayFormContextProvider, useOverlayForm } from "../Config/Form/Context";
import { MessageType, PeerProvider } from "../Peer";
import { OverlaySecretProvider, useOverlaySecret } from "../Secret";
import { OverlayRemoteStandalone } from "./Remote";

log.setLevel(0);
log.debug("Starting kuachi.gg overlay");

function OverlayView() {
  const form = useOverlayForm()!;

  (window as any).form = form;

  async function onRemoteData(data: MessageType["remote"]["recv"]) {
    if (data.tag === "set-config") {
      const cfg = await decodeConfig(data.config);
      log.debug("got config", cfg);
      form.setStore(reconcile(cfg, { merge: false }));
    }
  }

  return (
    <PeerProvider type="remote" onData={onRemoteData}>
      <OverlayRemoteStandalone />
    </PeerProvider>
  );
}

render(
  () => (
    <AuthProvider>
      <RouterProvider>
        <OverlaySecretProvider>
          <OverlayFormContextProvider disableRouteCfg>
            <OverlayView />
          </OverlayFormContextProvider>
        </OverlaySecretProvider>
      </RouterProvider>
    </AuthProvider>
  ),
  document.getElementById("app")!
);
