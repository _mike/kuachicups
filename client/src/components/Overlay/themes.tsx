import { getWeek } from "date-fns";
import { createContext, createMemo, JSX, onCleanup, useContext } from "solid-js";
import logo_text_png from "@/resources/logo_text.png";
import surfshamb from "@/resources/surfshamb_logo_640.png";
import Now from "@/shared/ui/Time";
import { useOverlayForm } from "./Config/Form/Context";
import { createStore } from "solid-js/store";

export interface OverlayTheme {
  name: string;
  seriesName: undefined | string;
  seriesSubtitle: undefined | (() => JSX.Element);
  floatingLogos: string[];
  bottomMiddleLogos: string[];
  topLogos: string[];
  backgroundStyle: "subtle" | "none" | "dark";
  backgroundImages: string[];
}

const generic: OverlayTheme = {
  name: "Generic",
  seriesName: undefined,
  seriesSubtitle: () => <Now precision="s" />,
  bottomMiddleLogos: [logo_text_png],
  floatingLogos: [],
  topLogos: [logo_text_png],
  backgroundStyle: "subtle",
  backgroundImages: [],
};

export const themes = {
  aql5: aqlTheme(4, new Date("Mon, 17 Jun 2023 12:00:00 GMT"), false),
  aql5Shaftastic: aqlTheme(4, new Date("Mon, 17 Jun 2023 12:00:00 GMT"), true),
  aql4: aqlTheme(4, new Date("Mon, 17 Apr 2023 12:00:00 GMT"), false),
  aql4Shaftastic: aqlTheme(4, new Date("Mon, 17 Apr 2023 12:00:00 GMT"), true),
  aql3: aqlTheme(3, new Date("Mon, 25 Jul 2022 12:00:00 GMT"), false),
  aql3Shaftastic: aqlTheme(3, new Date("Mon, 25 Jul 2022 12:00:00 GMT"), true),
  generic,
};

function aqlTheme(season: number, startDate: Date, shaftastic: boolean): OverlayTheme {
  const startWeek = getWeek(startDate, { weekStartsOn: 1 });

  function week(): number {
    const now = new Date();
    return getWeek(now, { weekStartsOn: 1 }) - startWeek + 1;
  }

  return {
    name: `AQL${season} - ${shaftastic ? " for ShaftasticTV" : ""}`,
    seriesName: "APAC QUAKE LEAGUE",
    seriesSubtitle: () => (
      <span>
        Season ${season} - Week {week()}
      </span>
    ),
    floatingLogos: shaftastic
      ? [surfshamb]
      : [surfshamb, "https://kuachi.gg/u/shaftastic_tv_white.png"],
    topLogos: [logo_text_png, surfshamb],
    bottomMiddleLogos: [logo_text_png],
    backgroundStyle: "none",
    backgroundImages: [],
  };
}

export const ThemeCtx = createContext<OverlayTheme>(generic);

export function OverlayThemeProvider(props: { children: JSX.Element }) {
  const f = useOverlayForm();
  const theme = createMemo<keyof typeof themes>(() => {
    const key = f.store.theme;
    log.debug("OVERLAYTHEME", key);
    if (key in themes) return key as any;
    return "generic";
  });
  const getTheme = createMemo(() => themes[theme()]);
  const [scale, setScale] = createStore({ x: 1, y: 1 });
  function setOverlayScale() {
    const wr = window.innerWidth / 1920;
    const hr = window.innerHeight / 1080;
    const scale = Math.min(wr, hr);
    setScale({ x: scale, y: scale });
  }
  window.addEventListener("resize", setOverlayScale);
  onCleanup(() => {
    window.removeEventListener("resize", setOverlayScale);
  });
  setOverlayScale();
  return (
    <ThemeCtx.Provider value={proxyGet(getTheme)}>
      <div
        style={{
          width: "1920px",
          height: "1080px",
          transform: "scale(var(--scale-x), var(--scale-y))",
          "transform-origin": "top left",
          "--scale-x": scale.x,
          "--scale-y": scale.y, // keep aspect ratio
          overflow: "hidden",
        }}
        data-overlay-theme={theme()}
      >
        {props.children}
      </div>
    </ThemeCtx.Provider>
  );
}

function proxyGet<T>(getter: () => T): T {
  return new Proxy({} as any, {
    get(_target, key, _recv) {
      return (getter() as any)[key];
    },
  });
}

export function useTheme() {
  return useContext(ThemeCtx);
}

export function ThemeName() {
  const t = useTheme();
  return <span>{t.name}</span>;
}
