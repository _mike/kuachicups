import { navigate, router } from "@/router";
import { useSetTooltipActive } from "@/shared/ui/Tooltip";
import { randomElem } from "fp-ts/lib/Random";
import { makeBy } from "fp-ts/lib/Array";
import {
  Accessor,
  createContext,
  createEffect,
  createSignal,
  JSX,
  Setter,
  useContext,
} from "solid-js";
import secretInputTheme from "./OverlaySecretInput.module.scss";
import Icon from "@/shared/ui/Icon";

const OVERLAY_SECRET_KEY = "OVERLAY_SECRET";

const OverlaySecretContext = createContext<{
  secret: Accessor<string>;
  setSecret: Setter<string>;
  newSecret: () => void;
}>();

export function OverlaySecretProvider(props: { children: JSX.Element }) {
  const firstSecret = getSecret();
  const [secret, setSecret] = createSignal(firstSecret);
  createEffect(() => {
    saveSecret(secret());
  });
  return (
    <OverlaySecretContext.Provider
      value={{
        secret,
        setSecret,
        newSecret() {
          setSecret(newSecret());
        },
      }}
    >
      {props.children}
    </OverlaySecretContext.Provider>
  );
}

export function useOverlaySecret() {
  return useContext(OverlaySecretContext)!;
}

export function OverlaySecretInput() {
  const os = useOverlaySecret();
  const setTooltipActive = useSetTooltipActive();
  const [showSecret, setShowSecret] = createSignal(false);
  return (
    <div
      class={secretInputTheme.secret}
      onPointerEnter={() => setTooltipActive(true)}
      onPointerLeave={() => setTooltipActive(false)}
    >
      <input
        class={secretInputTheme.input}
        type={showSecret() ? "text" : "password"}
        onChange={(ev) => os.setSecret(ev.currentTarget.value)}
        value={os.secret()}
        placeholder="Secret key"
      />
      <button class={secretInputTheme.button} onClick={() => setShowSecret((s) => !s)}>
        <Icon type={showSecret() ? "eye-off" : "eye"} />
      </button>
    </div>
  );
}

////////////////////////////////////////////////////////////////////////////////

function getSecret() {
  let secret: string | null = router.getState().params?.["secret"];
  if (typeof secret !== "string") secret = localStorage.getItem(OVERLAY_SECRET_KEY);
  if (secret === null) secret = newSecret();
  return secret;
}

function saveSecret(secret: string) {
  localStorage.setItem(OVERLAY_SECRET_KEY, secret);
  const state = router.getState();
  navigate({ to: state.name as any, params: { ...state.params, secret: secret } });
}

function newSecret(): string {
  return makeBy(12, () =>
    randomElem(["a", ..."bcdefghijklmnopqrstuvwxyz0123456789"] as const)()
  ).join("");
}
