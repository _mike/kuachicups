import { api, CupMatch2p, CupStage } from "@/api";
import { router } from "@/router";
import * as NonEmptyArray from "fp-ts/lib/NonEmptyArray";
import * as Record from "fp-ts/lib/Record";
import createClient from "json-url";
import * as OC from "../View/types";

export interface FormConfig {
  theme: string;
  cmd: OC.Overlay[];
}

////////////////////////////////////////////////////////////////////////////////

const { compress, decompress } = createClient("lzma");

export function decodeConfig(cfg: string): Promise<FormConfig> {
  return decompress(cfg) as Promise<FormConfig>;
}

export function encodeConfig(cfg: FormConfig): Promise<string> {
  cfg = JSON.parse(JSON.stringify(cfg));
  return compress(cfg);
}

////////////////////////////////////////////////////////////////////////////////

export async function getConfig(): Promise<FormConfig> {
  const cfg = (await getConfigParameter()) ?? (await getStoredConfig());
  if (cfg !== undefined) return cfg;
  return {
    theme: "generic",
    cmd: [],
  };
}

async function getStoredConfig(): Promise<FormConfig | undefined> {
  const storedConfigStr = localStorage.getItem("OVERLAY_CONFIG");
  if (typeof storedConfigStr === "string") {
    return decodeConfig(storedConfigStr);
  }
}

async function getConfigParameter(): Promise<FormConfig | undefined> {
  const cfg = router.getState().params?.["cfg"];
  if (typeof cfg === "string") {
    return decodeConfig(cfg);
  }
}

/** Save the config, saves to localStorage and the ?cfg param if disableRouteCfg
 * is not set */
export async function saveConfig(cfg: FormConfig, disableRouteCfg: boolean = false) {
  const enc = await encodeConfig(cfg);
  localStorage.setItem("OVERLAY_CONFIG", enc);
  if (!disableRouteCfg) {
    const route = router.getState();
    router.navigate(route.name, { ...route.params, cfg: enc });
  }
  return enc;
}

////////////////////////////////////////////////////////////////////////////////

const NO_STAGE_ID = "";

export async function createMatchOverlay(ungroupedMatches: CupMatch2p[]): Promise<FormConfig> {
  const matchesByStage = NonEmptyArray.groupBy((m: CupMatch2p) => m.cup_stage_id ?? NO_STAGE_ID)(
    ungroupedMatches
  );
  const stages = await api.cups.stagesById.byIds(
    Object.keys(matchesByStage).filter((s) => s != NO_STAGE_ID)
  );
  const stagesByCup = NonEmptyArray.groupBy((s: CupStage) => s.cup_id)(stages);
  const matchesByCupByStage = Record.toArray(
    Record.map((stages: CupStage[]) => {
      const matches = stages.map((s): [stage: CupStage, matches: CupMatch2p[]] => [
        s,
        matchesByStage[s.id],
      ]);
      return matches;
    })(stagesByCup)
  );
  const cmd: OC.Overlay[] = [];
  for (const [cupId, stageMatches] of matchesByCupByStage) {
    for (const [stage, matches] of stageMatches) {
      if (matches.length === 1) {
        cmd.push({
          tag: "cup-stage-provider",
          cupId,
          stage: { id: stage.id, stageNo: stage.stage_no },
          display: {
            tag: "scenes",
            scenes: matchScenes(matches[0]),
          },
        });
      } else {
        const scenes: OC.Scene[] = [];
        scenes.push(OC.scene(OC.create("cup-backdrop")));
        scenes.push(OC.scene(OC.create("cup-bracket")));
        scenes.push(OC.scene(OC.create("cup-standings")));
        for (const match of matches) {
          scenes.push(matchScene(match));
        }
        cmd.push({
          tag: "cup-stage-provider",
          cupId,
          stage: { id: stage.id, stageNo: stage.stage_no },
          display: {
            tag: "scenes",
            scenes,
          },
        });
      }
    }
  }
  return {
    cmd,
    theme: "generic",
  };
}

function matchScene(match: CupMatch2p): OC.Scene {
  return {
    aliveTimeSec: 60,
    autoplay: false,
    previs: false,
    scene: {
      tag: "cup-match",
      matchId: match.id,
      startTime: undefined,
    },
  };
}

function matchScenes(match: CupMatch2p): OC.Scene[] {
  return (["cup-backdrop", "cup-bracket", "cup-standings", "cup-match"] as const).map((tag) => ({
    aliveTimeSec: 60,
    autoplay: false,
    previs: false,
    scene:
      tag === "cup-match"
        ? {
            tag,
            matchId: match.id,
            startTime: undefined,
          }
        : OC.create(tag),
  }));
}
