import { Link } from "@/router";

export default function OverlayDocs() {
  return (
    <div class="container">
      <h1>Stream Overlay Manual</h1>
      <h2>Overview</h2>
      <p>
        The <Link to="stream">stream</Link> page enables creating stream overlays using elements
        from kuachi.gg. Basically, cup brackets and other details, match details, and game overlays.
        The form on that page lets you create the <b>stream config</b> for your overlay; it say:
        what cup, what matches, what start times, what order of scenes, etc.
      </p>
      <p>
        You connect your <b>stream config</b> to a <b>stream view</b> by putting the view in an
        OBS/StreamLabs browser source - then upload the config to the view by hitting "upload
        config". You connect the <b>stream view</b> to the <b>remote</b> by just opening the remote
        control while OBS/StreamLabs is running, and it enables controls like changing scenes,
        setting match scores, etc.
      </p>
      <h2>OBS/StreamLabs Instructions</h2>
      <ol>
        <li>
          <strong>Create a stream config</strong>
        </li>
        <li>
          <strong>Create a viewer:</strong> Keep the stream config page open. Copy your viewer link
          to a browser source in OBS. Set the width and height to 1920px and 1080px respectively.
        </li>
        <li>
          <strong>Upload a config:</strong> On the stream config page, once your viewer connects to
          the config page, hit "Upload config".
        </li>
        <li>
          <strong>Open the remote:</strong> Copy your remote control link to a browser dock in OBS,
          and open it.
        </li>
      </ol>
      <h2>How to update an overlay</h2>
      <ol>
        <li>Change or create a new stream config</li>
        <li>Open OBS</li>
        <li>Hit "upload config" once a connection is made on the config page</li>
      </ol>
    </div>
  );
}
