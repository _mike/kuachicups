import Icon from "@/shared/ui/Icon";
import Menu from "@/shared/ui/Menu";
import toast from "@/shared/ui/Toast";
import Tooltip from "@/shared/ui/Tooltip";
import clipboardCopy from "clipboard-copy";
import { createSignal } from "solid-js";
import { ConnectProgressBar, Peer, PeerProvider, usePeer } from "../Peer";
import { OverlaySecretInput, OverlaySecretProvider, useOverlaySecret } from "../Secret";
import OverlayForm from "./Form";
import { OverlayFormContextProvider, useOverlayForm } from "./Form/Context";

export default function OverlayConfig() {
  const [theirConfig, setTheirConfig] = createSignal<undefined | string>(undefined);
  return (
    <OverlaySecretProvider>
      <PeerProvider
        type="cfg"
        onConnect={() => {}}
        onData={(data) => {
          if (data.tag === "got-config") {
            setTheirConfig(data.config);
          }
        }}
      >
        <OverlayFormContextProvider>
          <Menu.Layout menu={() => <OverlayMenus theirConfig={theirConfig()} />}>
            <OverlayForm />
          </Menu.Layout>
        </OverlayFormContextProvider>
      </PeerProvider>
    </OverlaySecretProvider>
  );
}

function OverlayMenus(props: { theirConfig: undefined | string }) {
  const os = useOverlaySecret();

  function overlayRemoteLink() {
    const params = `?secret=${os.secret()}`;
    const path =
      (process.env.NODE_ENV === "development" ? "/stream/remote/index.html" : "/stream/remote") +
      params;
    return `${location.origin}${path}`;
  }

  function overlayViewLink() {
    const params = `?secret=${os.secret()}`;
    const path =
      (process.env.NODE_ENV === "development" ? "/stream/overlay/index.html" : "/stream/overlay") +
      params;
    return `${location.origin}${path}`;
  }

  return (
    <Menu.Menus>
      <Menu>
        <Menu.Title>Settings</Menu.Title>
        <Menu.Item>
          <Tooltip controlled tooltip="Connects an overlay\nto a remote.\n\nKeep this secret.">
            <label>
              Secret key
              <OverlaySecretInput />
            </label>
          </Tooltip>
        </Menu.Item>
        <Menu.Item>
          <ConnectProgressBar otherConfigStr={props.theirConfig} />
        </Menu.Item>
        <UploadConfigButton />
        <Menu.ItemLink
          data-tooltip="Used to control the running overlay"
          onClick={() => {
            const link = overlayRemoteLink();
            clipboardCopy(link).then(
              () => {
                toast.success("Copied remote link");
              },
              () => {
                toast.failure(() => (
                  <>
                    Cannot copy viewer link
                    <code>{link}</code>
                  </>
                ));
              }
            );
          }}
        >
          <Icon type="keypad" />
          <span>Copy remote link</span>
        </Menu.ItemLink>
        <Menu.ItemLink
          data-tooltip="Paste this into a browser source"
          onClick={() => {
            const link = overlayViewLink();
            clipboardCopy(link).then(
              () => {
                toast.success("Copied viewer link");
              },
              () => {
                toast.failure(() => (
                  <>
                    Cannot copy viewer link
                    <code>{link}</code>
                  </>
                ));
              }
            );
          }}
        >
          <Icon type="tv" />
          <span>Copy viewer link</span>
        </Menu.ItemLink>
      </Menu>
      <Menu>
        <Menu.Title>What is this?</Menu.Title>
        <Menu.Link to="stream.docs">See manual</Menu.Link>
      </Menu>
    </Menu.Menus>
  );
}

function UploadConfigButton() {
  const p = usePeer()!;
  const f = useOverlayForm()!;
  return (
    <Menu.ItemLink
      disabled={p.status?.conn !== true}
      onClick={() => {
        if (p.status?.conn !== true) return undefined;
        const peer: Peer<"cfg"> | undefined = p?.peer;
        if (peer !== undefined) {
          peer.send({ tag: "set-config", config: f.configStr()! });
        }
      }}
      data-tooltip={
        p.status?.conn !== true
          ? "Not connected. Your overlay may not be running or may have an incorrect secret key specified"
          : "Upload config to the running overlay"
      }
    >
      <Icon type="cloud-upload" />
      <span>Upload config</span>
    </Menu.ItemLink>
  );
}
