import { usePromise } from "@/shared/async/Awaitable";
import { ArrayFldProps, createField, FldProps, WrappedStore, wrapStore } from "@/shared/ui/Form2";
import FormatResponseError from "@/shared/ui/ResponseError";
import toast from "@/shared/ui/Toast";
import { createContext, createEffect, createMemo, JSX, untrack, useContext } from "solid-js";
import { createStore, SetStoreFunction, Store } from "solid-js/store";
import { useRoute } from "solid-typefu-router5";
import { encodeConfig, FormConfig, getConfig, saveConfig } from "../config";
import Keys from "./Keys";

const _formTypeWitness = () => createField<FormConfig>(undefined as any, undefined as any);

type CreateFieldForm = ReturnType<typeof _formTypeWitness>;

export type IOverlayFormContext = {
  keys: Keys;
  store: Store<FormConfig>;
  setStore: SetStoreFunction<FormConfig>;
  configStr: () => string | undefined;
} & CreateFieldForm &
  WrappedStore<FormConfig>;

export const OverlayFormContext = createContext<IOverlayFormContext>();

export function OverlayFormContextProvider(props: {
  children: JSX.Element;
  disableRouteCfg?: boolean;
}) {
  const [store, setStore] = createStore<FormConfig>({ theme: "generic", cmd: [] });
  const route = useRoute();

  const configStrPromise = usePromise<string | undefined>(async () => {
    const disableRouteCfg = props.disableRouteCfg === true;
    const param = route().params?.cfg;
    if (!disableRouteCfg) return param;
    return encodeConfig(store);
  });

  const configStr = createMemo(configStrPromise);

  untrack(() => {
    getConfig()
      .then((cfg) => {
        setStore(cfg);
        return saveConfig(cfg, props.disableRouteCfg);
      })
      .catch((err) => {
        toast.failure(() => (
          <>
            Could not get overlay config
            <FormatResponseError error={err} />
          </>
        ));
      });
  });

  createEffect(() => {
    const copy = JSON.parse(JSON.stringify(store));
    saveConfig(copy, props.disableRouteCfg).catch((err) => {
      <>
        Could not save overlay config
        <FormatResponseError error={err} />
      </>;
    });
  });

  const field: CreateFieldForm = createField(store, setStore);
  const wrapped: WrappedStore<FormConfig> = wrapStore(store, setStore, field.optic);
  const keys = new Keys();
  return (
    <OverlayFormContext.Provider
      value={{
        keys,
        store,
        setStore,
        configStr,
        ...field,
        ...wrapped,
      }}
    >
      {props.children}
    </OverlayFormContext.Provider>
  );
}

export function useOverlayForm() {
  return useContext(OverlayFormContext)!;
}

export function Fld<T>(props: FldProps<FormConfig, T>) {
  const ctx = useContext(OverlayFormContext)!;
  return ctx.Fld(props);
}

export function ArrayFld<T>(props: ArrayFldProps<FormConfig, T>) {
  const ctx = useContext(OverlayFormContext)!;
  return ctx.ArrayFld(props);
}
