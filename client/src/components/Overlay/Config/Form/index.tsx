import { api, Paged } from "@/api";
import { SignupLoader } from "@/components/Cups/Cup/CupSignup";
import { MatchName } from "@/components/Cups/Cup/Stage/MatchName";
import { Case } from "@/shared/Case";
import Dropdown, { Option as DropdownOption, unpaged } from "@/shared/ui/Dropdown";
import Dropdown2 from "@/shared/ui/Dropdown2";
import {
  CheckBoxField,
  DateTimeField,
  MultilineTextField,
  NumberField,
  RadioField,
  StrictNumberField,
  TextField,
} from "@/shared/ui/Form2";
import { asIDT } from "@/shared/ui/Form2/Field/DateTime";
import Icon from "@/shared/ui/Icon";
import Keyed from "@/shared/ui/Keyed";
import toast from "@/shared/ui/Toast";
import classNames from "classnames";
import * as NEA from "fp-ts/lib/NonEmptyArray";
import * as O from "optics-ts";
import {
  batch,
  createComputed,
  createContext,
  createEffect,
  createMemo,
  createSignal,
  getOwner,
  Index,
  JSX,
  runWithOwner,
  Show,
  useContext,
} from "solid-js";
import { createMutable } from "solid-js/store";
import { themes } from "../../themes";
import * as C from "../../View/types";
import { FormConfig } from "../config";
import { Fld, useOverlayForm } from "./Context";
import Keys, { keyEq, keyName, keyOrd, stripKeySuffix } from "./Keys";
import theme from "./theme.module.scss";

export default function OverlayForm() {
  return (
    <>
      <ThemeChooser />
      <CmdRoot />
    </>
  );
}

function ThemeChooser() {
  const sf = useOverlayForm();
  return (
    <Fld label="Theme" optic={sf.optic.prop("theme")}>
      {(value, onChange) => (
        <RadioField
          values={
            Object.keys(themes).map((key) => ({
              key,
              name: key,
            })) as any[]
          }
          value={value()}
          onChange={onChange}
        />
      )}
    </Fld>
  );
}

function CmdRoot() {
  const sf = useOverlayForm()!;

  // Ensure all keys are unique
  createEffect(() => {
    // 1. Collect all keys
    const ks: [key: string | undefined, optic: O.Prism<FormConfig, any, C.Overlay>][] = [];

    function go(here: O.Prism<FormConfig, any, C.Overlay>, cmd: C.Overlay) {
      let k = cmd.key;
      if (k === undefined || k === "") {
        ks.push([undefined, here]);
      } else {
        // NOTE the stripKeySuffix here means when sorting/grouping we'll catch
        // the name collisions using ordinary string equality/comparison
        ks.push([stripKeySuffix(k), here]);
      }
      if (cmd.display !== undefined) {
        go(here.prop("display").optional(), cmd.display);
      }
      if (cmd.tag === "scenes") {
        const scenes = cmd.scenes;
        const scenesOptic = here.guard(C.isScenes).prop("scenes");
        for (let i = 0; i < scenes.length; i++) {
          const scene = scenes[i];
          go(scenesOptic.at(i).prop("scene"), scene.scene);
        }
      }
    }

    const cmds = sf.store.cmd;

    const len = cmds.length;

    for (let i = 0; i < len; i++) {
      const cmd = cmds[i];
      const optic = sf.optic.prop("cmd").at(i);
      go(optic, cmd);
    }

    // 2. Disambiguate any collisions
    const neKs = NEA.fromArray(ks);
    if (neKs._tag === "Some") {
      const sortedKs = NEA.sort(keyOrd)(neKs.value);
      const keyGroups = NEA.group(keyEq)(sortedKs);
      const keys = new Keys();
      batch(() => {
        for (const keyGroup of keyGroups) {
          if (keyGroup.length === 1) {
            // It's safe to do stripKeySuffix since ordKey and sameKey strip suffixes when comparing
            let [key, optic] = keyGroup[0];
            if (key === undefined) {
              sf.set(
                optic.prop("key"),
                keyName(sf.preview(optic.prop("tag"))) ?? keys.add("unknown")
              );
            } else {
              sf.set(optic.prop("key"), key);
            }
          } else {
            // keyGroup.length > 1
            // Collision(s) found if we need more than one iteration here
            for (let i = 0; i < keyGroup.length; i++) {
              let [key, optic] = keyGroup[i];
              key = key ?? sf.preview(optic.prop("tag"));
              if (key !== undefined) {
                sf.set(optic.prop("key"), keys.add(key));
              }
            }
          }
        }
      });
      log.debug({ keys, keyGroups });
    }
  });

  return (
    <>
      <Cmds optic={sf.optic.prop("cmd").optional()} />
      <CmdAdder root optic={sf.optic.prop("cmd").optional()} />
    </>
  );
}

function Cmds(props: { optic: O.Prism<FormConfig, any, C.Overlay[]> }) {
  const sf = useOverlayForm()!;
  return (
    <div class={theme.cmds}>
      <Index each={sf.preview(props.optic)}>
        {(_, ix) => <Cmd optic={props.optic.index(ix)} />}
      </Index>
    </div>
  );
}

interface ICmdUIContext {
  panelUI: JSX.Element;
  expanded: boolean | undefined;
}

const CmdUIContext = createContext<ICmdUIContext>();

function CmdPanelUIPortal(props: { children: JSX.Element }) {
  const owner = getOwner();
  const ctx = useContext(CmdUIContext);
  log.debug("CmdPanelUIPortal", owner, ctx, props);
  if (ctx !== undefined) {
    ctx.panelUI = () => runWithOwner(owner!, () => props.children);
  }
  return null;
}

function CmdPanelUI() {
  const ctx = useContext(CmdUIContext);
  return <>{ctx?.panelUI}</>;
}

function Cmd(props: {
  optic: O.Prism<FormConfig, O.Params<any, any>, C.Overlay>;
  onRemove?: () => void;
  /** Extra config to render in the panel area */
  children?: JSX.Element;
}) {
  const s = useOverlayForm()!;
  const [expanded, setExpanded] = createSignal<boolean | undefined>(true);
  const tag = createMemo(() => s.preview(props.optic)?.tag ?? "none");
  const ctx = useContext(CmdUIContext);
  const ui = createMutable<ICmdUIContext>({
    expanded: true,
    panelUI: () => null,
  });

  createComputed(() => {
    batch(() => {
      if (ctx !== undefined) {
        setExpanded(ctx.expanded);
      } else {
        switch (tag()) {
          case "cup-stage-provider":
            setExpanded(true);
            ui.expanded = undefined;
        }
      }
    });
  });

  const _Cmd = () => (
    <div class={classNames(theme.cmd, theme[tag()])} data-expanded={expanded() ?? true}>
      <div class={theme.panel}>
        <Show
          when={expanded() !== undefined}
          fallback={() => (
            <button class={theme.expander + " secondary"}>
              <span>{tag()}</span>
            </button>
          )}
        >
          <button class={theme.expander + " secondary"} onClick={() => setExpanded((t) => !t)}>
            <Icon type={expanded() ? "contract" : "expand"} />
            <span>{tag()}</span>
          </button>
        </Show>
        <KeyInput optic={props.optic} />
        <div class={theme.panelConfig}>
          {props.children}
          <CmdPanelUI />
        </div>
        <button
          class={theme.remove + " small outline"}
          disabled={props.optic._removable !== true && props.onRemove === undefined}
          onClick={() => {
            const { optic, onRemove } = props;
            const key = s.preview(props.optic.prop("key"));
            if (key !== undefined) {
              s.keys.remove(key);
            }
            if (optic._removable === true) {
              s.remove(optic);
              if (key !== undefined) s.keys.remove(key);
            } else if (onRemove !== undefined) {
              onRemove();
              if (key !== undefined) s.keys.remove(key);
            } else {
              toast.failure(() => <p>Cannot remove that</p>);
              return;
            }
          }}
        >
          <Icon type="close" /> Remove
        </button>
      </div>
      <Case
        key={tag()}
        children={{
          img: () => <ImgVideoCmd optic={props.optic.guard(C.isImg)} />,
          video: () => <ImgVideoCmd optic={props.optic.guard(C.isVideo)} />,
          chat: () => <ChatCmd optic={props.optic.guard(C.isChat)} />,
          style: () => <StyleCmd optic={props.optic.guard(C.isStyle)} />,
          html: () => <HtmlCmd optic={props.optic.guard(C.isHtml)} />,
          scenes: () => <ScenesCmd optic={props.optic.guard(C.isScenes)} />,
          "cup-bracket": () => <CupBracket optic={props.optic.guard(C.isCupBracket)} />,
          "cup-match": () => <CupMatch optic={props.optic.guard(C.isCupMatch)} />,
          "cup-stage-provider": () => (
            <CupStageProvider optic={props.optic.guard(C.isCupStageProvider)} />
          ),
          "cup-backdrop": () => <CupBackdrop optic={props.optic.guard(C.isCupBackdrop)} />,
        }}
      />
    </div>
  );

  return (
    <Show when={s.preview(props.optic) !== undefined}>
      <Parent name={overlayName(s.preview(props.optic))}>
        <CmdUIContext.Provider value={ui}>
          <_Cmd />
        </CmdUIContext.Provider>
      </Parent>
    </Show>
  );
}

function KeyInput(props: { width?: string; optic: O.Prism<FormConfig, any, C.Overlay> }) {
  return (
    <Fld
      optic={props.optic.prop("key")}
      data-tooltip="Unique key for this component"
      style={{ width: props.width }}
    >
      {(value, onChange) => (
        <>
          <input
            ref={(inp) => {
              // Manage the input if the value changes "externally"
              createEffect(() => {
                const next = value();
                if (typeof next === "string" && inp.value !== next) {
                  inp.value = next;
                }
              });
            }}
            class={theme.key}
            type="text"
            value={value()}
            onChange={(ev) => onChange(ev.currentTarget.value)}
          />
        </>
      )}
    </Fld>
  );
}

function OptionalCmd(props: { optic: O.Prism<FormConfig, any, undefined | C.Overlay> }) {
  return (
    <>
      <CmdSelector optic={props.optic} />
      <Cmd optic={props.optic.optional()} />
    </>
  );
}

function CmdConfigArea(props: { children: JSX.Element }) {
  return (
    <div class={theme.config}>
      <fieldset class={theme.configFields}>{props.children}</fieldset>
    </div>
  );
}

function ChatCmd(props: { optic: O.Prism<FormConfig, any, C.Chat> }) {
  return (
    <CmdConfigArea>
      <Fld label="Twitch channel" optic={props.optic.prop("twitch")}>
        {(value, onChange) => <TextField value={value()} onChange={onChange} />}
      </Fld>
    </CmdConfigArea>
  );
}

export const CupProviderCtx = createContext<() => string | undefined>();
export const CupStageProviderCtx =
  createContext<() => { id: string; stageNo: number } | undefined>();

function CupStageProvider(props: { optic: O.Prism<FormConfig, any, C.CupStageProvider> }) {
  const s = useOverlayForm()!;
  const defaultCupId = useContext(CupProviderCtx);
  const getCupId = createMemo(() => {
    const def = defaultCupId?.();
    return s.preview(props.optic.prop("cupId")) ?? def;
  });
  const getCupStage = createMemo(() => s.preview(props.optic.prop("stage")));
  createEffect<string | undefined>((lastCupId) => {
    const cupId = getCupId();
    if (lastCupId !== cupId && lastCupId !== undefined) {
      s.set(props.optic.prop("stage"), undefined);
    }
    return cupId;
  });
  return (
    <CupProviderCtx.Provider value={getCupId}>
      <CupStageProviderCtx.Provider value={getCupStage}>
        <CmdPanelUIPortal>
          <Fld label="Cup" optic={props.optic.prop("cupId")}>
            {(value, onChange) => (
              <Dropdown
                kind="fetch"
                value={value()}
                onChange={(opt) => onChange(opt.key)}
                options={api.cups.dropdownOptions}
              >
                Choose a cup
              </Dropdown>
            )}
          </Fld>
          <Fld label="Stage" optic={props.optic.prop("stage")}>
            {(value, onChange) => (
              <Dropdown
                kind="fetch"
                value={value()?.id}
                deps={getCupId()}
                onChange={(opt) => {
                  if (opt.key !== undefined) {
                    onChange({ id: opt.key, stageNo: opt.extra });
                  } else {
                    onChange(undefined);
                  }
                }}
                options={api.cups.stages.dropdownOptions(getCupId())}
              >
                Choose a stage
              </Dropdown>
            )}
          </Fld>
        </CmdPanelUIPortal>
        <CmdConfigArea>
          <OptionalCmd optic={props.optic.prop("display")} />
        </CmdConfigArea>
      </CupStageProviderCtx.Provider>
    </CupProviderCtx.Provider>
  );
}

function CupBracket(props: { optic: O.Prism<FormConfig, any, C.CupBracket> }) {
  return (
    <CmdConfigArea>
      <Fld label="Group number" optic={props.optic.prop("groupNo")}>
        {(value, onChange) => (
          <StrictNumberField
            value={value()}
            onChange={(v) =>
              onChange(typeof v === "number" && Number.isSafeInteger(v) && v >= 0 ? v : undefined)
            }
          />
        )}
      </Fld>
      <Fld label="Group round" optic={props.optic.prop("roundNo")}>
        {(value, onChange) => (
          <StrictNumberField
            value={value()}
            onChange={(v) =>
              onChange(typeof v === "number" && Number.isSafeInteger(v) && v >= 0 ? v : undefined)
            }
          />
        )}
      </Fld>
      <Fld label="Start round (upper bracket)" optic={props.optic.prop("roundCutoff")}>
        {(value, onChange) => (
          <StrictNumberField
            value={value()}
            onChange={(v) =>
              onChange(typeof v === "number" && Number.isSafeInteger(v) && v >= 0 ? v : undefined)
            }
          />
        )}
      </Fld>
      <Fld label="Start round (lower bracket)" optic={props.optic.prop("roundCutoffLb")}>
        {(value, onChange) => (
          <StrictNumberField
            value={value()}
            onChange={(v) =>
              onChange(typeof v === "number" && Number.isSafeInteger(v) && v >= 0 ? v : undefined)
            }
          />
        )}
      </Fld>
    </CmdConfigArea>
  );
}

function CupMatch(props: { optic: O.Prism<FormConfig, any, C.CupMatch> }) {
  const getCupId = useContext(CupProviderCtx);
  const getCupStageId = useContext(CupStageProviderCtx);
  async function getMatches(): Promise<[Paged, DropdownOption<number>[]]> {
    const cupId = getCupId?.();
    const stage = getCupStageId?.();
    if (cupId === undefined || stage?.id === undefined) return [unpaged, []];
    const { array: matches } = await api.cups.matches.byParentId(stage.id);
    return [
      unpaged,
      matches
        // .filter((m) => m.high_id && m.low_id)
        .map((m) => ({
          title: () => (
            <>
              <MatchName match={m} /> &mdash; <SignupLoader signup={m.high_id ?? undefined} />
              {" vs "}
              <SignupLoader signup={m.low_id ?? undefined} />
            </>
          ),
          key: m.id,
        })),
    ];
  }
  return (
    <CmdConfigArea>
      <Fld optic={props.optic.prop("matchId")}>
        {(value, onChange) => (
          <Keyed key={getCupStageId?.()}>
            <Dropdown
              kind="fetch"
              value={value()}
              onChange={(opt) => opt.key && onChange(opt.key)}
              deps={[getCupId?.(), getCupStageId?.()]}
              options={getMatches}
              renderOption={(opt) => opt.option.title}
            >
              Match (high vs low)
            </Dropdown>
          </Keyed>
        )}
      </Fld>
      <Fld
        label="Start time"
        optic={props.optic
          .prop("startTime")
          .reread((s) => (typeof s === "string" ? new Date(s) : s))
          .reread((s) => (typeof s === "string" ? new Date(s) : s))}
      >
        {(value, onChange) => (
          <DateTimeField
            class={theme.datetime}
            value={asIDT(value())}
            onChange={(v) => onChange(v.parsed)}
          />
        )}
      </Fld>
    </CmdConfigArea>
  );
}

function CupBackdrop(props: { optic: O.Prism<FormConfig, any, C.CupBackdrop> }) {
  return (
    <CmdConfigArea>
      <Fld optic={props.optic.prop("iframes")} label="Webcam URIs">
        {(value, onChange) => (
          <MultilineTextField
            placeholder="Each line here is a URI that will render within this overlay."
            innerProps={{
              style: "--spacing: 0.5rem; font-family: monospace; font-size: 14px",
            }}
            value={value().join("\n") ?? "\n"}
            onChange={(v) => onChange(lines(v))}
          />
        )}
      </Fld>
      <br />
      <Fld optic={props.optic.prop("width")} label="Width">
        {(value, onChange) => (
          <StrictNumberField
            value={value()}
            onChange={(v) => onChange(typeof v === "number" ? v : 0)}
          />
        )}
      </Fld>
      <Fld optic={props.optic.prop("height")} label="Height">
        {(value, onChange) => (
          <StrictNumberField
            value={value()}
            onChange={(v) => onChange(typeof v === "number" ? v : 0)}
          />
        )}
      </Fld>
    </CmdConfigArea>
  );
}

export function lines(v: string) {
  return v
    .trim()
    .split("\n")
    .map((v) => v.trim())
    .filter((v) => v !== "");
}

function StyleCmd(props: { optic: O.Prism<FormConfig, any, C.Style> }) {
  return (
    <CmdConfigArea>
      <Fld optic={props.optic.prop("style")}>
        {(value, onChange) => (
          <MultilineTextField
            class="is-family-monospace"
            value={value()}
            onChange={onChange}
            placeholder="CSS goes here"
          />
        )}
      </Fld>
    </CmdConfigArea>
  );
}

function HtmlCmd(props: { optic: O.Prism<FormConfig, any, C.Html> }) {
  return (
    <CmdConfigArea>
      <Fld
        label="Props"
        description="JSON props object for the div, for e.g. class, style"
        error={(v) => (typeof v === "string" ? "Must be valid JSON" : undefined)}
        optic={props.optic.prop("props")}
      >
        {(value, onChange) => (
          <MultilineTextField
            style={{ "font-family": "var(--font-family-mono)" }}
            value={(() => {
              const v = value();
              if (typeof v === "string") return v;
              return JSON.stringify(v, null, 2);
            })()}
            onChange={(v) => {
              try {
                onChange(JSON.parse(v));
              } catch (_e) {
                onChange(v);
              }
            }}
            placeholder="JSON goes here"
          />
        )}
      </Fld>
      <Fld label="Inner HTML" optic={props.optic.prop("content")}>
        {(value, onChange) => (
          <MultilineTextField
            style={{ "font-family": "var(--font-family-mono)" }}
            value={value()}
            onChange={onChange}
            placeholder="HTML goes here"
          />
        )}
      </Fld>
    </CmdConfigArea>
  );
}

function ImgVideoCmd<C extends Omit<C.Img, "tag">>(props: { optic: O.Prism<FormConfig, any, C> }) {
  return (
    <CmdConfigArea>
      <Fld label="Source" optic={props.optic.prop("src")}>
        {(value, onChange) => <TextField value={value()} onChange={onChange} />}
      </Fld>
      <Fld label="Width" optic={props.optic.prop("width")}>
        {(value, onChange) => <TextField value={value()} onChange={onChange} />}
      </Fld>
      <Fld label="Height" optic={props.optic.prop("height")}>
        {(value, onChange) => <TextField value={value()} onChange={onChange} />}
      </Fld>
      <Fld optic={props.optic.prop("background")}>
        {(value, onChange) => (
          <CheckBoxField label="Background" value={value()} onChange={onChange} />
        )}
      </Fld>
    </CmdConfigArea>
  );
}

function ScenesCmd(props: { optic: O.Prism<FormConfig, any, C.Scenes> }) {
  const s = useOverlayForm()!;
  return (
    <>
      <CmdConfigArea>
        <Index each={s.preview(props.optic.prop("scenes"))}>
          {(t, index) => (
            <Cmd
              optic={props.optic.prop("scenes").at(index).prop("scene")}
              onRemove={() => s.remove(props.optic.prop("scenes").at(index))}
            >
              <CheckBoxField
                label="Autoplay"
                value={t().autoplay}
                onChange={(checked) =>
                  s.set(props.optic.prop("scenes").at(index).prop("autoplay"), checked)
                }
              />
              <CheckBoxField
                label="Previs"
                value={t().previs}
                onChange={(checked) =>
                  s.set(props.optic.prop("scenes").at(index).prop("previs"), checked)
                }
              />
              <Show when={t().autoplay}>
                <Fld
                  optic={props.optic.prop("scenes").at(index).prop("aliveTimeSec")}
                  label="Alive time (sec)"
                >
                  {(value, onChange) => (
                    <NumberField
                      value={value()}
                      onChange={(v) => typeof v === "number" && onChange(v)}
                    />
                  )}
                </Fld>
              </Show>
            </Cmd>
          )}
        </Index>
        <footer>
          <CmdAdder
            optic={props.optic.prop("scenes")}
            wrap={(scene: C.Overlay): C.Scenes["scenes"][number] => ({
              aliveTimeSec: 60,
              autoplay: false,
              previs: false,
              scene,
            })}
          />
        </footer>
      </CmdConfigArea>
    </>
  );
}

////////////////////////////////////////////////////////////////////////////////

/** Dropdown to add a command */
function CmdAdder<T>(props: {
  root?: boolean;
  wrap: (cmd: C.Overlay) => T;
  onCreate?: () => void;
  optic: O.Prism<FormConfig, any, T[]>;
}): JSX.Element;
function CmdAdder(props: {
  root?: boolean;
  onCreate?: () => void;
  optic: O.Prism<FormConfig, any, C.Overlay[]>;
}): JSX.Element;
function CmdAdder<T = C.Overlay>(props: {
  root?: boolean;
  wrap?: (cmd: C.Overlay) => T;
  onCreate?: () => void;
  optic: O.Prism<FormConfig, any, T[]>;
}): JSX.Element {
  const s = useOverlayForm()!;
  function onAdd(tag: C.Overlay["tag"]) {
    const { wrap, onCreate } = props;
    let cmd: any = C.create(tag);
    if (wrap !== undefined) cmd = wrap(cmd);
    s.modify(props.optic, (ss) => [...ss, cmd as T]);
    onCreate?.();
  }
  const tags = useAvailableTags(() => props.root ?? false);
  return (
    <Dropdown2 title="Add an overlay">
      {tags().map((tag) => (
        <li>
          <a onClick={() => onAdd(tag)}>{tag}</a>
        </li>
      ))}
    </Dropdown2>
  );
}

/** Dropdown to add a command */
function CmdSelector(props: {
  optic: O.Prism<FormConfig, any, undefined | C.Overlay>;
}): JSX.Element {
  const s = useOverlayForm()!;
  function onSelect(tag: C.Overlay["tag"]) {
    log.debug("onSelect", tag, props.optic, s.preview(props.optic));
    detailsRef!.open = false;
    s.set(props.optic, C.create(tag));
  }
  const tags = useAvailableTags(() => false);
  let detailsRef: HTMLDetailsElement | undefined;
  return (
    <div class={theme.cmdSelector}>
      <details class={theme.cmdAdder} role="list" ref={(r: HTMLDetailsElement) => (detailsRef = r)}>
        <summary aria-haspopup="listbox">{s.preview(props.optic)?.tag ?? "Add command"}</summary>
        <ul role="listbox">
          {tags().map((tag) => (
            <li>
              <a onClick={() => onSelect(tag)}>{tag}</a>
            </li>
          ))}
        </ul>
      </details>
      <Show when={s.preview(props.optic) !== undefined}>
        <KeyInput width="120px" optic={props.optic.optional()} />
      </Show>
    </div>
  );
}

function useAvailableTags(root: () => boolean = () => false): () => readonly C.Overlay["tag"][] {
  const getCupId = useContext(CupProviderCtx);
  const getCupStageId = useContext(CupStageProviderCtx);
  return createMemo(() => {
    const r: Set<C.Overlay["tag"]> = new Set();
    if (root()) {
      r.add("style");
    }
    r.add("html");
    r.add("img");
    r.add("video");
    r.add("chat");
    r.add("scenes");
    const cupId = getCupId?.();
    const stageId = getCupStageId?.();
    if (cupId === undefined) {
      r.add("cup-stage-provider");
    } else {
      r.add("cup-countdown");
      r.add("cup-standings");
      r.add("cup-final-standings");
      r.add("cup-bracket");
      r.add("cup-header");
      r.add("cup-backdrop");
    }
    if (stageId === undefined) {
      r.add("cup-stage-provider");
    } else {
      r.add("cup-match");
    }
    return Array.from(r);
  });
}

const AncestorsContext = createContext<(() => string | undefined)[]>();
function Parent(props: { name: string | undefined; children: JSX.Element }) {
  const arr = useContext(AncestorsContext) ?? [];
  return (
    <AncestorsContext.Provider value={[...arr, () => props.name]}>
      {props.children}
    </AncestorsContext.Provider>
  );
}

function useAncestors() {
  return useContext(AncestorsContext) ?? [];
}

function overlayName(overlay: undefined | C.Overlay) {
  if (overlay === undefined) return undefined;
  return overlay.key === "" ? overlay.tag : overlay.key;
}
