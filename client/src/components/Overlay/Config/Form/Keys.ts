import * as fp from "fp-ts";

export default class Keys {
  private dict: Record<string, number> = {};

  public add(key: string, prev?: string): string {
    key = stripKeySuffix(key);
    if (prev !== undefined) {
      this.remove(prev);
    }
    const suffix = (this.dict[key] = (this.dict[key] ?? 0) + 1);
    return key + "-" + suffix;
  }

  public remove(key: string) {
    key = stripKeySuffix(key);
    const v = this.dict[key];
    if (v !== undefined) {
      if (v < 1) {
        delete this.dict[key];
      } else {
        this.dict[key]--;
      }
    }
  }
}

export function stripKeySuffix(key: string) {
  return key.replace(/\/+/g, "/").replace(/-[0-9]+$/, "");
}

export const keyOrd = fp.ord.contramap<string, [key: undefined | string, optic: any]>(
  (a) => a[0] ?? ""
)(fp.string.Ord);

export const keyEq = fp.eq.fromEquals<[key: undefined | string, optic: any]>(
  (a, b) => (a[0] ?? "") === (b[0] ?? "")
);

export function keyName(tag: undefined | string): undefined | string {
  if (tag === undefined || tag === "") return undefined;
  if (tag === "cup-stage-provider") return "csp";
  return tag;
}
