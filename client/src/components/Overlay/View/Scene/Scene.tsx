import classNames from "classnames";
import { createContext, createEffect, createMemo, JSX, useContext } from "solid-js";
import { SceneCtx } from "./Context";
import theme from "./theme.module.scss";

interface SetSceneProps {
  visible(): boolean;
  previs: boolean;
}

const SetScene = createContext<SetSceneProps>();

export function useSetScene() {
  return useContext(SetScene);
}

export function Scene(props: {
  index: number;
  children: JSX.Element;
  class?: string;
  onVisible?: () => void;
  onHide?: () => void;
  previs?: boolean;
  centreContent?: boolean;
}) {
  const { getCurrentScene } = useContext(SceneCtx)!;
  const visible = createMemo(() => props.index === getCurrentScene()?.[0]);
  if (props.onVisible || props.onHide)
    createEffect(() => {
      if (visible()) {
        props.onVisible?.();
      } else {
        props.onHide?.();
      }
    });
  return (
    <div
      class={classNames(theme.scene, props.class)}
      classList={{ [theme.sceneCentreContent]: props.centreContent }}
      data-visible={visible()}
    >
      <SetScene.Provider value={{ visible, previs: props.previs ?? false }}>
        {props.children}
      </SetScene.Provider>
    </div>
  );
}
