import { createEffect, For, JSX } from "solid-js";
import { usePeer } from "../../Peer";
import { useListen } from "../RemoteListener";
import * as OC from "../types";
import { default as SceneBar } from "./Bar";
import { SceneProvider } from "./Context";
import { Scene } from "./Scene";

export { default as SceneBar } from "./Bar";
export { SceneProvider, useScene } from "./Context";
export { Scene } from "./Scene";

export default function Scenes(props: {
  scenes: OC.Overlay & {
    tag: "scenes";
    key: string;
    scenes: (OC.Scene & { scene: { key: string } })[];
  };
  children: (el: OC.KeyedOverlay) => JSX.Element;
}) {
  const listen = useListen();
  const ctx = usePeer<"view">()!;
  return (
    <SceneProvider
      count={props.scenes.scenes.length}
      duration={(i) => props.scenes.scenes[i].aliveTimeSec}
      hook={(so) => {
        const {
          scenes: { key, scenes },
        } = props;
        const k2i: Record<string, number> = {};
        for (let i = 0; i < props.scenes.scenes.length; i++) {
          k2i[scenes[i].scene.key] = i;
        }
        listen(key, "scene-next", so.next);
        listen(key, "scene-prev", so.prev);
        listen(key, "scene-pause", so.pause);
        listen(key, "scene-set", (cmd) => {
          const sc = k2i[cmd.sceneKey];
          if (sc !== undefined) {
            so.jump(sc);
          } else {
            log.warn("unknown scene", sc);
            log.warn("available scenes are", Object.keys(k2i));
          }
        });
        createEffect(() => {
          const [ix] = so.scene();
          if (scenes[ix].autoplay) {
            so.play();
          } else {
            so.pause();
          }
        });
        createEffect(() => {
          const [ix] = so.scene();
          const peer = ctx.peer!;
          if (peer.status()?.conn) {
            peer.sendQueue.splice(0);
            peer.send(
              {
                tag: "scene-info",
                key,
                scene: [scenes[ix].scene.key, ix],
              },
              true
            );
          }
        });
      }}
    >
      <For each={props.scenes.scenes}>
        {(sc, ix) => (
          <Scene index={ix()} previs={sc.previs}>
            {props.children(sc.scene)}
          </Scene>
        )}
      </For>
      <SceneBar />
    </SceneProvider>
  );
}
