import { createEffect, createMemo, createSignal, onCleanup, useContext } from "solid-js";
import { SceneCtx, sceneSwitchDuration } from "./Context";
import theme from "./theme.module.scss";

function setupSceneBar(div: HTMLDivElement) {
  const [init, setInit] = createSignal(false);
  const { getCurrentScene, getSwitching, manualMode } = useContext(SceneCtx)!;
  let killed = false;
  onCleanup(() => (killed = true));
  setTimeout(() => setInit(true), sceneSwitchDuration);
  createEffect(() => {
    if (!init() || killed) return;
    const sw = getSwitching();
    const [_sc, dur] = getCurrentScene();
    if (sw || manualMode()) {
      div.style.transitionDuration = "0s";
      div.style.transform = "scaleX(0.0)";
    } else {
      div.style.transitionDuration = dur * 1000 - sceneSwitchDuration + "ms";
      div.style.transform = "scaleX(1.0)";
    }
  });
}

export default function SceneBar(props: { track?: number }) {
  const { manualMode, outerScenes } = useContext(SceneCtx)!;
  return createMemo(() => {
    const mm = manualMode();
    const track = props.track ?? outerScenes() - 1;
    if (mm) return undefined;
    return (
      <div
        class={theme.sceneBar}
        ref={setupSceneBar}
        style={{
          transform: "scaleX(0.0)",
          transitionDuration: "0ms",
          bottom: `${12 + track * 8}px`,
        }}
      />
    );
  });
}
