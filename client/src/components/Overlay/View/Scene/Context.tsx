import log from "loglevel";
import {
  batch,
  createContext,
  createEffect,
  createMemo,
  createSignal,
  JSX,
  onCleanup,
  untrack,
  useContext,
} from "solid-js";
import { useSetScene } from "./Scene";
import theme from "./theme.module.scss";

export interface SceneProviderProps {
  key?: string;
  class?: string;
  count: number;
  hook?: (sceneObj: {
    scene(): [ix: number, duration: number];
    jump(scene: number): void;
    next(): void;
    prev(): void;
    pause(): void;
    play(): void;
  }) => void;
  duration(ix: number): number;
  children: JSX.Element;
  index?: number;
  manualMode?: boolean;
  visible?: () => boolean;
}

export interface SceneCtxProps {
  key: undefined | string;
  manualMode(): boolean;
  getCurrentScene(): [index: number, duration: number];
  /// Duration in ms
  getSwitching(): boolean;
  outerScenes(): number;
}

export function SceneProvider(props: SceneProviderProps) {
  const outerScene = useSetScene();
  const { visible, previs } = outerScene ?? alwaysVisible;
  const canRender = createMemo(() => visible() && props.count > 0);
  return createMemo(() =>
    previs || canRender() ? <_SceneProvider {...props} visible={visible} /> : undefined
  );
}

const alwaysVisible = { visible: () => true, previs: false };

function _SceneProvider(props: SceneProviderProps) {
  const [getActive, setActive] = createSignal<[number, number]>([
    0,
    untrack(() => props.duration(0)),
  ]);
  const [switching, setSwitching] = createSignal(false);
  const [manualMode, setManualMode] = createSignal(untrack(() => props.manualMode ?? false));
  let alive = true;

  untrack(() => {
    props?.hook?.({
      scene: getActive,
      jump(scene) {
        setManualMode(true);
        setActive([scene, props.duration(scene)]);
      },
      next() {
        const [active] = getActive();
        const nextScene = (active + 1) % props.count;
        const nextDuration = props.duration(nextScene);
        batch(() => {
          setManualMode(true);
          setActive([nextScene, nextDuration]);
        });
      },
      prev() {
        const [active] = getActive();
        const nextScene = mod(active - 1, props.count);
        const nextDuration = props.duration(nextScene);
        batch(() => {
          setManualMode(true);
          setActive([nextScene, nextDuration]);
        });
      },
      pause() {
        setManualMode(true);
      },
      play() {
        setManualMode(false);
      },
    });
  });

  let t0: undefined | NodeJS.Timeout;
  let t1: undefined | NodeJS.Timeout;
  function timers() {
    if (props.count <= 1) return;
    if (!alive) return;
    if (manualMode()) return;
    const [active, duration] = getActive();
    t0 = setTimeout(() => {
      const nextScene = (active + 1) % props.count;
      const nextDuration = props.duration(nextScene);
      log.info("scene switching", props.key, {
        nextScene,
        nextDuration,
        count: props.count,
      });
      batch(() => {
        setSwitching(true);
        setActive([nextScene, nextDuration]);
      });
      t1 = setTimeout(() => {
        setSwitching(false);
        timers();
      }, sceneSwitchDuration);
    }, duration * 1000);
  }

  onCleanup(() => {
    alive = false;
    if (t0 !== undefined) clearTimeout(t0);
    if (t1 !== undefined) clearTimeout(t1);
  });

  createEffect(() => {
    if (t1 !== undefined) clearTimeout(t1);
    if (t0 !== undefined) clearTimeout(t0);
    if (!manualMode()) {
      untrack(timers);
    }
  });

  const outerCtx = useContext(SceneCtx);
  const outerScenes = createMemo(() => Number(outerCtx?.outerScenes ?? 0) + 1);

  const value = untrack(() => ({
    key: props.key,
    getCurrentScene: getActive,
    getSwitching: switching,
    manualMode,
    outerScenes,
  }));

  return (
    <div
      class={theme.scenes + " " + (props.class ?? "")}
      data-index={getActive()[0]}
      data-count={props.count}
      data-visible={props.visible?.() ?? false}
    >
      <SceneCtx.Provider value={value}>{props.children}</SceneCtx.Provider>
    </div>
  );
}

export function useScene() {
  return useContext(SceneCtx)!.getCurrentScene;
}

export const SceneCtx = createContext<SceneCtxProps>();

export const sceneSwitchDuration = 200;

function mod(n: number, m: number): number {
  return ((n % m) + m) % m;
}
