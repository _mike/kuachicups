import { useTheme } from "../themes";

export default function BarSeriesTitle() {
  const theme = useTheme();
  return (
    <div>
      <label>{theme.seriesName}</label>
      <br />
      {theme.seriesSubtitle?.()}
    </div>
  );
}
