import { Provider as AuthProvider } from "@/context/Auth";
import "@/injectLogLevel"; // @ import so tsfmt doesn't put it on the bottom
import { Provider as RouterProvider } from "@/router";
import "@/serviceWorker";
import { render } from "solid-js/web";
import { decodeConfig } from "../Config/config";
import { OverlayFormContextProvider, useOverlayForm } from "../Config/Form/Context";
import { MessageType, Peer, PeerProvider } from "../Peer";
import { OverlaySecretProvider } from "../Secret";
import { OverlayThemeProvider } from "../themes";
import { RemoteListenerProvider } from "./RemoteListener";
import Viewer from "./Viewer";

log.setLevel(0);
log.debug("Starting kuachi.gg overlay");

function OverlayView() {
  const form = useOverlayForm()!;

  async function onViewCfgData(data: MessageType["view-cfg"]["recv"], self: Peer<"view-cfg">) {
    form.setStore(await decodeConfig(data.config));
    self.send({ tag: "got-config", config: data.config });
  }

  return (
    <PeerProvider
      type="view-cfg"
      onData={onViewCfgData}
      onConnect={(peer) => {
        // send them our config, if it matches initially it's no worries, they
        // can just continue
        peer.send({ tag: "got-config", config: form.configStr()! });
      }}
    >
      <RemoteListenerProvider>
        <Viewer />
      </RemoteListenerProvider>
    </PeerProvider>
  );
}

render(
  () => (
    <AuthProvider>
      <RouterProvider>
        <OverlaySecretProvider>
          <OverlayFormContextProvider disableRouteCfg>
            <OverlayThemeProvider>
              <OverlayView />
            </OverlayThemeProvider>
          </OverlayFormContextProvider>
        </OverlaySecretProvider>
      </RouterProvider>
    </AuthProvider>
  ),
  document.getElementById("app")!
);
