import { flatten, takeRight } from "fp-ts/lib/Array";
import { merge } from "lodash";
import { createEffect, createMemo, For, onCleanup, untrack } from "solid-js";
import { createStore, reconcile } from "solid-js/store";
import * as tmi from "tmi.js";
import theme from "./theme.module.scss";

const maxMsgs = 24;

interface Message {
  id: string;
  tags?: tmi.ChatUserstate;
  message?: string;
}

interface State {
  messages: Message[];
  badges:
    | undefined
    | {
        [badge: string]: {
          versions: {
            [version: string]: {
              image_url_1x: string;
              image_url_2x: string;
              image_url_4x: string;
              title: string;
              description: string;
              click_url: string;
            };
          };
        };
      };
}

function fakeMsgs() {
  const r: Message[] = [];
  for (let i = 0; i < maxMsgs; i++) {
    r.push({ id: "@@fake-" + i });
  }
  return r;
}

export default function Chat(props: { channel: string; hook?: (clear: () => void) => void }) {
  const [state, setState] = createStore<State>({
    messages: fakeMsgs(),
    badges: undefined,
  });

  const [heights, setHeights] = createStore<Record<string, number>>({});

  untrack(() => {
    props.hook?.(() => {
      setState("messages", fakeMsgs());
    });
  });

  const tops = createMemo(() => {
    let acc = 0;
    const sums = [];
    for (let i = 0; i < state.messages.length; i++) {
      sums.push(acc);
      acc += heights[state.messages[i].id!] ?? 0;
    }
    return { sum: acc, sums };
  });

  const client = new tmi.Client({
    options: { debug: true, messagesLogLevel: "info" },
    connection: {
      reconnect: true,
      secure: true,
    },
    channels: [props.channel],
  });
  client.connect().catch(console.error);

  async function onMessage(_channel: string, tags: tmi.ChatUserstate, message: string) {
    if (tags.id === undefined) return;
    const room = tags["room-id"];
    if (room !== undefined && state.badges === undefined) {
      const badgesRoom = await getBadges(room);
      const badgesGlobal = await getBadges();
      const badges = merge(badgesGlobal.badge_sets, badgesRoom.badge_sets);
      setState("badges", badges);
    }
    setState(
      "messages",
      reconcile(takeRight(maxMsgs)([...state.messages, { id: tags.id, tags, message }]), {
        key: "id",
        merge: false,
      })
    );
  }

  client.on("message", onMessage);
  client.on("cheer", onMessage);
  client.on("messagedeleted", (_channel, _username, _deleted, tags) => {
    const del = tags["target-msg-id"];
    if (del !== undefined) {
      setState("messages", reconcile(state.messages.filter((m) => m.id !== del)));
    }
  });

  onCleanup(() => client.disconnect());
  let div: HTMLDivElement | undefined;
  let actualHeight: number = 0;
  createEffect(() => {
    actualHeight = div!.getBoundingClientRect().height;
  });
  return (
    <div class={theme.chat} ref={div}>
      <div class={theme.messages} style={{ height: tops().sum + "px" }}>
        <For each={state.messages}>
          {(m, index) => (
            <Message
              index={index()}
              fadeout={(() => {
                const height = tops().sum;
                const sums = tops().sums[index()];
                return height - actualHeight > sums;
              })()}
              badges={state.badges ?? {}}
              message={m}
              top={tops().sums[index()]}
              setHeight={(h) => setHeights(m.id!, h)}
              unsetHeight={() =>
                setHeights((h) => {
                  delete h[m.id!];
                  return h;
                })
              }
            />
          )}
        </For>
      </div>
    </div>
  );
}

function renderEmote(emote: string) {
  return `https://static-cdn.jtvnw.net/emoticons/v1/${emote}/2.0`;
}

function parseMessage(
  message: string,
  emotes: [emoteid: string, emotePositions: string[]][]
): ({ type: "emote"; img: string } | { type: "text"; text: string })[] {
  if (emotes === undefined || emotes.length === 0) return [{ type: "text", text: message }];
  const r: ReturnType<typeof parseMessage> = [];
  const parsedEmotes = flatten(
    emotes.map(([emoteid, ranges]) => {
      const emopos = [] as [start: number, end: number, emoteid: string][];
      for (const range of ranges) {
        const ms = range.match(/^([0-9]+)-([0-9]+)$/);
        if (ms === null) continue;
        emopos.push([Number(ms[1]), Number(ms[2]), emoteid]);
      }
      return emopos;
    })
  );
  parsedEmotes.sort((a, b) => a[0] - b[0]);
  let prior = 0;
  for (const [start, end, emoteid] of parsedEmotes) {
    if (prior < start) {
      r.push({ type: "text", text: message.slice(prior, start) });
    }
    r.push({ type: "emote", img: renderEmote(emoteid) });
    prior = end + 1;
  }
  if (prior < message.length) {
    r.push({ type: "text", text: message.slice(prior) });
  }
  return r;
}

function Message(props: {
  message: Message;
  badges: State["badges"];
  index: number;
  top: number;
  fadeout: boolean;
  setHeight(height: number): void;
  unsetHeight(): void;
}) {
  let div: HTMLDivElement | undefined;
  createEffect(() => {
    props.setHeight(div!.getBoundingClientRect().height);
    div!.style.opacity = "1";
  });
  createEffect(() => {
    if (props.fadeout) {
      div!.style.opacity = "0";
    }
  });
  onCleanup(() => props.unsetHeight());
  return (
    <div
      class={theme.message}
      data-fake={props.message.message === undefined || props.message.tags === undefined}
      style={{ transform: "translateY(" + props.top + "px)" }}
      ref={div}
    >
      <For each={Object.entries(props.message.tags?.badges ?? {})}>
        {(badge) => (
          <img
            class={theme.badge}
            data-badge={badge[0]}
            data-badge-version={badge[1]}
            src={badgeSrc(props.badges, badge[0], badge[1]!)}
          />
        )}
      </For>
      <b class={theme.username} style={{ color: props.message.tags?.color }}>
        {props.message.tags?.["display-name"]}
      </b>
      &nbsp;
      <span class={theme.content}>
        <For
          each={parseMessage(
            props.message?.message ?? "",
            Object.entries(props.message.tags?.emotes ?? {})
          )}
        >
          {(msg) => (msg.type === "text" ? msg.text : <img class={theme.emote} src={msg.img} />)}
        </For>
      </span>
    </div>
  );
}

function badgeSrc(badges: State["badges"], badge: string, version: string) {
  const versions = badges?.[badge]?.versions;
  const target = versions?.[version];
  if (target !== undefined) return target?.image_url_2x;
  return versions?.[1]?.image_url_2x;
}

async function getBadges(channel?: string): Promise<any> {
  const resp = await fetch(
    `https://badges.twitch.tv/v1/badges/${
      channel === undefined ? "global" : "channels/" + channel
    }/display?language=en`,
    {
      method: "GET",
      headers: new Headers({
        Accept: "application/vnd.twitchtv.v5+json",
        "Client-ID": "adzdkc2vfl1vaofuv6zg4jsj599kpq",
      }),
    }
  );
  return resp.json();
}
