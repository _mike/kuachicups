export type KeyedOverlay = { key: string | undefined } & Overlay;

export type Overlay =
  | Style
  | Html
  | Img
  | Video
  | Chat
  | Scenes
  | CupStageProvider
  | Cup
  | CupBracket
  | CupMatch
  | CupBackdrop;

export interface BaseOverlay {
  key?: string;
  display?: Overlay;
}

export interface Scene {
  aliveTimeSec: number;
  autoplay: boolean;
  previs: boolean;
  scene: Overlay;
}

export interface Scenes extends BaseOverlay {
  tag: "scenes";
  scenes: Scene[];
}

export interface Style extends BaseOverlay {
  tag: "style";
  style: string;
}

export interface Html extends BaseOverlay {
  tag: "html";
  props: string | Record<string, string>;
  content: string;
}

export interface Img extends BaseOverlay {
  tag: "img";
  src: string;
  width: string;
  height: string;
  background: boolean;
  display: undefined | Overlay;
}

export interface Video extends BaseOverlay {
  tag: "video";
  src: string;
  width: string;
  height: string;
  background: boolean;
  display: undefined | Overlay;
}

export interface CupBracket extends BaseOverlay {
  tag: "cup-bracket";
  roundCutoff: undefined | number;
  roundCutoffLb: undefined | number;
  groupNo: undefined | number;
  roundNo: undefined | number;
}

export interface CupBackdrop extends BaseOverlay {
  tag: "cup-backdrop";
  iframes: string[];
  width: number;
  height: number;
}

export interface Cup extends BaseOverlay {
  tag: "cup-standings" | "cup-final-standings" | "cup-countdown" | "cup-header";
  cupId?: undefined | string;
}

export interface CupStageProvider extends BaseOverlay {
  tag: "cup-stage-provider";
  cupId: undefined | string;
  stage: undefined | { id: string; stageNo: number };
  display: undefined | Overlay;
}

export interface CupMatch extends BaseOverlay {
  tag: "cup-match";
  matchId: number;
  startTime: undefined | Date;
}

export interface Chat extends BaseOverlay {
  tag: "chat";
  twitch: string;
}

export function isChat(cmd: Overlay): cmd is Chat {
  return cmd.tag === "chat";
}

export function isStyle(cmd: Overlay): cmd is Style {
  return cmd.tag === "style";
}

export function isHtml(cmd: Overlay): cmd is Html {
  return cmd.tag === "html";
}

export function isImg(cmd: Overlay): cmd is Img {
  return cmd.tag === "img";
}

export function isVideo(cmd: Overlay): cmd is Video {
  return cmd.tag === "video";
}

export function isScenes(cmd: Overlay): cmd is Scenes {
  return cmd.tag === "scenes";
}

export function isCupMatch(cmd: Overlay): cmd is CupMatch {
  return cmd.tag === "cup-match";
}

export function isCupBracket(cmd: Overlay): cmd is CupBracket {
  return cmd.tag === "cup-bracket";
}

export function isCupStageProvider(cmd: Overlay): cmd is CupStageProvider {
  return cmd.tag === "cup-stage-provider";
}

export function isCupBackdrop(cmd: Overlay): cmd is CupBackdrop {
  return cmd.tag === "cup-backdrop";
}

export function scene(cmd: Overlay): Scene {
  return {
    aliveTimeSec: 60,
    autoplay: false,
    previs: false,
    scene: cmd,
  };
}

export function create<T extends Overlay["tag"]>(tag: T): Overlay {
  switch (tag) {
    case "scenes":
      return { tag, scenes: [] };

    case "style":
      return { tag, style: "" };

    case "html":
      return {
        tag,
        props: {},
        content: "",
      };

    case "chat":
      return { key: "", tag, twitch: "kuachicups" };

    case "img":
    case "video":
      return {
        tag,
        src: "",
        width: "100vw",
        height: "100vh",
        background: true,
        display: undefined,
      };

    case "cup-standings":
    case "cup-final-standings":
    case "cup-header":
    case "cup-countdown":
      return { key: "", tag };

    case "cup-backdrop":
      return { key: "", tag, width: 1750, height: 600, iframes: [] };

    case "cup-bracket":
      return {
        tag,
        key: "",
        groupNo: undefined,
        roundCutoff: undefined,
        roundCutoffLb: undefined,
        roundNo: undefined,
      };

    case "cup-match":
      return { key: "", tag, startTime: undefined, matchId: -1 };

    case "cup-stage-provider":
      return {
        tag,
        cupId: undefined,
        stage: undefined,
        display: undefined,
      };
  }
  throw new Error("unsupported tag");
}

interface Ctx {
  scene: string[];
  cupId?: string;
  stageId?: string;
}

export function forOverlay_(
  cmd_: Overlay | Overlay[] | undefined,
  fn: (cmd: Overlay, ctx: Ctx) => void,
  ctx: Ctx = { scene: [] }
): void {
  let cmds: Overlay[];
  if (Array.isArray(cmd_)) {
    cmds = cmd_;
  } else if (cmd_ !== undefined) {
    cmds = [cmd_];
  } else {
    cmds = [];
  }

  for (const c of cmds) {
    fn(c, ctx);
    switch (c.tag) {
      case "scenes":
        forOverlay_(
          c.scenes.map((s) => s.scene),
          fn,
          { ...ctx, scene: [c.key!, ...ctx.scene] }
        );
        break;

      case "cup-stage-provider":
        forOverlay_(c.display, fn, {
          ...ctx,
          cupId: c.cupId,
          stageId: c.stage?.id,
        });
        break;
    }
  }
}
