import { CupProvider } from "@/components/Cups/Cup/Context";
import { CupStageProvider } from "@/components/Cups/Cup/Stage/Context";
import { createElementSizer } from "@/context/ElementSizer";
import { CaseOf } from "@/shared/Case";
import { For } from "solid-js";
import { createStore } from "solid-js/store";
import { styled } from "solid-styled-components";
import { useOverlayForm } from "../Config/Form/Context";
import Chat from "./Chat";
import CupBracketOverlay from "./Cup/Bracket";
// import CupHeader from "./Cup/";
import CupMatchGameOverlay, { MatchOverlayMode } from "./Cup/Match";
import Layout from './Layout';
import { CupFinalStandingsOverlay, CupStandingsOverlay } from "./Cup/Standings";
import "./globals.scss";
import { useListen } from "./RemoteListener";
import Scenes from "./Scene";
import * as OC from "./types";
import CupBottomBar from "./Cup/BottomBar";
import { is } from "superstruct";
import CupBackdrop from "./Cup/Backdrop";

const TARGET_WIDTH = 1920;
const TARGET_HEIGHT = 1080;

export default function Viewer() {
  const form = useOverlayForm()!;
  const sizer = createElementSizer();
  return (
    <div id="overlay-viewer" ref={(div) => sizer.manage(div)}>
      <For each={form.store.cmd}>{(cmd) => <RenderOverlay overlay={cmd} />}</For>
    </div>
  );
}

function RenderOverlay(props: { overlay: OC.Overlay | undefined }) {
  const listen = useListen();
  return (
    <CaseOf key="tag" data={props.overlay}>
      {{
        chat: (p) => (
          <Chat
            channel={p.twitch}
            hook={(clear) => {
              listen(p.key, "chat-clear", clear);
            }}
          />
        ),
        video: (p) => (
          <Video
            style={{
              width: p.width,
              height: p.height,
              position: p.background ? "absolute" : "relative",
              top: 0,
              left: 0,
            }}
            controls={false}
            loop={true}
            autoplay={true}
            muted={true}
          >
            <source src={p.src} />
          </Video>
        ),
        img: (p) => (
          <Img
            style={{
              width: p.width,
              height: p.height,
              position: p.background ? "absolute" : "relative",
              top: 0,
              left: 0,
            }}
            src={p.src}
          />
        ),
        scenes: (s) => <Scenes scenes={s as any}>{(i) => <RenderOverlay overlay={i} />}</Scenes>,
        html: (s) => <div {...s.props as object} innerHTML={s.content} />,
        style: (s) => <style>{s.style}</style>,
        // Cup overlays
        "cup-stage-provider": (s) =>
          typeof s.cupId === "string" ? (
            <CupProvider cupId={s.cupId}>
              {s.stage !== undefined ? (
                <CupStageProvider stageId={s.stage.id}>
                  <RenderOverlay overlay={s.display} />
                </CupStageProvider>
              ) : undefined}
            </CupProvider>
          ) : (
            <RenderOverlay overlay={s.display} />
          ),
        "cup-backdrop": (s) => <CupBackdrop {...s} />,
        "cup-bracket": (s) => (
          <CupBracketOverlay
            roundNo={s.roundNo}
            groupNo={s.groupNo}
            roundCutoff={s.roundCutoff}
            roundCutoffLb={s.roundCutoffLb}
            setupRefresh={(refresh) => {
              listen(s.key, "cup-bracket-refresh", refresh);
            }}
          />
        ),
        "cup-standings": () => <CupStandingsOverlay />,
        "cup-final-standings": () => <CupFinalStandingsOverlay />,
        "cup-match": (s) => <CupGameOverlayCmd {...s} />,
        // "cup-header": () => <CupHeader />,
      }}
    </CaseOf>
  );
}

const Video = styled.video``;

const Img = styled.img``;

function CupGameOverlayCmd(s: OC.CupMatch) {
  const [store, setStore] = createStore({
    high: 0,
    low: 0,
    mode: MatchOverlayMode.Repr.BeforeGames,
  });
  const listen = useListen();
  if (s.key !== undefined) {
    listen(s.key, "scene-set", (cmd) => {
      const m = MatchOverlayMode.fromString(cmd.key);
      if (m !== undefined) {
        setStore("mode", m);
      }
    });
    listen(s.key, "match-set-score", (cmd) => {
      setStore({ high: cmd.high, low: cmd.low });
    });
    listen(s.key, "match-set-view-mode", (cmd) => {
      setStore("mode", cmd.mode);
    });
  }
  return (
    <CupMatchGameOverlay
      highScore={store.high}
      lowScore={store.low}
      matchId={s.matchId}
      startTime={s.startTime}
      mode={store.mode}
    />
  );
}
