import { Show } from "solid-js";
import { createStore } from "solid-js/store";
import { styled } from "solid-styled-components";
import { useTheme } from "../../themes";
import Layout from "../Layout";
import { useListen } from "../RemoteListener";
import { CupBackdrop as CupBackdropProps } from "../types";
import CupBottomBar from "./BottomBar";

const IframeGrid = styled("div")({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  alignItems: "center",
  justifyItems: "center",
  gap: "var(--spacing)",
});

const Iframe = styled("iframe")({
  flexGrow: 1,
  overflow: "hidden",
  borderRadius: "var(--border-radius)",
  boxShadow: "var(--box-shadow)",
});

export default function CupBackdrop(props: Partial<CupBackdropProps>) {
  const theme = useTheme();
  const listen = useListen();
  const [s, setStore] = createStore({
    iframes: props.iframes ?? ([] as string[]),
  });
  listen(props.key, "set-cup-backdrop-iframes", (data) => {
    setStore({ iframes: data.iframes });
  });
  return (
    <Layout>
      <Layout.Content style={{ display: "grid", "align-self": "center", "justify-self": "center" }}>
        <IframeGrid
          style={{
            opacity: s.iframes.length === 0 ? 0 : 1,
            width: props.width + "px",
            height: props.height + "px",
          }}
        >
          {s.iframes.map((iframe) => (
            <Iframe src={iframe} height={props.height} />
          ))}
        </IframeGrid>
      </Layout.Content>
      <CupBottomBar />
    </Layout>
  );
}
