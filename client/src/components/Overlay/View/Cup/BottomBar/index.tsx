import { CupMatch2p } from "@/api";
import { useCup } from "@/components/Cups/Cup/Context";
import { SignupLoader } from "@/components/Cups/Cup/CupSignup";
import { useMatch } from "@/components/Cups/Cup/Match/Context";
import { useCupStage } from "@/components/Cups/Cup/Stage/Context";
import StageTitle from "@/components/Cups/Cup/Stage/Title";
import { themes, useTheme } from "@/components/Overlay/themes";
import { JSX, Show } from "solid-js";
import Layout from "../../Layout";
import BrandLogo from "../BrandLogo";
import css from "./theme.module.scss";

export default function CupBottomBar(props: { children?: JSX.Element }) {
  return (
    <Layout.Bar>
      <BarSeriesTitle />
      <Layout.BottomLogos />
      <BottomBarCupTitle />
    </Layout.Bar>
  );
}

export function BarSeriesTitle() {
  const theme = useTheme();
  return (
    <BarTitle
      class={css.series}
      header={fn(theme.seriesName)}
      footer={theme.seriesSubtitle}
    />
  );
}

export function BottomBarCupTitle() {
  const stage = useCupStage();
  const cup = useCup();
  return (
    <BarTitle
      class={css.BottomBarCupTitle}
      header={fn(cup.cup()?.title)}
      footer={fn(stage.stage()?.title)}
    />
  );
}

function fn<T>(t: T | undefined): undefined | (() => T) {
  return t === undefined ? undefined : () => t;
}

function BarTitle(props: { class: string; header?: () => JSX.Element; footer?: () => JSX.Element }) {
  return (
    <div
      class={css.bartitle + ' ' + props.class}
      data-hassubtitle={props.header !== undefined}
      data-hasseries={props.footer !== undefined}
    >
      {props.header === undefined ? undefined :
        <header>{props.header()}</header>
      }
      {props.footer === undefined ? undefined : (
        <footer>
          {props.footer()}
        </footer>
      )}
    </div>
  );
}

export function CupBottomBarScores(props: { high?: number; low?: number }) {
  const m = useMatch();
  return (
    <Show when={m.match()?.high_id !== undefined && m.match()?.low_id !== undefined}>
      <div class={css.scores}>
        <SignupLoader class={css.signupHigh} signup={m.match()?.high_id} align="right" />
        <b class={css.scoreHigh}>{props.high}</b>
        <span class={css.scoreDivider}> : </span>
        <b class={css.scoreLow}>{props.low}</b>
        <SignupLoader class={css.signupLow} signup={m.match()?.low_id} />
      </div>
    </Show>
  );
}
