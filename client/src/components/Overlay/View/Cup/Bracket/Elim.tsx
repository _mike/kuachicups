import { ArrayMap, CupMatch2p } from "@/api";
import { ElimMatchesSvg } from "@/components/Cups/Cup/Stage/ElimMatches";
import { Show } from "solid-js";
import Layout from "../../Layout";
import CupBottomBar, { BarSeriesTitle, BottomBarCupTitle } from "../BottomBar";
import BrandLogo from "../BrandLogo";
import css from "./theme.module.scss";

export default function Elim(props: { matches: ArrayMap<CupMatch2p, number> | undefined }) {
  return (
    <>
      <Layout.Content class={css.elimWrapper}>
        <Show when={props.matches}>
          {(ms) => <ElimMatchesSvg theme={css} matches={ms} onClickMatch={() => {}} />}
        </Show>
      </Layout.Content>
      <CupBottomBar />
    </>
  );
}
