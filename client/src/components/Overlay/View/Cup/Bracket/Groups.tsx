import { CupMatch2p } from "@/api";
import { GroupRoundTable } from "@/components/Cups/Cup/Stage/GroupMatches";
import { group } from "d3";
import { chunk } from "lodash";
import { createMemo, createSignal, For, Index } from "solid-js";
import Layout from "../../Layout";
import { Scene, SceneProvider } from "../../Scene";
import CupHeader from "../Header";
import theme from "./theme.module.scss";

const MAX_COL_PER_SCENE = 2;
const MAX_MATCH_PER_COL = 8;

export interface MatchCol {
  groupNo: number;
  roundNo: number;
  matches: CupMatch2p[];
}

export default function Groups(props: { matches: undefined | readonly CupMatch2p[] }) {
  const cols = createMemo(() => groupMatches(props.matches));
  const scenes = createMemo(() => chunk(cols(), MAX_COL_PER_SCENE));
  const [_getScene, setGetScene] = createSignal<() => number>(() => 0);

  return (
    <>
      <CupHeader />
      <Layout.Content>
        <SceneProvider
          hook={(sc) => setGetScene(() => () => sc.scene()[0])}
          class={theme.groupScenes}
          count={scenes().length}
          duration={() => 5}
        >
          <Index each={scenes()}>
            {(scene, index) => (
              <Scene class={theme.groupGrid} index={index}>
                <For each={scene()}>{(sc) => <GroupRoundTable {...sc} />}</For>
              </Scene>
            )}
          </Index>
        </SceneProvider>
      </Layout.Content>
    </>
  );
}

function groupMatches(matches: undefined | readonly CupMatch2p[]): MatchCol[] {
  const r: MatchCol[] = [];
  if (matches === undefined) return r;
  const ms = group(
    matches,
    (m) => m.group_round,
    (m) => m.group_no
  );
  ms.forEach((im0) => {
    im0.forEach((matches) => {
      if (matches.length > 0) {
        const { group_no: groupNo, group_round: roundNo } = matches[0];
        if (typeof groupNo !== "number" || typeof roundNo !== "number") return;
        for (const matchChunk of chunk(matches, MAX_MATCH_PER_COL)) {
          r.push({ groupNo, roundNo, matches: matchChunk });
        }
      }
    });
  });
  return r;
}
