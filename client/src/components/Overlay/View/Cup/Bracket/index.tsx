import { CupMatch2pWithStage } from "@/api";
import { useCupStage } from "@/components/Cups/Cup/Stage/Context";
import { createMemo, Match, Switch } from "solid-js";
import Layout from "../../Layout";
import Elim from "./Elim";
import Groups from "./Groups";

export default function CupBracketOverlay(props: {
  roundCutoff: undefined | number;
  roundCutoffLb: undefined | number;
  groupNo: undefined | number;
  roundNo: undefined | number;
  setupRefresh: (fetch: () => void) => void;
}) {
  const stageCtx = useCupStage();
  const matches = createMemo(() => {
    const { groupNo, roundNo, roundCutoff, roundCutoffLb } = props;
    let matches = stageCtx.matches();
    if (matches === undefined) return;
    // Filter by group settings
    if (groupNo !== undefined || roundNo !== undefined) {
      const r: { map: Record<number, CupMatch2pWithStage>; array: CupMatch2pWithStage[] } = {
        map: {},
        array: [],
      };
      for (const m of matches.array) {
        if (
          (groupNo === undefined || m.group_no === groupNo) &&
          (roundNo === undefined || m.group_round === roundNo)
        ) {
          r.map[m.lid] = m;
          r.array.push(m);
        }
      }
      matches = r;
    }
    // Filter by elim settings
    if (roundCutoff !== undefined) {
      const r: { map: Record<number, CupMatch2pWithStage>; array: CupMatch2pWithStage[] } = {
        map: {},
        array: [],
      };
      for (const m of matches.array) {
        const round = m.elim_logical_round ?? m.elim_round;
        if (
          (m.elim_type === "WB" && typeof round === "number" && round >= roundCutoff) ||
          (m.elim_type === "LB" &&
            typeof round === "number" &&
            typeof roundCutoffLb === "number" &&
            round >= roundCutoffLb) ||
          m.elim_type === "GF1" ||
          m.elim_type === "GF2"
        ) {
          r.map[m.lid] = m;
          r.array.push(m);
        }
      }
      matches = r;
    }
    return matches;
  });

  return (
    <Layout>
      <Switch>
        <Match when={stageCtx?.stage()?.format === "groups" && matches()?.array?.length! > 0}>
          <Groups matches={matches()?.array} />
        </Match>
        <Match
          when={stageCtx?.stage()?.format?.endsWith?.("_elim") && matches()?.array?.length! > 0}
        >
          <Elim matches={matches()} />
        </Match>
      </Switch>
    </Layout>
  );
}
