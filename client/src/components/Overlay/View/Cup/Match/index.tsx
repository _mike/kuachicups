import { api } from "@/api";
import { useCup } from "@/components/Cups/Cup/Context";
import { SignupLoader, SignupProvider } from "@/components/Cups/Cup/CupSignup";
import {
  getActorFromStepKind,
  isWinner,
  MatchProvider,
  NicePickbanStep,
  PickbanStepRule,
  subPickItemName,
  useMatch,
} from "@/components/Cups/Cup/Match/Context";
import { MatchTitle } from "@/components/Cups/Cup/Match/MatchTitle";
import PoolCard from "@/components/Cups/Cup/Match/PoolCard";
import { PickbanImageProvider } from "@/components/Cups/Cup/Match/PoolCard/Image";
import { winLoss } from "@/components/Player/CupSignupMatchResultsTable";
import { CupSignupResultsTable } from "@/components/Player/CupSignupResultsTable";
import { useAsync } from "@/shared/async/Awaitable";
import { Countdown, DateTime } from "@/shared/ui/Time";
import classNames from "classnames";
import { createComputed, createMemo, For, Index } from "solid-js";
import Layout from "../../Layout";
import { Scene, SceneProvider, useScene } from "../../Scene";
import { BarSeriesTitle, CupBottomBarScores } from "../BottomBar";
import theme from "./theme.module.scss";

export namespace MatchOverlayMode {
  export enum Repr {
    BeforeGames,
    Pickbans,
    IngameSpectator,
    IngamePlayer,
    BetweenGames,
    AfterGames,
  }

  export type Strings = string & keyof typeof Repr;

  export function fromString(str: string): Repr | undefined {
    if (str in Repr) {
      return Repr[str as Strings];
    }
  }

  export function label(self: Repr): string {
    switch (self) {
      case Repr.BeforeGames:
        return "Before games";
      case Repr.Pickbans:
        return "Picks and bans";
      case Repr.IngameSpectator:
        return "Spectator view";
      case Repr.IngamePlayer:
        return "Player view";
      case Repr.BetweenGames:
        return "Between games";
      case Repr.AfterGames:
        return "After games";
    }
  }
}

export default function CupMatchGameOverlay(props: {
  matchId: number;
  startTime: undefined | Date;
  mode: MatchOverlayMode.Repr;
  highScore: undefined | number;
  lowScore: undefined | number;
}) {
  return (
    <MatchProvider match={props.matchId}>
      <SceneProvider
        count={4}
        duration={() => 0}
        manualMode
        hook={(s) => {
          createComputed(() => {
            s.jump(props.mode);
          });
        }}
      >
        <Layout
          style={{
            "--header-height": props.mode === MatchOverlayMode.Repr.Pickbans ? "0px" : "200px",
          }}
          transparent={
            props.mode === MatchOverlayMode.Repr.IngamePlayer ||
            props.mode === MatchOverlayMode.Repr.IngameSpectator
          }
        >
          <Scene class={Layout.inherit} index={MatchOverlayMode.Repr.BeforeGames}>
            <BeforeGames startTime={props.startTime} />
          </Scene>
          <Scene class={Layout.inherit} index={MatchOverlayMode.Repr.Pickbans}>
            <Pickbans highScore={props.highScore} lowScore={props.lowScore} />
          </Scene>
          <Scene class={Layout.inherit} index={MatchOverlayMode.Repr.IngamePlayer}>
            <Ingame />
          </Scene>
          <Scene class={Layout.inherit} index={MatchOverlayMode.Repr.IngameSpectator}>
            <Ingame />
          </Scene>
          <Scene class={Layout.inherit} index={MatchOverlayMode.Repr.BetweenGames}>
            <BetweenGames />
          </Scene>
          <Scene class={Layout.inherit} index={MatchOverlayMode.Repr.AfterGames}>
            <AfterGames />
          </Scene>
          <Layout.Bar small={props.mode === MatchOverlayMode.Repr.IngameSpectator}>
            <BarSeriesTitle />
            <Layout.BottomLogos />
            <CupBottomBarScores high={props.highScore} low={props.lowScore} />
          </Layout.Bar>
        </Layout>
      </SceneProvider>
    </MatchProvider>
  );
}

////////////////////////////////////////////////////////////////////////////////

function BeforeGames(props: { startTime: undefined | Date }) {
  const m = useMatch();
  return (
    <>
      <Layout.Header>
        <Layout.Middle>
          <MatchHeader />
          {props.startTime === undefined ? undefined : (
            <h2 class={theme.startTime}>
              Match commencing soon <DateTime time={props.startTime} timeZone="Australia/Sydney" />{" "}
              Starts <Countdown tick="s" target={props.startTime} />
            </h2>
          )}
        </Layout.Middle>
      </Layout.Header>
      <Layout.Content class={theme.splitLayout}>
        <PastResultsOf signupId={m.match()?.high_id} />
        <PastResultsOf signupId={m.match()?.low_id} />
      </Layout.Content>
    </>
  );
}

function PastResultsOf(props: { signupId: string | null | undefined }) {
  function SceneName() {
    const scene = useScene();
    return <>{scene()?.[0] === 0 ? "Recent matches" : "Recent cups"}</>;
  }
  return (
    <SignupProvider signup={props.signupId}>
      <SceneProvider class={theme.pastResultsOfScenes} count={2} duration={() => 15}>
        <article class={"flex " + theme.pastResultsOf}>
          <header>
            <h2 style="--avatar-size-override: 1em">
              <SignupProvider.Img /> &nbsp;
              <SignupProvider.Name /> &mdash; <SceneName />
            </h2>
          </header>
          <div class="flush">
            <Scene class={theme.resultsScene} index={0}>
              <PastMatchResults />
            </Scene>
            <Scene class={theme.resultsScene} index={1}>
              <PastCupResults />
            </Scene>
          </div>
        </article>
      </SceneProvider>
    </SignupProvider>
  );
}

function PastMatchResults() {
  const getSignup = SignupProvider.use();
  const results = useAsync(async () => {
    const props = getSignup();
    if (props === undefined) return;
    if (props.player === undefined) return;
    return api.cupsCupFinishedMatchesForPlayers([props.player.id]);
  });
  return (
    <table>
      <thead>
        <tr>
          <th>Opponent</th>
          <th>Score</th>
        </tr>
      </thead>
      <tbody>
        <For each={results()}>
          {(r) => (
            <tr>
              <td>
                <SignupLoader signup={r.am_high ? r.match_.low_id : r.match_.high_id} />
              </td>
              <td>{winLoss(r.match_, r.am_high ? "high" : "low", "")}</td>
            </tr>
          )}
        </For>
      </tbody>
    </table>
  );
}

function PastCupResults() {
  const getSignup = SignupProvider.use();
  const results = useAsync(async () => {
    const props = getSignup();
    if (props === undefined) return;
    if (props.player === undefined) return;
    return (await api.playerPastResults(props.player.id)).cup_results;
  }, []);
  return <CupSignupResultsTable disableTime results={results()} />;
}

function MatchHeader() {
  const c = useCup();
  const m = useMatch();
  return (
    <h1>
      {c.cup()?.title} &mdash; <MatchTitle match={m.match()} />
    </h1>
  );
}

////////////////////////////////////////////////////////////////////////////////

function Ingame() {
  return <Layout.FloatingLogos />;
}

////////////////////////////////////////////////////////////////////////////////

function BetweenGames() {
  const m = useMatch();
  return (
    <>
      <Layout.Header>
        <Layout.Middle>
          <MatchHeader />
          <h2>Match underway</h2>
        </Layout.Middle>
      </Layout.Header>
      <Layout.Content class={theme.splitLayout}>
        <PastResultsOf signupId={m.match()?.high_id} />
        <PastResultsOf signupId={m.match()?.low_id} />
      </Layout.Content>
    </>
  );
}

////////////////////////////////////////////////////////////////////////////////

function AfterGames() {
  const m = useMatch();
  return (
    <>
      <Layout.Header>
        <Layout.Middle>
          <MatchHeader />
          <h2>Thanks for watching!</h2>
        </Layout.Middle>
      </Layout.Header>
      <Layout.Content class={theme.splitLayout}>
        <PastResultsOf signupId={m.match()?.high_id} />
        <PastResultsOf signupId={m.match()?.low_id} />
      </Layout.Content>
    </>
  );
}

////////////////////////////////////////////////////////////////////////////////

function Pickbans(props: { highScore: number | undefined; lowScore: number | undefined }) {
  const m = useMatch();

  m.startInterval();

  function MapBanCard(props: { rule: PickbanStepRule; step: undefined | NicePickbanStep }) {
    return (
      <PoolCard
        theme={theme}
        class={classNames(theme.card, theme.mapBan, {
          [theme.next]: props.rule.index === m?.pickbanState?.()?.next_step?.pickban_index,
          [theme.hidden]: props.step?.item_name === undefined,
        })}
        isNext={props.rule.index === m?.pickbanState?.()?.next_step?.pickban_index}
        pickIndex={props.rule.pick_index}
        poolName={props.rule.pool_name}
        itemName={props.step?.item_name!}
        signupId={getActorFromStepKind(props.rule.kind, m.match())}
        high={props.rule?.kind?.startsWith("high")}
        low={props.rule?.kind?.startsWith("low")}
        ban
      />
    );
  }

  function Card(props: { rule: PickbanStepRule; step: undefined | NicePickbanStep }) {
    return (
      <div class={theme.pickbanRow}>
        <PoolCard
          theme={theme}
          class={classNames(theme.card, theme.pick, {
            [theme.hidden]: props.step?.item_name === undefined,
          })}
          isNext={props.rule.index === m?.pickbanState?.()?.next_step?.pickban_index}
          pickIndex={props.rule.pick_index}
          poolName={props.rule.pool_name}
          itemName={props.step?.item_name!}
          signupId={getActorFromStepKind(props.rule.kind, m.match())}
          high={props.rule?.kind?.startsWith("high")}
          low={props.rule?.kind?.startsWith("low")}
        />
        <For
          each={m
            .pickbanRules()
            ?.steps?.filter?.(
              (m) => m.parent_index === props.rule.index && m.kind.endsWith("_ban")
            )}
        >
          {(sub) => (
            <PoolCard
              theme={theme}
              class={classNames(theme.card, theme.ban, {
                [theme.hidden]: subPickItemName(sub, m) === undefined,
              })}
              pickIndex={sub.sub_pick_index}
              signupId={getActorFromStepKind(sub.kind, m.match())}
              poolName={sub.pool_name}
              itemName={subPickItemName(sub, m)!}
              high={sub.kind.startsWith("high")}
              low={sub.kind.startsWith("low")}
              isNext={sub.index === m?.pickbanState?.()?.next_step?.pickban_index}
              ban
            />
          )}
        </For>
        <For each={props.rule.sub_picks}>
          {(sub) => (
            <PoolCard
              theme={theme}
              class={classNames(theme.card, theme.subpick, {
                [theme.hidden]: subPickItemName(sub, m) === undefined,
              })}
              isNext={sub.index === m?.pickbanState?.()?.next_step?.pickban_index}
              pickIndex={sub.sub_pick_index}
              signupId={getActorFromStepKind(sub.kind, m.match())}
              poolName={sub.pool_name}
              itemName={subPickItemName(sub, m)!}
              high={sub.kind.startsWith("high")}
              low={sub.kind.startsWith("low")}
            />
          )}
        </For>
      </div>
    );
  }

  return (
    <Layout.Content class={theme.pickbans}>
      <PickbanImageProvider pickbans={m.pickbanRules()}>
        <div class={theme.pickbanMapBanRow}>
          <For
            each={m
              .pickbanRules()
              ?.steps?.filter((m) => m.kind.endsWith("_ban") && m.pool_name === "Map")}
          >
            {(rule) => <MapBanCard rule={rule} step={m.pickbanState()?.steps?.[rule.index]} />}
          </For>
        </div>
        <For each={m.pickbanRules()?.pickSteps}>
          {(rule) => <Card rule={rule} step={m.pickbanState()?.steps?.[rule.index]} />}
        </For>
      </PickbanImageProvider>
    </Layout.Content>
  );
}
