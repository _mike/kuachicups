import { For, Show } from "solid-js";
import { api, CupStage } from "@/api";
import { useAsync } from "@/shared/async/Awaitable";
import { SignupLoader } from "@/components/Cups/Cup/CupSignup";
import { Countdown } from "@/shared/ui/Time";
import Chat from "../Chat";
import { SceneBar, SceneProvider } from "../Scene";
import theme from "./theme.module.scss";
import { useCup } from "@/components/Cups/Cup/Context";
import { parseDate } from "@/shared/date";

export function CupNotStartedOverlay(props: { channel?: string }) {
  const c = useCup();
  const stage0 = useAsync((ref) => api.cups.stages.byId(c.cupId, 0, ref));
  return (
    <stage0.Await>
      {(s) => (
        <SceneProvider
          class={props.channel !== undefined ? theme.cupNotStartedWithChat : theme.cupNotStarted}
          count={1}
          duration={(i) => durationOf(i, s)}
        >
          <Show when={!!s.starttime}>
            <div class={theme.cupNotStartedTimer}>
              <span>
                Starts <Countdown tick="s" target={s.starttime!} />
              </span>
            </div>
          </Show>
          <div class={theme.cupNotStartedChat}>
            {props.channel !== undefined && <Chat channel={props.channel!} />}
          </div>
          <div class={theme.cupNotStartedSignups}>
            <h1 class="title">Signups ({c.signups().array.length})</h1>
            <div class={theme.signups}>
              <For each={c.signups().array}>
                {(c) => <SignupLoader class={theme.signup} signup={c} showCheckin />}
              </For>
            </div>
          </div>
          <SceneBar />
        </SceneProvider>
      )}
    </stage0.Await>
  );
}

function durationOf(i: number, s: CupStage) {
  if (i === 0) {
    const st = parseDate(s.starttime);
    const now = Date.now();
    if (st instanceof Date && st.getTime() > now) {
      return (st.getTime() - now) * 0.001;
    }
  }
  return Number.MAX_SAFE_INTEGER;
}
