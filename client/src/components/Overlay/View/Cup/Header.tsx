import { Show } from "solid-js";
import { useTheme } from "../../themes";
import Layout from "../Layout";

export default function CupHeader() {
  const theme = useTheme();
  return (
    <Layout.Header>
      <Layout.TopLogo index={0} />
      <Show when={theme.seriesName !== undefined && theme.seriesSubtitle !== undefined}>
        <Layout.Middle>
          <h1>{theme.seriesName}</h1>
          <h2>{theme.seriesSubtitle}</h2>
        </Layout.Middle>
      </Show>
      <Layout.FloatingLogosStatic />
    </Layout.Header>
  );
}
