import { Ranking } from "@/api";
import { SignupLoader } from "@/components/Cups/Cup/CupSignup";
import { useCupStage } from "@/components/Cups/Cup/Stage/Context";
import StageTitle from "@/components/Cups/Cup/Stage/Title";
import Ordinal from "@/shared/Ordinal";
import { createMemo, For } from "solid-js";
import Layout from "../../Layout";
import css from "./theme.module.scss";

export function CupStandingsOverlay() {
  const stage = useCupStage();
  return (
    <Layout>
      <article>
        <header>Standings</header>
        <div class={css.cupOverlayStandings}>
          <For each={stage.rankings()?.rankings}>
            {(r: Ranking) => (
              <div class={css.cupOverlayStanding}>
                <SignupLoader signup={r.signup_id} />
                <Ordinal placement={r.placement} noIcon />
              </div>
            )}
          </For>
        </div>
      </article>
    </Layout>
  );
}

export function CupFinalStandingsOverlay() {
  const stage = useCupStage();
  const firstNon123Rank = createMemo(() => {
    return stage.rankings()?.rankings?.findIndex((m) => m.placement > 2);
  });
  function standingsFor(ranks: () => Ranking[]) {
    return (
      <For each={ranks()}>
        {(r) => (
          <div class={css.cupOverlayStanding}>
            <SignupLoader signup={r.signup_id} />
            <span>
              <Ordinal placement={r.placement} />
            </span>
          </div>
        )}
      </For>
    );
  }
  const rankings = createMemo(() => stage.rankings()?.rankings ?? []);
  return (
    <Layout>
      <article>
        <header>
          Final standings - <StageTitle stage={stage.stage()} />
        </header>
        <div class={css.cupOverlayStandingsFinalTop3}>
          {standingsFor(() => rankings().slice(0, firstNon123Rank()))}
        </div>
        <br />
        <div class={css.cupOverlayStandingsFinal}>
          {standingsFor(() => rankings().slice(firstNon123Rank(), rankings().length))}
        </div>
      </article>
    </Layout>
  );
}
