import { api, CupSignup, CupStage } from "@/api";
import { PendingMatch } from "@/components/Cups/Cup/Context";
import { CupSignupProps, SignupLoader, SignupView } from "@/components/Cups/Cup/CupSignup";
import { MatchName } from "@/components/Cups/Cup/Stage/MatchName";
import { CupSignupMatchResultsTable } from "@/components/Player/CupSignupMatchResultsTable";
import { CupSignupResultsTable } from "@/components/Player/CupSignupResultsTable";
import { useAsync } from "@/shared/async/Awaitable";
import { For, JSX } from "solid-js";
import { Scene, SceneBar, SceneProvider } from "../Scene";

const maxResults = 5;

export function MatchSignup(
  props: CupSignupProps & {
    wrapSignup?: (p: { children: JSX.Element }) => JSX.Element;
  }
) {
  const cupResults = useAsync(async () => {
    if (props.team === undefined && props.player !== undefined) {
      return (await api.players.results.byId(props.player.id, false)).cup_results.slice(
        0,
        maxResults
      );
    }
    return [];
  }, []);

  const matchResults = useAsync(async () => {
    if (props.team === undefined && props.player !== undefined) {
      const ms = await api.cupsCupFinishedMatchesForPlayers([props.player.id]);
      return ms;
    }
    return [];
  }, []);

  function WrapSignup(p: { children: JSX.Element }) {
    const wrapSignup = props.wrapSignup;
    if (typeof wrapSignup === "function") {
      return wrapSignup(p);
    }
    return p.children;
  }

  return (
    <div>
      <h1 class="title has-text-centered mb-3">
        <WrapSignup>{SignupView(props)}</WrapSignup>
      </h1>
      <SceneProvider count={2} duration={() => 15}>
        <Scene index={0}>
          <CupSignupMatchResultsTable
            cupId={props.signup!.cup_id}
            playerId={props.signup!.player_id}
            results={matchResults()}
            centre
          />
        </Scene>
        <Scene index={1}>
          <CupSignupResultsTable results={cupResults()} centre />
        </Scene>
      </SceneProvider>
    </div>
  );
}

export function CupStagePendingMatches(p: { stage: CupStage; pendingMatches: PendingMatch[] }) {
  return (
    <SceneProvider key="matches" count={p.pendingMatches.length} duration={() => 5}>
      <For each={p.pendingMatches}>
        {(m, i) => (
          <Scene index={i()}>
            <div class="overlay-middle">
              <div class="box cup-overlay-match">
                <h2 class="heading mb-3">
                  Match <MatchName match={m.match} />
                </h2>
                <div class="columns">
                  <div class="column">
                    <SignupLoader
                      signup={m.match.high_id ?? undefined}
                      render={MatchSignup}
                      size="2em"
                    />
                  </div>
                  <div class="column">
                    <SignupLoader
                      signup={m.match.low_id ?? undefined}
                      render={MatchSignup}
                      size="2em"
                    />
                  </div>
                </div>
              </div>
            </div>
          </Scene>
        )}
      </For>
      <SceneBar track={1} />
    </SceneProvider>
  );
}
