import { createEffect, createSignal, JSX, onCleanup } from "solid-js";
import { styled } from "solid-styled-components";

const scrollDuration = 40000;

function setupScroller(div: HTMLDivElement) {
  let alive = true;
  let scrollId = 0;
  const [height, setHeight] = createSignal(0);

  setInterval(() => {
    setHeight(div.scrollHeight);
  }, 1000);

  createEffect(() => {
    const h = height();
    if (h > 0) down(h, ++scrollId);
  });

  function down(h: number, myId: number) {
    if (!alive || myId !== scrollId) return;
    div.style.transform = `translateY(${-h}px)`;
    setTimeout(() => up(h, myId), scrollDuration);
  }

  function up(h: number, myId: number) {
    if (!alive || myId !== scrollId) return;
    div.style.transform = `translateY(${h}px)`;
    setTimeout(() => down(h, myId), scrollDuration);
  }

  onCleanup(() => {
    alive = false;
  });
}

const Div = styled.div`
  position: fixed;
  top: 0;
  transition-property: transform;
  transition-timing-function: linear;
  transform-origin: top;
`;

export default function VerticalScroller(props: { children: JSX.Element }) {
  return (
    <Div ref={setupScroller} style={{ "transition-duration": scrollDuration + "ms" }}>
      {props.children}
    </Div>
  );
}
