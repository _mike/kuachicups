import { usePromise } from "@/shared/async/Awaitable";
import classNames from "classnames";
import {
  Component,
  createContext,
  createSignal,
  JSX,
  Match,
  onCleanup,
  Show,
  splitProps,
  Switch,
  useContext,
} from "solid-js";
import { setup } from "solid-styled-components";
import { themes, useTheme } from "../../themes";
import { Scene, SceneProvider } from "../Scene";
import css from "./theme.module.scss";

function Layout(
  outerProps: { children: JSX.Element; transparent?: boolean } & JSX.IntrinsicElements["div"]
) {
  const [props, divProps] = splitProps(outerProps, ["children", "transparent", "class"]);
  const [hasHeader, setHasHeader] = createSignal(false);
  const [hasBar, setHasBar] = createSignal(false);
  function createHeader() {
    setHasHeader(true);
    onCleanup(() => setHasHeader(false));
  }
  function createBar() {
    setHasBar(true);
    onCleanup(() => setHasBar(false));
  }
  return (
    <>
      <div
        class={classNames(css.layout, props?.class)}
        data-transparent={props.transparent === true}
        data-header={hasHeader()}
        data-bar={hasBar()}
        {...divProps}
      >
        <Layout.Context.Provider value={{ createBar, createHeader }}>
          {props.children}
        </Layout.Context.Provider>
      </div>
      <Show when={!props.transparent}>
        <Layout.VideoBackground />
      </Show>
    </>
  );
}

namespace Layout {
  let lastVideoTime: number = 0;

  export function VideoBackground() {
    let ref: HTMLVideoElement | undefined = undefined;
    onCleanup(() => {
      if (ref === undefined) return;
      lastVideoTime = ref.currentTime;
    });
    const [opacity, setOpacity] = createSignal(0);
    return (
      <div class={css.background}>
        <video
          class={css.video}
          style={{ opacity: opacity(), transition: "opacity 1s" }}
          loop
          src="https://kuachi.gg/u/bg.mp4"
          ref={(self) => {
            ref = self;
            if (self.currentTime !== lastVideoTime) {
              self.currentTime = lastVideoTime;
              setTimeout(() => {
                self.play();
                setOpacity(1.0);
              }, 1000);
            } else {
              setTimeout(() => {
                const s = Math.random() * self.duration;
                if (Number.isFinite(s)) {
                  self.currentTime = s;
                }
                self.play();
                setOpacity(1.0);
              }, 1000);
            }
          }}
        />
      </div>
    );
  }

  interface ILayoutContext {
    createHeader(): void;
    createBar(): void;
  }

  export const Context = createContext<ILayoutContext>();

  export const Header: Component<JSX.IntrinsicElements["div"]> = (outerProps) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class"]);
    const ctx = useContext(Context);
    ctx?.createHeader();
    return <div class={classNames(css.header, ownProps.class)} {...divProps} />;
  };

  export const Bar: Component<JSX.IntrinsicElements["div"] & { small?: boolean }> = (
    outerProps
  ) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class", "small"]);
    const ctx = useContext(Context);
    ctx?.createBar();
    return (
      <div
        class={classNames(css.bar, ownProps.class)}
        classList={{ [css.small]: ownProps.small }}
        {...divProps}
      />
    );
  };

  export const Content: Component<JSX.IntrinsicElements["div"]> = (outerProps) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class"]);
    return <div class={classNames(css.content, ownProps.class)} {...divProps} />;
  };

  export const Floating: Component<JSX.IntrinsicElements["div"]> = (outerProps) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class"]);
    return <div class={classNames(css.floating, ownProps.class)} {...divProps} />;
  };

  export function TopLogos() {
    const theme = useTheme();
    return (
      <Switch>
        <Match when={theme.topLogos.length === 1}>
          <img class={css.logo} data-index={0} src={theme.topLogos[0]} />
        </Match>
        <Match when={theme.topLogos.length > 1}>
          {theme.topLogos.map((src, ix) => (
            <img class={css.logo} data-index={ix} src={src} />
          ))}
        </Match>
      </Switch>
    );
  }

  export function TopLogo(props: { index: number }) {
    const theme = useTheme();
    return (
      <Show when={theme.topLogos?.[props.index]}>
        {(src) => <img class={css.logo} data-index={props.index} src={src} />}
      </Show>
    );
  }

  export function FloatingLogosStatic() {
    const theme = useTheme();
    log.debug("FLOATNIG LOGOS", theme);
    return (
      <div class={css.floatingStatic}>
        <SceneProvider count={theme.floatingLogos.length} duration={() => 60}>
          {theme.floatingLogos.map((src, ix) => (
            <Scene index={ix}>
              <img class={css.floatingStaticImg} src={src} />
            </Scene>
          ))}
        </SceneProvider>
      </div>
    );
  }

  export function FloatingLogos() {
    const theme = useTheme();
    return (
      <div class={css.floating}>
        <Switch>
          <Match when={theme.floatingLogos.length === 1}>
            <img src={theme.floatingLogos[0]} />
          </Match>
          <Match when={theme.floatingLogos.length > 1}>
            <SceneProvider count={theme.floatingLogos.length} duration={() => 60}>
              {theme.floatingLogos.map((src, ix) => (
                <Scene index={ix}>
                  <img src={src} />
                </Scene>
              ))}
            </SceneProvider>
          </Match>
        </Switch>
      </div>
    );
  }

  export function BottomLogos() {
    const theme = useTheme();
    return (
      <div class={css.logo}>
        {theme.bottomMiddleLogos.map((src) => (
          <img src={src} />
        ))}
      </div>
    );
  }

  export const { left, middle, right, inherit, layout, logo } = css;

  export const Left: Component<JSX.IntrinsicElements["div"]> = (outerProps) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class"]);
    return <div class={classNames(css.left, ownProps.class)} {...divProps} />;
  };

  export const Middle: Component<JSX.IntrinsicElements["div"]> = (outerProps) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class"]);
    return <div class={classNames(css.middle, ownProps.class)} {...divProps} />;
  };

  export const Right: Component<JSX.IntrinsicElements["div"]> = (outerProps) => {
    const [ownProps, divProps] = splitProps(outerProps, ["class"]);
    return <div class={classNames(css.right, ownProps.class)} {...divProps} />;
  };
}

export default Layout;
