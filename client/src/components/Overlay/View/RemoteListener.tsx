import {
  createContext,
  createEffect,
  getOwner,
  JSX,
  onCleanup,
  runWithOwner,
  useContext,
} from "solid-js";
import { useOverlayForm } from "../Config/Form/Context";
import { MessageType, PeerProvider } from "../Peer";
import { RemoteCmd } from "../Remote/command";

export interface IRemoteListener {
  listen<T extends RemoteCmd["tag"]>(
    key: string | undefined,
    tag: T,
    fn: (cmd: RemoteCmd & { tag: T }) => void
  ): void;
}

const RemoteListenerContext = createContext<IRemoteListener>();

export function RemoteListenerProvider(props: { children: JSX.Element }) {
  const form = useOverlayForm()!;
  const listeners: Record<string, Record<string, (cmd: RemoteCmd) => void>> = {};
  function listen<T extends RemoteCmd["tag"]>(
    key: string | undefined,
    tag: T,
    fn: (cmd: RemoteCmd & { tag: T }) => void
  ) {
    if (key === undefined) {
      log.warn("Cannot listen for", tag);
      return;
    }
    listeners[key] = listeners[key] ?? {};
    listeners[key][tag] = fn as any;
    onCleanup(() => {
      delete listeners[key][tag];
      if (Object.keys(listeners[key]).length === 0) {
        delete listeners[key];
      }
    });
  }

  function onViewData(cmd: MessageType["view"]["recv"]) {
    const ls = listeners?.[cmd.key][cmd.tag];
    if (ls !== undefined) {
      try {
        ls(cmd);
      } catch (err) {
        log.error("onViewData caught", err);
      }
    }
  }

  const owner = getOwner()!;

  return (
    <PeerProvider
      type="view"
      onData={onViewData}
      onConnect={(peer) => {
        runWithOwner(owner, () => {
          createEffect(() => {
            const str = form.configStr();
            log.debug("config", str);
            if (str !== undefined) peer.send({ tag: "set-config", config: str });
          });
        });
      }}
    >
      <RemoteListenerContext.Provider value={{ listen }}>
        {props.children}
      </RemoteListenerContext.Provider>
    </PeerProvider>
  );
}

export function listen<T extends RemoteCmd["tag"]>(
  key: string | undefined,
  tag: T,
  fn: (cmd: RemoteCmd & { tag: T }) => void
) {
  const ctx = useContext(RemoteListenerContext)!;
  ctx.listen<T>(key, tag, fn);
}

export function useListen() {
  return useContext(RemoteListenerContext)!.listen;
}
