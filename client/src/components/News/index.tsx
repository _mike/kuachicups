import { PostAnn } from "@/api";
import useAuth from "@/context/Auth";
import { intParam } from "@/router";
import Resource from "@/shared/async/Resource";
import { Lazily, LazyProvider } from "@/shared/Lazy";
import Icon from "@/shared/ui/Icon";
import Menu from "@/shared/ui/Menu";
import { PagedProps, PageLinks, SimplePaged } from "@/shared/ui/Paginate";
import { JSX, Show } from "solid-js";
import { useRoute } from "solid-typefu-router5";
import theme from "./theme.module.scss";
import AnnounceView from "./View";
import Container from "@/shared/ui/Container";

export function NewsLayout(props: { children: JSX.Element }) {
  const auth = useAuth();
  return (
    <Container>
      <Menu.Layout
        class={theme.layout}
        menu={
          <Show when={auth().admin}>
            <Menu.Menus>
              <Menu>
                <Menu.Link to="news">News</Menu.Link>
                <Menu.Link to="news.unpublished">Unpublished news</Menu.Link>
                <Menu.Link to="news.new">
                  <Icon type="create" /> Create
                </Menu.Link>
              </Menu>
            </Menu.Menus>
          </Show>
        }
      >
        {props.children}
      </Menu.Layout>
    </Container>
  );
}

export default function News(props: { source: PagedProps<() => Promise<PostAnn>>["source"] }) {
  const route = useRoute();
  return (
    <div>
      <SimplePaged<() => Promise<PostAnn>>
        fallback="No news items"
        source={props.source}
        getPage={() => intParam(route().params?.page)}
        renderItem={(item, index) => (
          <div>
            <Lazily>
              <Resource key={`ann-${index()}`} data={item}>
                {(item) => <AnnounceView item={item} />}
              </Resource>
            </Lazily>
            <br />
          </div>
        )}
        renderItems={(p) => (
          <LazyProvider length={() => p.items.length} monotonic>
            <div class={theme.items}>{p.children}</div>
          </LazyProvider>
        )}
        renderContent={(content) => content}
        renderPageButtons={(p) => <PageLinks to="news" next={p.next} prev={p.prev} />}
      />
    </div>
  );
}
