import { api, PostAnn } from "@/api";
import useAuth, { RequireLogin } from "@/context/Auth";
import { navigate, router } from "@/router";
import Resource from "@/shared/async/Resource";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import { CheckBoxField, createField, MarkdownField, TextField } from "@/shared/ui/Form2";
import toast from "@/shared/ui/Toast";
import { For, Show, untrack } from "solid-js";
import { createStore } from "solid-js/store";

export default function NewAnn() {
  return (
    <RequireLogin>
      <Inner />
    </RequireLogin>
  );
}

export function EditAnn(props: { id: string }) {
  return (
    <Resource data={() => api.announcements.byId(props.id)}>
      {(ann) => <Inner ann={ann} />}
    </Resource>
  );
}

interface S {
  titleMd: string;
  contentMd: string;
  imageUri: string;
  publishImmediately: boolean | undefined;
  pings: Record<string, string>;
}

function Inner(props: { ann?: PostAnn }) {
  const auth = useAuth();
  const [state, setState] = createStore<S>(
    untrack(() => ({
      titleMd: props.ann?.title_md ?? "",
      contentMd: props.ann?.content_md ?? "",
      imageUri: props.ann?.image_uri ?? "",
      publishImmediately: props.ann?.is_published ? undefined : true,
      pings: {},
    }))
  );

  const { Fld, optic: sf } = createField(state, setState);

  function navigateToAnn(id: string) {
    router.navigate("announcements.announcement", {
      id,
    });
  }

  function messageLineMd() {
    return Object.entries(state.pings)
      .filter((t) => t[1] !== "")
      .map((t) => t[0])
      .sort()
      .join(" ");
  }

  async function onCreate() {
    const authorId = auth().login!.id;
    const { titleMd, contentMd, imageUri, publishImmediately } = state;
    const m = await api.postsCreateAnn({
      author_id: authorId,
      title_md: titleMd,
      content_md: contentMd,
      image_uri: imageUri === "" ? null : imageUri,
      message_line_md: messageLineMd(),
    });
    if (publishImmediately) {
      await api.postsPublishAnn(m.id, annUri(m.id));
      api.announcements.cache[m.id] = undefined;
    } else {
      api.announcements.cache[m.id] = Promise.resolve(m);
    }
    navigateToAnn(m.id);
  }

  async function onUpdate() {
    const authorId = auth().login!.id;
    const { titleMd, contentMd, imageUri, publishImmediately } = state;
    const ann = props.ann;
    if (ann === undefined) return;
    const isPublished = ann.is_published === true;
    const m = await api.postsUpdateAnn({
      content_md: contentMd,
      author_id: authorId,
      title_md: titleMd,
      image_uri: imageUri,
      message_line_md: messageLineMd(),
      id: ann.id,
    });
    if (!isPublished && publishImmediately) {
      await api.postsPublishAnn(m.id, annUri(m.id));
      api.announcements.cache[m.id] = undefined;
    } else {
      api.announcements.cache[m.id] = Promise.resolve(m);
    }
    navigateToAnn(m.id);
  }

  function onSubmit() {
    if (props.ann === undefined) {
      onCreate();
    } else {
      onUpdate();
    }
  }

  function onDelete() {
    api.postsDeleteAnn(props!.ann!.id).then(
      () => {
        navigate({ to: "news" });
      },
      () => {
        toast.failure("Failed to delete post");
      }
    );
  }

  return (
    <>
      <h2>{props.ann === undefined ? "Create announcement" : "Edit announcement"}</h2>
      <div>
        <Fld label="Title" optic={sf.prop("titleMd")}>
          {(value, onChange) => <TextField value={value()} onChange={onChange} />}
        </Fld>
        <Fld label="Content" optic={sf.prop("contentMd")}>
          {(value, onChange) => <MarkdownField value={value()} onChange={onChange} />}
        </Fld>
        <Fld
          label={
            <>
              Pings &mdash; <b>if updating an announcement, remember to update this!</b>
            </>
          }
          optic={sf.prop("pings")}
        >
          {(value, onChange) => (
            <fieldset>
              <For each={pings}>
                {(role) => (
                  <CheckBoxField
                    label={role.name}
                    onChange={(next) =>
                      onChange({ ...state.pings, [role.name]: next ? role.insert : "" })
                    }
                    value={!!value()?.[role.name]}
                  />
                )}
              </For>
            </fieldset>
          )}
        </Fld>
        <Fld label="Image URI" optic={sf.prop("imageUri")}>
          {(value, onChange) => <TextField value={value()} onChange={onChange} />}
        </Fld>
        <Fld optic={sf.prop("publishImmediately")}>
          {(value, onChange) => (
            <CheckBoxField label="Publish immediately" value={value()} onChange={onChange} />
          )}
        </Fld>
        <div class="buttons">
          <Show when={props.ann !== undefined}>
            <ConfirmButton class="is-danger" onConfirm={onDelete}>
              Delete
            </ConfirmButton>
          </Show>
          <button class="button is-primary" onClick={onSubmit}>
            {props.ann === undefined ? "Create" : "Update"}
          </button>
        </div>
      </div>
    </>
  ); // "New announcement";
}

const pings = [
  {
    name: "Competition Announcement",
    insert: "<@&819451835953446924>",
  },
  { name: "QC Competitions", insert: "<@&819451835953446924>" },
  { name: "QC Ranked Night", insert: "<@&819451835953446924>" },
  { name: "DBT Competitions", insert: "<@&819451687164444713>" },
  { name: "DBT Ranked Night", insert: "<@&819451188285014038>" },
  { name: "everyone", insert: "@everyone" },
];

export function annUri(id: string) {
  return (
    location.origin +
    router.buildPath("news.item", {
      id,
    })
  );
}

// p
// p              tuiRef={(tui) => {
//                 const roles = [
