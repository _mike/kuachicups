import { api, PostAnn } from "@/api";
import PlayerLink from "@/components/Player/Link";
import useAuth from "@/context/Auth";
import { Link, navigate, router } from "@/router";
import { DateTime } from "@/shared/ui/Time";
import toast from "@/shared/ui/Toast";
import { createSignal, Show } from "solid-js";
import { annUri } from "./New";
import theme from "./theme.module.scss";

export default function NewsView(p: { item: PostAnn }) {
  const auth = useAuth();
  const [heroActive, setHeroActive] = createSignal(false);
  return (
    <article class={theme.announceView}>
      <header class={theme.announceHeader}>
        <div class="headings">
          <h1>
            <Link to="news.item" params={{ id: p.item.id }}>
              {p.item.title_md}
            </Link>
          </h1>
          <h2>
            <PlayerLink playerId={p.item.author_id} />
            <small>
              &nbsp;published <DateTime time={p.item.created_at} />
            </small>
          </h2>
        </div>
        <Show when={auth().admin}>
          <div class="buttons">
            <Link display="button" to="news.item.edit" params={{ id: p.item.id }}>
              Edit
            </Link>
            <Show when={!p.item.is_published}>
              <button onClick={() => onPublish(p.item)}>Publish</button>
            </Show>
          </div>
        </Show>
      </header>
      {typeof p.item.image_uri === "string" && (
        <div
          class={theme.hero}
          onClick={() => setHeroActive((h) => !h)}
          onBlur={() => setHeroActive(false)}
          data-active={heroActive()}
        >
          <img src={p.item.image_uri} />
        </div>
      )}
      <div class="content" innerHTML={p.item.content_html ?? ""} />
    </article>
  );
}

function onPublish(item: PostAnn) {
  if (item.is_published) return;
  api.postsPublishAnn(item.id, annUri(item.id)).then(
    () => {
      toast.success("Published!");
      navigate({ to: "news" });
    },
    (err) => {
      toast.failure(() => (
        <>
          Could not publish
          <pre>{JSON.stringify(err, null, 2)}</pre>
        </>
      ));
    }
  );
}
