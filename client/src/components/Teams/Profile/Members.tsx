import { api, Role } from "@/api";
import { titleCase } from "@/components/App/Nav/Breadcrumbs";
import PlayerDropdown from "@/components/Player/Dropdown";
import PlayerLink from "@/components/Player/Link";
import useAuth from "@/context/Auth";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import toast from "@/shared/ui/Toast";
import { createSignal, For } from "solid-js";
import { createStore } from "solid-js/store";
import TeamLink from "../TeamLink";
import { useTeam } from "./Context";

export default function ManageMembers() {
  const ctx = useTeam();

  const [state, setState] = createStore({
    invite: undefined as string | undefined,
  });

  const auth = useAuth();

  async function onInvite() {
    const { invite } = state;
    if (invite === undefined) return;
    try {
      const invs = await api.teamsMakeInvite({
        player_id: invite,
        team_id: ctx.teamId,
        // bit of a wart that this field is here at all, but it is checked
        invite_by_player_id: auth().login!.id,
      });
      if (invs <= 0) {
        throw new Error("no invites made");
      }
      toast.success(() => (
        <span>
          Invite sent to <PlayerLink playerId={invite} />
        </span>
      ));
    } catch (e: any) {
      toast.failure(() => (
        <p>
          Cannot invite that player. They may already be in this team or have a pending invite to
          it.
        </p>
      ));
    }
    setState({ invite: undefined });
  }

  return (
    <>
      <h1 class="flex-row" style="justify-content:space-between">
        <span>
          Members of <TeamLink team={ctx.team()} />
        </span>
        <ctx.members.RefreshButton />
      </h1>

      <h2>Invite</h2>
      <div class="buttons">
        <PlayerDropdown
          value={state.invite}
          onChange={(playerId) => setState({ invite: playerId })}
        />
        <button onClick={onInvite} disabled={state.invite === undefined}>
          Invite
        </button>
      </div>
      <h2>Current members</h2>
      <table>
        <tbody>
          <For each={ctx.members()?.array}>{(member) => <ManageMember {...member} />}</For>
        </tbody>
      </table>
    </>
  );
}

function ManageMember(p: { playerId: string; role: Role }) {
  const ctx = useTeam();

  const [listen, setListen] = createSignal(true, { equals: false });

  function onSetRole(role: Role | null) {
    return api
      .teamsSetMember({
        player_id: p.playerId,
        team_id: ctx.teamId,
        player_role: role,
      })
      .then(
        () => {
          ctx.members.update();
          toast.success(() => (
            <span>
              {role !== null ? (
                <>
                  Updated <PlayerLink playerId={p.playerId} /> to be a {role}
                </>
              ) : (
                <>
                  Dropped <PlayerLink playerId={p.playerId} />
                </>
              )}
            </span>
          ));
        },
        () => {
          toast.failure(() => (
            <span>
              Could not update the role of <PlayerLink playerId={p.playerId} />
            </span>
          ));
          setListen(true);
        }
      );
  }

  return (
    <tr style="margin:0">
      <td>
        <PlayerLink playerId={p.playerId} />
      </td>
      <td>
        <select
          onChange={(ev) => {
            ev.preventDefault();
            const parsedRole = roles[ev.currentTarget.value];
            if (parsedRole !== undefined) {
              onSetRole(parsedRole);
            }
          }}
        >
          {(["admin", "captain", "player"] as const).map((role) => (
            <option value={role} selected={listen() && role === p.role}>
              {titleCase(role)}
            </option>
          ))}
        </select>
      </td>
      <td>
        <ConfirmButton onConfirm={() => onSetRole(null)}>Drop</ConfirmButton>
      </td>
    </tr>
  );
}

const roles: Record<string, Role> = {
  admin: "admin",
  captain: "captain",
  player: "player",
};
