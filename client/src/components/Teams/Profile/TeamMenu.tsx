import Menu from "@/shared/ui/Menu";
import { Show } from "solid-js";
import { useTeam } from "./Context";

export function TeamMenu() {
  const p = useTeam();
  return (
    <Show when={p.ownRole() !== undefined}>
      <Menu>
        <Menu.Title>Team Control</Menu.Title>
        <Show when={p.ownRole() === "admin"}>
          <Menu.Link to="teams.profile.manage" params={{ id: p.teamId }}>
            Manage {p.ownRole()}
          </Menu.Link>
        </Show>
        <Menu.Link to="teams.profile.members" params={{ id: p.teamId }}>
          Members
        </Menu.Link>
        <Menu.SubMenu>
          <Menu.Link to="teams.profile.your-membership" params={{ id: p.teamId }}>
            Your Membership
          </Menu.Link>
        </Menu.SubMenu>
      </Menu>
    </Show>
  );
}
