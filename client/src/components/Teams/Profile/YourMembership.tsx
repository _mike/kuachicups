import { api } from "@/api";
import { navigate } from "@/router";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import Icon from "@/shared/ui/Icon";
import toast from "@/shared/ui/Toast";
import TeamLink from "../TeamLink";
import { useTeam } from "./Context";

export default function YourMembership() {
  const ctx = useTeam();

  function onLeaveTeam() {
    api.teamsLeave(ctx.teamId).then(
      () => {
        toast.success(() => <>Left team {ctx.team()?.name}</>);
        ctx.members.update();
        navigate({ to: "teams.profile", params: { id: ctx.teamId } });
      },
      (err: any) => {
        toast.failure(() => (
          <>
            Cannot leave team
            <pre>{JSON.stringify(err, null, 2)}</pre>
          </>
        ));
      }
    );
  }

  return (
    <>
      <h1>
        Your membership of <TeamLink team={ctx.team()} />
      </h1>
      <dl>
        <dt>Your role</dt>
        <dd>{ctx.ownRole}</dd>
      </dl>
      <ConfirmButton onConfirm={onLeaveTeam}>
        <Icon type="exit" />
        <span>Leave team</span>
      </ConfirmButton>
    </>
  );
}
