import { parseDate } from "@/shared/date";
import { format } from "date-fns";
import { For, Show } from "solid-js";
import { Link } from "../../../router";
import PlayerLink from "../../Player/Link";
import { useTeam } from "./Context";

export default function TeamProfile() {
  const p = useTeam()!;
  return (
    <>
      <h1>
        <Link to="teams.profile" params={{ id: p.teamId }}>
          {p.team()?.name}
        </Link>
      </h1>
      <Show when={isNonNilString(p.team()?.blurb_html)}>
        <blockquote innerHTML={p.team()?.blurb_html ?? ""} />
      </Show>
      <dl>
        <dt>Created</dt>
        <dd>{format(parseDate(p.team()?.created_at) ?? 0, "PPPPpppp")}</dd>
        <dt>Updated</dt>
        <dd>{format(parseDate(p.team()?.updated_at) ?? 0, "PPPPpppp")}</dd>
      </dl>
      <h2>Members</h2>
      <div class="grid small">
        <For each={p.members()?.array}>{(mem) => <PlayerLink playerId={mem.playerId} />}</For>
      </div>
    </>
  );
}

function isNonNilString(s: unknown): s is string {
  return typeof s === "string" && s.length > 0;
}
