import TeamCreate from "../Create";
import { useTeam } from "./Context";

export default function TeamManage() {
  const ctx = useTeam();
  return <TeamCreate team={ctx.team()} />;
}
