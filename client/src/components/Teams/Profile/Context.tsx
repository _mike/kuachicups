import { api, ArrayMap, Role, Team } from "@/api";
import useAuth from "@/context/Auth";
import { Awaitable, useAsync } from "@/shared/async/Awaitable";
import magicalExplodingObject from "@/shared/explosion";
import Container from "@/shared/ui/Container";
import Menu from "@/shared/ui/Menu";
import { createContext, createMemo, JSX, useContext } from "solid-js";
import { TeamMenu } from "./TeamMenu";

export interface ITeamContext {
  teamId: string;
  team: Awaitable<Team>;
  members: Awaitable<
    ArrayMap<
      {
        playerId: string;
        teamId: string;
        role: Role;
      },
      string
    >
  >;
  ownRole: () => undefined | Role;
  canManage: boolean;
}

export const TeamContext = createContext(
  magicalExplodingObject<ITeamContext>("global team context")
);

export function TeamProvider(props: { teamId: string; children: JSX.Element }) {
  const state = createTeamState(props);
  return (
    <TeamContext.Provider value={state}>
      <Container>
        <Menu.Layout menu={<TeamMenu />}>{props.children}</Menu.Layout>
      </Container>
    </TeamContext.Provider>
  );
}

export function useTeam() {
  return useContext(TeamContext);
}

function createTeamState(props: { teamId: string }): ITeamContext {
  const auth = useAuth();
  const team = useAsync((ref) => api.teams.byId(props.teamId, ref));
  const members = useAsync((ref) => api.teams.players.byParentId(props.teamId, ref));
  const ownRole = createMemo(() => {
    const self = auth();
    if (!self.loggedIn) return undefined;
    for (const { playerId, role } of members()?.array ?? []) {
      if (playerId === self.login.id) {
        log.debug("got role", role);
        return role;
      }
    }
  });
  return {
    get teamId() {
      return props.teamId;
    },
    team,
    members,
    ownRole,
    get canManage() {
      return ownRole() === "admin" || ownRole() === "captain";
    },
  };
}
