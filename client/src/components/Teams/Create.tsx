import { api, Team } from "@/api";
import useAuth from "@/context/Auth";
import { navigate } from "@/router";
import ConfirmButton from "@/shared/ui/ConfirmButton";
import { createField, MarkdownField, TextField } from "@/shared/ui/Form2";
import Icon from "@/shared/ui/Icon";
import toast from "@/shared/ui/Toast";
import log from "loglevel";
import { createComputed, Show } from "solid-js";
import { createStore } from "solid-js/store";
import TeamLink from "./TeamLink";

interface S {
  name: string;
  blurb: string;
}

export default function TeamCreate(props: { team?: Team | undefined }) {
  const auth = useAuth();
  const [state, setState] = createStore<S>({
    name: props.team?.name ?? "",
    blurb: props.team?.blurb_md ?? "",
  });

  createComputed(() => {
    setState({
      name: props.team?.name ?? "",
      blurb: props.team?.blurb_md ?? "",
    });
  });

  const { Fld, optic: sf } = createField(state, setState);
  return (
    <>
      <h1>
        {props.team !== undefined ? (
          <>
            Editing <TeamLink team={props.team} />
          </>
        ) : (
          "Create team..."
        )}
      </h1>

      <Fld label="Name" optic={sf.prop("name")}>
        {(value, onChange) => <TextField value={value()} onChange={onChange} />}
      </Fld>

      <Fld label="Blurb" optic={sf.prop("blurb")}>
        {(value, onChange) => <MarkdownField value={value()} onChange={onChange} />}
      </Fld>

      <div class="flex-row">
        <button
          class="button is-primary"
          onClick={() => {
            const name = state.name.trim();
            const blurb = state.blurb.trim();
            if (name === "") {
              toast.failure(() => "Team name must be provided");
              return;
            }
            if (props.team !== undefined) {
              api
                .teamsUpdate({
                  id: props.team.id,
                  name: name,
                  blurb_md: blurb,
                })
                .then((t) => {
                  log.info("update team", t);
                  api.teams.cache[t.id] = Promise.resolve(t);
                  navigate({ to: "teams.profile", params: { id: t.id } });
                });
            } else {
              api
                .teamsCreate({
                  name: name,
                  blurb_md: blurb,
                  owner_id: auth().login!.id,
                })
                .then((t) => {
                  log.info("create team", t);
                  api.teams.cache[t.id] = Promise.resolve(t);
                  navigate({ to: "teams.profile", params: { id: t.id } });
                });
            }
          }}
        >
          {props.team !== undefined ? "Update" : "Create"}
        </button>

        <Show when={props.team !== undefined}>
          <ConfirmButton
            class="is-danger"
            onConfirm={() => {
              api.teamsDelete(props.team!.id).then(
                () => {
                  toast.success(() => <>Deleted {props.team?.name}</>);
                  navigate({ to: "teams" });
                },
                (err: any) => {
                  toast.failure(() => (
                    <>
                      Could not delete {props.team?.name}
                      <pre>{JSON.stringify(err, null, 2)}</pre>
                    </>
                  ));
                }
              );
            }}
            confirming={() => (
              <>
                <Icon type="trash" />
                <span>Really delete team?</span>
              </>
            )}
          >
            <Icon type="trash" />
            <span>Delete team</span>
          </ConfirmButton>
        </Show>
      </div>
    </>
  );
}
