import { api, PagedForUuid, Team } from "@/api";
import useAuth from "@/context/Auth";
import { navigate, PagedLink, useRoute } from "@/router";
import { AwaitableI, useAsync } from "@/shared/async/Awaitable";
import { CounterProvider, useCounter } from "@/shared/async/Counter";
import Container from "@/shared/ui/Container";
import { unpaged } from "@/shared/ui/Dropdown";
import Menu from "@/shared/ui/Menu";
import Pages, { emptyPages, PageLinks, PaginateProps } from "@/shared/ui/Paginate";
import toast from "@/shared/ui/Toast";
import { debounce } from "lodash";
import { createMemo, For, JSX, Show } from "solid-js";
import PlayerLink from "../../Player/Link";
import TeamLink from "../TeamLink";
import theme from "./theme.module.scss";

export default function TeamList() {
  const route = useRoute();
  const auth = useAuth();

  interface ISource {
    name: "teams.mine" | "teams.invites" | "teams";
    fetch: (page: number, refresh?: boolean) => Promise<[PagedForUuid, Team[]]>;
    hasPages: boolean;
    fallback: () => JSX.Element;
    header?: (team: Team) => JSX.Element;
  }

  const source = createMemo<ISource>((): ISource => {
    const { params, name } = route();
    switch (name) {
      case "teams.mine":
        return {
          name: "teams.mine",
          fetch: async (page) => {
            const pid = auth()?.login?.id;
            if (pid === undefined) return [emptyPages.paged, emptyPages.items];
            const paged = await api.teamsPlayerTeamsPage(pid, page, params?.query);
            const teams = await api.teams.byIds(paged.items.map((t) => t.id));
            return [paged, teams];
          },
          hasPages: false,
          fallback: () => "You are not currently a member of any team",
        };

      case "teams.invites":
        return {
          name: "teams.invites",
          fetch: async () => {
            log.debug("teams.invites.fetch");
            const invites = await api.playerCurrentSessionTeamInvites();
            const teams = await api.teams.byIds(
              invites.map((ti) => ti.team_id),
              false
            );
            return [unpaged, teams];
          },
          hasPages: false,
          fallback: () => "You currently have no invites",
          header: (t) => {
            const counter = useCounter();
            log.debug("header has counter", counter);
            return (
              <div class="buttons">
                <button
                  class="contrast"
                  onClick={async () => {
                    try {
                      await api.teamsAcceptInvite(t.id);
                      toast.success(() => <>Joined {t.name}</>);
                      navigate({ to: "teams.profile", params: { id: t.id } });
                    } catch (err: any) {
                      toast.failure(() => (
                        <>
                          Could not join {t.name}
                          <pre>{JSON.stringify(err, null, 2)}</pre>
                        </>
                      ));
                    }
                    counter.signal.increment();
                  }}
                >
                  Accept invite
                </button>
                <button
                  class="secondary"
                  onClick={async () => {
                    try {
                      await api.teamsRejectInvite(t.id);
                      toast.success(() => <>Rejected invite for {t.name}</>);
                    } catch (err: any) {
                      toast.failure(() => (
                        <>
                          Could not reject invite for {t.name}
                          <pre>{JSON.stringify(err, null, 2)}</pre>
                        </>
                      ));
                    }
                    counter.signal.increment();
                  }}
                >
                  Reject invite
                </button>
              </div>
            );
          },
        };

      case "teams":
      default:
        return {
          name: "teams",
          fetch: (page: number) => api.teams.page(page, params?.query),
          hasPages: true,
          fallback: () => "No teams",
        };
    }
  });

  return (
    <Container>
      <Menu.Layout
        menu={() => (
          <>
            <Menu>
              <Menu.Title>Search</Menu.Title>
              <Menu.Link to="teams">Teams</Menu.Link>
              <Menu.Item>
                <input
                  type="text"
                  placeholder="Search"
                  onInput={debounce((ev) => {
                    const value = ev.target.value;
                    const cur = route();
                    const query = value === "" ? undefined : value;
                    navigate({ to: cur.name as any, params: { ...cur.params, query } });
                  }, 400)}
                />
              </Menu.Item>
            </Menu>
            <Show when={auth().loggedIn}>
              <Menu>
                <Menu.Link to="teams.mine">Your teams</Menu.Link>
                <Menu.SubMenu>
                  <Menu.Link to="teams.invites">Invites</Menu.Link>
                  <Menu.Link to="teams.new">Create a team</Menu.Link>
                </Menu.SubMenu>
              </Menu>
            </Show>
          </>
        )}
      >
        <Show when={source()}>
          {(source) => (
            <CounterProvider name="teams">
              <TeamListView
                header={source.header}
                source={source.fetch}
                pageButtons={(p) =>
                  source.hasPages ? <PageLinks to={source.name as PagedLink} {...p} /> : undefined
                }
                getTeamPlayerIds={(teamId) => useTeamPlayerIds(teamId)}
                fallback={source.fallback as unknown as JSX.Element}
              />
            </CounterProvider>
          )}
        </Show>
      </Menu.Layout>
    </Container>
  );
}

function useTeamPlayerIds(teamId: string): AwaitableI<string[]> {
  return useAsync(
    async () => (await api.teams.players.byParentId(teamId)).array.map((r) => r.playerId),
    []
  );
}

function TeamListView(props: {
  source: (page: number) => Promise<[PagedForUuid, Team[]]>;
  header?: (team: Team) => JSX.Element;
  fallback?: JSX.Element;
  pageButtons: (props: PaginateProps) => JSX.Element;
  getTeamPlayerIds: (teamId: string) => () => string[];
}) {
  return (
    <Pages
      class={theme.teamList}
      source={props.source}
      renderItem={(t: Team) => (
        <TeamPreview
          header={props?.header?.(t)}
          team={t}
          usePlayerIds={() => props.getTeamPlayerIds(t.id)}
        />
      )}
      renderItems={(t) => (
        <div class="pages-items">{t.items.length === 0 ? props.fallback : t.children}</div>
      )}
      renderPageButtons={props.pageButtons}
    />
  );
}

function TeamPreview(p: {
  team: Team;
  header: JSX.Element | undefined;
  usePlayerIds: () => () => string[];
}) {
  const playerIds = p.usePlayerIds();
  return (
    <article>
      <header class={theme.header}>
        <h3>
          <TeamLink team={p.team} />
        </h3>
        {p.header}
      </header>
      <Show when={!!p.team.blurb_html || playerIds().length > 0}>
        {typeof p.team.blurb_html === "string" && (
          <div innerHTML={p.team.blurb_html} class="summary" />
        )}
        <div class="grid small">
          <For each={playerIds()}>{(pid) => <PlayerLink playerId={pid} />}</For>
        </div>
      </Show>
    </article>
  );
}
