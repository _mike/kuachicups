import { Link } from "@//router";
import { Team } from "@/api";

export default function TeamLink(props: { team: Team | undefined }) {
  return (
    <Link to="teams.profile" params={{ id: props.team?.id ?? "" }}>
      {props.team?.name}
    </Link>
  );
}
