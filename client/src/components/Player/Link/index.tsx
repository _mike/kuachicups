import { Player, Team, api } from "@/api";
import { Link } from "@/router";
import { usePromise } from "@/shared/async/Awaitable";
import { Size, DiscordAvatarImg } from "@/shared/ui/Discord/Avatar";
import theme from "./theme.module.scss";

export type Props = (
  | {
    playerId: string;
    teamId?: string;
  }
  | {
    player: Player | undefined;
    playerId?: undefined;
    team?: Team | undefined;
  }
) & {
  size?: Size;
  align?: "left" | "right";
  nameOnly?: boolean;
  seedValue?: number;
  tabbable?: boolean;
};

export function usePlayerTeam(props: {
  teamId?: undefined | string;
  playerId?: undefined | string;
}) {
  return usePromise(async (ref) => {
    const { playerId, teamId } = props;
    const [player, team] = await Promise.all([
      typeof playerId === "string" ? api.players.byId(playerId, ref) : undefined,
      typeof teamId === "string" ? api.teams.byId(teamId, ref) : undefined,
    ]);
    return { player, team };
  });
}

export default function PlayerLink(props: Props) {
  if (props.playerId !== undefined) {
    const p = usePlayerTeam(props);
    return (
      <View player={p()?.player} team={p()?.team} size={props.size} align={props.align ?? "left"} />
    );
  }
  return (
    <View
      size={props.size}
      align={props.align ?? "left"}
      player={props.player}
      team={props.team}
      seedValue={props.seedValue}
      tabbable={props.tabbable}
    />
  );
}

export function PlayerTeamView(props: {
  player: Player | undefined;
  team: Team | undefined;
  seedValue?: number;
  tabbable?: boolean;
}) {
  return (
    <>
      {props.team === undefined ? undefined : (
        <Link
          tabIndex={props.tabbable ? undefined : -1}
          class={theme.playerName}
          to="teams.profile"
          params={{ id: props.team.id }}
        >
          {props.team.name}
        </Link>
      )}
      {props.player === undefined || props.team !== undefined ? undefined : (
        <Link
          tabIndex={props.tabbable ? undefined : -1}
          class={theme.playerName}
          to="players.profile"
          params={{ id: props.player.id }}
        >
          {props.player.discord_username}
        </Link>
      )}
      {typeof props.seedValue !== "number" ? undefined : (
        <sub class={theme.seedValue}>{props.seedValue}</sub>
      )}
    </>
  );
}

export function PlayerTeamImg(props: {
  player: Player | undefined;
  // team: Team | undefined;
  size?: Size;
}) {
  // TODO add team avatars
  return (
    <DiscordAvatarImg
      class="playerImg"
      discordId={props.player?.discord_id}
      discordAvatar={props.player?.discord_avatar}
      size={props.size}
    />
  );
}

const View = (props: {
  size?: Size;
  align: "left" | "right";
  nameOnly?: boolean;
  seedValue?: number;
  tabbable?: boolean;
  player: Player | undefined;
  team: Team | undefined;
}) => (
  <span class={theme.playerLink} data-align={props.align}>
    {props.align === "right" || props.nameOnly || props.player === undefined ? undefined : (
      <PlayerTeamImg size={props.size} player={props.player} />
    )}
    <PlayerTeamView
      tabbable={props.tabbable}
      seedValue={props.seedValue}
      player={props.player}
      team={props.team}
    />
    {props.align === undefined ||
      props.align === "left" ||
      props.nameOnly ||
      props.player === undefined ? undefined : (
      <PlayerTeamImg size={props.size} player={props.player} />
    )}
  </span>
);
