import { splitProps } from "solid-js";
import { api } from "@/api";
import Dropdown, { Props } from "@/shared/ui/Dropdown";

export default function PlayerDropdown(
  p0: {
    value: undefined | string;
    onChange: (playerId: string) => void;
  } & Omit<
    Props<string>,
    "children" | "value" | "onChange" | "options" | "kind" | "textSearchRequireTerms" | "title"
  >
) {
  const [props, outerProps] = splitProps(p0, ["value", "onChange"]);
  return (
    <Dropdown
      value={props.value}
      onChange={(p) => props.onChange(p.key!)}
      kind="search"
      textSearchRequireTerms
      options={(p, terms) =>
        api.players.page(p, terms).then(([pg, players]) => [
          pg,
          players.map((p) => ({
            key: p.id,
            title: p.discord_tag.slice(1),
          })),
        ])
      }
      {...outerProps}
    >
      Player
    </Dropdown>
  );
}
