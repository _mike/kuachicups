import { SignupLoader } from "@/components/Cups/Cup/CupSignup";
import { parseDate } from "@/shared/date";
import { format } from "date-fns";
import { createMemo, For } from "solid-js";
import { CupMatch2p, PlayerFinishedMatch } from "../../api";

export function formatPlus(s: number): string {
  if (s > 0) return "+" + s;
  if (s < 0) return "" + s;
  return "0";
}

export function CupSignupMatchResultsTable(props: {
  cupId: string;
  playerId: string;
  results: PlayerFinishedMatch[];
  centre?: boolean;
}) {
  interface MatchData {
    match: CupMatch2p;
    self: { key: "high" | "low"; signupId: string };
    oppo: { key: "high" | "low"; signupId: string };
  }

  const matches = createMemo(() => {
    let { results } = props;
    const mds: MatchData[] = [];
    for (let i = 0; i < results.length; i++) {
      const { match_: match, am_high: amHigh } = results[i];
      const { high_id: highId, low_id: lowId } = match;
      if (typeof highId !== "string" || typeof lowId !== "string") continue;
      if (amHigh) {
        mds.push({
          match,
          self: { key: "high", signupId: highId },
          oppo: { key: "low", signupId: lowId },
        });
      } else {
        mds.push({
          match,
          self: { key: "low", signupId: lowId },
          oppo: { key: "high", signupId: highId },
        });
      }
    }
    return mds;
  });

  return (
    <>
      <h2 class="heading my-0" classList={{ "has-text-centered": props.centre }}>
        Match history &mdash; recent results
      </h2>
      <table class="table is-fullwidth">
        <colgroup>
          <col />
          <col style={{ width: "120px" }} />
          <col style={{ width: "180px" }} />
        </colgroup>
        <thead>
          <tr>
            <th>Opponent</th>
            <th>Score</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>
          <For each={matches()}>
            {(md) => (
              <tr>
                <td>
                  <SignupLoader signup={md.oppo.signupId} />
                </td>
                <td>{winLoss(md.match, md.self.key)}</td>
                <td>{format(parseDate(md.match.updated_at) ?? 0, "P")}</td>
              </tr>
            )}
          </For>
        </tbody>
      </table>
    </>
  );
}

export function winLoss(match: CupMatch2p, self: "high" | "low", className?: string) {
  let w = 0;
  let l = 0;
  for (const { high, low } of match.low_report ?? []) {
    if (high > low) {
      if (self === "high") w += 1;
      if (self === "low") l += 1;
    }
    if (low > high) {
      if (self === "high") l += 1;
      if (self === "low") w += 1;
    }
  }
  return (
    <span class={className} data-winner={w > l}>
      {w} : {l}
    </span>
  );
}
