import { api, CupSignupResult } from "@/api";
import { Link } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import { parseDate } from "@/shared/date";
import Ordinal from "@/shared/Ordinal";
import { format } from "date-fns";
import { For } from "solid-js";

export function CupSignupResultsTable(props: {
  class?: string;
  results: CupSignupResult[];
  centre?: boolean;
  disableTime?: boolean;
}) {
  function CupSignupResultRow(p: CupSignupResult) {
    const info = useAsync(async (ref) => {
      const cup = await api.cups.byId(p.cup_id, ref);
      const mode = await api.games.modes.byId(cup.game_mode_id, ref);
      const game = await api.games.byId(mode.game_id, ref);
      return { cup, game, mode };
    });
    return (
      <tr>
        <info.Await>
          {(info) => (
            <>
              <td>
                <Ordinal placement={p.placement} />
              </td>
              <td>
                <span class="help">
                  {info.game?.name} / {info.mode?.name}
                </span>
              </td>
              <td>
                <Link to="cups.cup" params={{ id: info.cup.id }}>
                  {info.cup.title}
                </Link>
              </td>
              {!props.disableTime && <td>{format(parseDate(p.updated_at) ?? 0, "P")}</td>}
            </>
          )}
        </info.Await>
      </tr>
    );
  }

  return (
    <table class={props.class}>
      <thead>
        <tr>
          <th>Pl.</th>
          <th>Game</th>
          <th>Cup</th>
          {!props.disableTime && <th>Time</th>}
        </tr>
      </thead>
      <tbody>
        <For each={props.results} children={CupSignupResultRow} />
      </tbody>
    </table>
  );
}
