import { api, CupSignupResult, Player, PlayerEloUpdate, Team } from "@/api";
import { Link } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import Container from "@/shared/ui/Container";
import { DiscordAvatarImg } from "@/shared/ui/Discord/Avatar";
import { For, Show } from "solid-js";
import { CupSignupResultsTable } from "../CupSignupResultsTable";
import theme from "./theme.module.scss";
import PlayerEloTable from "./PlayerEloTable";

export default function PlayerProfile(p: { playerId: string }) {
  const player = useAsync((ref) => api.players.byId(p.playerId, ref));
  const teams = useAsync(async (ref) => {
    p.playerId; // listen to player id change
    const roles = await api.players.teamRoles.byParentId(p.playerId, ref);
    return api.teams.byIds(roles.array.map((r) => r.team_id));
  }, []);
  const results = useAsync(() => api.players.results.byId(p.playerId));
  return (
    <Container>
      <_PlayerProfile
        playerId={p.playerId}
        player={player()}
        teams={teams()}
        results={results()?.cup_results ?? []}
        elos={results()?.elo_history ?? {}}
      />
    </Container>
  );
}

export function _PlayerProfile(p: {
  playerId: string;
  player: undefined | Player;
  teams: Team[];
  results: CupSignupResult[];
  elos: Record<string, PlayerEloUpdate[]>;
}) {
  return (
    <div class={theme.layout}>
      <div class={theme.left}>
        <div>
          <DiscordAvatarImg
            class={theme.avatar}
            discordId={p?.player?.discord_id}
            discordAvatar={p?.player?.discord_avatar}
            size={256}
          />
          <div class={theme.avatarCaption}>
            <h2 class={theme.username}>
              <Link to="players.profile" params={{ id: p.playerId }}>
                {p?.player?.discord_username}
              </Link>
            </h2>
            <h3 class={theme.tag}>
              <Show when={p.player}>
                {(p) => <DiscordTagLink id={p.discord_id} tag={p.discord_tag} />}
              </Show>
            </h3>
          </div>
        </div>
      </div>
      <div class={theme.right}>
        <div class={theme.results}>
          <CupSignupResultsTable class={theme.resultsTable} results={p.results} />
          <PlayerEloTable history={p.elos} />
        </div>
        <div class={theme.teams}>
          <h3>Teams</h3>
          <ul>
            <For each={p.teams} fallback={<p>No teams</p>}>
              {(t) => (
                <li>
                  <Link to="teams.profile" params={{ id: t.id }}>
                    {t.name}
                  </Link>
                </li>
              )}
            </For>
          </ul>
        </div>
      </div>
    </div>
  );
}

export function DiscordTagLink(props: { id: string; tag: string }) {
  return (
    <a class={theme.discordTag} href={`https://discord.com/users/${props.id}`}>
      {props.tag}
    </a>
  );
}
