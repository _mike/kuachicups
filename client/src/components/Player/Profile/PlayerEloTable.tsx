import { NiceTabs } from "@/shared/ui/Tabs";
import { Game, GameMode, PlayerEloUpdate, api } from "@/api";
import { useAsync } from "@/shared/async/Awaitable";
import { createMemo, For } from "solid-js";
import { last } from "fp-ts/lib/Array";
import { parseDate } from "@/shared/date";
import { formatDateMaybe } from "@/shared/ui/Form2/Field/Date";
import theme from './theme.module.scss';
import { compareAsc, compareDesc } from "date-fns";
import { DateTime } from "@/shared/ui/Time";

export default function PlayerEloTable(props: { history: Record<string, PlayerEloUpdate[]> }) {
  const gameModeTabs = useAsync(async () => {
    const history = props.history;
    const gameModeDropdown = await api.games.modes.dropdownOptions(0);
    const tabs = gameModeDropdown[1]
      .filter((f) => typeof f !== "string")
      .map((f) => (f as { extra: { game: Game; mode: GameMode } }).extra)
      .map((t) => ({
        title: (
          <span>
            {t.game.name} / {t.mode.name}
          </span>
        ),
        data: t.mode.id,
        history: history[t.mode.id],
        lastResultTime: parseDate(history[t.mode.id]?.[0]?.created_at)
      }))
      .filter((t) => (t.history?.length ?? 0) > 0 && t.lastResultTime !== undefined)
      .sort((a, b) => compareDesc(a.lastResultTime!, b.lastResultTime!));

    let minKey: [string, Date] | undefined;
    for (const tab of tabs) {
      const { data: key, lastResultTime } = tab as { data: string, lastResultTime: Date };
      if (minKey === undefined || minKey[1] < lastResultTime) {
        minKey = [key, lastResultTime];
      }
    }

    let defaultTab: undefined | number;
    if (minKey !== undefined) {
      const [key, _date] = minKey;
      defaultTab = tabs.findIndex(v => v.data === key);
      if (defaultTab < 0) {
        defaultTab = undefined;
      }
    }

    log.debug({ defaultTab, tabs })

    return { defaultTab, tabs }
  });

  return (
    <gameModeTabs.Await>
      {(data) => (
        <div class={theme.eloTabs}>
          <NiceTabs
            class={theme.eloTabBar}
            display="tabs"
            reverse={true}
            defaultTab={data.defaultTab}
            tabs={data.tabs}
          >
            {(t) =>
              <div class={theme.eloContents}>
                <EloGraph
                  elos={props.history[t.data]}
                  width={500}
                  height={100}
                />
                <div class={theme.eloTableContainer}>
                  <table class={theme.eloTable}>
                    <thead>
                      <tr>
                        <th>When</th>
                        <th>Rating</th>
                      </tr>
                    </thead>
                    <tbody>
                      <For each={props.history[t.data]}>
                        {row => <tr>
                          <td><DateTime time={row.created_at} /></td>
                          <td>{row.elo} ({row.elo_diff})</td>
                        </tr>}
                      </For>
                    </tbody>
                  </table>
                </div>
              </div>
            }
          </NiceTabs>
        </div>
      )}
    </gameModeTabs.Await>

  );
}

const MIN_ELO = -500;
const MAX_ELO = 2500;

function EloGraph(props: {
  elos: PlayerEloUpdate[]
  width: number,
  height: number,
}) {
  const svgdata = createMemo(() => {
    const vs: [number, number][] = [];
    const { elos, width, height } = props;
    if (elos.length <= 1) return "";
    let minElo: number | undefined;
    let maxElo: number | undefined;
    for (let i = 0; i < elos.length; i++) {
      const e = elos[i].elo;
      if (typeof minElo !== "number" || e < minElo) minElo = e;
      if (typeof maxElo !== "number" || e > maxElo) maxElo = e;
    }
    minElo = Math.floor((minElo ?? MIN_ELO) / 100) * 100;
    maxElo = Math.ceil((maxElo ?? MAX_ELO) / 100) * 100;
    const eloRange = Math.abs(maxElo - minElo);
    for (let i = elos.length - 1; i >= 0; i--) {
      const e = elos[i];
      const scaled = (e.elo - minElo) / eloRange;
      const xpx = width - (i / (elos.length - 1)) * width;
      const ypx = height - scaled * height;
      vs.push([xpx, ypx]);
    }
    let s = "";
    for (const [x, y] of vs) {
      s += `${x} ${y},`;
    }
    return s;
  });

  return (
    <svg class={theme.eloSvg} viewBox={`0 0 ${props.width} ${props.height}`} onMouseOver={(ev) => { }}>
      <polyline points={svgdata()} fill="none" stroke="var(--color)" stroke-width={2} />
    </svg>
  );
}
