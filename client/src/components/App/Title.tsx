import { api } from "@/api";
import { intParam, RoutesByName, useRoute } from "@/router";
import { createEffect } from "solid-js";

type TitleSegment = string | undefined | null;
type TitleRender<Props> = string | ((props: Props) => Promise<TitleSegment>);
type Titles<Props> = TitleRender<Props>;

const titles: {
  [K in keyof RoutesByName]?: Titles<RoutesByName[K]["params"]>;
} = {
  "players.profile": async (params) => {
    const p = await api.players.byId(params.id);
    return p.discord_username;
  },

  cups: "Cups - kuachi cups",
  "cups.cup": async (params) => {
    const cup = await api.cups.byId(params.id);
    return cup.title;
  },

  "cups.cup.stage": async (params) => {
    const cup = await api.cups.byId(params.id);
    const stage = await api.cups.stages.byId(params.id, Number(params.stageNo));
    return cup.title + " - " + (stage?.title ?? "stage");
  },

  "cups.cup.stage.bracket": async (params) => {
    const cup = await api.cups.byId(params.id);
    const stage = await api.cups.stages.byId(params.id, Number(params.stageNo));
    return "bracket - " + cup.title + " - " + (stage?.title ?? "stage");
  },

  "cups.cup.matches": async (params) => {
    const cup = await api.cups.byId(params.id);
    return "matches - " + cup.title;
  },

  "cups.cup.matches.own": async (params) => {
    const cup = await api.cups.byId(params.id);
    return "your matches - " + cup.title;
  },

  "cups.cup.matches.match": async (params) => {
    const cup = await api.cups.byId(params.id);
    const match = await api.cupsMatchResult2P(intParam(params.matchId)); // XXX not cached
    if (match === undefined) return "match";
    const stage =
      typeof match.cup_stage_id === "string"
        ? await api.cups.stagesById.byId(match.cup_stage_id)
        : undefined;
    const signups = await api.cups.signupsByIdOnly.byIds(
      [match?.high_id, match?.low_id].filter((t): t is string => typeof t === "string")
    );
    const players = await api.players.byIds(signups.map((s) => s.player_id));
    return (
      players.map((p) => p.discord_username).join(" vs ") +
      " - " +
      (stage?.title ?? "title") +
      " - " +
      cup.title
    );
  },

  "teams.profile": async (params) => {
    const team = await api.teams.byId(params.id);
    return team.name;
  },

  "news.item": async (params) => {
    const post = await api.announcements.byId(params.id);
    return post.title_md + " - kuachi cups";
  },

  news: "News - kuachi cups",
  privacy_policy: "Privacy Policy - kuachi cups",
  media: "Media - kuachi cups",
  stream: "Stream Overlay Creator - kuachi cups",
  "stream.docs": "Stream Overlay Manual - kuachi cups",
  teams: "Teams - kuachi cups",
};

export default function Title() {
  const getRoute = useRoute();
  createEffect(() => {
    const { name: _name, nameArray, params } = getRoute();
    for (let i = nameArray.length - 1; i >= 0; i--) {
      const prefixArray = nameArray.slice(0, i + 1);
      const prefix = prefixArray.join(".");
      const seg = titles[prefix as keyof RoutesByName];
      if (seg !== undefined) {
        if (typeof seg === "string") {
          document.title = seg;
        } else {
          seg(params as any).then(
            (title) => {
              if (typeof title === "string") {
                document.title = title;
              }
            },
            () => {
              document.title = "kuachi cups";
            }
          );
        }
        return;
      }
    }
    document.title = "kuachi cups";
  });
  return <div style="display:none" />;
}
