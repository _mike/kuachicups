import { Player } from "@/api";
import { loginRedirBack, logout } from "@/api/oauth2";
import useAuth from "@/context/Auth";
import logo from "@/resources/logo.webp";
import { Link, navigate, RouteName, router, useRoute } from "@/router";
import Icon from "@/shared/ui/Icon";
import theme from "./theme.module.scss";
import discord_logo from "@/resources/discord_logo.png?w=32&h=32";
import { IfThenElse } from "@/shared/Case";
import Hoverdown from "@/shared/ui/Hoverdown";
import Breadcrumbs from "./Breadcrumbs";
import { AvatarSpan, DiscordAvatarImg } from "@/shared/ui/Discord/Avatar";
import { createEffect, createSignal, JSX } from "solid-js";
import { Portal } from "solid-js/web";

const navItems: readonly {
  readonly to: RouteName;
  readonly icon?: Icon.Type | (() => Icon.Type);
  readonly element?: () => JSX.Element;
  readonly params?: Record<string, string>;
  readonly class?: string;
}[] = [
  { to: "news", icon: "calendar" },
  { to: "media", icon: "videocam" },
  { to: "games", icon: "logo-game-controller-b" },
  { to: "cups", icon: cupsIcon, params: { page: "0", active: "true" } },
  { to: "teams", icon: "contacts" },
  { to: "stream", icon: "play" },
  { to: "donate", class: theme.donateItem, icon: "heart", element: () => "support us!" },
] as const;

export const navMenuId = "NAV_MENU";

export let navMenuPortal: HTMLDivElement | undefined;
export let navMenuButton: HTMLButtonElement | undefined;
enum NS {
  Closed,
  Closing,
  Open,
}

export default function Nav() {
  const auth = useAuth();
  const [open, setOpen] = createSignal(NS.Closed);
  const route = useRoute();
  createEffect(() => {
    if (open() === NS.Closing) {
      setTimeout(() => {
        if (open() === NS.Closing) setOpen(NS.Closed);
      }, 500);
    }
  });
  return (
    <nav
      class={theme.nav}
      data-state={NS[open()]}
      aria-role="navigation"
      aria-label="main navigation"
    >
      <button
        ref={navMenuButton}
        class={theme.mobileMenuButton + " outline contrast"}
        onClick={() => setOpen((t) => (t === NS.Open ? NS.Closing : NS.Open))}
        onBlur={(ev) => {
          let rel = ev.relatedTarget;
          log.debug("nav blurred by", rel);
          // required to allow navigation to work, otherwise the blur event here will
          // fire before the click one does, thus preventing navigation
          if (rel !== null && rel instanceof HTMLElement) {
            if (
              rel.classList.contains(theme.item) ||
              rel.classList.contains(theme.links) ||
              rel.closest("." + theme.nav) !== null
            ) {
              log.debug("nav blurred and preventing default on ev");
              ev.preventDefault();
            }
          }
          setTimeout(() => setOpen(NS.Closing), 50);
        }}
      >
        <Icon type="menu" /> Menu
      </button>
      <img class={theme.logo} src={logo} />
      <div class={theme.items}>
        <div class={theme.links}>
          {navItems.map((props) => (
            <a
              class={props.class ?? theme.item}
              classList={{ "is-active": (route().name, router.isActive(props.to)) }}
              href={router.buildPath(props.to, props?.params)}
              onClick={(ev) => {
                ev.preventDefault();
                navigate({ to: props.to as any, params: props.params as any });
              }}
            >
              {props.icon === undefined ? undefined : (
                <Icon type={typeof props.icon === "string" ? props.icon : props.icon()} />
              )}
              {props.element === undefined ? props.to : props.element()}
            </a>
          ))}
          <a class={theme.discordItem} href="https://kuachi.gg/discord">
            <img class="icon" src={discord_logo} />
            discord
          </a>
        </div>
        <div id={navMenuId} ref={navMenuPortal} />
      </div>
      <div class={theme.menu}>
        <IfThenElse
          data={auth()}
          key="loggedIn"
          fallback={() => <button onClick={() => loginRedirBack()}>Login</button>}
        >
          {(s) => (
            <Hoverdown
              title={
                <AvatarSpan>
                  <DiscordAvatarImg
                    size="24"
                    discordId={s.login.discord_id}
                    discordAvatar={s.login.discord_avatar}
                  />
                  {s.login.discord_username}
                </AvatarSpan>
              }
            >
              <Link to="players.profile" params={{ id: s.login.id }}>
                Profile
              </Link>
              <button onClick={logout}>Logout</button>
            </Hoverdown>
          )}
        </IfThenElse>
        <div id={navMenuId}></div>
      </div>
      <Breadcrumbs />
    </nav>
  );
}

function AuthMenu(p: { player: Player; onClick: () => void }) {
  return (
    <div class="navbar-item has-dropdown is-hoverable">
      <a class="navbar-item navbar-link">
        <span class="player-link">
          <span>{p.player.discord_username}</span>
        </span>
      </a>
      <div class="navbar-dropdown is-boxed">
        <Link class="navbar-item" to="players.profile" params={{ id: "self" }} onClick={p.onClick}>
          Profile
        </Link>
        <a class="navbar-item" onClick={logout}>
          Logout
        </a>
      </div>
    </div>
  );
}

const cupsIcons = ["trophy", "beer", "cafe"] as const;
function cupsIcon() {
  return cupsIcons[Math.floor(Math.random() * cupsIcons.length) % cupsIcons.length];
}
