import { api } from "@/api";
import StageTitle from "@/components/Cups/Cup/Stage/Title";
import { Link, RoutesByName, useRoute } from "@/router";
import { usePromise } from "@/shared/async/Awaitable";
import memoize from "micro-memoize";
import { createMemo, JSX } from "solid-js";
import { RouteState } from "solid-typefu-router5";
import { Any, O } from "ts-toolbelt";
import theme from "./theme.module.scss";

export default function Breadcrumbs() {
  const route = useRoute();
  return <_Breadcrumbs state={route()} />;
}

type CrumbRender<Props> = (props: Props) => JSX.Element;

type Crumbs<Props> = CrumbRender<Props>;

const crumbs: {
  [K in keyof RoutesByName]?: Crumbs<RoutesByName[K]["params"]>;
} = {
  "players.profile": (params) => {
    const p = usePromise(() => api.players.byId(params.id));
    return <span>{p()?.discord_username}</span>;
  },

  "cups.cup": (params) => {
    const cup = usePromise(() => api.cups.byId(params.id));
    return <span>{cup()?.title ?? "Cup"}</span>;
  },

  "cups.cup.stage": (params) => {
    const stage = usePromise(() => api.cups.stages.byId(params.id, Number(params.stageNo)));
    return <StageTitle stage={stage()} />;
  },

  privacy_policy: () => "Privacy Policy",

  "teams.profile": (params) => {
    const team = usePromise(() => api.teams.byId(params.id));
    return <span>{team()?.name ?? "Profile"}</span>;
  },

  "news.item": (params) => {
    const post = usePromise(() => api.announcements.byId(params.id));
    return <span>{post()?.title_md}</span>;
  },
};

function _Breadcrumbs(props: { state: O.Readonly<RouteState, Any.Key, "deep"> }) {
  return (
    <span class={theme.breadcrumbs}>
      {props.state.nameArray.map((_fragment, i) => {
        const { nameArray, params } = props.state;
        const prefixArray = nameArray.slice(0, i + 1);
        const prefix = prefixArray.join(".");
        let crumb: CrumbRender<any> | string | undefined = crumbs[prefix as keyof RoutesByName];
        if (crumb === undefined) {
          crumb = titleCase(prefixArray[prefixArray.length - 1]);
        }
        const inner: JSX.Element = (
          <>{typeof crumb === "string" ? crumb : crumb(props.state?.params)}</>
        );
        const link = createMemo(() => {
          if (i === props.state.nameArray.length - 1) {
            return inner;
          } else {
            return (
              <Link to={prefix as any} params={params}>
                {inner}
              </Link>
            );
          }
        });
        return <Crumb>{link}</Crumb>;
      })}
    </span>
  );
}

function Crumb(props: { children: JSX.Element }) {
  return <span class={theme.crumb}>{props.children}</span>;
}

export const titleCase = memoize((arg: string) => {
  if (arg.length === 0) return arg;
  const head = arg[0];
  const tail = arg.slice(1);
  return head.toUpperCase() + tail.replace("-", " ");
});
