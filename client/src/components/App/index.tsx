import { api } from "@/api";
import CupIndex, { CupHome } from "@/components/Cups/Cup";
import { CupProvider } from "@/components/Cups/Cup/Context";
import CupCreateForm from "@/components/Cups/Cup/Form/Create";
import CupManage from "@/components/Cups/Cup/Manage";
import Match from "@/components/Cups/Cup/Match/Match";
import MatchesOverview from "@/components/Cups/Cup/Match/MatchesOverview";
import { CupStageHome } from "@/components/Cups/Cup/Stage";
import {
  CupStageProvider,
  useCupStage,
} from "@/components/Cups/Cup/Stage/Context";
import CupStageManage from "@/components/Cups/Cup/Stage/Manage";
import StageRankings from "@/components/Cups/Cup/Stage/Rankings";
import CupList from "@/components/Cups/List";
import PlayerProfile from "@/components/Player/Profile";
import { arrayParam, boolParam, intParam, Router, stringParam } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import { CounterProvider } from "@/shared/async/Counter";
import Load from "@/shared/async/Load";
import Menu from "@/shared/ui/Menu";
import { lazy } from "solid-js";
import CupEditForm from "../Cups/Cup/Form/Edit";
import { MatchProvider, MatchStageProvider } from "../Cups/Cup/Match/Context";
import MatchesQuery from "../Cups/Cup/Match/MatchesOverview/Query";
import Medias from "../Media";
import News, { NewsLayout } from "../News";
import NewAnn, { EditAnn } from "../News/New";
import NewsView from "../News/View";
import PrivacyPolicy from "../PrivacyPolicy";
import TeamList from "../Teams/List";
import { GameList } from '../Games/List';
import GameRankings from '../Games/Rankings';
import PickbansManager from '../Games/PickbansManager';
import TeamProfile from "../Teams/Profile";
import { TeamProvider } from "../Teams/Profile/Context";
import TeamManage from "../Teams/Profile/Manage";
import TeamMembers from "../Teams/Profile/Members";
import YourMembership from "../Teams/Profile/YourMembership";
import Footer from "./Footer";
import Nav from "./Nav";
import { ScrollableProvider, useScrollable } from "./ScrollableContext";
import theme from "./theme.module.scss";
import Title from "./Title";
import Container from "@/shared/ui/Container";

const OverlayHome = lazy(() => import("@/components/Overlay/Config"));
const OverlayDocs = lazy(() => import("@/components/Overlay/Config/Docs"));
const Donate = lazy(() => import("@/components/Donate"));

export default function App() {
  return (
    <ScrollableProvider>
      <Title />
      <div id={theme.container} data-scrollable={useScrollable().scrollable()}>
        <Nav />
        <div id={theme.content}>
          <AppRoutes />
        </div>
        <Footer />
      </div>
    </ScrollableProvider>
  );
}

function AppRoutes() {
  return (
    <Router>
      {{
        players: {
          profile: {
            // Player profile loading
            render: (props) => <PlayerProfile playerId={props.params.id} />,
          },
        },

        teams: {
          fallback: () => <TeamList />, // also provides routes for teams.mine, teams.invites
          profile: {
            render: (p) => (
              <TeamProvider teamId={p.params.id}>{p.children}</TeamProvider>
            ),
            fallback: () => <TeamProfile />,
            manage: { render: () => <TeamManage /> },
            members: { render: () => <TeamMembers /> },
            "your-membership": { render: () => <YourMembership /> },
          },
        },

        media: {
          render: () => <Medias />,
        },

        games: {
          render: (p) => <GameList>{p.children}</GameList>,
          rankings: { render: () => "Not implemented (yet)" },
          pickbans: { render: () => <PickbansManager /> },
        },

        donate: {
          render: () => <Donate />,
        },

        privacy_policy: {
          render: () => <PrivacyPolicy />,
        },

        news: {
          render: (p) => <NewsLayout>{p.children}</NewsLayout>,
          fallback: () => (
            <News source={(page) => api.announcements.page(page)} />
          ),
          unpublished: {
            render: () => (
              <News source={(page) => api.announcements.unpublished(page)} />
            ),
          },
          item: {
            fallback: (p) => {
              const getItem = useAsync(() =>
                api.announcements.byId(p.params.id)
              );
              return (
                <div>
                  <Load component={NewsView} item={Load.memo(getItem)} />
                </div>
              );
            },
            edit: {
              render: (p) => (
                <div>
                  <EditAnn id={p.params.id} />
                </div>
              ),
            },
          },
          new: {
            render: () => <NewAnn />,
          },
        },

        cups: {
          // Cup list
          fallback: (p) => (
            <CupList
              type={p.params?.active === "inactive" ? "inactive" : "active"}
            />
          ),

          new: {
            render: () => <CupCreateForm />,
          },

          matches: {
            render: (p) => <MatchesQuery />,
          },

          cup: {
            fallback: () => <CupHome />,
            render: (p) => (
              <CounterProvider>
                <CupProvider cupId={p.params.id}>
                  <CupIndex children={p.children} />
                </CupProvider>
              </CounterProvider>
            ),

            edit: {
              render: (p) => <CupEditForm cupId={p.params.id} />,
            },

            manage: {
              fallback: () => <CupManage />,
              stage: {
                render: (p) => (
                  <div>
                    <CupStageProvider
                      cupId={p.params.id}
                      stageNo={Number(p.params.stageNo)}
                    >
                      <Load
                        component={CupStageManage}
                        stage={Load.memo(() => useCupStage().stage())}
                      />
                    </CupStageProvider>
                  </div>
                ),
              },
            },

            matches: {
              fallback: () => <MatchesOverview source="all" />,
              match: {
                render: (p) => (
                  <MatchProvider match={intParam(p.params.matchId)}>
                    <MatchStageProvider>
                      <Match />
                    </MatchStageProvider>
                  </MatchProvider>
                ),
              },
              own: {
                render: () => <MatchesOverview source="self" />,
              },
            },

            stage: {
              render: (p) => (
                <div class="fullscreenish">
                  <CupStageProvider
                    cupId={p.params.id}
                    stageNo={Number(p.params.stageNo)}
                  >
                    {p.children}
                  </CupStageProvider>
                </div>
              ),

              fallback: (p) => {
                const data = useCupStage();
                return (
                  <Load
                    component={CupStageHome}
                    stage={Load.memo(data.stage)}
                    matches={Load.memo(data.matches)}
                  />
                );
              },

              placements: {
                render: () => {
                  // const ctx = useCup();
                  const data = useCupStage();
                  return (
                    <Load
                      component={StageRankings}
                      rankings={Load.memo(data.rankings)}
                      stage={Load.memo(data.stage)}
                    />
                  );
                },
              },
            },
          },
        },

        stream: {
          fallback: () => <OverlayHome />,
          docs: {
            render: () => <OverlayDocs />,
          },
        },
      }}
    </Router>
  );
}
