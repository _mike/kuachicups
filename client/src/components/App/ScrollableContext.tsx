import { useMobile } from "@/context/Size";
import { createContext, createSignal, JSX, Accessor, Setter, useContext } from "solid-js";

export interface IScrollableCtx {
  readonly setScrollable: Setter<boolean>;
  readonly scrollable: Accessor<boolean>;
}

const Context = createContext<IScrollableCtx>({
  get setScrollable() {
    throw new Error("cannot use scrollable ctx outside of a scrollableprovider");
    return undefined as any;
  },
  get scrollable() {
    throw new Error("cannot use scrollable ctx outside of a scrollableprovider");
    return undefined as any;
  },
});

export function useScrollable(): IScrollableCtx {
  return useContext(Context);
}

export function ScrollableProvider(props: { children: JSX.Element }) {
  const [scrollable, setScrollable] = createSignal(true);
  const mobile = useMobile();
  return (
    <Context.Provider
      value={{
        setScrollable,
        scrollable() {
          const s = scrollable();
          const m = mobile();
          return m || s;
        },
      }}
    >
      {props.children}
    </Context.Provider>
  );
}
