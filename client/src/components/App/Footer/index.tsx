import { Link } from "@/router";
import Icon from "@/shared/ui/Icon";
import theme from "./theme.module.scss";

export default function Footer() {
  return (
    <footer class={theme.footer}>
      <div class={theme.left}>
        <p>
          © 2020-2022 <span class="is-family-monospace">kuachi cups</span>
          <br />
          <Link to="privacy_policy">Privacy policy</Link>
        </p>
      </div>
      <div class={theme.middle}>
        <p>
          <b>kuachi cups</b> is the home of competitive AFPS, by and for passionate Diabotical,
          Quake Champions, and Quake Live players, across Australia, New Zealand, and Oceania.
        </p>
      </div>
      <div class={theme.right}>
        <p>
          <a href="https://gitlab.com/_mike/kuachicups">
            <Icon type="code" />
            <span>Source</span>
          </a>{" "}
          by <a href="https://quasimal.com">mike</a>
        </p>
      </div>
    </footer>
  );
}
