import memoize from "micro-memoize";

function _parseDate(date: unknown): Date | undefined {
  if (date instanceof Date) return date;
  if (typeof date === "string" || typeof date === "number") return new Date(date);
  return undefined;
}

export const parseDate = memoize(_parseDate, {
  maxSize: 32,
});
