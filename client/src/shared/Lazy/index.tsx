import { debounce } from "lodash";
import {
  createContext,
  createEffect,
  createMemo,
  createSignal,
  JSX,
  onCleanup,
  untrack,
  useContext,
} from "solid-js";
import theme from "./theme.module.scss";

function isElementInViewport(el: HTMLElement): boolean {
  const rect = el.getBoundingClientRect();
  return (
    !(rect.top === rect.bottom || rect.left === rect.right) &&
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */ &&
    rect.right <=
      (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */
  );
}

type LazyEl =
  | {
      exists: true;
      div: HTMLDivElement;
      setInViewport: (inView: boolean) => void;
    }
  | {
      exists: false;
    };

interface LazyContext {
  monotonic: boolean;
  divs: LazyEl[];
  nextId(): number;
  checkDivs(): void;
  listener: () => void;
  updater: () => void;
  length?: () => number;
}

function mkLazyContext(monotonic: boolean, length?: () => number): LazyContext {
  const [listener, updater] = createSignal(void 0, { equals: false });
  const divs: LazyContext["divs"] = [];

  const checkDivs = debounce(
    () => {
      if (divs.length === 0) return;
      for (let i = 0; i < divs.length; i++) {
        const d = divs[i];
        if (d.exists) {
          const { div, setInViewport } = d;
          const inViewport = isElementInViewport(div);
          if (inViewport) {
            divs[i] = done;
            setInViewport(true);
            if (monotonic) {
              // schedule another checkdivs
              if (i < divs.length - 1) {
                setTimeout(checkDivs, 350);
              }
              break;
            }
          }
        }
      }
    },
    100,
    {
      leading: false,
      trailing: true,
      maxWait: 1000,
    }
  ) as () => void;

  return {
    monotonic,
    length,
    divs,
    nextId: () => divs.length,
    checkDivs,
    listener,
    updater: updater as any,
  };
}

export function LazyProvider(props: {
  children: JSX.Element;
  length?: () => number;
  monotonic?: boolean;
}) {
  const ctx = untrack(() => mkLazyContext(props.monotonic === true, props.length));
  const checkDivs = ctx.checkDivs;
  createEffect(() => {
    ctx.listener();
    checkDivs();
  });
  addEventListener("resize", checkDivs);
  addEventListener("scroll", checkDivs);
  onCleanup(() => {
    removeEventListener("resize", checkDivs);
    removeEventListener("scroll", checkDivs);
    ctx.divs = noDivs;
    ctx.checkDivs = noCheck;
  });
  return <LazyContext.Provider value={ctx}>{props.children}</LazyContext.Provider>;
}

export interface LazyFallbackProps {
  class: string;
  inViewport: boolean;
  setup: (div: HTMLElement) => void;
}

export function Lazily(props: {
  children: JSX.Element;
  fallback?: (props: LazyFallbackProps) => JSX.Element;
}) {
  /* - Create a wrapper div with a loading icon in it
     - Add it to a global array of divs to check if in viewport (and if so then
       trigger load)
     - On cleanup check if global array still has our wrapper div, if so delete
   */
  const ctx = useContext(LazyContext);
  const [inViewport, setInViewport] = createSignal(false);
  const self: LazyEl = { exists: false };

  function cleanup() {
    Object.assign(self, {
      exists: false,
      setInViewport: undefined,
      div: undefined,
    });
  }

  onCleanup(cleanup);

  function LazyLoader() {
    onCleanup(cleanup);
    function setup(div: HTMLElement) {
      div.onmouseover = () => setInViewport(true);
      div.onpointerover = () => setInViewport(true);
      setTimeout(() => {
        Object.assign(self, { exists: true, div, setInViewport });
        ctx.divs.push(self);
        ctx.updater();
      }, 0);
    }
    if (props.fallback) {
      return <props.fallback class={theme.loader} inViewport={inViewport()} setup={setup} />;
    }
    return <div class={theme.loader} data-in-viewport={inViewport()} ref={setup} />;
  }

  return createMemo(() => {
    const inView = inViewport();
    if (inView) return props.children;
    return <LazyLoader />;
  });
}

export function LazyMargin() {
  return <div class={theme.margin} />;
}

const done: LazyEl = { exists: false } as const;
const noDivs = [] as any[];
const noCheck = () => {};

const globalLazy = mkLazyContext(false);
addEventListener("resize", globalLazy.checkDivs);
addEventListener("scroll", globalLazy.checkDivs);

const LazyContext = createContext<LazyContext>(globalLazy);
