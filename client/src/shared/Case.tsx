import { createMemo, JSX } from "solid-js";
import { Union } from "ts-toolbelt";

export function Case<Key extends string | number | symbol>(props: {
  key: Key;
  fallback?: JSX.Element;
  children: { [K in Key]?: JSX.Element };
}): JSX.Element {
  const getter = createMemo<[Key, undefined | JSX.Element], undefined>(
    () => {
      const key = props.key;
      const elem = props.children[key];
      return [key, elem];
    },
    undefined,
    {
      equals: (a, b) => {
        const eq = a !== undefined && a[0] === b[0];
        return eq;
      },
    }
  );
  return () => {
    return getter()[1] ?? props.fallback;
  };
}

type CaseOpts<Data, Key extends keyof Data, Result> = Partial<
  Union.IntersectOf<
    Data[Key] extends infer Value
      ? Value extends string | symbol | number
        ? Record<Value, (data: Data & Record<Key, Value>) => Result>
        : never
      : never
  >
>;

export function CaseOf<
  Data extends { [Val in Key]: string | number | symbol },
  Key extends keyof Data,
  R = JSX.Element
>(props: {
  data: Data | undefined;
  key: Key;
  fallback?: R;
  children: CaseOpts<Data, Key, JSX.Element>;
}): () => undefined | R {
  const getter = createMemo<[Data, undefined | ((data: Data) => R)] | undefined, undefined>(
    () => {
      const { key, data, children } = props;
      if (data === undefined) return undefined;
      const render: undefined | ((data: Data) => R) = (children as any)[data[key]];
      return [data, render];
    },
    undefined,
    { equals: (a, b) => a !== undefined && a[0] === b?.[0] && a[1] === b?.[1] }
  );
  return () => {
    const m = getter();
    if (m !== undefined) {
      const [data, render] = m;
      return typeof render === "function" ? render(data) : props.fallback;
    } else {
      return props.fallback;
    }
  };
}

export function IfThenElse<
  Data extends { [Val in Key]: any },
  Key extends keyof Data,
  R = JSX.Element
>(props: {
  data: Data;
  key: Key;
  fallback?: (data: Data & Record<Key, Exclude<Data[Key], true>>) => R;
  children: (data: Data & Record<Key, true>) => R;
}) {
  const _getCondData = createMemo<[boolean, Data]>(() => {
    const { key, data } = props;
    return [data[key], data];
  });

  const getCondData: () =>
    | [true, Data & Record<Key, true>]
    | [false, Data & Record<Key, Exclude<Data[Key], true>>] = _getCondData as any;

  return () => {
    const [cond, data] = getCondData();
    if (cond === true) {
      return props.children(data);
    } else if (typeof props.fallback === "function") {
      return props.fallback(data);
    } else {
      return null;
    }
  };
}

export function EnumFromTo(props: {
  from: number;
  to: number;
  step?: number;
  children: (index: number) => JSX.Element;
}): JSX.Element {
  // let cache: any[] = [];
  // let lastCount: number = -1;
  return createMemo(() => {
    const { from, step = 1, to, children: render } = props;
    const r: JSX.Element = [];
    for (let i = from; i < to; i += step) {
      r.push(render(i));
    }
    return r;
  });
}
