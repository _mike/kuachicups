export interface Listen<T, U = undefined> {
  listen(): NonNullable<T> | U;
  defined(): boolean;
}
