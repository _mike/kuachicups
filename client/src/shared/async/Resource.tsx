import { createResource, JSX } from "solid-js";
import Await, { NonUndefined } from "./Await";

export default function Resource<T, K>(
  props: {
    children: (val: NonUndefined<T>) => JSX.Element;
    fallback?: JSX.Element;
  } & (
    | {
        key: K | (() => K);
        data: (key: K) => Promise<T>;
      }
    | {
        key?: undefined;
        data: () => Promise<T>;
      }
  )
) {
  const [res] =
    props.key === undefined
      ? createResource(props.data)
      : createResource(
          typeof props.key === "function" ? (props.key as any) : () => props.key,
          props.data
        );
  return <Await data={res} children={props.children} fallback={props.fallback} />;
}
