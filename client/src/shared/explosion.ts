const explode = Object.freeze({});

export default function magicalExplodingObject<T extends object>(name: string): T {
  return new Proxy(explode as any, {
    get: (_target, prop, _recv) => {
      throw new Error(`tried to access ${prop.toString()} on ${name}`);
    },
  });
}
