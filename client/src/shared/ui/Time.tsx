import { createSignal, onCleanup, createMemo, JSX } from "solid-js";
import { formatDistance } from "date-fns";
import { format, utcToZonedTime, zonedTimeToUtc } from "date-fns-tz";

(window as any).dft = { utcToZonedTime, format };

const fromMs: Record<"h" | "m" | "s", number> = {
  h: 1000 * 60 * 60,
  m: 1000 * 60,
  s: 1000,
};

export function createTicker(tick: "m" | "s" | "h" | number, onComplete?: () => void): () => Date {
  const [getTime, setTime] = createSignal(new Date());
  const interval = setInterval(
    () => {
      setTime(new Date());
    },
    typeof tick === "number" ? tick : fromMs[tick]
  );
  onCleanup(() => clearInterval(interval));
  return getTime;
}

export function Alarm(props: { target: string | undefined | Date }): JSX.Element {
  const ticker = createTicker(1000);
  const target = createMemo(() => {
    const t = props.target;
    if (t instanceof Date) return t;
    if (typeof t === "string") return new Date(t);
    return new Date();
  });
  const result = createMemo(() => {
    const t0 = target().getTime();
    const t1 = ticker().getTime();
    const ms = t0 - t1;
    const sec = ms / 1000;
    if (ms > 0) {
      const minPart = String(Math.floor(sec / 60)).padStart(2, "0");
      const secPart = String(Math.floor(sec % 60)).padStart(2, "0");
      return `${minPart}:${secPart}`;
    }
    return "00:00";
  });
  return result;
}

export function Countdown(props: {
  target: Date | string;
  class?: string;
  tick: "m" | "s" | "h" | number;
  onComplete?: () => void;
}): JSX.Element {
  const ticker = createTicker(props.tick);
  const result = createMemo(() => {
    let target = props.target;
    if (typeof target === "string") target = new Date(target);
    if (target instanceof Date) {
      return formatDistance(target.getTime(), ticker(), {
        includeSeconds: true,
        addSuffix: true,
      });
    }
    return undefined;
  });
  return <span class={props.class ?? "countdown"}>{result}</span>;
}

export function DateTime(props: { time: string | Date; timeZone?: string }) {
  return createMemo(() => {
    const t = props.time;
    const tz = props.timeZone;
    let date = typeof t === "string" ? new Date(t) : t;
    if (tz !== undefined) {
      date = zonedTimeToUtc(date, tz);
    }
    return format(date, "PPpp z");
  });
}

export default function Now(props: {
  precision: "m" | "s" | "h" | number;
  timeZone?: string;
}): JSX.Element {
  const ticker = createTicker(props.precision);
  return createMemo(() => {
    const tz = props.timeZone;
    const now = ticker();
    if (tz === undefined) {
      return format(now, "PPpp");
    } else {
      const utc = zonedTimeToUtc(now, tz);
      return format(utc, "PPpp z");
    }
  });
}
