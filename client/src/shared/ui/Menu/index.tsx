import { navMenuButton, navMenuPortal } from "@/components/App/Nav";
import useAuth from "@/context/Auth";
import { useMobile } from "@/context/Size";
import { Link as BaseLink, Routes } from "@/router";
import classnames from "classnames";
import {
  children,
  createContext,
  createEffect,
  createSignal,
  createUniqueId,
  getOwner,
  onCleanup,
  runWithOwner,
  Show,
  splitProps,
  untrack,
  useContext,
} from "solid-js";
import { JSX } from "solid-js/jsx-runtime";
import { createMutable } from "solid-js/store";
import { Dynamic, Portal } from "solid-js/web";
import { LinkComponent } from "solid-typefu-router5";
import Icon from "../Icon";
import theme from "./theme.module.scss";

function Menu(props: Menu.Props) {
  const [ownProps, divProps] = splitProps(props, ["class", "children"]);
  return (
    <div class={classnames(ownProps.class, theme.menu)} {...divProps}>
      <button class={theme.menuButton}>
        <Icon type="menu" />
        <span>Menu</span>
      </button>
      <div class={theme.menuContents}>{ownProps.children}</div>
    </div>
  );
}

namespace Menu {
  export type Props = JSX.IntrinsicElements["div"] & {
    class?: string;
    children: JSX.Element;
  };

  export function Admin(props: Props) {
    const auth = useAuth();
    return (
      <Show when={auth().admin === true}>
        <Menu {...props} />
      </Show>
    );
  }

  export interface ItemProps {
    class?: string;
    border?: boolean;
    children: JSX.Element;
  }

  export function Item(props: ItemProps) {
    return (
      <div class={classnames(props.class, theme.item)} classList={{ [theme.border]: props.border }}>
        {props.children}
      </div>
    );
  }

  export function ItemLink(props: JSX.IntrinsicElements["a"] & { disabled?: boolean }) {
    const [ownProps, aProps] = splitProps(props, [
      "class",
      "children",
      "disabled",
      "href",
      "onClick",
    ]);
    return (
      <Dynamic
        component={ownProps.disabled ? "span" : "a"}
        class={classnames(
          theme.item,
          theme.link,
          ownProps.class,
          props.disabled ? theme.disabledLink : undefined
        )}
        href={ownProps.disabled ? undefined : ownProps.href}
        onClick={ownProps.disabled ? undefined : ownProps.onClick}
        {...aProps}
      >
        {ownProps.children}
      </Dynamic>
    );
  }

  export function Title(props: { class?: string; children: JSX.Element }) {
    return <div class={classnames(theme.title, props.class)}>{props.children}</div>;
  }

  export function SubMenu(props: { class?: string; children: JSX.Element }) {
    return <div class={classnames(theme.submenu, props.class)}>{props.children}</div>;
  }

  export const Link: LinkComponent<Routes> = (props) => (
    <BaseLink
      {...props}
      nav
      navIsActive={{
        ignoreQueryParams: true,
        strictEquality: true,
      }}
      class={classnames(theme.item, theme.link, props.class)}
    />
  );

  export interface LayoutProps {
    class?: string;
    contentClass?: string;
    menu?: JSX.Element | (() => JSX.Element);
    children: JSX.Element;
  }

  interface ILayoutContext {
    available: boolean;
    left: Node | undefined;
    fullscreen: boolean;
  }

  export const LayoutContext = createContext<ILayoutContext>({
    available: false,
    left: undefined,
    fullscreen: false,
  });

  export function MenuPortal(props: { side: "left"; children: JSX.Element }) {
    const ctx = useContext(LayoutContext);
    if (untrack(() => !ctx.available)) {
      return props.children;
    }
    return (
      <Portal mount={ctx[props.side]} ref={(div) => (div.className = theme.menuPortalElem)}>
        {props.children}
      </Portal>
    );
  }

  export function useFullscreenLayout() {
    const l = useContext(LayoutContext);
    if (untrack(() => l.available)) {
      const prev = untrack(() => l.fullscreen);
      l.fullscreen = true;
      onCleanup(() => (l.fullscreen = prev));
    }
  }

  export function Layout(props: LayoutProps) {
    const [isLeftEmpty, setLeftEmpty] = createSignal(false);
    const leftId = createUniqueId();
    const store = createMutable<ILayoutContext>({
      available: true,
      left: undefined,
      fullscreen: false,
    });
    const mobile = useMobile();
    function TheMenus() {
      return (
        <Menus portalId={leftId} setEmpty={setLeftEmpty} isEmpty={isLeftEmpty}>
          {typeof props.menu === "function" ? props.menu() : props.menu}
        </Menus>
      );
    }
    return (
      <LayoutContext.Provider value={store}>
        <div
          class={classnames(theme.layout, props.class)}
          data-has-left={props.menu !== undefined && !isLeftEmpty()}
          data-fullscreen={!mobile() && store.fullscreen}
        >
          {props.menu === undefined ? undefined : (
            <Show when={mobile() && !isLeftEmpty()} fallback={TheMenus}>
              <Portal mount={navMenuPortal}>
                {() => {
                  if (navMenuButton !== undefined) {
                    navMenuButton.classList.add(theme.navMenuButton);
                    onCleanup(() => {
                      navMenuButton?.classList.remove(theme.navMenuButton);
                    });
                  }
                  return null;
                }}
                <hr />
                <div class={theme.menusInNav}>
                  <TheMenus />
                </div>
              </Portal>
            </Show>
          )}
          <div class={classnames(theme.content, props.contentClass)}>{props.children}</div>
        </div>
      </LayoutContext.Provider>
    );
  }

  export function Menus(props: {
    portalId?: string;
    children: JSX.Element;
    setEmpty?: (hasItems: boolean) => void;
    isEmpty?: () => boolean;
  }) {
    const store = useContext(LayoutContext);
    return (
      <div
        ref={(div) => {
          const { setEmpty } = props;
          if (setEmpty !== undefined) {
            createEmptinessListener(div, setEmpty);
          }
        }}
        class={classnames(theme.menus)}
        data-empty={!!props?.isEmpty?.()}
      >
        <div class={theme.menusTop}>{props.children}</div>
        {props.portalId === undefined ? undefined : (
          <div
            id={props.portalId}
            class={theme.menuPortal}
            ref={(div) => {
              if (props.portalId !== undefined && store.available) {
                store.left = div;
              }
            }}
          />
        )}
      </div>
    );
  }
}

export default Menu;

function hasChildren(el: HTMLElement) {
  if (el.childElementCount === 0) {
    return false;
  }
  for (const c of Array.from(el.childNodes)) {
    if (c.hasChildNodes()) {
      return true;
    }
  }
  return false;
}

function createEmptinessListener(div: HTMLDivElement, setEmpty: (items: boolean) => void) {
  function observe() {
    setEmpty(!hasChildren(div));
  }
  const observer = new MutationObserver(observe);
  observer.observe(div, { childList: true, subtree: true });
  createEffect(() => untrack(observe));
  onCleanup(() => {
    observer.disconnect();
  });
}
