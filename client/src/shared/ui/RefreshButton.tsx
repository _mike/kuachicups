import Icon from "./Icon";
import theme from "./RefreshButton.module.scss";

export default function RefreshButton(props: { update: () => void; isLoading: () => boolean }) {
  return (
    <button class={theme.refresh} onClick={() => props.update()}>
      <Icon type="refresh" />
    </button>
  );
}
