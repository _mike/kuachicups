import { ApiError } from "@/api";
import { createMemo } from "solid-js";
import Resource from "../async/Resource";

export default function FormatResponseError(props: { error: any }) {
  return createMemo(() => {
    const err = props.error;
    if (err instanceof ApiError) {
      return (
        <div style="font-size:0.5em">
          <h2>Response error</h2>
          <pre>
            <Resource data={() => readBody(err.body)}>{(text) => text}</Resource>
          </pre>
        </div>
      );
    }
    return <pre>{JSON.stringify(err, null, 2)}</pre>;
  });
}

async function readBody(body: unknown) {
  body = await body;
  if (typeof body === "string") return body;
  if (body instanceof Blob) return body.text();
  return "unknown";
}
