import { Paged, PagedForUuid } from "@/api";
import { ClampY } from "@/context/Size";
import Await from "@/shared/async/Await";
import classNames from "classnames";
import {
  createEffect,
  createMemo,
  createSignal,
  JSX,
  onCleanup,
  Show,
  splitProps,
  untrack,
} from "solid-js";
import Icon from "../Icon";
import { SimplePaged } from "../Paginate";
import theme from "./theme.module.scss";

export type Props<K = number> = DropdownCoreProps<K> & DropdownDivProps<K>;

type DropdownCoreProps<K = number> = {
  children: JSX.Element;
  renderOption?: (option: { option: Option<K> }) => JSX.Element;
  class?: string;
  buttonClass?: string;
  value?: null | K;
  onChange?: (option: Option<K>) => void;
  disabled?: boolean;
  isRight?: boolean;
  hoverable?: boolean;
  deps?: any;
} & (
  | {
      kind?: undefined;
      options: Options<K>;
    }
  | {
      kind: "fetch";
      options: (page: number) => Promise<undefined | [Paged, Options<K>]>;
    }
  | {
      kind: "search";
      options: (page: number, terms: string) => Promise<undefined | [Paged, Options<K>]>;
      textSearchRequireTerms?: boolean;
    }
);

export type DropdownDivProps<K = number> = Omit<
  JSX.IntrinsicElements["div"],
  keyof DropdownCoreProps<K> | "ref"
>;

export interface Option<K> {
  key?: K;
  title: JSX.Element;
  type?: "content" | "button";
  onClick?: (event: MouseEvent) => void;
  isActive?: boolean;
  extra?: any;
}

export type Options<K> = (Option<K> | string)[];

export default function Comp<K extends string | number | symbol>(outerProps: Props<K>) {
  const [props, divProps] = splitProps(outerProps, [
    "value",
    "onChange",
    "disabled",
    "children",
    "renderOption",
    "isRight",
    "hoverable",
    "kind",
    "options",
    "deps",
    "textSearchRequireTerms" as any,
    "class",
    "buttonClass",
  ]) as [DropdownCoreProps<K>, DropdownDivProps<K>];

  const { hoverable, onChange, renderOption: DropdownItem = (props) => props.option.title } = props;

  const [getPage, setPage] = createSignal(0);
  const [getTerms, setTerms] = createSignal<string | undefined>(undefined);
  const [isOpen, setOpen] = createSignal(false);
  const [activeKey, setActiveKey] = createSignal<null | K>(untrack(() => props.value) ?? null);

  interface R {
    items: Options<K>;
    byKey: { [k in K]?: Option<K> };
    page: Paged;
  }

  const [getOptions, setOptions] = createSignal<R>();
  createEffect(() => {
    // listen to these, but don't actually access yet, to help typescript infer
    // the final type of "options" based on "kind"
    props.kind;
    props.options;
    props.deps;
    const page = getPage();
    const terms = getTerms();
    untrack(async () => {
      if (typeof props.options === "function") {
        if (
          props.kind === "search" &&
          props.textSearchRequireTerms &&
          (terms === "" || terms === undefined)
        ) {
          // i.e. require search terms to be there before having any items
          setOptions({
            items: [] as string[],
            byKey: {},
            page: unpaged,
          });
          return;
        }

        let r: [PagedForUuid, Options<K>] | undefined;

        if (props.kind === "search") {
          r = await props.options(page, terms ?? "");
        } else if (props.kind === "fetch") {
          r = await props.options(page);
        } else {
          throw new Error(`Invalid Dropdown kind, expected fetch or search but got ${props.kind}`);
        }

        if (r === undefined) return;
        setOptions({
          items: r[1],
          page: r[0],
          byKey: processOptions(r[1]),
        });
      } else {
        setOptions({
          items: props.options,
          page: {
            page: 0,
            has_more: false,
            items: [],
            total_count: props.options.length,
          },
          byKey: processOptions(props.options),
        });
      }
    });
  });

  createEffect(() => {
    const value = props.value;
    if (value !== untrack(activeKey)) {
      const v: null | K = value ?? null;
      setActiveKey(() => v);
    }
  });

  const onClickItem = (option: Option<K>) => (event: MouseEvent) => {
    if (option.onClick !== undefined) {
      option.onClick(event);
    }
    if (onChange !== undefined) {
      onChange(option);
    }
    if (props.value === undefined) {
      setActiveKey(() => option.key as K);
    }
    console.log("onClickItem");
    setOpen(false);
  };

  createEffect(() => {
    if (props.disabled) setOpen(false);
  });

  function onCloseMenu() {
    setOpen(false);
  }

  function DropdownTrigger() {
    const getActiveOption = createMemo(() => {
      const optionsByKey = getOptions()?.byKey;
      const activeOption: Option<K> | undefined = optionsByKey?.[activeKey() as K];
      return activeOption;
    });
    return () =>
      getActiveOption() !== undefined ? (
        <DropdownItem option={getActiveOption()!} />
      ) : (
        props.children
      );
  }

  let dropdownButton: HTMLButtonElement | undefined = undefined;

  return (
    <div
      class={classNames(theme.dropdown, props.class)}
      data-active={isOpen()}
      onMouseLeave={() => {
        if (hoverable) setOpen(false);
      }}
      onMouseEnter={() => {
        if (hoverable) setOpen(true);
      }}
      {...divProps}
    >
      <button
        ref={dropdownButton}
        disabled={props.disabled}
        class={classNames(theme.dropdownButton, props.buttonClass)}
        onClick={() => setOpen((o) => !o)}
        aria-haspopup="true"
      >
        <div class={theme.dropdownTrigger}>
          <DropdownTrigger />
          <Icon type="arrow-dropdown" />
        </div>
      </button>
      <ClampY>
        <div
          class={theme.dropdownMenu}
          ref={(div: HTMLDivElement) => {
            function onClick(ev: MouseEvent) {
              const target = ev.target;
              if (target instanceof Node) {
                if (!div.contains(target)) {
                  onCloseMenu();
                }
              }
            }
            document.body.addEventListener("click", onClick);
            onCleanup(() => document.body.removeEventListener("click", onClick));
          }}
        >
          <div class={theme.dropdownContent}>
            <Show when={props.kind === "search"}>
              <div class={theme.dropdownSearchBox}>
                <input
                  type="text"
                  placeholder="Search..."
                  onInput={(ev) => {
                    const val = ev.currentTarget.value.trim();
                    setTerms(val === "" ? undefined : val);
                  }}
                />
                <Icon class="inputIcon" type="search" />
              </div>
            </Show>
            <Await
              data={getOptions}
              children={(opts) => (
                <SimplePaged
                  source={() => Promise.resolve([opts.page, opts.items])}
                  getPage={getPage}
                  renderContent={(c) => c}
                  renderItems={(c) => <div class={theme.dropdownContents}>{c.children}</div>}
                  renderItem={(item) => {
                    if (typeof item === "string") {
                      return (
                        <div class={classNames(theme.dropdownItem, theme.dropdownHeading)}>
                          {item}
                        </div>
                      );
                    }
                    if (item.type === "content") {
                      return (
                        <div class={classNames(theme.dropdownItem)}>
                          <DropdownItem option={item} />
                        </div>
                      );
                    }
                    return (
                      <a
                        class={theme.dropdownItem}
                        onClick={onClickItem(item)}
                        data-active={item.isActive !== false && activeKey() === item.key}
                      >
                        <DropdownItem option={item} />
                      </a>
                    );
                  }}
                  renderPageButtons={(props) => (
                    <Show when={props.prev !== undefined || props.next !== undefined}>
                      <div class={theme.dropdownPageButtons}>
                        <button
                          class="secondary"
                          disabled={props.prev === undefined}
                          onClick={() => setPage(props.prev as number)}
                        >
                          Previous
                        </button>
                        <button
                          class="secondary"
                          disabled={props.next === undefined}
                          onClick={() => setPage(props.next as number)}
                        >
                          Next
                        </button>
                      </div>
                    </Show>
                  )}
                />
              )}
            />
          </div>
        </div>
      </ClampY>
    </div>
  );
}

// TODO clean up this so the types are a bit less horrific

export const unpaged: Paged = {
  total_count: Number.MAX_SAFE_INTEGER,
  items: [],
  has_more: false,
  page: 0,
};

type SimpleItem = {
  id: string | number;
} & (
  | {
      name: string;
      title?: undefined;
    }
  | {
      name?: undefined;
      title: string;
    }
);

export function simpleItems<T extends SimpleItem>(items: T[]): [Paged, Option<T["id"]>[]] {
  return [
    { total_count: items.length, items: [], has_more: false, page: 0 },
    items.map((i) => ({
      key: i.id,
      title: i.name ?? i.title,
      extra: i,
    })),
  ];
}

function processOptions<K extends string | number | symbol>(options: Options<K>) {
  const optionTitlesByKey: { [k in K]?: Option<K> } = {};
  for (let i = 0; i < options.length; i++) {
    const opt = options[i];
    if (typeof opt === "string") continue;
    if (opt.key === undefined) opt.key = i as any;
    optionTitlesByKey[opt.key as K] = opt;
  }
  return optionTitlesByKey;
}
