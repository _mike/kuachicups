import { useIntersectionObserver } from "@/context/IntersectionObserver";
import {
  Accessor,
  createContext,
  createEffect,
  createSignal,
  JSX,
  Setter,
  splitProps,
  useContext,
} from "solid-js";
import theme from "./theme.module.scss";
import {
  computePosition,
  shift,
  flip,
  arrow,
  ComputePositionReturn,
  Placement,
  offset,
  Side,
} from "@floating-ui/dom";
import { Portal } from "solid-js/web";

const TOOLTIP_LAYER = theme.layer;
export function TooltipLayer() {
  return (
    <div id={theme.layerWrapper}>
      <div id={TOOLTIP_LAYER} />
    </div>
  );
}

export default function Tooltip(
  _props: {
    tooltip: JSX.Element;
    children: JSX.Element;
    dotted?: boolean;
    controlled?: boolean;
    placement?: Placement;
    zIndex?: number;
  } & Omit<JSX.IntrinsicElements["div"], "tooltip" | "children" | "dotted" | "controlled">
) {
  const [active, setActive] = createSignal(false);
  const [props, divProps] = splitProps(_props, [
    "tooltip",
    "children",
    "dotted",
    "controlled",
    "placement",
    "zIndex",
  ]);

  let caretDiv: HTMLDivElement;
  let tooltipDiv: HTMLDivElement;
  let childrenDiv: HTMLDivElement;

  createEffect((wasActive: undefined | boolean) => {
    if (wasActive === undefined && !active()) {
      tooltipDiv.style.setProperty("width", "0");
      tooltipDiv.style.setProperty("height", "0");
      tooltipDiv.style.setProperty("overflow", "hidden");
      return false;
    } else {
      tooltipDiv.style.setProperty("width", "");
      tooltipDiv.style.setProperty("height", "");
      tooltipDiv.style.setProperty("overflow", "");
      computePosition(childrenDiv, tooltipDiv, {
        placement: props.placement,
        middleware: [offset(8), flip(), shift(), arrow({ element: caretDiv })],
      }).then((pos) => {
        const staticSide = {
          top: "bottom",
          right: "left",
          bottom: "top",
          left: "right",
        }[pos.placement.split("-")[0]];
        tooltipDiv.dataset["placement"] = pos.placement;
        tooltipDiv.style.setProperty("position", pos.strategy);
        tooltipDiv.style.setProperty("left", pos.x + "px");
        tooltipDiv.style.setProperty("top", pos.y + "px");
        caretDiv.dataset["side"] = staticSide;
        caretDiv.style.setProperty("position", pos.strategy);
        const arrow = pos.middlewareData.arrow;
        const arrowX = arrow?.x;
        const arrowY = arrow?.y;
        caretDiv.style.setProperty("left", arrowX !== undefined ? arrowX + "px" : "");
        caretDiv.style.setProperty("top", arrowY !== undefined ? arrowY + "px" : "");
        caretDiv.style.setProperty(staticSide!, "-0.3rem");
      });
      return true;
    }
  });

  return (
    <TooltipContext.Provider value={{ active, setActive }}>
      <div
        class={theme.container}
        classList={{
          [theme.dotted]: props.dotted,
          [theme.active]: active(),
          [theme.controlled]: props.controlled,
        }}
        {...divProps}
      >
        <div
          ref={(d) => (tooltipDiv = d)}
          class={theme.tooltip}
          style={{ "--tooltip-z-index": props.zIndex }}
        >
          <div class={theme.caret} ref={(d) => (caretDiv = d)} />
          <div class={theme.tooltipContent}>{props.tooltip}</div>
        </div>
        <div ref={(d) => (childrenDiv = d)} class={theme.children}>
          {props.children}
        </div>
      </div>
    </TooltipContext.Provider>
  );
}

const TooltipContext = createContext<{
  active: Accessor<boolean>;
  setActive: Setter<boolean>;
}>({
  active() {
    log.warn("ToolTipContext.active: No tooltip context available");
    return false;
  },
  setActive() {
    log.warn("ToolTipContext.setActive: No tooltip context available");
    return false as any;
  },
});

export function useSetTooltipActive() {
  return useContext(TooltipContext).setActive;
}
