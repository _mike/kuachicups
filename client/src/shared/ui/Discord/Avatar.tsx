import { Player } from "@/api";
import useBlobCache from "@/blobCache";
import { JSX, splitProps } from "solid-js";
import theme from "./theme.module.scss";
import fallbackAvatar from "@/resources/aql1.png";

export type Size = number | string;

export interface DiscordAvatarLinkProps {
  player: Player;
  size?: Size;
}

export function discordAvatarLink(id: string, avatar: undefined | null | string, size?: Size) {
  if (avatar == null) return "";
  const clampedSize =
    size === undefined
      ? undefined
      : typeof size !== "number"
      ? 256
      : size <= 100
      ? 80
      : size <= 128
      ? 128
      : 256;
  const param = size === undefined ? "" : `?size=${clampedSize}`;
  return `https://cdn.discordapp.com/avatars/${id}/${avatar}.png${param}`;
}

const defaultSize: Size = 32;

export function useDiscordAvatar(
  getData: () => {
    discordId?: undefined | null | string;
    discordAvatar?: undefined | null | string;
    size?: Size;
  }
) {
  return useBlobCache(() => {
    const { discordId, discordAvatar, size = defaultSize } = getData();
    if (typeof discordId !== "string") {
      return {
        id: "avatar-none",
        src: fallbackAvatar,
        fallback: "",
      };
    }
    return {
      id: `avatar-${discordId}-${size}`,
      src: discordAvatarLink(discordId, discordAvatar, size),
      fallback: discordAvatarLink(discordId, discordAvatar),
    };
  });
}

export function DiscordAvatarImg(
  p: {
    class?: string;
    discordId?: undefined | null | string;
    discordAvatar?: undefined | null | string;
    size?: Size;
  } & JSX.ImgHTMLAttributes<any>
) {
  const [dp, imgProps] = splitProps(p, ["discordId", "discordAvatar", "size"]);
  const img = useDiscordAvatar(() => dp);
  return () => (
    <picture
      class={theme.avatar + " " + (p.class ?? "")}
      style={{ "--avatar-size": `var(--avatar-size-override, ${(dp.size ?? defaultSize) + "px"})` }}
    >
      <source srcset={img()!} />
      <img class={theme.avatarImg} src={fallbackAvatar} {...imgProps} />
    </picture>
  );
}

export function FallbackAvatarImg(props: { size?: Size }) {
  const size = Number(props.size ?? defaultSize);
  return (
    <svg width={size} height={size}>
      <circle
        cx={size * 0.5}
        cy={size * 0.5}
        r={size * 0.5 - 1}
        stroke="var(--muted-border-color)"
        stroke-width="1"
        fill="var(--background-color)"
      />
    </svg>
  );
}

export function DiscordServerAvatarImg(
  p: {
    discordId: string;
    discordAvatar: string;
    size?: Size;
  } & JSX.ImgHTMLAttributes<any>
) {
  const [dp, imgProps] = splitProps(p, ["discordId", "discordAvatar", "size"]);
  const img = useBlobCache(() => ({
    src: `https://cdn.discordapp.com/icons/${dp.discordId}/${dp.discordAvatar}.png?size=${dp.size}`,
    fallback: `https://cdn.discordapp.com/icons/${dp.discordId}/${dp.discordAvatar}.png`,
    id: `team-${dp.discordId}-${dp.discordAvatar}`,
  }));
  return (
    <img
      class={theme.avatar}
      style={{ "--avatar-size": (dp.size ?? defaultSize) + "px" }}
      src={img()!}
      {...imgProps}
    />
  );
}

export function AvatarSpan(props: { children: JSX.Element }) {
  return <span class={theme.avatarSpan}>{props.children}</span>;
}
