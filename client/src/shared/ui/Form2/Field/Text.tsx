import { debounce } from "lodash";
import { createEffect, JSX } from "solid-js";
import { BaseField } from "./Base";
import { createSetInitialValue } from "./control";

export interface TextFieldProps extends BaseField<"input"> {
  placeholder?: string;
  value: string;
  defaultValue?: string;
  onChange: (value: string) => void;
}

export function TextField(props: TextFieldProps) {
  // note https://github.com/ryansolid/solid/issues/203
  const setInitialValue = createSetInitialValue(props);
  return (
    <input
      ref={el => {
        setInitialValue(el);
        createEffect(() => {
          const next = props.value;
          if (next !== el.value) {
            el.value = next;
          }
        });
      }}
      class="input"
      type="text"
      placeholder={props.placeholder ?? ""}
      onInput={debounce((ev) => props.onChange?.(ev.target.value), 100)}
      {...(props.innerProps ?? {})}
    />
  );
}
