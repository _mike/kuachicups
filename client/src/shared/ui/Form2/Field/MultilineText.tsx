import { debounce } from "lodash";
import { createEffect, JSX } from "solid-js";
import { BaseField } from "./Base";
import { createSetInitialValue } from "./control";
import theme from "./theme.module.scss";

export interface MultilineTextFieldProps extends BaseField<"textarea"> {
  class?: string;
  style?: JSX.CSSProperties;
  value?: string;
  onChange?: (value: string) => void;
  placeholder?: string;
  lines?: number;
  expand?: boolean;
}

export function MultilineTextField(props: MultilineTextFieldProps) {
  const setInitialValue = createSetInitialValue(props);
  return (
    <div
      style={props.style}
      class={theme.textareaSizer + " " + props.class}
      data-value={props.value}
    >
      <textarea
        ref={(el) => {
          setInitialValue(el);
          createEffect(() => {
            const next = props.value;
            if (next !== el.value && next !== undefined) {
              el.value = next;
            }
          });
        }}
        class={theme.multilineText + " " + props.class}
        classList={{ expand: props.expand === true }}
        placeholder={props.placeholder ?? ""}
        onInput={debounce((ev) => props.onChange?.(ev.target.value), 100)}
        style={{
          "overflow-y": props.expand ? "hidden" : "auto",
          "min-height": "1em",
        }}
        {...(props.innerProps ?? {})}
      />
    </div>
  );
}
