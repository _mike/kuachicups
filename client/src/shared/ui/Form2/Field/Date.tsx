import { format, isMatch } from "date-fns";
import { createMemo } from "solid-js";
import { createSetInitialValue } from "./control";
import { BaseField } from "./Base";
import { debounce } from "lodash";

export function formatDateMaybe(
  date?: string | number | Date | [Date | undefined, { date: string } | string]
): undefined | string {
  if (Array.isArray(date)) {
    const [obj, str] = date;
    return (
      formatDateMaybe(obj) ??
      (typeof str === "string" ? formatDateMaybe(str) : formatDateMaybe(str.date))
    );
  }
  if (typeof date === "string") return date;
  return date instanceof Date || typeof date === "number" ? format(date, "yyyy-MM-dd") : undefined;
}

export interface DateFieldProps extends BaseField<"input"> {
  value?: string;
  defaultValue?: string;
  onChange?: (value: string) => void;
}

export function DateField(props: DateFieldProps) {
  const selfIsValid = createMemo(() => {
    const val = props.value;
    return val === "" || (typeof val === "string" && isMatch(val, "yyyy-MM-dd"));
  });
  const ref = createSetInitialValue(props);
  return (
    <>
      <input
        ref={ref}
        class="input"
        type="date"
        pattern="\d{4}-\d{2}-\d{2}"
        onChange={debounce((ev) => props.onChange?.(ev.target.value), 100)}
      >
        Date
      </input>
      {selfIsValid() ? undefined : <p class="help is-danger">Invalid date</p>}
    </>
  );
}
