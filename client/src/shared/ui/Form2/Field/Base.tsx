import { JSX } from "solid-js";

export type BaseField<CompName extends keyof JSX.IntrinsicElements> = {
  innerProps?: JSX.IntrinsicElements[CompName];
};
