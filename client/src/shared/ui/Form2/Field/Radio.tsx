import { For, JSX } from "solid-js";
import { BaseField } from "./Base";
import { createSetInitialChecked } from "./control";

export interface RadioFieldProps<T> extends BaseField<"input"> {
  trueLabel?: string;
  falseLabel?: string;
  values: readonly { key: T; name: JSX.Element }[];
  value?: T;
  defaultValue?: T;
  onChange?: (value: T) => void;
}

export function RadioField<T>(props: RadioFieldProps<T>) {
  return (
    <div class="control">
      <For each={props.values}>
        {(v) => (
          <label class="radio">
            <input
              type="radio"
              checked={props.value === v.key}
              onChange={() => props.onChange?.(v.key)}
            />
            &nbsp;{v.name}
          </label>
        )}
      </For>
    </div>
  );
}
