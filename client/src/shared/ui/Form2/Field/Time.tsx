import { format, isMatch } from "date-fns";
import { createMemo } from "solid-js";
import { createSetInitialValue } from "./control";
import { BaseField } from "./Base";
import { debounce } from "lodash";

export function formatTimeMaybe(date?: string | number | Date): undefined | string {
  if (typeof date === "string") {
    return date;
  }
  return date instanceof Date || typeof date === "number" ? format(date, "HH:mm") : undefined;
}

export interface TimeFieldProps extends BaseField<"input"> {
  value?: string;
  defaultValue?: string;
  onChange?: (value: string) => void;
}

export function TimeField(props: TimeFieldProps) {
  const selfIsValid = createMemo(() => {
    const val = props.value;
    return val === "" || (typeof val === "string" && isMatch(val, "HH:mm"));
  });
  const ref = createSetInitialValue(props);
  return (
    <>
      <input
        ref={ref}
        class="input"
        type="time"
        pattern="\d{2}:\d{2}"
        onChange={debounce((ev) => props.onChange?.(ev.target.value), 100)}
      >
        Date
      </input>
      {selfIsValid() ? undefined : <p class="help is-danger">Invalid time</p>}
    </>
  );
}
