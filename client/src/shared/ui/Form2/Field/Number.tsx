import debounce from "lodash/debounce";
import { createEffect, createSignal, splitProps } from "solid-js";
import { BaseField } from "./Base";

export interface StrictNumberFieldProps extends BaseField<"input"> {
  value?: number | undefined;
  defaultValue?: number;
  onChange?: (value: number | undefined) => void;
  precision?: number;
  min?: number;
  max?: number;
  step?: number;
  constraint?: (val: number) => boolean;
}

export function StrictNumberField(props: StrictNumberFieldProps) {
  const [ownProps, innerProps] = splitProps(props, ["value", "onChange", "defaultValue"]);
  return (
    <NumberField
      {...innerProps}
      value={typeof ownProps.value === "number" ? ownProps.value : ""}
      defaultValue={ownProps.defaultValue}
      onChange={(v) =>
        ownProps.onChange?.(typeof v === "number" ? v : ownProps?.defaultValue ?? undefined)
      }
    />
  );
}

export interface NumberFieldProps extends BaseField<"input"> {
  value?: string | number;
  defaultValue?: number;
  onChange?: (value: string | number) => void;
  precision?: number;
  min?: number;
  max?: number;
  step?: number;
  constraint?: (val: number) => boolean;
}

export function NumberField(props: NumberFieldProps) {
  const [getIsValid, setIsValid] = createSignal(false);
  return (
    <>
      <input
        ref={(ref: HTMLInputElement) => {
          if (typeof props.value === "number") {
            ref.valueAsNumber = props.value;
          } else if (typeof props.value === "string") {
            ref.value = props.value;
          } else if (typeof props.defaultValue === "number") {
            ref.valueAsNumber = props.defaultValue;
          }
          setIsValid(ref.validity.valid);
        }}
        type="number"
        min={props.min}
        max={props.max}
        step={props.step ?? 1}
        onInput={debounce((ev) => {
          const val = ev.target.value;
          const n = Number.parseInt(val);
          let valid = ev.target.validity.valid;
          if (typeof props.constraint === "function") {
            valid = valid && props.constraint(n);
          }
          setIsValid(valid);
          props.onChange?.(
            valid && Number.isFinite(n) && Number.isSafeInteger(n) && n >= 0 ? n : val
          );
        }, 100)}
        {...(props.innerProps ?? {})}
      />
      {getIsValid() ? undefined : <p class="help is-danger">Invalid number</p>}
    </>
  );
}
