import { BaseField } from "./Base";
import { createSetInitialChecked } from "./control";

export interface CheckBoxFieldProps extends BaseField<"input"> {
  value?: boolean;
  defaultValue?: boolean;
  onChange?: (value: boolean) => void;
  label?: string;
  description?: string;
}

export function CheckBoxField(props: CheckBoxFieldProps) {
  const ref = createSetInitialChecked(props);
  return (
    <label class="checkbox">
      <input
        ref={ref}
        type="checkbox"
        onChange={(ev) => props.onChange?.(ev.currentTarget.checked)}
        {...(props.innerProps ?? {})}
      />{" "}
      {props.label}
      {props.description && <p class="help">{props.description}</p>}
    </label>
  );
}
