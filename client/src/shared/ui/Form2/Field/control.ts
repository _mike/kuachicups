export function createSetInitialValue<
  Comp extends { value: T; defaultValue?: T; ref?: T | ((el: T) => void) },
  Props extends {
    value?: T;
    defaultValue?: T;
    innerProps?: {
      ref?: Comp["ref"];
    };
  },
  T
>(props: Props): (ref: Comp) => void {
  return (comp) => {
    const val = props.value ?? props.defaultValue;
    if (val !== undefined) {
      comp.value = val;
    }
    const ref = props.innerProps?.ref;
    if (ref !== undefined && typeof ref === "function") {
      ref(comp);
    }
  };
}

export function createSetInitialChecked<
  Comp extends { checked: boolean; ref?: Comp | ((el: Comp) => void) },
  Props extends {
    value?: boolean;
    defaultValue?: boolean;
    innerProps?: {
      ref?: Comp["ref"];
    };
  }
>(props: Props): (comp: Comp) => void {
  return (comp) => {
    const val = props.value ?? props.defaultValue;
    if (val !== undefined) {
      comp.checked = val;
    }
    const ref = props.innerProps?.ref;
    if (ref !== undefined && typeof ref === "function") {
      ref(comp);
    }
  };
}
