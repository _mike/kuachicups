import { debounce } from "lodash";
import { createSignal, JSX } from "solid-js";
import { BaseField } from "./Base";

export interface DurationFieldProps extends BaseField<"input"> {
  precision: "s" | "m" | "h";
  defaultValue?: number | string;
  value?: number | string;
  onChange?: (next: number | string) => void;
}

const durationPlaceholder = {
  m: "Duration in minutes",
  s: "Duration in seconds",
  h: "Duration in hours",
} as const;

export function DurationField(props: DurationFieldProps): JSX.Element {
  const [getIsValid, setIsValid] = createSignal(false);
  return (
    <>
      <input
        class="input"
        type="number"
        ref={(ref: HTMLInputElement) => {
          ref.value = String(props.value ?? props.defaultValue);
          setIsValid(ref.validity.valid);
        }}
        placeholder={durationPlaceholder[props.precision]}
        min={0}
        pattern="\d+"
        onInput={debounce((ev) => {
          const val = ev.target.value;
          const n = Number.parseInt(val);
          const valid = ev.target.validity.valid;
          setIsValid(ev.target.validity.valid);
          props.onChange?.(
            valid && Number.isFinite(n) && Number.isSafeInteger(n) && n >= 0
              ? n
              : val
          );
        }, 100)}
      />
      {getIsValid() ? undefined : (
        <p class="help is-danger">Invalid duration</p>
      )}
    </>
  );
}
