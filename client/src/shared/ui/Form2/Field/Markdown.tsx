import type { EditorOptions, Editor } from "@toast-ui/editor/types/editor";
import { getOwner, onCleanup, runWithOwner } from "solid-js";
import theme from "./theme.module.scss";

let TUI: Promise<typeof import("@toast-ui/editor/types/index")> | undefined;

async function _loadTUI() {
  await import("@toast-ui/editor/dist/theme/toastui-editor-dark.css");
  await import("@toast-ui/editor/dist/toastui-editor.css");
  return await import("@toast-ui/editor");
}

function loadTUI() {
  if (TUI !== undefined) return TUI;
  return (TUI = _loadTUI());
}

export interface MarkdownFieldProps {
  value?: string;
  onChange?: (value: string) => void;
  options?: Partial<EditorOptions>;
  tuiRef?: (tui: Editor) => void;
}

export function MarkdownField(props: MarkdownFieldProps) {
  const _TUI = loadTUI();
  return (
    <div
      class={theme.markdownEditor}
      ref={(div) => {
        const owner = getOwner();
        _TUI.then((TUI) => {
          const tui = new TUI.Editor({
            ...props.options,
            el: div,
            theme: "dark",
            initialEditType: "wysiwyg",
            initialValue: props.value,
            height: "600px",
            events: {
              change: () => {
                const md = tui.getMarkdown();
                props.onChange?.(md);
              },
            },
          });
          props?.tuiRef?.(tui);
          runWithOwner(owner!, () => {
            onCleanup(() => {
              tui.destroy();
            });
          });
        });
      }}
    />
  );
}
