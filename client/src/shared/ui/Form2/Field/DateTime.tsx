import { isValid, parseISO } from "date-fns";
import { JSX } from "solid-js";
import { DateField, formatDateMaybe } from "./Date";
import { TimeField, formatTimeMaybe } from "./Time";

export interface IDateTimeStr {
  time: string;
  date: string;
  parsed: undefined | Date;
}

export function parseIDT(idt: IDateTimeStr): IDateTimeStr {
  const str = `${idt.date} ${idt.time}`;
  const obj = parseISO(str);
  return {
    time: idt.time,
    date: idt.date,
    parsed: isValid(obj) ? obj : undefined,
  };
}

export function asIDT(parsed: undefined | Date): IDateTimeStr {
  return {
    time: formatTimeMaybe(parsed) ?? "",
    date: formatDateMaybe(parsed) ?? "",
    parsed,
  };
}

export interface DateTimeFieldProps {
  class?: string;
  label?: JSX.Element;
  description?: JSX.Element;
  tooltip?: string;
  defaultValue?: IDateTimeStr;
  value: IDateTimeStr;
  onChange?: (value: IDateTimeStr) => void;
}

export function DateTimeField(props: DateTimeFieldProps) {
  const defaultDate = formatDateMaybe(props.defaultValue?.date);
  const defaultTime = formatTimeMaybe(props.defaultValue?.time);
  return (
    <div class={"grid small " + props.class}>
      <DateField
        defaultValue={defaultDate}
        value={props.value?.date}
        onChange={(date: string) => {
          const val = { date, time: props.value.time, parsed: undefined };
          props.onChange?.(parseIDT(val));
        }}
      />
      <TimeField
        defaultValue={defaultTime}
        value={props.value?.time}
        onChange={(time: string) => {
          const val = { date: props.value.date, time, parsed: undefined };
          props.onChange?.(parseIDT(val));
        }}
      />
    </div>
  );
}
