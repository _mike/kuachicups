import * as O from "optics-ts";
import { createMemo, ErrorBoundary, For, JSX, onError, splitProps } from "solid-js";
import { reconcile, SetStoreFunction, Store } from "solid-js/store";
import { Object } from "ts-toolbelt";
import Keyed from "../Keyed";
export { CheckBoxField } from "./Field/CheckBox";
export { DateField } from "./Field/Date";
export { DateTimeField } from "./Field/DateTime";
export { DurationField } from "./Field/Duration";
export { MarkdownField } from "./Field/Markdown";
export { MultilineTextField } from "./Field/MultilineText";
export { NumberField, StrictNumberField } from "./Field/Number";
export { RadioField } from "./Field/Radio";
export { TextField } from "./Field/Text";
export { TimeField } from "./Field/Time";
import log from "loglevel";

export interface ManagerProps<S> {
  store: Store<S>;
  setStore: SetStoreFunction<S>;
}

export type FieldProps<
  S,
  T,
  Props extends { value?: T; onChange?: (value: T) => void },
  Comp extends (props: Props) => JSX.Element
> = {
  component: Comp;
  lens: O.Lens<S, any, T>; // Lens<S, T>;
} & Omit<Props, "component" | "value" | "onChange">;

export interface Fs<S> {
  fld<T>(
    lens: O.Lens<S, any, T>,
    children: (value: () => T, onChange: (value: T) => void) => JSX.Element
  ): JSX.Element;
}

export interface WrappedStore<T> {
  get: () => Store<T>;
  view: <X>(lens: O.Lens<T, any, X>) => X;
  preview: <X>(
    lens: O.Lens<T, any, X> | O.Prism<T, any, X> | O.Traversal<T, any, X>
  ) => X | undefined;
  set: <X>(lens: O.Lens<T, any, X> | O.Prism<T, any, X> | O.Traversal<T, any, X>, value: X) => void;
  remove: <X>(lens: O.Prism<T, O.Params<any, true>, X>) => void;
  modify: <X>(
    lens: O.Lens<T, any, X> | O.Prism<T, any, X> | O.Traversal<T, any, X>,
    fn: (val: X) => X
  ) => void;
}

export function wrapStore<S extends {}, T extends {}>(
  store: Store<S>,
  setStore: SetStoreFunction<S>,
  optic: O.Lens<S, any, T> | O.Equivalence<S, any, T>
): WrappedStore<T> {
  return {
    get: () => O.get(optic)(store as S) as Store<T>,
    view: (inner) => O.get(optic.compose(inner))(store),
    preview: (inner) => O.preview(optic.compose(inner as O.Prism<T, any, any>))(store),
    set: (lens, value) => {
      setStore((s: any): any =>
        reconcile(O.set(lens)(value)(s as any), { merge: false })(s as any)
      );
    },
    remove: (lens) => {
      setStore((s: any): any => reconcile(O.remove(lens)(s as any), { merge: false })(s as any));
    },
    modify: (lens, fn) => {
      setStore((s: any): any =>
        reconcile(O.modify(lens)(fn)(s as any), { merge: false })(s as any)
      );
    },
  };
}

export type FldProps<S, T> = Object.Merge<
  JSX.IntrinsicElements["div"],
  {
    key?: string;
    label?: JSX.Element;
    legend?: JSX.Element;
    description?: JSX.Element;
    tooltip?: string;
    defaultValue?: T;
    optic: O.Lens<S, any, T> | O.Prism<S, any, T> | O.Traversal<S, any, T>;
    beforeChangeHook?: (next: T) => void;
    afterChangeHook?: () => void;
    error?: (value: T) => undefined | JSX.Element;
    children(
      value: () => T,
      onChange: (value: T) => void,
      self: O.Lens<S, any, T> | O.Prism<S, any, T> | O.Traversal<S, any, T>
    ): JSX.Element;
  }
>;

export interface ArrayFldProps<S, T> {
  optic: O.Lens<S, any, T[]> | O.Prism<S, any, T[]>;
  create(index: number): T;
  children(props: {
    Each(props: {
      children: (value: T, index: () => number, optic: O.Prism<S, any, T>) => JSX.Element;
    }): JSX.Element;
    onChange: (index: number) => (value: T) => void;
    onAdd: () => void;
    onDelete: (index: number) => void;
  }): JSX.Element;
}

export function createField<S extends {}>(
  ...args: [store: Store<S>, setStore: SetStoreFunction<S>] | [WrappedStore<S>]
): {
  optic: O.OpticFor<S>;
  fld<T>(
    optic: O.Lens<S, any, T> | O.Prism<S, any, T> | O.Traversal<S, any, T>,
    children: (value: () => T, onChange: (value: T) => void) => JSX.Element
  ): JSX.Element;
  Fld<T>(props: FldProps<S, T>): JSX.Element;
  ArrayFld<T>(props: ArrayFldProps<S, T>): JSX.Element;
} {
  const s = args.length === 2 ? wrapStore(args[0], args[1], O.optic()) : args[0];

  function fld<T>(
    optic: O.Lens<S, any, T> | O.Prism<S, any, T> | O.Traversal<S, any, T>,
    children: (
      value: () => T,
      onChange: (value: T) => void,
      optic: O.Lens<S, any, T> | O.Prism<S, any, T> | O.Traversal<S, any, T>
    ) => JSX.Element,
    defaultValue?: T,
    beforeChangeHook?: (next: T) => void,
    afterChangeHook?: () => void,
    error?: (value: T) => undefined | JSX.Element
  ): JSX.Element {
    const getter = () =>
      optic._tag === "Lens"
        ? (O.get(optic)(s.get()) ?? defaultValue)!
        : (O.preview(optic)(s.get()) ?? defaultValue)!;
    const err = createMemo(() => {
      const e = error?.(getter());
      if (e !== undefined) return <label class="help is-danger">{e}</label>;
      return undefined;
    });
    return (
      <>
        {children(
          getter,
          (val) => {
            beforeChangeHook?.(val);
            s.set(optic, val);
            afterChangeHook?.();
          },
          optic
        )}
        {err()}
      </>
    );
  }

  return {
    optic: O.optic(),
    fld,
    Fld(props) {
      const [lp, divProps] = splitProps(props, [
        "label",
        "legend",
        "tooltip",
        "description",
        "children",
        "optic",
        "defaultValue",
        "beforeChangeHook",
        "afterChangeHook",
        "key",
        "error",
      ]);
      function Children() {
        onError((e) => {
          log.error("form error", e);
        });
        return fld(
          lp.optic,
          lp.children as any,
          lp.defaultValue,
          lp.beforeChangeHook,
          lp.afterChangeHook,
          lp.error
        );
      }
      return (
        <div {...divProps}>
          {lp.label && <label>{lp.label}</label>}
          {lp.legend && <legend>{lp.legend}</legend>}
          <ErrorBoundary fallback={(_err) => <span>Error</span>}>
            {lp.key !== undefined ? (
              <Keyed key={lp.key}>
                <Children />
              </Keyed>
            ) : (
              <Children />
            )}
          </ErrorBoundary>
          {lp.description && <small>{lp.description}</small>}
        </div>
      );
    },

    ArrayFld(props) {
      function getArray() {
        return props.optic._tag === "Lens"
          ? O.get(props.optic)(s.get())
          : O.preview(props.optic)(s.get()) ?? [];
      }
      return props.children({
        Each(p) {
          return (
            <For each={getArray()}>{(val, ix) => p.children(val, ix, props.optic.at(ix()))}</For>
          );
        },
        onChange: (index) => (value) => s.set(props.optic.at(index), value),
        onAdd: () => {
          const cur = getArray();
          const next = [...cur, props.create(cur.length)];
          s.set(props.optic, next);
        },
        onDelete: (index) => {
          const cur = getArray();
          const next = [...cur];
          next.splice(index, 1);
          s.set(props.optic, next);
        },
      });
    },
  };
}
