import {
  JSX,
  splitProps,
} from "solid-js";
import theme from "./theme.module.scss";

export default function Container(_props: JSX.IntrinsicElements["div"]) {
  const [props, divProps] = splitProps(_props, ["class", "children"]);
  return (
    <div class={theme.wrapper + " " + (props.class ?? "")} {...divProps}>
      <div class={theme.container}>{props.children}</div>
    </div>
  );
}
