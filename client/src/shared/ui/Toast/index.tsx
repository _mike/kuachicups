import classNames from "classnames";
import clipboardCopy from "clipboard-copy";
import log from "loglevel";
import {
  createContext,
  createSignal,
  For,
  getOwner,
  JSX,
  onCleanup,
  onError,
  splitProps,
  untrack,
  runWithOwner,
} from "solid-js";
import { createStore, produce } from "solid-js/store";
import { Owner } from "solid-js/types/reactive/signal";
import Icon from "../Icon";
import theme from "./theme.module.scss";

const toastAnimTime = 200;
const toastTimeout = 5000;

namespace toast {
  export const success = toastSuccess;
  export const failure = toastFailure;
}

export default toast;

function mkToast(
  props: {
    timeout?: number;
    children?: JSX.Element;
  } & JSX.HTMLAttributes<HTMLDivElement>
): null {
  const { owner, push, pop } = globalCtx;
  if (owner === null) {
    log.error("no toast context available");
    return null;
  }

  return runWithOwner(owner, () => {
    const self = push((me: { id: number }) => {
      let contentsRef: HTMLDivElement | undefined;
      const [popping, setPopping] = createSignal(false);
      const timeout = untrack(() => props.timeout ?? toastTimeout);
      function onPop() {
        setPopping(true);
        setTimeout(() => {
          pop(me.id);
        }, toastAnimTime);
      }
      function onCopy() {
        const content = contentsRef?.textContent;
        if (typeof content !== "string") return;
        clipboardCopy(content).then(
          () => {
            toast.success("Copied notification contents");
          },
          () => {
            toast.failure("Could not copy notification contents");
          }
        );
      }
      onCleanup(onPop);
      if (timeout > 0) {
        setTimeout(onPop, timeout - toastAnimTime);
      }
      const [p, dp] = splitProps(props, ["class", "children"]);
      return (
        <div
          class={classNames(theme.toast, p.class)}
          data-popping={popping()}
          {...dp}
          data-theme="light"
        >
          <div class={theme.buttons}>
            <button class="small outline secondary" onClick={onCopy}>
              <Icon type="copy" /> Copy
            </button>
            <button class="small outline secondary" onClick={onPop}>
              <Icon type="close" />
            </button>
          </div>
          <div ref={(div) => (contentsRef = div)} class={theme.toastContents}>
            {p.children}
          </div>
        </div>
      );
    });
    onCleanup(() => pop(self));
    return null;
  });
}

export function toastSuccess(children: string | (() => JSX.Element), timeout?: number) {
  return untrack(() => mkToast({ children, class: theme.success, timeout }));
}

export function toastFailure(children: string | (() => JSX.Element), timeout?: number) {
  return untrack(() => mkToast({ children, class: theme.failure, timeout }));
}

export interface ToastCtx {
  owner: Owner | null;
  push(toast: (props: { id: number }) => JSX.Element): number;
  pop(toastId: number): void;
  toastError(err: any): void;
}

const globalCtx: ToastCtx = {
  owner: null,
  push() {
    return 0;
  },
  pop() {},
  toastError() {},
};

const Ctx = createContext<ToastCtx>(globalCtx);

export function ToastProvider(props: { children: JSX.Element }) {
  let nextId: number = 0;

  const [state, setState] = createStore<{
    toasts: { id: number; toast: () => JSX.Element }[];
    error: undefined | any;
  }>({
    toasts: [],
    error: undefined,
  });

  function push(Toast: (props: { id: number }) => JSX.Element) {
    const id = nextId++;
    setState(
      "toasts",
      produce((ts) => {
        ts.push({ id, toast: () => <Toast id={id} /> });
        return ts;
      })
    );
    return id;
  }

  function pop(toastId: number) {
    setState(
      "toasts",
      produce((ts) => {
        const ix = ts.findIndex((v) => v.id === toastId);
        if (ix >= 0) {
          ts.splice(ix, 1);
        }
        return ts;
      })
    );
  }

  function toastError(err: any) {
    try {
      log.error(err);
      setState("error", err);
    } catch (err) {
      console.error("error while creating toast", err);
    }
  }

  const owner = getOwner();
  const toasts = { owner, push, pop, toastError };
  Object.assign(globalCtx, toasts);

  onError(toastError);

  return (
    <Ctx.Provider value={toasts}>
      {props.children}
      <div id={theme.toasts} style={{ "--toast-anim-time": toastAnimTime + "ms" }}>
        <For each={state.toasts}>{(t) => t.toast()}</For>
      </div>
    </Ctx.Provider>
  );
}
