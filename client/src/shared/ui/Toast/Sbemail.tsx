import { randomInt } from "fp-ts/lib/Random";
import { JSX } from "solid-js";
import "./Sbemail.scss";

export interface Props {
  children?: JSX.Element;
}

export default function SomeError(props: Props) {
  const choice = randomInt(0, choices.length - 1)();
  const ErrorMsg = choices[choice];
  return ErrorMsg(props);
}

function FlagrantSystemError(props: Props) {
  return (
    <blockquote class="sbemail sbemail-flagrant">
      FLAGRANT SYSTEM ERROR
      <br />
      <br />
      The system is down. I dunno what <br />
      you did, moron, but you sure <br />
      screwed everything up good.
      <hr />
      {props.children}
    </blockquote>
  );
}

function ComputerOver(props: Props) {
  return (
    <blockquote class="sbemail sbemail-computerover">
      FLAGRANT SYSTEM ERROR
      <br />
      <br />
      Computer over.
      <br />
      Virus = Very Yes.
      <hr />
      {props.children}
    </blockquote>
  );
}

function FlamboyantSystemError(props: Props) {
  return (
    <blockquote class="sbemail sbemail-flamboyant">
      Flamboyant System Error
      <br />
      <br />
      3rd Qtr. Projection= Bad News
      <hr />
      {props.children}
    </blockquote>
  );
}

function TealScreenOfNearDeath(props: Props) {
  return (
    <blockquote class="sbemail-tsond">
      <div class="sbemail-tsond-dialog">
        <h1 class="sbemail-tsond-heading">
          <span class="sbemail-tsond-icon">&#128683;</span>
          Oh, Child!
          <span class="sbemail-tsond-icon">&#9888;&#65039;</span>
        </h1>
        <p>
          It's the Teal Screen
          <br />
          &nbsp;of Near Death! (TSoND)&nbsp;
        </p>
      </div>
      <hr />
      {props.children}
    </blockquote>
  );
}

function FakeFragrantSystemError(props: Props) {
  return (
    <blockquote class="sbemail sbemail-fragrant">
      FRAGRANT SYSTEM ERROR
      <br />
      Your brand new computer is bloke.
      <br />
      Please prepare to wait on hold
      <br />
      with tech support for several hours.
      <br />
      The current tech support smugness
      <br />
      level is RED.
      <hr />
      {props.children}
    </blockquote>
  );
}

const choices = [
  FlagrantSystemError,
  ComputerOver,
  FlamboyantSystemError,
  TealScreenOfNearDeath,
  FakeFragrantSystemError,
];
