import { Loading } from "@/shared/ui/Loading";
import createDOMPurify from "dompurify";
import log from "loglevel";
import { createEffect } from "solid-js";
import { WorkerPostMessage } from "./types";
import Worker from "./worker?worker";

let worker: Worker | undefined = undefined;
let nextId: number = 0;
const handlers: { [id: number]: (html: string) => void } = {};

let sanitize: createDOMPurify.DOMPurifyI["sanitize"] | undefined;

function renderMarkdown(markdown: string): Promise<string> {
  if (sanitize === undefined) {
    const dompurify = createDOMPurify(window);
    sanitize = dompurify.sanitize.bind(dompurify);
  }

  if (worker === undefined) {
    worker = new Worker();
    worker.onmessage = (ev: MessageEvent<WorkerPostMessage>) => {
      const { id, html } = ev.data;
      const h = handlers[id];
      if (typeof h === "function") {
        delete handlers[id];
        h(sanitize!(html));
      } else {
        log.error("tried to rerun markdown handler", id);
      }
    };
  }

  return new Promise((resolve, reject) => {
    if (worker !== undefined) {
      const id = nextId++;
      handlers[id] = resolve;
      worker.postMessage({ id, markdown });
    } else {
      reject("markdown worker didn't start");
    }
  });
}

export default (props: { class?: string; text?: null | string; inline?: boolean }) => {
  return (
    <div
      ref={(div: HTMLDivElement) => {
        createEffect(() => {
          const { text: markdown, inline } = props;
          if (typeof markdown === "string") {
            renderMarkdown(markdown).then((html) => {
              if (inline) {
                div.innerHTML = html;
              } else {
                div.innerHTML = html;
              }
            });
          }
        });
      }}
      class={"md " + (props.class ?? "")}
    >
      <Loading />
    </div>
  );
};

export function summary(desc: string): string {
  return desc.split("\n\n")[0];
}
