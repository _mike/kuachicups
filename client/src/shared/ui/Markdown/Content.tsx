import theme from "./theme.module.scss";
export function UnsafeMarkdownContent(props: { class?: string; html: string }) {
  return (
    <div
      class={theme.content + " " + (props.class ?? "")}
      innerHTML={props.html}
    />
  );
}
