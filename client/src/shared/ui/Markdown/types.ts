export type WorkerRecvMessage = { id: number; markdown: string };

export type WorkerPostMessage = { id: number; html: string };
