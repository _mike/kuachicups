import log from "loglevel";
import { createEffect, createMemo, For, JSX } from "solid-js";
import { useRoute } from "solid-typefu-router5";
import { Paged } from "@/api";
import { Link, PagedLink } from "@/router";
import { useAsync } from "@/shared/async/Awaitable";
import theme from "./theme.module.scss";

export interface PagedProps<T> {
  source: (page: number, refresh?: boolean) => undefined | [Paged, T[]] | Promise<[Paged, T[]]>;
  fallback?: JSX.Element;
  getPage: () => number;
  renderItem: (item: T, index: () => number) => JSX.Element;
  renderItems: (p: { children: JSX.Element; items: T[] }) => JSX.Element;
  renderContent: (children: JSX.Element) => JSX.Element;
  renderPageButtons: (props: { prev: number | undefined; next: number | undefined }) => JSX.Element;
}

export interface Props<T> {
  class?: string;
  itemClass?: string;
  onClickPage?: (page: number) => void;
  pageHref?: (page: number) => string;
  getPage?: () => number;
  source: (page: number, refresh?: boolean) => undefined | [Paged, T[]] | Promise<[Paged, T[]]>;
  fallback?: JSX.Element;
  render?: (item: T) => JSX.Element;
  renderItem?: (item: T, index: () => number) => JSX.Element;
  renderItems?: (p: { children: JSX.Element; items: T[] }) => JSX.Element;
  renderContent?: (children: JSX.Element) => JSX.Element;
  renderPageButtons?: (props: PaginateProps) => JSX.Element;
}

export const emptyPages: { paged: Paged; items: any[] } = {
  paged: {
    page: 0,
    items: [],
    has_more: false,
    total_count: 0,
  },
  items: [],
};

export function SimplePaged<T>(props: PagedProps<T>): JSX.Element {
  const {
    getPage,
    renderItem,
    renderItems: RenderItems,
    renderContent,
    source,
    renderPageButtons: PageButtons,
  } = props;

  const getPaged = useAsync(async (ref) => {
    const cur = getPage();
    const r = await source(cur, ref);
    if (r !== undefined) {
      const [paged, items] = r;
      return { paged, items };
    }
    return undefined;
  }, emptyPages);

  const nextPage = createMemo(() => {
    const cur = getPage();
    const hasMore = getPaged().paged.has_more;
    const dest = typeof cur === "number" ? cur + 1 : 1;
    return hasMore ? dest : undefined;
  });

  const prevPage = createMemo(() => {
    const cur = getPage();
    return cur > 0 ? cur - 1 : undefined;
  });

  return renderContent(
    createMemo(() => {
      const items: T[] = getPaged().items;
      return (
        <>
          <RenderItems items={items}>
            {() => (
              <For each={items} fallback={props.fallback}>
                {renderItem}
              </For>
            )}
          </RenderItems>
          <PageButtons prev={prevPage()} next={nextPage()} />
        </>
      );
    })
  );
}

export default function Pages<T>(props: Props<T>): JSX.Element {
  const here = useRoute();
  const {
    class: className = theme.pages,
    render,
    source,
    renderItem: _renderItem,
    renderContent,
    renderItems,
    renderPageButtons,
    getPage = () => {
      const param = Number.parseInt(here().params.page);
      if (Number.isSafeInteger(param) && param >= 0) {
        return param;
      }
      return 0;
    },
  } = props;

  function renderItem(item: T, index: () => number) {
    if (_renderItem !== undefined) {
      return _renderItem(item, index);
    } else if (render !== undefined) {
      return <div>{render(item)}</div>;
    }
  }

  // listen to page changes, scroll to top when they happen
  createEffect(() => {
    getPage();
    scroll({ top: 0, behavior: "smooth" });
  });

  return (
    <div class={className}>
      <SimplePaged
        source={source}
        fallback={props.fallback}
        getPage={getPage}
        renderItem={renderItem}
        renderItems={
          renderItems ?? ((p) => <div class={props.itemClass ?? theme.item}>{p.children}</div>)
        }
        renderContent={renderContent ?? passthru}
        renderPageButtons={
          renderPageButtons ??
          ((p) => (
            <PageButtons
              onClickPage={
                props.onClickPage ??
                (() => {
                  log.error("no onClickPage supplied to Paged component");
                })
              }
              next={p.next}
              prev={p.prev}
            />
          ))
        }
      />
    </div>
  );
}

function passthru<T>(p: T): T {
  return p;
}

export interface PaginateProps {
  prev: undefined | number;
  next: undefined | number;
}

export interface PageLinksProps extends PaginateProps {
  to: PagedLink;
}

export function PageLinks(props: PageLinksProps) {
  log.debug("PageLinks", props.prev, props.next);
  return (
    <div class={theme.pagination} aria-label="pagination">
      <Link
        role="button"
        class={"secondary " + theme.paginationPrev}
        to={props.to as "cups"}
        params={{ page: props.prev + "" }}
        disabled={typeof props.prev !== "number"}
      >
        Previous
      </Link>
      <Link
        role="button"
        class={"secondary " + theme.paginationNext}
        to={props.to as "cups"}
        params={{ page: props.next + "" }}
        disabled={typeof props.next !== "number"}
      >
        Next
      </Link>
    </div>
  );
}

export interface PageButtonsProps extends PaginateProps {
  onClickPage(page: number): void;
}

export function PageButtons(props: PageButtonsProps) {
  return (
    <div class={theme.pagination} aria-label="pagination">
      <button
        class={"secondary " + theme.paginationPrev}
        disabled={typeof props.prev !== "number"}
        onClick={() => {
          if (typeof props.prev === "number") props.onClickPage(props.prev);
        }}
      >
        Previous
      </button>
      <button
        class={"secondary " + theme.paginationNext}
        disabled={typeof props.next !== "number"}
        onClick={() => {
          if (typeof props.next === "number") props.onClickPage(props.next);
        }}
      >
        Next
      </button>
    </div>
  );
}
