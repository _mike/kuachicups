import { computePosition, flip } from "@floating-ui/dom";
import { JSX, splitProps } from "solid-js";
import { css } from "solid-styled-components";
import { O } from "ts-toolbelt";

const detailsCls = css`
  display: inline-block;
`;

export default function Dropdown2(
  props: O.Merge<
    { title: JSX.Element; children: JSX.Element; role?: undefined; onToggle?: undefined },
    JSX.IntrinsicElements["details"]
  >
) {
  const [, detailsProps] = splitProps(props, ["title", "children", "role", "onToggle"]);
  let div: HTMLDivElement;
  return (
    <details
      class={detailsCls}
      role="list"
      onToggle={(ev) => {
        const details = ev.currentTarget;
        if (details.open) {
          computePosition(details, div, {
            middleware: [flip()],
          }).then((pos) => {
            const place = pos.placement.split("-")[0];
            div.dataset["placement"] = place;
            details.dataset["placement"] = place;
          });
        }
      }}
      {...detailsProps}
    >
      <summary aria-popup="listbox">{props.title}</summary>
      <div ref={(d) => (div = d)} role="listbox">
        {props.children}
      </div>
    </details>
  );
}
