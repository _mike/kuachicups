import { createSignal, splitProps, JSX, untrack, createEffect } from "solid-js";
import { TransparentLoading } from "./Loading";
import { Case } from "../Case";

enum Confirm {
  Unclicked = 0b000,
  Clicked = 0b100,
  ClickedAndCanConfirm = 0b110,
  Confirmed = 0b001,
}

const CONFIRMING_CLICK_DELAY = 1000;
const CONFIRMING_TIMEOUT = 3000;

function clickable(c: Confirm) {
  return c === Confirm.Unclicked || c === Confirm.ClickedAndCanConfirm;
}

export default function ConfirmButton(
  props: {
    onConfirm: () => void;
    confirmed?: JSX.Element;
    confirming?: JSX.Element;
    confirmingClickDelay?: number;
    confirmingTimeout?: number;
  } & JSX.IntrinsicElements["button"]
) {
  const [getState, setState] = createSignal(Confirm.Unclicked);
  const [p0, p1] = splitProps(props, ["class", "children", "confirming", "confirmed", "onConfirm"]);
  createEffect(() => {
    if (getState() === Confirm.Confirmed) {
      setTimeout(() => setState(Confirm.Unclicked), 1000);
    }
  });
  function onClick() {
    switch (getState()) {
      case Confirm.Unclicked:
        setState(Confirm.Clicked);
        setTimeout(() => {
          setState(Confirm.ClickedAndCanConfirm);
          setTimeout(() => {
            if (getState() !== Confirm.Clicked) {
              setState(Confirm.Unclicked);
            }
          }, props.confirmingTimeout ?? CONFIRMING_TIMEOUT);
        }, props.confirmingClickDelay ?? CONFIRMING_CLICK_DELAY);
        break;

      case Confirm.ClickedAndCanConfirm:
        setState(Confirm.Confirmed);
        p0.onConfirm();
        break;
    }
  }
  return (
    <button
      class={"button " + (p0.class ?? "")}
      classList={{ "is-light": getState() === Confirm.Clicked }}
      onClick={onClick}
      disabled={p1.disabled || !clickable(getState())}
      {...p1}
    >
      <Case key={getState() & 0b101}>
        {{
          [Confirm.Unclicked]: () => p0.children,
          [Confirm.Clicked]: () =>
            p0.confirming ?? (
              <>
                Confirm? <TransparentLoading />
              </>
            ),
          [Confirm.Confirmed]: () => p0.confirmed ?? "Ok",
        }}
      </Case>
    </button>
  );
}
