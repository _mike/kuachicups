import { JSX, createSignal } from "solid-js";
import Icon from "../Icon";
import theme from "./theme.module.scss";

export interface HoverdownProps {
  title: JSX.Element;
  children: JSX.Element;
}

export default function Hoverdown(props: HoverdownProps) {
  const [clicked, setClicked] = createSignal(false);
  return (
    <div class={theme.hoverdown} data-clicked={clicked()}>
      <button
        class={theme.title + " outline contrast"}
        onClick={() => setClicked((c) => !c)}
        onBlur={() => setClicked(false)}
      >
        <span class={theme.titleContent}>{props.title}</span>
        <Icon class={theme.dropdownIcon} type="arrow-dropdown" />
      </button>
      <div class={theme.contents}>{props.children}</div>
    </div>
  );
}
