import classNames from "classnames";
import {
  createComputed,
  createEffect,
  createMemo,
  createSignal,
  For,
  JSX,
  Match,
  Show,
  Switch,
  untrack,
} from "solid-js";
import theme from "./theme.module.scss";

function Tabs<T extends string>(props: {
  class?: string;
  tabs: T[];
  defaultTab: T;
  children: (tab: T) => JSX.Element;
}): JSX.Element;

function Tabs<T>(props: {
  class?: string;
  tabs: T[];
  defaultTab: T;
  title: (tab: T) => JSX.Element;
  children: (tab: () => T) => JSX.Element;
}): JSX.Element;

function Tabs<T>(props: {
  class?: string;
  tabs: T[];
  defaultTab: T;
  title?: (tab: T) => JSX.Element;
  children: (tab: () => T) => JSX.Element;
}) {
  const [curTab, setCurTab] = createSignal<T | undefined>(undefined);
  const initialised = createMemo(() => curTab() !== undefined);
  createComputed(() => {
    setCurTab(() => props.defaultTab);
  });
  return (
    <div>
      <div class={theme.tabs}>
        <For each={props.tabs}>
          {(tab) => (
            <a
              class={theme.tab}
              data-active={tab === curTab()}
              onClick={() => setCurTab(() => tab)}
            >
              {props.title !== undefined ? props.title(tab) : (tab as any)}
            </a>
          )}
        </For>
      </div>
      <div class={theme.children}>{initialised() && props.children(curTab as () => T)}</div>
    </div>
  );
}

export default Tabs;

export function NiceTabs<D>(props: {
  display?: "buttons" | "tabs" | "menu";
  reverse?: boolean;
  tabRef?: (curTab: () => number | undefined, setCurTab: (tab: number | undefined) => void) => void;
  class?: string;
  childClass?: string;
  tab?: number;
  defaultTab?: number;
  tabs: { title: JSX.Element; data: D }[];
  children: (props: {
    title: JSX.Element;
    data: D;
    index: number;
    jumpTo(tab: number): void;
  }) => JSX.Element;
  fallback?: JSX.Element;
}) {
  const [curTab, setCurTab] = createSignal<number | undefined>(undefined);
  createEffect(() => {
    props.tabRef?.(curTab, setCurTab);
  });
  createComputed(() => {
    const len = props.tabs.length;
    const ix = untrack(curTab);
    if (typeof ix === "number" && ix >= len) {
      setCurTab(() => len - 1);
    } else {
      setCurTab(() => props.defaultTab);
    }
  });
  const tabDisplay = createMemo(() => {
    const ix = curTab();
    if (ix === undefined) return undefined;
    const tab = props.tabs[ix];
    if (tab === undefined) return undefined;
    return <props.children data={tab.data} title={tab.title} index={ix} jumpTo={setCurTab} />;
  });
  return (
    <>
      <Show when={props.reverse}>
        <div class={props.childClass}>{tabDisplay() ?? props.fallback}</div>
      </Show>
      <Switch>
        <Match when={props.display === undefined || props.display === "tabs"}>
          <div class={classNames(theme.tabs, props.class)} data-bottom={props.reverse === true}>
            <For each={props.tabs}>
              {(tab, ix) => (
                <a
                  class={theme.tab}
                  data-active={curTab() === ix()}
                  onClick={() => setCurTab(() => ix())}
                >
                  {tab.title}
                </a>
              )}
            </For>
          </div>
        </Match>
        <Match when={props.display === "buttons"}>
          <div class={theme.buttons}>
            <For each={props.tabs}>
              {(tab, ix) => (
                <button
                  class="button is-small"
                  classList={{
                    "is-link": curTab() === ix(),
                    "is-selected": curTab() === ix(),
                  }}
                  onClick={() => setCurTab(() => ix())}
                >
                  {tab.title}
                </button>
              )}
            </For>
          </div>
        </Match>
        <Match when={props.display === "menu"}>
          <aside class={"menu " + props.class}>
            <ul class="menu-list">
              <For each={props.tabs}>
                {(tab, ix) => (
                  <li>
                    <a
                      classList={{ "is-active": curTab() === ix() }}
                      onClick={() => setCurTab(() => ix())}
                    >
                      {tab.title}
                    </a>
                  </li>
                )}
              </For>
            </ul>
          </aside>
        </Match>
      </Switch>
      <Show when={!props.reverse}>
        <div class={props.childClass}>{tabDisplay() ?? props.fallback}</div>
      </Show>
    </>
  );
}
