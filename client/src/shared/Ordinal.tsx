import ordinal from "ordinal";
import Icon from "./ui/Icon";

const placementIcon: Record<number, Icon.Type> = {
  0: "trophy",
  1: "beer",
  2: "cafe",
};

export default function Ordinal(props: { placement: number; noIcon?: boolean }) {
  return (
    <span class="ordinal">
      {placementIcon[props.placement] === undefined || props.noIcon
        ? undefined
        : [<Icon type={placementIcon[props.placement]} />, " "]}
      {ordinal(props.placement + 1)}
    </span>
  );
}
