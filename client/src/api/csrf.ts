import jscookie from "js-cookie";
import log from "loglevel";

export const CSRF_HEADER_NAME = "X-CSRF-Token";
export const CSRF_COOKIE_NAME = "csrf";

let header: undefined | string;
let cookie: undefined | string;

export interface Csrf {
  header: string;
}

async function initialise() {
  log.debug("csrf/initialise");
  header = jscookie.get(CSRF_HEADER_NAME);
  cookie = jscookie.get(CSRF_COOKIE_NAME);
  const testCsrfResult = ((await fetch("/api/test_csrf", { credentials: "include" })).ok);

  if (header === undefined || !testCsrfResult) {
    if (testCsrfResult) {
      log.error("csrf tested OK but header still undefined");
    }
    await refreshCsrf();
  }

  if (typeof header === "string" && typeof cookie === "string") {
    log.debug("got csrf", { header, cookie });
    return header;
  } else {
    const hasHeader = typeof header === "string";
    const hasCookie = typeof cookie === "string";
    log.error("csrf could not load", { hasHeader, hasCookie });
  }
}

async function refreshCsrf() {
  log.debug("refreshCsrf");
  // Remove existing cookies to force refresh
  jscookie.remove(CSRF_HEADER_NAME);
  jscookie.remove(CSRF_COOKIE_NAME);
  // Refresh credentials
  const credResp = await fetch("/api/blank", { credentials: "include" });
  if (!credResp.ok) {
    log.error("refreshCsrf failed", credResp)
  }
  cookie = jscookie.get(CSRF_COOKIE_NAME);
  header = jscookie.get(CSRF_HEADER_NAME);
}

export function getCsrfHeader() {
  return header;
}

export const csrfInit = new Promise((resolve) => {
  setTimeout(() => {
    initialise().then(() => resolve(true));
  }, 250);
});


