import log from "loglevel";
import { api } from ".";
import { toastFailure } from "@/shared/ui/Toast";
import { LOGIN_DEST_KEY, router, RouteState } from "../router";
import { getCsrfHeader } from "./csrf";

const redirectUrl = `${location.origin}/logged_in`;
const discordClientId = "723068810630004772";
const scopes = "identify email";

export function discordOAuth2Redirect(
  challenge: string,
  csrfHeader: string,
  dest: RouteState
): string {
  const state = csrfHeader;

  if (dest.name !== "reset_csrf") {
    localStorage.setItem(
      LOGIN_DEST_KEY,
      JSON.stringify({
        state,
        dest,
      })
    );
  }

  const redir =
    "https://discord.com/api/oauth2/authorize" +
    `?client_id=${encodeURIComponent(discordClientId)}` +
    `&redirect_uri=${encodeURIComponent(redirectUrl)}` +
    `&response_type=code` +
    `&scope=${encodeURIComponent(scopes)}` +
    `&state=${encodeURIComponent(state)}` +
    `&code_challenge=${encodeURIComponent(challenge)}` +
    `&code_challenge_method=S256`;

  log.info("login uri", redir, { state, dest, scopes, challenge, redirectUrl });

  return redir;
}

export async function login(dest: RouteState) {
  const header = getCsrfHeader();
  const challenge = await api!.playerGetOauth2Challenge();
  if (header !== undefined) {
    location.href = discordOAuth2Redirect(challenge, header, dest);
  } else {
    log.error("oauth2/login: no CSRF header");
  }
}

export function loginRedirBack(attemptAgain: boolean = true) {
  login(router.getState()).catch(() => {
    if (attemptAgain) {
      toastFailure(() => "Cannot perform login, CSRF is invalid; refreshing");
      setTimeout(() => {
        location.href = "/reset_csrf";
      }, 1000);
    } else {
      toastFailure(() => "Cannot perform login");
    }
  });
}

export function logout() {
  api!.playerLogout().then(() => {
    location.reload();
  });
}
