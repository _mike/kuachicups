/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PageItem_for_Uuid = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
