/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $KDCSplitGeneratorForm = {
    properties: {
        split_number: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        maps: {
            type: 'array',
            contains: {
                type: 'KDCSplitMap',
            },
            isRequired: true,
        },
    },
} as const;
