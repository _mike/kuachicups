/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PlayerTeam = {
    properties: {
        role: {
            type: 'Role',
            isRequired: true,
        },
        team_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
    },
} as const;
