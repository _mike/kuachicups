/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PickbanStep = {
    description: `A pickban step _description_ (not an actual step done on a match)`,
    properties: {
        pool_name: {
            type: 'string',
            isRequired: true,
        },
        kind: {
            type: 'PickbanStepKind',
            isRequired: true,
        },
        parent_index: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
    },
} as const;
