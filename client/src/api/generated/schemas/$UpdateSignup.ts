/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $UpdateSignup = {
    type: 'one-of',
    contains: [{
        description: `Update the player, team, or seed value, for existing signups`,
        properties: {
            tag: {
                type: 'Enum',
                isRequired: true,
            },
            content: {
                type: 'SetSignup',
                isRequired: true,
            },
        },
    }, {
        description: `Add some new signups`,
        properties: {
            tag: {
                type: 'Enum',
                isRequired: true,
            },
            content: {
                type: 'AddSignup',
                isRequired: true,
            },
        },
    }, {
        description: `Drop a signup that has had no matches yet Throws an error if there are matches completed already`,
        properties: {
            tag: {
                type: 'Enum',
                isRequired: true,
            },
            content: {
                type: 'string',
                isRequired: true,
                format: 'uuid',
            },
        },
    }, {
        description: `Forfeit a signup in a running cup Drops them if the cup is not running`,
        properties: {
            tag: {
                type: 'Enum',
                isRequired: true,
            },
            content: {
                type: 'string',
                isRequired: true,
                format: 'uuid',
            },
        },
    }],
} as const;
