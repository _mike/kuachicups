/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $NextPickbanStep = {
    description: `NOTE client must check if the pickban_index is valid! If it isn't, that means that the pickbans are done`,
    properties: {
        pickban_index: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        pool_filter: {
            type: 'dictionary',
            contains: {
                type: 'dictionary',
                contains: {
                    type: 'PoolItemState',
                },
            },
            isRequired: true,
        },
    },
} as const;
