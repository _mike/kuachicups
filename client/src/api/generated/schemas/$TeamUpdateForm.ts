/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TeamUpdateForm = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        name: {
            type: 'string',
            isNullable: true,
        },
        blurb_md: {
            type: 'string',
            isNullable: true,
        },
    },
} as const;
