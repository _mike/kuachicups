/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PostAnn = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        author_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        title_md: {
            type: 'string',
            isNullable: true,
        },
        title_html: {
            type: 'string',
            isNullable: true,
        },
        content_md: {
            type: 'string',
            isNullable: true,
        },
        content_html: {
            type: 'string',
            isNullable: true,
        },
        message_line_md: {
            type: 'string',
            isNullable: true,
        },
        message_line_html: {
            type: 'string',
            isNullable: true,
        },
        image_uri: {
            type: 'string',
            isNullable: true,
        },
        hero_image_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        uri: {
            type: 'string',
            isNullable: true,
        },
        published_time: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        is_published: {
            type: 'boolean',
            isRequired: true,
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
