/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupStageScoringForm = {
    properties: {
        elim_round: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        is_lb: {
            type: 'boolean',
            isNullable: true,
        },
        groups: {
            type: 'boolean',
            isRequired: true,
        },
        bestof: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        pickbans: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        is_point_score: {
            type: 'boolean',
            isNullable: true,
        },
        point_score_greatest_is_winner: {
            type: 'boolean',
            isNullable: true,
        },
        is_time_score: {
            type: 'boolean',
            isNullable: true,
        },
        is_time_score_race: {
            type: 'boolean',
            isNullable: true,
        },
    },
} as const;
