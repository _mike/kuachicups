/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Player = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        discord_id: {
            type: 'string',
            isRequired: true,
        },
        discord_username: {
            type: 'string',
            isRequired: true,
        },
        discord_discriminator: {
            type: 'string',
            isNullable: true,
        },
        discord_tag: {
            type: 'string',
            isRequired: true,
        },
        discord_avatar: {
            type: 'string',
            isNullable: true,
        },
        discord_email: {
            type: 'string',
            isNullable: true,
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
