/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Paged_for_int32 = {
    properties: {
        page: {
            type: 'Page',
            isRequired: true,
        },
        has_more: {
            type: 'boolean',
            isRequired: true,
        },
        total_count: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        items: {
            type: 'array',
            contains: {
                type: 'PageItem_for_int32',
            },
            isRequired: true,
        },
    },
} as const;
