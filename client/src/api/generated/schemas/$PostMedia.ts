/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PostMedia = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        approver_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        discord_msg_id: {
            type: 'string',
            isRequired: true,
        },
        discord_user_id: {
            type: 'string',
            isRequired: true,
        },
        discord_name: {
            type: 'string',
            isRequired: true,
        },
        discord_discriminator: {
            type: 'string',
            isRequired: true,
        },
        discord_avatar: {
            type: 'string',
            isNullable: true,
        },
        discord_timestamp: {
            type: 'string',
            isRequired: true,
        },
        discord_content_md: {
            type: 'string',
            isNullable: true,
        },
        discord_content_html: {
            type: 'string',
            isNullable: true,
        },
        embed_url: {
            type: 'string',
            isNullable: true,
        },
        embed_description: {
            type: 'string',
            isNullable: true,
        },
        embed_title: {
            type: 'string',
            isNullable: true,
        },
        embed_author_name: {
            type: 'string',
            isNullable: true,
        },
        embed_author_url: {
            type: 'string',
            isNullable: true,
        },
        embed_author_icon_url: {
            type: 'string',
            isNullable: true,
        },
        embed_thumbnail_url: {
            type: 'string',
            isNullable: true,
        },
        embed_thumbnail_width: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        embed_thumbnail_height: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        embed_image_width: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        embed_image_height: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        embed_image_url: {
            type: 'string',
            isNullable: true,
        },
        embed_kind: {
            type: 'string',
            isNullable: true,
        },
        embed_video_url: {
            type: 'string',
            isNullable: true,
        },
        embed_video_width: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        embed_video_height: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        embed_provider_name: {
            type: 'string',
            isNullable: true,
        },
        embed_provider_name_url: {
            type: 'string',
            isNullable: true,
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
