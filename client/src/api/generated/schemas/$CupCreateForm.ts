/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupCreateForm = {
    properties: {
        name: {
            type: 'string',
            isRequired: true,
        },
        game_mode_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        owner_team_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        is_signups_closed: {
            type: 'boolean',
            isRequired: true,
        },
        is_published: {
            type: 'boolean',
            isRequired: true,
        },
        description: {
            type: 'string',
            isRequired: true,
        },
        stages: {
            type: 'array',
            contains: {
                type: 'CupStageCreateForm',
            },
            isRequired: true,
        },
    },
} as const;
