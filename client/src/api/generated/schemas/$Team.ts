/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Team = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        name: {
            type: 'string',
            isRequired: true,
        },
        blurb_md: {
            type: 'string',
            isNullable: true,
        },
        blurb_html: {
            type: 'string',
            isNullable: true,
        },
        discord_server_id: {
            type: 'string',
            isNullable: true,
        },
        discord_server_avatar: {
            type: 'string',
            isNullable: true,
        },
        is_deleted: {
            type: 'boolean',
            isRequired: true,
        },
    },
} as const;
