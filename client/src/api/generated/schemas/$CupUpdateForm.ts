/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupUpdateForm = {
    properties: {
        game_mode_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        name: {
            type: 'string',
            isNullable: true,
        },
        description: {
            type: 'string',
            isNullable: true,
        },
        is_signups_closed: {
            type: 'boolean',
            isNullable: true,
        },
        is_published: {
            type: 'boolean',
            isNullable: true,
        },
    },
} as const;
