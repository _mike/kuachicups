/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $SetSignup = {
    properties: {
        signup_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        team_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        seed_value: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        checkin: {
            type: 'boolean',
            isNullable: true,
        },
    },
} as const;
