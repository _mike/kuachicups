/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupMatch2p = {
    properties: {
        id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        lid: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        cup_stage_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        group_no: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_round: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_round: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_logical_round: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_index: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_type: {
            type: 'all-of',
            contains: [{
                type: 'ElimMatchType',
            }],
            isNullable: true,
        },
        elim_winner_match_id: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_winner_to_high: {
            type: 'boolean',
            isNullable: true,
        },
        elim_loser_match_id: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_loser_to_high: {
            type: 'boolean',
            isNullable: true,
        },
        elim_low_match_id: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_high_match_id: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        scoring_id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        low_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        high_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        low_report: {
            type: 'array',
            contains: {
                type: 'ScoreReport',
            },
            isNullable: true,
        },
        high_report: {
            type: 'array',
            contains: {
                type: 'ScoreReport',
            },
            isNullable: true,
        },
        is_scored: {
            type: 'boolean',
            isRequired: true,
        },
        winner_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        loser_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
