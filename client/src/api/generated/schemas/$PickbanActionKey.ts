/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PickbanActionKey = {
    properties: {
        pool_name: {
            type: 'string',
            description: `What pool they're actioning (picking or banning depending on the step)`,
            isRequired: true,
        },
        item_name: {
            type: 'string',
            description: `What item in the pool they're actioning`,
            isRequired: true,
        },
    },
} as const;
