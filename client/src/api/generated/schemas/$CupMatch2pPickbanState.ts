/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupMatch2pPickbanState = {
    properties: {
        steps: {
            type: 'array',
            contains: {
                type: 'CupMatch2pPickbanStepAbbrev',
            },
            isRequired: true,
        },
        next_step: {
            type: 'all-of',
            contains: [{
                type: 'NextPickbanStep',
            }],
            isNullable: true,
        },
        is_done: {
            type: 'boolean',
            isRequired: true,
        },
    },
} as const;
