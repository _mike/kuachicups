/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TeamMemberInvite = {
    properties: {
        invite_by_player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        team_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
