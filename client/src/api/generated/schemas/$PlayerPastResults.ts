/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PlayerPastResults = {
    properties: {
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        cup_results: {
            type: 'array',
            contains: {
                type: 'CupSignupResult',
            },
            isRequired: true,
        },
        elo_history: {
            type: 'dictionary',
            contains: {
                type: 'array',
                contains: {
                    type: 'PlayerEloUpdate',
                },
            },
            isRequired: true,
        },
    },
} as const;
