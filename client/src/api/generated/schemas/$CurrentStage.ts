/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CurrentStage = {
    properties: {
        stage_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
    },
} as const;
