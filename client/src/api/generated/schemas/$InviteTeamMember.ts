/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $InviteTeamMember = {
    properties: {
        invite_by_player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        team_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
    },
} as const;
