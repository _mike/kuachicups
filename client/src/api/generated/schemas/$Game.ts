/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Game = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        name: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
        description_md: {
            type: 'string',
            isRequired: true,
        },
        description_html: {
            type: 'string',
            isRequired: true,
        },
        rules_md: {
            type: 'string',
            isRequired: true,
        },
        rules_html: {
            type: 'string',
            isRequired: true,
        },
        avatar: {
            type: 'string',
            isRequired: true,
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
