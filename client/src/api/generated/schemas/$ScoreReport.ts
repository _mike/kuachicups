/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $ScoreReport = {
    properties: {
        high: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        low: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
    },
} as const;
