/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TeamCreateForm = {
    properties: {
        owner_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        name: {
            type: 'string',
            isRequired: true,
        },
        blurb_md: {
            type: 'string',
            isRequired: true,
        },
    },
} as const;
