/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PlayerEloUpdate = {
    properties: {
        source_match: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        source_match_game: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elo: {
            type: 'number',
            isRequired: true,
            format: 'float',
        },
        elo_diff: {
            type: 'number',
            isRequired: true,
            format: 'float',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
