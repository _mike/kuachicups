/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PickbanPool = {
    properties: {
        name: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
        items: {
            type: 'array',
            contains: {
                type: 'PickbanPoolItem',
            },
            isRequired: true,
        },
    },
} as const;
