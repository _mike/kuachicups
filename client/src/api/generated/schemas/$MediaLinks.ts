/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $MediaLinks = {
    properties: {
        cup_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        stage_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        match_id: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
    },
} as const;
