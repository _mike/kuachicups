/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Ranking = {
    properties: {
        signup_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        placement: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        score: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        wins: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        losses: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        map_wins: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        map_losses: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_no: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        last_round_alive: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
    },
} as const;
