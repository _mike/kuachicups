/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupCreate = {
    properties: {
        cup: {
            type: 'Cup',
            isRequired: true,
        },
        stages: {
            type: 'array',
            contains: {
                type: 'CupStage',
            },
            isRequired: true,
        },
    },
} as const;
