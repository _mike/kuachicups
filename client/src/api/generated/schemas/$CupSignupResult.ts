/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupSignupResult = {
    description: `Final result for a signup in a cup

    Used for the final results in a cup`,
    properties: {
        id: {
            type: 'string',
            description: `links to CupSignup(id)`,
            isRequired: true,
            format: 'uuid',
        },
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        team_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        cup_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        score: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        placement: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
