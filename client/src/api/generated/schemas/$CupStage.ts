/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupStage = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        stage_no: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        division: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        cup_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        title: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
        start_immediately: {
            type: 'boolean',
            isRequired: true,
        },
        is_started: {
            type: 'boolean',
            isRequired: true,
        },
        checkintime: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        starttime: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        format: {
            type: 'CupStageFormat',
            isRequired: true,
        },
        max_participants: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_size: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_rematches: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
