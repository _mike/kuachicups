/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupStageCreateForm = {
    properties: {
        title: {
            type: 'string',
            isRequired: true,
        },
        start_immediately: {
            type: 'boolean',
            isRequired: true,
        },
        checkintime: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        starttime: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        format: {
            type: 'CupStageFormat',
            isRequired: true,
        },
        max_participants: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_size: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_rematches: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        scorings: {
            type: 'array',
            contains: {
                type: 'CupStageScoringForm',
            },
            isRequired: true,
        },
    },
} as const;
