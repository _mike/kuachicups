/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $SetTeamMember = {
    properties: {
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        player_role: {
            type: 'all-of',
            contains: [{
                type: 'Role',
            }],
            isNullable: true,
        },
        team_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
    },
} as const;
