/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupMatch2pPickbanStepAbbrev = {
    properties: {
        cup_match_id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        actor: {
            type: 'string',
            description: `who did this step None indicates the step happened automatically`,
            isNullable: true,
            format: 'uuid',
        },
        pool_name: {
            type: 'string',
            description: `what pool (by name) the step was on`,
            isRequired: true,
        },
        item_name: {
            type: 'string',
            description: `the item (by name) of the pool that was actioned None indicates that the step was a reset`,
            isNullable: true,
        },
        kind: {
            type: 'all-of',
            description: `what the kind (high/low pick/ban) of step was`,
            contains: [{
                type: 'PickbanStepKind',
            }],
            isRequired: true,
        },
        parent_index: {
            type: 'number',
            description: `parent step, if any`,
            isNullable: true,
            format: 'int32',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
