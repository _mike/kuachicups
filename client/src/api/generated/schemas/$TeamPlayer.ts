/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $TeamPlayer = {
    properties: {
        role: {
            type: 'Role',
            isRequired: true,
        },
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
    },
} as const;
