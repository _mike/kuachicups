/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Rankings = {
    properties: {
        stage_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        rankings: {
            type: 'array',
            contains: {
                type: 'Ranking',
            },
            isRequired: true,
        },
        is_complete: {
            type: 'boolean',
            isRequired: true,
        },
    },
} as const;
