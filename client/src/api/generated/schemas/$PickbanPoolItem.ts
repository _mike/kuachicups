/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PickbanPoolItem = {
    properties: {
        name: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
    },
} as const;
