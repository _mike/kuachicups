/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupStageScoring = {
    description: `DEPRECATED`,
    properties: {
        id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        cup_stage_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        bestof: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        pickbans: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        elim_round: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        is_lb: {
            type: 'boolean',
            isNullable: true,
        },
        groups: {
            type: 'boolean',
            isRequired: true,
        },
        is_point_score: {
            type: 'boolean',
            isNullable: true,
        },
        point_score_greatest_is_winner: {
            type: 'boolean',
            isNullable: true,
        },
        is_time_score: {
            type: 'boolean',
            isNullable: true,
        },
        is_time_score_race: {
            type: 'boolean',
            isNullable: true,
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
