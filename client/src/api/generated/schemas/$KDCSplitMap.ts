/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $KDCSplitMap = {
    properties: {
        title: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
    },
} as const;
