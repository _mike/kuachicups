/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PageItem_for_int32 = {
    properties: {
        id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
