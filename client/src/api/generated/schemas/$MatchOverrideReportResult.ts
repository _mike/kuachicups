/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $MatchOverrideReportResult = {
    properties: {
        match_now: {
            type: 'CupMatch2p',
            isRequired: true,
        },
        error: {
            type: 'string',
            isNullable: true,
        },
    },
} as const;
