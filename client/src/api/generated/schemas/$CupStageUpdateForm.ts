/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupStageUpdateForm = {
    properties: {
        stage_id: {
            type: 'string',
            description: `None means this is a new stage`,
            isNullable: true,
            format: 'uuid',
        },
        title: {
            type: 'string',
            isNullable: true,
        },
        checkintime: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        starttime: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        format: {
            type: 'all-of',
            contains: [{
                type: 'CupStageFormat',
            }],
            isNullable: true,
        },
        max_participants: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_size: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        group_rematches: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        scorings: {
            type: 'array',
            contains: {
                type: 'CupStageScoringForm',
            },
            isRequired: true,
        },
    },
} as const;
