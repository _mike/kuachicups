/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $GameMode = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        game_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        name: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
        description_md: {
            type: 'string',
            isRequired: true,
        },
        description_html: {
            type: 'string',
            isRequired: true,
        },
        rules_md: {
            type: 'string',
            isRequired: true,
        },
        rules_html: {
            type: 'string',
            isRequired: true,
        },
        avatar: {
            type: 'string',
            isRequired: true,
        },
        entrant_type: {
            type: 'all-of',
            contains: [{
                type: 'EntrantType',
            }],
            isNullable: true,
        },
        entrants_per_match_min: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        entrants_per_match_max: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        team_size_min: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
