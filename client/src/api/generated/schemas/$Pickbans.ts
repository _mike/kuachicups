/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Pickbans = {
    description: `a pickban system`,
    properties: {
        id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        name: {
            type: 'string',
            isRequired: true,
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
        game_mode: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        bestof: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        pool: {
            type: 'array',
            contains: {
                type: 'PickbanPool',
            },
            isRequired: true,
        },
        steps: {
            type: 'array',
            contains: {
                type: 'PickbanStep',
            },
            isRequired: true,
        },
    },
} as const;
