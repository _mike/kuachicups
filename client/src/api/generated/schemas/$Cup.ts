/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Cup = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        owner_team_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        game_mode_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        slug: {
            type: 'string',
            isRequired: true,
        },
        title: {
            type: 'string',
            isRequired: true,
        },
        description_md: {
            type: 'string',
            isRequired: true,
        },
        description_html: {
            type: 'string',
            isRequired: true,
        },
        current_stage: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        is_signups_closed: {
            type: 'boolean',
            isRequired: true,
        },
        is_finished: {
            type: 'boolean',
            isRequired: true,
        },
        is_published: {
            type: 'boolean',
            isRequired: true,
        },
        overlay_theme: {
            type: 'string',
            isNullable: true,
        },
        created_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        updated_at: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
    },
} as const;
