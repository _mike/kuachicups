/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $IsAdmin = {
    properties: {
        is_admin: {
            type: 'boolean',
            isRequired: true,
        },
    },
} as const;
