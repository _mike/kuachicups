/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CreatePostAnn = {
    properties: {
        author_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        title_md: {
            type: 'string',
            isNullable: true,
        },
        content_md: {
            type: 'string',
            isNullable: true,
        },
        message_line_md: {
            type: 'string',
            isNullable: true,
        },
        image_uri: {
            type: 'string',
            isNullable: true,
        },
    },
} as const;
