/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $AddSignup = {
    properties: {
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        team_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        seed_value: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        stage_no: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        checkin: {
            type: 'boolean',
            isNullable: true,
        },
    },
} as const;
