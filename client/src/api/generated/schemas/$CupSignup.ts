/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupSignup = {
    properties: {
        id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        player_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        team_id: {
            type: 'string',
            isNullable: true,
            format: 'uuid',
        },
        cup_id: {
            type: 'string',
            isRequired: true,
            format: 'uuid',
        },
        checked_in: {
            type: 'boolean',
            isRequired: true,
        },
        signup_time: {
            type: 'string',
            isRequired: true,
            format: 'date-time',
        },
        checkedin_time: {
            type: 'string',
            isNullable: true,
            format: 'date-time',
        },
        seed_value: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        add_to_stage_no: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
        forfeit_stage_no: {
            type: 'number',
            isNullable: true,
            format: 'int32',
        },
    },
} as const;
