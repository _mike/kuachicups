/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $PlayerFinishedMatch = {
    properties: {
        match_: {
            type: 'CupMatch2p',
            isRequired: true,
        },
        am_high: {
            type: 'boolean',
            isRequired: true,
        },
    },
} as const;
