/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Paged_for_Uuid = {
    properties: {
        page: {
            type: 'Page',
            isRequired: true,
        },
        has_more: {
            type: 'boolean',
            isRequired: true,
        },
        total_count: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        items: {
            type: 'array',
            contains: {
                type: 'PageItem_for_Uuid',
            },
            isRequired: true,
        },
    },
} as const;
