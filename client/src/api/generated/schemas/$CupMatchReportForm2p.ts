/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $CupMatchReportForm2p = {
    properties: {
        match_id: {
            type: 'number',
            isRequired: true,
            format: 'int32',
        },
        low_report: {
            type: 'array',
            contains: {
                type: 'number',
                format: 'int32',
            },
            isRequired: true,
        },
        high_report: {
            type: 'array',
            contains: {
                type: 'number',
                format: 'int32',
            },
            isRequired: true,
        },
    },
} as const;
