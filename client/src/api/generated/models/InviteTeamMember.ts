/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type InviteTeamMember = {
    invite_by_player_id: string;
    player_id: string;
    team_id: string;
};

