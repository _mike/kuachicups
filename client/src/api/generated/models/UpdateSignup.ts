/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { AddSignup } from './AddSignup';
import type { SetSignup } from './SetSignup';

export type UpdateSignup = ({
    tag: 'SetSignup';
    content: SetSignup;
} | {
    tag: 'AddSignup';
    content: AddSignup;
} | {
    tag: 'DropSignup';
    content: string;
} | {
    tag: 'ForfeitSignup';
    content: string;
});

