/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Ranking = {
    signup_id: string;
    placement: number;
    score: number;
    wins: number;
    losses: number;
    map_wins?: number | null;
    map_losses?: number | null;
    group_no?: number | null;
    last_round_alive?: number | null;
};

