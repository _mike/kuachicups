/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PostMedia = {
    id: string;
    approver_id: string;
    discord_msg_id: string;
    discord_user_id: string;
    discord_name: string;
    discord_discriminator: string;
    discord_avatar?: string | null;
    discord_timestamp: string;
    discord_content_md?: string | null;
    discord_content_html?: string | null;
    embed_url?: string | null;
    embed_description?: string | null;
    embed_title?: string | null;
    embed_author_name?: string | null;
    embed_author_url?: string | null;
    embed_author_icon_url?: string | null;
    embed_thumbnail_url?: string | null;
    embed_thumbnail_width?: number | null;
    embed_thumbnail_height?: number | null;
    embed_image_width?: number | null;
    embed_image_height?: number | null;
    embed_image_url?: string | null;
    embed_kind?: string | null;
    embed_video_url?: string | null;
    embed_video_width?: number | null;
    embed_video_height?: number | null;
    embed_provider_name?: string | null;
    embed_provider_name_url?: string | null;
    created_at: string;
    updated_at: string;
};

