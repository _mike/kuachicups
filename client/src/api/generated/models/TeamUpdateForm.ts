/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamUpdateForm = {
    id: string;
    name?: string | null;
    blurb_md?: string | null;
};

