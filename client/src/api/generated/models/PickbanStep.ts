/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PickbanStepKind } from './PickbanStepKind';

/**
 * A pickban step _description_ (not an actual step done on a match)
 */
export type PickbanStep = {
    pool_name: string;
    kind: PickbanStepKind;
    parent_index?: number | null;
};

