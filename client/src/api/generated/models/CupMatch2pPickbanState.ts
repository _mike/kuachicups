/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupMatch2pPickbanStepAbbrev } from './CupMatch2pPickbanStepAbbrev';
import type { NextPickbanStep } from './NextPickbanStep';

export type CupMatch2pPickbanState = {
    steps: Array<CupMatch2pPickbanStepAbbrev>;
    next_step?: NextPickbanStep | null;
    is_done: boolean;
};

