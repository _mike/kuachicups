/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * NOTE Available items are ones that are _not_ in the pool filter
 */
export type PoolItemState = 'Picked' | 'Banned';
