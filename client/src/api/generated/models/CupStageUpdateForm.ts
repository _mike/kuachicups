/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupStageFormat } from './CupStageFormat';
import type { CupStageScoringForm } from './CupStageScoringForm';

export type CupStageUpdateForm = {
    /**
     * None means this is a new stage
     */
    stage_id?: string | null;
    title?: string | null;
    checkintime?: string | null;
    starttime?: string | null;
    format?: CupStageFormat | null;
    max_participants?: number | null;
    group_size?: number | null;
    group_rematches?: number | null;
    scorings: Array<CupStageScoringForm>;
};

