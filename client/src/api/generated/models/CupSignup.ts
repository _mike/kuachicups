/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CupSignup = {
    id: string;
    player_id: string;
    team_id?: string | null;
    cup_id: string;
    checked_in: boolean;
    signup_time: string;
    checkedin_time?: string | null;
    seed_value?: number | null;
    add_to_stage_no?: number | null;
    forfeit_stage_no?: number | null;
};

