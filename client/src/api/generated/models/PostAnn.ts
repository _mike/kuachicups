/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PostAnn = {
    id: string;
    author_id: string;
    title_md?: string | null;
    title_html?: string | null;
    content_md?: string | null;
    content_html?: string | null;
    message_line_md?: string | null;
    message_line_html?: string | null;
    image_uri?: string | null;
    hero_image_id?: string | null;
    uri?: string | null;
    published_time?: string | null;
    is_published: boolean;
    created_at: string;
    updated_at: string;
};

