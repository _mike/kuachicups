/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PlayerEloUpdate = {
    source_match?: number | null;
    source_match_game?: number | null;
    elo: number;
    elo_diff: number;
    created_at: string;
};

