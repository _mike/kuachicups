/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ElimMatchType = 'WB' | 'LB' | 'GF1' | 'GF2';
