/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Cup } from './Cup';
import type { CupStage } from './CupStage';

export type CupCreate = {
    cup: Cup;
    stages: Array<CupStage>;
};

