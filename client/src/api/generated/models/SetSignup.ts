/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SetSignup = {
    signup_id: string;
    player_id: string;
    team_id?: string | null;
    seed_value?: number | null;
    checkin?: boolean | null;
};

