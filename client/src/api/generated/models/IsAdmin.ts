/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type IsAdmin = {
    is_admin: boolean;
};

