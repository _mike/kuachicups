/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Team = {
    id: string;
    created_at: string;
    updated_at: string;
    name: string;
    blurb_md?: string | null;
    blurb_html?: string | null;
    discord_server_id?: string | null;
    discord_server_avatar?: string | null;
    is_deleted: boolean;
};

