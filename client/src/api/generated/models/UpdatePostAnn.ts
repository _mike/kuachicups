/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UpdatePostAnn = {
    id: string;
    author_id: string;
    title_md?: string | null;
    content_md?: string | null;
    message_line_md?: string | null;
    image_uri?: string | null;
};

