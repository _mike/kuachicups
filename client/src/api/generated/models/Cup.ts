/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Cup = {
    id: string;
    owner_team_id: string;
    game_mode_id: string;
    slug: string;
    title: string;
    description_md: string;
    description_html: string;
    current_stage?: number | null;
    is_signups_closed: boolean;
    is_finished: boolean;
    is_published: boolean;
    overlay_theme?: string | null;
    created_at: string;
    updated_at: string;
};

