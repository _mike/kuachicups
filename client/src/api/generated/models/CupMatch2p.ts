/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ElimMatchType } from './ElimMatchType';
import type { ScoreReport } from './ScoreReport';

export type CupMatch2p = {
    id: number;
    lid: number;
    cup_stage_id?: string | null;
    group_no?: number | null;
    group_round?: number | null;
    elim_round?: number | null;
    elim_logical_round?: number | null;
    elim_index?: number | null;
    elim_type?: ElimMatchType | null;
    elim_winner_match_id?: number | null;
    elim_winner_to_high?: boolean | null;
    elim_loser_match_id?: number | null;
    elim_loser_to_high?: boolean | null;
    elim_low_match_id?: number | null;
    elim_high_match_id?: number | null;
    scoring_id: number;
    low_id?: string | null;
    high_id?: string | null;
    low_report?: Array<ScoreReport> | null;
    high_report?: Array<ScoreReport> | null;
    is_scored: boolean;
    winner_id?: string | null;
    loser_id?: string | null;
    created_at: string;
    updated_at: string;
};

