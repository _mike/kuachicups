/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupStageCreateForm } from './CupStageCreateForm';

export type CupCreateForm = {
    name: string;
    game_mode_id: string;
    owner_team_id: string;
    is_signups_closed: boolean;
    is_published: boolean;
    description: string;
    stages: Array<CupStageCreateForm>;
};

