/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupStageFormat } from './CupStageFormat';

export type CupStage = {
    id: string;
    stage_no: number;
    division: number;
    cup_id: string;
    title: string;
    slug: string;
    start_immediately: boolean;
    is_started: boolean;
    checkintime?: string | null;
    starttime?: string | null;
    format: CupStageFormat;
    max_participants?: number | null;
    group_size?: number | null;
    group_rematches?: number | null;
    created_at: string;
    updated_at: string;
};

