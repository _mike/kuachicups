/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Player = {
    id: string;
    discord_id: string;
    discord_username: string;
    discord_discriminator?: string | null;
    discord_tag: string;
    discord_avatar?: string | null;
    discord_email?: string | null;
    created_at: string;
    updated_at: string;
};

