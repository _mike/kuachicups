/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupMatch2p } from './CupMatch2p';

export type PlayerFinishedMatch = {
    match_: CupMatch2p;
    am_high: boolean;
};

