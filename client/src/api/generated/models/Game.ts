/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Game = {
    id: string;
    name: string;
    slug: string;
    description_md: string;
    description_html: string;
    rules_md: string;
    rules_html: string;
    avatar: string;
    created_at: string;
    updated_at: string;
};

