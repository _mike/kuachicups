/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PickbanActionKey = {
    /**
     * What pool they're actioning (picking or banning depending on the step)
     */
    pool_name: string;
    /**
     * What item in the pool they're actioning
     */
    item_name: string;
};

