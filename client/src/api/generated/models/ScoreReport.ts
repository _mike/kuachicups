/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ScoreReport = {
    high: number;
    low: number;
};

