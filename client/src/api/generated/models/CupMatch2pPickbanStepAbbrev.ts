/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PickbanStepKind } from './PickbanStepKind';

export type CupMatch2pPickbanStepAbbrev = {
    cup_match_id: number;
    /**
     * who did this step None indicates the step happened automatically
     */
    actor?: string | null;
    /**
     * what pool (by name) the step was on
     */
    pool_name: string;
    /**
     * the item (by name) of the pool that was actioned None indicates that the step was a reset
     */
    item_name?: string | null;
    /**
     * what the kind (high/low pick/ban) of step was
     */
    kind: PickbanStepKind;
    /**
     * parent step, if any
     */
    parent_index?: number | null;
    created_at: string;
    updated_at: string;
};

