/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Page } from './Page';
import type { PageItem_for_int32 } from './PageItem_for_int32';

export type Paged_for_int32 = {
    page: Page;
    has_more: boolean;
    total_count: number;
    items: Array<PageItem_for_int32>;
};

