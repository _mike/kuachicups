/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CupStageFormat = 'double_elim' | 'single_elim' | 'groups' | 'ladder';
