/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupStageFormat } from './CupStageFormat';
import type { CupStageScoringForm } from './CupStageScoringForm';

export type CupStageCreateForm = {
    title: string;
    start_immediately: boolean;
    checkintime?: string | null;
    starttime?: string | null;
    format: CupStageFormat;
    max_participants?: number | null;
    group_size?: number | null;
    group_rematches?: number | null;
    scorings: Array<CupStageScoringForm>;
};

