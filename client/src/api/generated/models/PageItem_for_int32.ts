/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PageItem_for_int32 = {
    id: number;
    updated_at: string;
};

