/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamCreateForm = {
    owner_id: string;
    name: string;
    blurb_md: string;
};

