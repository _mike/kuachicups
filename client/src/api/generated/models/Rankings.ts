/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Ranking } from './Ranking';

export type Rankings = {
    stage_id: string;
    rankings: Array<Ranking>;
    is_complete: boolean;
};

