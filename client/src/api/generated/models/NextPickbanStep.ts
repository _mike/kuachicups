/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PoolItemState } from './PoolItemState';

/**
 * NOTE client must check if the pickban_index is valid! If it isn't, that means that the pickbans are done
 */
export type NextPickbanStep = {
    pickban_index: number;
    pool_filter: Record<string, Record<string, PoolItemState>>;
};

