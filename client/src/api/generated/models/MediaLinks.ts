/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type MediaLinks = {
    cup_id?: string | null;
    stage_id?: string | null;
    match_id?: number | null;
};

