/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * DEPRECATED
 */
export type CupStageScoring = {
    id: number;
    cup_stage_id?: string | null;
    bestof: number;
    pickbans?: number | null;
    elim_round?: number | null;
    is_lb?: boolean | null;
    groups: boolean;
    is_point_score?: boolean | null;
    point_score_greatest_is_winner?: boolean | null;
    is_time_score?: boolean | null;
    is_time_score_race?: boolean | null;
    created_at: string;
    updated_at: string;
};

