/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupMatch2p } from './CupMatch2p';

export type MatchOverrideReportResult = {
    match_now: CupMatch2p;
    error?: string | null;
};

