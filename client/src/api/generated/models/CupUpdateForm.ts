/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CupUpdateForm = {
    game_mode_id?: string | null;
    name?: string | null;
    description?: string | null;
    is_signups_closed?: boolean | null;
    is_published?: boolean | null;
};

