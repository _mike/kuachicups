/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PageItem_for_Uuid = {
    id: string;
    updated_at: string;
};

