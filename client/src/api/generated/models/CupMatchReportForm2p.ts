/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CupMatchReportForm2p = {
    match_id: number;
    low_report: Array<number>;
    high_report: Array<number>;
};

