/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { KDCSplitMap } from './KDCSplitMap';

export type KDCSplitGeneratorForm = {
    split_number: number;
    maps: Array<KDCSplitMap>;
};

