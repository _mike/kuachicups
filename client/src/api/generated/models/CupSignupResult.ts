/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Final result for a signup in a cup
 *
 * Used for the final results in a cup
 */
export type CupSignupResult = {
    /**
     * links to CupSignup(id)
     */
    id: string;
    player_id: string;
    team_id?: string | null;
    cup_id: string;
    score: number;
    placement: number;
    updated_at: string;
};

