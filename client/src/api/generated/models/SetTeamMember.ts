/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Role } from './Role';

export type SetTeamMember = {
    player_id: string;
    player_role?: Role | null;
    team_id: string;
};

