/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Page } from './Page';
import type { PageItem_for_Uuid } from './PageItem_for_Uuid';

export type Paged_for_Uuid = {
    page: Page;
    has_more: boolean;
    total_count: number;
    items: Array<PageItem_for_Uuid>;
};

