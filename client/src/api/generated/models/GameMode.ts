/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { EntrantType } from './EntrantType';

export type GameMode = {
    id: string;
    game_id: string;
    name: string;
    slug: string;
    description_md: string;
    description_html: string;
    rules_md: string;
    rules_html: string;
    avatar: string;
    entrant_type?: EntrantType | null;
    entrants_per_match_min: number;
    entrants_per_match_max?: number | null;
    team_size_min?: number | null;
    created_at: string;
    updated_at: string;
};

