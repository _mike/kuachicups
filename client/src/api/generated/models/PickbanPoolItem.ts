/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PickbanPoolItem = {
    name: string;
    slug: string;
};

