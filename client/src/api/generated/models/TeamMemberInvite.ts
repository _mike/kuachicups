/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TeamMemberInvite = {
    invite_by_player_id: string;
    player_id: string;
    team_id: string;
    created_at: string;
};

