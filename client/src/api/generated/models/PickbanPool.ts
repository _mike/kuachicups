/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PickbanPoolItem } from './PickbanPoolItem';

export type PickbanPool = {
    name: string;
    slug: string;
    items: Array<PickbanPoolItem>;
};

