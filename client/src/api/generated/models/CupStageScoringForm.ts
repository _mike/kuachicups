/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CupStageScoringForm = {
    elim_round?: number | null;
    is_lb?: boolean | null;
    groups: boolean;
    bestof: number;
    pickbans?: number | null;
    is_point_score?: boolean | null;
    point_score_greatest_is_winner?: boolean | null;
    is_time_score?: boolean | null;
    is_time_score_race?: boolean | null;
};

