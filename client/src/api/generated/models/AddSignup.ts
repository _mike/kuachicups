/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type AddSignup = {
    player_id: string;
    team_id?: string | null;
    seed_value?: number | null;
    stage_no?: number | null;
    checkin?: boolean | null;
};

