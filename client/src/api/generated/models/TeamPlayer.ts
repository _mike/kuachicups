/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Role } from './Role';

export type TeamPlayer = {
    role: Role;
    player_id: string;
};

