/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Role } from './Role';

export type PlayerTeam = {
    role: Role;
    team_id: string;
};

