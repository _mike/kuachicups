/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CupSignupResult } from './CupSignupResult';
import type { PlayerEloUpdate } from './PlayerEloUpdate';

export type PlayerPastResults = {
    player_id: string;
    cup_results: Array<CupSignupResult>;
    elo_history: Record<string, Array<PlayerEloUpdate>>;
};

