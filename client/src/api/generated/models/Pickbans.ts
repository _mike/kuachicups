/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { PickbanPool } from './PickbanPool';
import type { PickbanStep } from './PickbanStep';

/**
 * a pickban system
 */
export type Pickbans = {
    id: number;
    name: string;
    slug: string;
    game_mode?: string | null;
    bestof: number;
    pool: Array<PickbanPool>;
    steps: Array<PickbanStep>;
};

