/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PickbanStepKind = 'low_ban' | 'low_pick' | 'high_ban' | 'high_pick' | 'low_lose' | 'reset';
