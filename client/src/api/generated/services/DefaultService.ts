/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CreatePostAnn } from '../models/CreatePostAnn';
import type { Cup } from '../models/Cup';
import type { CupCreate } from '../models/CupCreate';
import type { CupCreateForm } from '../models/CupCreateForm';
import type { CupMatch2p } from '../models/CupMatch2p';
import type { CupMatch2pPickbanState } from '../models/CupMatch2pPickbanState';
import type { CupMatchReportForm2p } from '../models/CupMatchReportForm2p';
import type { CupSignup } from '../models/CupSignup';
import type { CupStage } from '../models/CupStage';
import type { CupStageCreateForm } from '../models/CupStageCreateForm';
import type { CupStageScoring } from '../models/CupStageScoring';
import type { CupStageUpdateForm } from '../models/CupStageUpdateForm';
import type { CupUpdateForm } from '../models/CupUpdateForm';
import type { CurrentStage } from '../models/CurrentStage';
import type { Game } from '../models/Game';
import type { GameMode } from '../models/GameMode';
import type { InviteTeamMember } from '../models/InviteTeamMember';
import type { IsAdmin } from '../models/IsAdmin';
import type { KDCSplitGeneratorForm } from '../models/KDCSplitGeneratorForm';
import type { MatchOverrideReportResult } from '../models/MatchOverrideReportResult';
import type { MediaLinks } from '../models/MediaLinks';
import type { Paged_for_int32 } from '../models/Paged_for_int32';
import type { Paged_for_Uuid } from '../models/Paged_for_Uuid';
import type { PickbanActionKey } from '../models/PickbanActionKey';
import type { Pickbans } from '../models/Pickbans';
import type { Player } from '../models/Player';
import type { PlayerFinishedMatch } from '../models/PlayerFinishedMatch';
import type { PlayerPastResults } from '../models/PlayerPastResults';
import type { PlayerTeam } from '../models/PlayerTeam';
import type { PostAnn } from '../models/PostAnn';
import type { PostMedia } from '../models/PostMedia';
import type { Rankings } from '../models/Rankings';
import type { SetTeamMember } from '../models/SetTeamMember';
import type { Team } from '../models/Team';
import type { TeamCreateForm } from '../models/TeamCreateForm';
import type { TeamMemberInvite } from '../models/TeamMemberInvite';
import type { TeamPlayer } from '../models/TeamPlayer';
import type { TeamUpdateForm } from '../models/TeamUpdateForm';
import type { UpdatePostAnn } from '../models/UpdatePostAnn';
import type { UpdateSignup } from '../models/UpdateSignup';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class DefaultService {

    /**
     * @returns any
     * @throws ApiError
     */
    public static handlersApiBlank(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/blank',
        });
    }

    /**
     * @returns any
     * @throws ApiError
     */
    public static handlersApiTestCsrf(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/test_csrf',
        });
    }

    /**
     * @returns Player
     * @throws ApiError
     */
    public static playerCurrentSession(): CancelablePromise<Player> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/session/user',
        });
    }

    /**
     * @returns IsAdmin
     * @throws ApiError
     */
    public static playerCurrentSessionIsAdmin(): CancelablePromise<IsAdmin> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/session/is_admin',
        });
    }

    /**
     * @returns TeamMemberInvite
     * @throws ApiError
     */
    public static playerCurrentSessionTeamInvites(): CancelablePromise<Array<TeamMemberInvite>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/session/team_invites',
        });
    }

    /**
     * OAuth2 challenge using private cookie to store the code verifier
     * @returns string
     * @throws ApiError
     */
    public static playerGetOauth2Challenge(): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/oauth2_challenge',
        });
    }

    /**
     * @returns any
     * @throws ApiError
     */
    public static playerLogout(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/logout',
        });
    }

    /**
     * @param page
     * @param terms
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static playerPlayersPage(
        page?: number | null,
        terms?: string | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/players/page',
            query: {
                'page': page,
                'terms': terms,
            },
        });
    }

    /**
     * @param ids
     * @returns Player
     * @throws ApiError
     */
    public static playerProfile(
        ids: Array<string>,
    ): CancelablePromise<Array<Player>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/profile/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * @param id
     * @returns PlayerTeam
     * @throws ApiError
     */
    public static playerTeams(
        id: string,
    ): CancelablePromise<Array<PlayerTeam>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/profile/{id}/teams',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param playerId
     * @param solo
     * @returns PlayerPastResults
     * @throws ApiError
     */
    public static playerPastResults(
        playerId: string,
        solo?: boolean | null,
    ): CancelablePromise<PlayerPastResults> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/profile/{player_id}/past_results',
            path: {
                'player_id': playerId,
            },
            query: {
                'solo': solo,
            },
        });
    }

    /**
     * @param ids
     * @returns PostAnn
     * @throws ApiError
     */
    public static postsAnns(
        ids: Array<string>,
    ): CancelablePromise<Array<PostAnn>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/anns/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * @param page
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static postsAnnsPage(
        page?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/anns/page',
            query: {
                'page': page,
            },
        });
    }

    /**
     * @param page
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static postsAnnsUnpublishedPage(
        page?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/anns/unpublished/page',
            query: {
                'page': page,
            },
        });
    }

    /**
     * @param requestBody
     * @returns PostAnn
     * @throws ApiError
     */
    public static postsCreateAnn(
        requestBody: CreatePostAnn,
    ): CancelablePromise<PostAnn> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/posts/new',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns PostAnn
     * @throws ApiError
     */
    public static postsUpdateAnn(
        requestBody: UpdatePostAnn,
    ): CancelablePromise<PostAnn> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/posts/update',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param postId
     * @param uri
     * @returns any
     * @throws ApiError
     */
    public static postsPublishAnn(
        postId: string,
        uri: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/posts/{post_id}/publish_ann',
            path: {
                'post_id': postId,
            },
            query: {
                'uri': uri,
            },
        });
    }

    /**
     * @param postId
     * @returns any
     * @throws ApiError
     */
    public static postsDeleteAnn(
        postId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/posts/delete/{post_id}',
            path: {
                'post_id': postId,
            },
        });
    }

    /**
     * @param page
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static postsMediasPage(
        page?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/media/page',
            query: {
                'page': page,
            },
        });
    }

    /**
     * @param ids
     * @returns PostMedia
     * @throws ApiError
     */
    public static postsMedias(
        ids: Array<string>,
    ): CancelablePromise<Array<PostMedia>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/media/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * @param page
     * @param cupId
     * @param stageId
     * @param matchId
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static postsMediasQuery(
        page?: number | null,
        cupId?: string | null,
        stageId?: string | null,
        matchId?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/media/query',
            query: {
                'page': page,
                'cup_id': cupId,
                'stage_id': stageId,
                'match_id': matchId,
            },
        });
    }

    /**
     * @param id
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static postsLinkMedia(
        id: string,
        requestBody: MediaLinks,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/media/{id}/link',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Retrieve cups by ids
     * @param ids
     * @returns Cup
     * @throws ApiError
     */
    public static cupsCups(
        ids: Array<string>,
    ): CancelablePromise<Array<Cup>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cups/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * Retrieve a page of cups
     * @param page
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static cupsCupsPage(
        page?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cups/page',
            query: {
                'page': page,
            },
        });
    }

    /**
     * Retrieve unpublished cups
     * @param page
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static cupsCupsUnpublishedPage(
        page?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cups/unpublished_page',
            query: {
                'page': page,
            },
        });
    }

    /**
     * Retrieve the stages of a cup
     * @param id
     * @returns CupStage
     * @throws ApiError
     */
    public static cupsCupStages(
        id: string,
    ): CancelablePromise<Array<CupStage>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{id}/stages',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param stageIds
     * @returns CupStage
     * @throws ApiError
     */
    public static cupsCupStagesByStageId(
        stageIds: Array<string>,
    ): CancelablePromise<Array<CupStage>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage/{stage_ids}',
            path: {
                'stage_ids': stageIds,
            },
        });
    }

    /**
     * Retrieve the scorings of a stage
     * @param stageId
     * @returns CupStageScoring
     * @throws ApiError
     */
    public static cupsCupStageScorings(
        stageId: string,
    ): CancelablePromise<Array<CupStageScoring>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage/{stage_id}/scorings',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * Retrieve signups
     * @param ids
     * @returns CupSignup
     * @throws ApiError
     */
    public static cupsSignups(
        ids: Array<string>,
    ): CancelablePromise<Array<CupSignup>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_signups/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * Retrieve the signups of a cup, ordered by seed value
     * @param id
     * @returns CupSignup
     * @throws ApiError
     */
    public static cupsCupSignups(
        id: string,
    ): CancelablePromise<Array<CupSignup>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{id}/signups',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Retrieve signups that match the current session
     * @param cupId
     * @returns CupSignup
     * @throws ApiError
     */
    public static cupsCupSignupsSelf(
        cupId: string,
    ): CancelablePromise<Array<CupSignup>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{cup_id}/signups_self',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * Checkin solo as the current user with a specified team
     * @param cupId
     * @param teamId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupSignupTeam(
        cupId: string,
        teamId?: string | null,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/team_signup',
            path: {
                'cup_id': cupId,
            },
            query: {
                'team_id': teamId,
            },
        });
    }

    /**
     * Checkin solo as the current user
     * @param cupId
     * @param teamId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupSignupSolo(
        cupId: string,
        teamId?: string | null,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/solo_signup',
            path: {
                'cup_id': cupId,
            },
            query: {
                'team_id': teamId,
            },
        });
    }

    /**
     * Leave whatever signup has the current user session player id
     * @param cupId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupSignupLeave(
        cupId: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/leave',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * Checkin whatever signup we have under this session
     * @param cupId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupSignupCheckin(
        cupId: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/checkin',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * Checkout whatever signup we have under this session
     * @param cupId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupSignupCheckout(
        cupId: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/checkout',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * Checkout whatever signup we have under this session
     * @param cupId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupSignupForfeit(
        cupId: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/forfeit',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * Create a cup. Must be an admin
     * @param requestBody
     * @returns CupCreate
     * @throws ApiError
     */
    public static cupsCupCreate(
        requestBody: CupCreateForm,
    ): CancelablePromise<CupCreate> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Update a cup. Must be a captain of the team that owns the cup.
     * @param id
     * @param requestBody
     * @returns Cup
     * @throws ApiError
     */
    public static cupsCupUpdate(
        id: string,
        requestBody: CupUpdateForm,
    ): CancelablePromise<Cup> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{id}/update',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Create an extra cup stage. Must be a captain of the team that owns the cup.
     * @param cupId
     * @param stageNo
     * @param requestBody
     * @returns CupStage
     * @throws ApiError
     */
    public static cupsCupStageCreate(
        cupId: string,
        stageNo: number,
        requestBody: CupStageCreateForm,
    ): CancelablePromise<CupStage> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{cup_id}/{stage_no}/create',
            path: {
                'cup_id': cupId,
                'stage_no': stageNo,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Update a cup stage. Must be a captain of the team that owns the cup.
     * @param stageId
     * @param requestBody
     * @returns CupStage
     * @throws ApiError
     */
    public static cupsCupStageUpdate(
        stageId: string,
        requestBody: CupStageUpdateForm,
    ): CancelablePromise<CupStage> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_stage/{stage_id}/update',
            path: {
                'stage_id': stageId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Restart a cup stage. Must be a captain of the team that owns the cup.
     * @param stageId
     * @returns any
     * @throws ApiError
     */
    public static cupsCupStageRestart(
        stageId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_stage/{stage_id}/restart',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * Force advance() to be called on a cup stage. Should never need to be used *except* for adding the results to cups that are finished but were made before cup_signup_result was added
     * @param stageId
     * @returns any
     * @throws ApiError
     */
    public static cupsCupStageAdvance(
        stageId: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_stage/{stage_id}/advance',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * @param id
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupDelete(
        id: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{id}/delete',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Update a cup. Must be a captain of the team that owns the cup.
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static cupsCupReset(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{id}/reset',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param stageId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupStageDelete(
        stageId: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_stage/{stage_id}/delete',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * Set the signups
     * @param id
     * @param requestBody
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupUpdateSignups(
        id: string,
        requestBody: Array<UpdateSignup>,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup/{id}/update_signups',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get a list of matches under a cup, ordered by 1) being scored 2) update time
     * @param cupId
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsCupMatches(
        cupId: string,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{cup_id}/matches',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * Find what matches are finished for the given cup and signups
     * @param signupIds
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsCupFinishedMatchesForSignups(
        signupIds: Array<string>,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_finished_matches_for_signups/{signup_ids}',
            path: {
                'signup_ids': signupIds,
            },
        });
    }

    /**
     * Find what matches are finished for the given cup and players
     * @param playerIds
     * @returns PlayerFinishedMatch
     * @throws ApiError
     */
    public static cupsCupFinishedMatchesForPlayers(
        playerIds: Array<string>,
    ): CancelablePromise<Array<PlayerFinishedMatch>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_finished_matches_for_players/{player_ids}',
            path: {
                'player_ids': playerIds,
            },
        });
    }

    /**
     * Get a list of matches under a cup, ordered by 1) being scored 2) update time
     * @param cupId
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsCupOwnPendingMatches(
        cupId: string,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{cup_id}/own_pending_matches',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * @param stageId
     * @returns boolean
     * @throws ApiError
     */
    public static cupsCupStageInitialise(
        stageId: string,
    ): CancelablePromise<boolean> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_stage/{stage_id}/initialise',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * @param stageId
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsCupStageMatches2P(
        stageId: string,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage/{stage_id}/matches_2p',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * @param stageId
     * @param signupIds
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsCupStagePendingMatchesFor(
        stageId: string,
        signupIds: Array<string>,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage/{stage_id}/pending_matches/{signup_ids}',
            path: {
                'stage_id': stageId,
                'signup_ids': signupIds,
            },
        });
    }

    /**
     * @param stageId
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsCupStagePendingMatches(
        stageId: string,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage/{stage_id}/pending_matches',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * @param scoringId
     * @returns CupStageScoring
     * @throws ApiError
     */
    public static cupsCupStageScoring(
        scoringId: number,
    ): CancelablePromise<CupStageScoring> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage_scoring/{scoring_id}',
            path: {
                'scoring_id': scoringId,
            },
        });
    }

    /**
     * @param stageId
     * @returns Rankings
     * @throws ApiError
     */
    public static cupsCupStageRankings(
        stageId: string,
    ): CancelablePromise<Rankings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_stage/{stage_id}/rankings',
            path: {
                'stage_id': stageId,
            },
        });
    }

    /**
     * @param cupId
     * @returns CurrentStage
     * @throws ApiError
     */
    public static cupsCupCurrentStage(
        cupId: string,
    ): CancelablePromise<CurrentStage> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{cup_id}/current_stage',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * @param cupId
     * @returns Rankings
     * @throws ApiError
     */
    public static cupsCupRankings(
        cupId: string,
    ): CancelablePromise<Rankings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup/{cup_id}/rankings',
            path: {
                'cup_id': cupId,
            },
        });
    }

    /**
     * @param matchId
     * @param requestBody
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsMatchReport2P(
        matchId: number,
        requestBody: CupMatchReportForm2p,
    ): CancelablePromise<CupMatch2p> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_match/{match_id}/report',
            path: {
                'match_id': matchId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param matchId
     * @param requestBody
     * @returns MatchOverrideReportResult
     * @throws ApiError
     */
    public static cupsMatchOverrideReport2P(
        matchId: number,
        requestBody: CupMatchReportForm2p,
    ): CancelablePromise<MatchOverrideReportResult> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_match/{match_id}/override_report',
            path: {
                'match_id': matchId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param matchId
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsMatchResetReport2P(
        matchId: number,
    ): CancelablePromise<CupMatch2p> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_match/{match_id}/reset_report',
            path: {
                'match_id': matchId,
            },
        });
    }

    /**
     * @param matchId
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsMatchResult2P(
        matchId: number,
    ): CancelablePromise<CupMatch2p> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_match/{match_id}/result',
            path: {
                'match_id': matchId,
            },
        });
    }

    /**
     * @param matchId
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static cupsCupPickbanAction(
        matchId: number,
        requestBody: PickbanActionKey,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cup_match/{match_id}/pickbans/action',
            path: {
                'match_id': matchId,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Get the pickban system used for a particular match
     * @param matchId
     * @returns Pickbans
     * @throws ApiError
     */
    public static cupsCupMatchPickbans(
        matchId: number,
    ): CancelablePromise<Pickbans> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_match/{match_id}/pickbans',
            path: {
                'match_id': matchId,
            },
        });
    }

    /**
     * @param matchId
     * @returns CupMatch2pPickbanState
     * @throws ApiError
     */
    public static cupsCupMatchPickbanState(
        matchId: number,
    ): CancelablePromise<CupMatch2pPickbanState> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/cup_match/{match_id}/pickbans/state',
            path: {
                'match_id': matchId,
            },
        });
    }

    /**
     * Get a list of all the pickban systems available
     * @returns Pickbans
     * @throws ApiError
     */
    public static cupsPickbans(): CancelablePromise<Array<Pickbans>> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/pickbans',
        });
    }

    /**
     * @param matchIds
     * @returns CupMatch2p
     * @throws ApiError
     */
    public static cupsMatches(
        matchIds: Array<number>,
    ): CancelablePromise<Array<CupMatch2p>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/matches/{match_ids}',
            path: {
                'match_ids': matchIds,
            },
        });
    }

    /**
     * Get a list of matches according to some query options
     * @param playerIds
     * @param teamIds
     * @param isScored
     * @param cupId
     * @param stageId
     * @param page
     * @returns Paged_for_int32
     * @throws ApiError
     */
    public static cupsMatchesQuery(
        playerIds: Array<string>,
        teamIds: Array<string>,
        isScored?: boolean | null,
        cupId?: string | null,
        stageId?: string | null,
        page?: number | null,
    ): CancelablePromise<Paged_for_int32> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/matches',
            query: {
                'is_scored': isScored,
                'cup_id': cupId,
                'stage_id': stageId,
                'player_ids': playerIds,
                'team_ids': teamIds,
                'page': page,
            },
        });
    }

    /**
     * @param ids
     * @returns Team
     * @throws ApiError
     */
    public static teamsTeams(
        ids: Array<string>,
    ): CancelablePromise<Array<Team>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/teams/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * @param page
     * @param terms
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static teamsTeamsPage(
        page?: number | null,
        terms?: string | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/teams/page',
            query: {
                'page': page,
                'terms': terms,
            },
        });
    }

    /**
     * search for teams under a certain player
     * @param playerId
     * @param page
     * @param terms
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static teamsPlayerTeamsPage(
        playerId: string,
        page?: number | null,
        terms?: string | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/teams/{player_id}/page',
            path: {
                'player_id': playerId,
            },
            query: {
                'page': page,
                'terms': terms,
            },
        });
    }

    /**
     * @param id
     * @returns TeamPlayer
     * @throws ApiError
     */
    public static teamsPlayers(
        id: string,
    ): CancelablePromise<Array<TeamPlayer>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/team/{id}/players',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns Team
     * @throws ApiError
     */
    public static teamsCreate(
        requestBody: TeamCreateForm,
    ): CancelablePromise<Team> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/create',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns Team
     * @throws ApiError
     */
    public static teamsUpdate(
        requestBody: TeamUpdateForm,
    ): CancelablePromise<Team> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/update',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns any
     * @throws ApiError
     */
    public static teamsDelete(
        id: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/{id}/delete',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns number
     * @throws ApiError
     */
    public static teamsSetMember(
        requestBody: SetTeamMember,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/set_member',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param requestBody
     * @returns number
     * @throws ApiError
     */
    public static teamsOverrideMember(
        requestBody: SetTeamMember,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/override_member',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param id
     * @returns TeamMemberInvite
     * @throws ApiError
     */
    public static teamsInvites(
        id: string,
    ): CancelablePromise<Array<TeamMemberInvite>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/team/{id}/invites',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns number
     * @throws ApiError
     */
    public static teamsMakeInvite(
        requestBody: InviteTeamMember,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/make_invite',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * @param teamId
     * @returns number
     * @throws ApiError
     */
    public static teamsAcceptInvite(
        teamId: string,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/{team_id}/accept_invite',
            path: {
                'team_id': teamId,
            },
        });
    }

    /**
     * @param teamId
     * @returns number
     * @throws ApiError
     */
    public static teamsRejectInvite(
        teamId: string,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/{team_id}/reject_invite',
            path: {
                'team_id': teamId,
            },
        });
    }

    /**
     * @param id
     * @returns number
     * @throws ApiError
     */
    public static teamsLeave(
        id: string,
    ): CancelablePromise<number> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/team/{id}/leave',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param ids
     * @returns Game
     * @throws ApiError
     */
    public static gameGames(
        ids: Array<string>,
    ): CancelablePromise<Array<Game>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/games/{ids}',
            path: {
                'ids': ids,
            },
        });
    }

    /**
     * @param page
     * @returns Paged_for_Uuid
     * @throws ApiError
     */
    public static gameGamesPage(
        page?: number | null,
    ): CancelablePromise<Paged_for_Uuid> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/games',
            query: {
                'page': page,
            },
        });
    }

    /**
     * @param id
     * @returns GameMode
     * @throws ApiError
     */
    public static gameGameModes(
        id: string,
    ): CancelablePromise<Array<GameMode>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/games/{id}/modes',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param id
     * @returns GameMode
     * @throws ApiError
     */
    public static gameGameModeById(
        id: string,
    ): CancelablePromise<GameMode> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/games/modes/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * @param requestBody
     * @returns any
     * @throws ApiError
     */
    public static gameKdcSplitPickbanGenerate(
        requestBody: KDCSplitGeneratorForm,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/games/kdc-split-pickban-generate',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

}
