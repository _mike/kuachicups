import { ApiCache, createCache } from "./cache";
import { CSRF_HEADER_NAME, getCsrfHeader } from "./csrf";
import { OpenAPI, DefaultService, Paged_for_Uuid, Page } from "./generated";
export * from "./generated";
export type { CupMatch2pWithStage } from "./cache/cups";
export type { ArrayMap } from "./cache/helpers/types";

export type Paged<Id = any> = {
  page: Page;
  has_more: boolean;
  total_count: number;
  items: {
    id: Id;
    updated_at: string;
  }[];
};

export type PagedForUuid = Paged_for_Uuid;

OpenAPI.BASE = process.env.basePath ?? `${location.origin}/api`;

OpenAPI.HEADERS = async () => {
  return {
    [CSRF_HEADER_NAME]: getCsrfHeader(),
    api: "yes",
  } as Record<string, string>;
};

OpenAPI.WITH_CREDENTIALS = true;
OpenAPI.CREDENTIALS = "include";

export type Api = typeof DefaultService & ApiCache;
export const api: Api = Object.assign(DefaultService, createCache());
