import createAnnouncementsCache from "./announcements";
import createCupCache from "./cups";
import createGamesCache from "./games";
import createMediaCache from "./media";
import createPlayersCache from "./players";
import createTeamsCache from "./teams";

export type ApiCache = ReturnType<typeof createCache>;

export function createCache() {
  return {
    players: createPlayersCache(),
    teams: createTeamsCache(),
    cups: createCupCache(),
    games: createGamesCache(),
    announcements: createAnnouncementsCache(),
    media: createMediaCache(),
  };
}
