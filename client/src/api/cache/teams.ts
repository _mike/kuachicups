import { Accessor } from "solid-js";
import { DefaultService as api } from "../generated";
import { cacheByIds, cacheByParentId, cachePaginated } from "./helpers";

export default function createTeamsCache() {
  const teamsById = cacheByIds({
    name: "teams",
    key: "id",
    byIds: (ids: string[]) => api.teamsTeams(ids),
  });

  const teamsPages = cachePaginated({
    name: "teams",
    key: "id",
    page: (page, terms) => api.teamsTeamsPage(page, terms),
    byIds: teamsById.byIds,
    cache: teamsById.cache,
  });

  const teamPlayers = cacheByParentId({
    name: "teams.players",
    parentKey: "teamId",
    childKey: "playerId",
    byParentId: async (teamId: string) =>
      (await api.teamsPlayers(teamId)).map((pt) => ({
        playerId: pt.player_id,
        teamId,
        role: pt.role,
      })),
  });

  return {
    ...teamsById,
    ...teamsPages,
    players: teamPlayers,
  };
}
