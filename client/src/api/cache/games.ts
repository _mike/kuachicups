import { unpaged } from "@/shared/ui/Dropdown";
import { DefaultService as api, Game, GameMode, Paged_for_Uuid as PagedForUuid } from "../generated";
import { cacheById, cacheByIds, cacheByParentId, cacheOnce, cachePaginated } from "./helpers";

export default function createGamesCache() {
  const gamesById = cacheByIds({
    name: "games",
    key: "id",
    byIds: (ids: string[]) => api.gameGames(ids),
  });

  const gamesPages = cachePaginated({
    name: "games",
    key: "id",
    page: (page: number) => api.gameGamesPage(page),
    cache: gamesById.cache,
    byIds: gamesById.byIds,
  });

  const modesById = cacheById({
    name: "games.modes",
    key: "id",
    byId: (id: string) => api.gameGameModeById(id),
  });

  const modesByGameId = cacheByParentId({
    name: "games.modes",
    parentKey: "game_id",
    byParentId: (id: string) => api.gameGameModes(id),
  });

  const allGamesAndModes = cacheOnce(async () => {
    let page = 0;
    let haveRemainingPages = true;
    const items: { game: Game, mode: GameMode }[] = [];
    while (haveRemainingPages) {
      const [paged, games] = await gamesPages.page(page);
      for (const game of games) {
        const modes = (await modesByGameId.byParentId(game.id)).array;
        for (const mode of modes) {
          items.push({ game, mode });
        }
      }
      haveRemainingPages = paged.has_more;
    }
    return items;
  });

  async function dropdownOptions(_page: number): Promise<[PagedForUuid, {
    title: string, key: string, extra: { game: Game, mode: GameMode }
  }[]]> {
    const gamesAndModes = await allGamesAndModes();
    const options = [];
    for (const gameAndMode of gamesAndModes) {
      options.push({ title: gameAndMode.mode.name, key: gameAndMode.mode.id, extra: gameAndMode });
    }
    return [unpaged, options];
  }

  return {
    ...gamesById,
    ...gamesPages,
    allGamesAndModes,
    modes: {
      ...modesById,
      ...modesByGameId,
      dropdownOptions,
    },
  };
}
