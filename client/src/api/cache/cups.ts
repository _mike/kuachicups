import {
  CancelablePromise,
  CupCreateForm,
  CupMatch2p,
  DefaultService as api,
  Paged_for_Uuid as PagedForUuid,
} from "../generated";
import { cacheById, cacheByIds, cacheByParentId, cachePaginated, cacheOnce } from "./helpers";
import { Option as DropdownOption, unpaged } from "@/shared/ui/Dropdown";

export type CupMatch2pWithStage = CupMatch2p & {
  cup_stage_id: string;
};

export default function createCupCache() {
  const cupsById = cacheByIds({
    name: "cups",
    key: "id",
    byIds: (ids: string[]) => api.cupsCups(ids),
  });

  const cupsPages = cachePaginated({
    name: "cups",
    key: "id",
    byIds: cupsById.byIds,
    page: (page) => api.cupsCupsPage(page),
    cache: cupsById.cache,
  });

  const signupsByIdOnly = cacheByIds({
    name: "cups.signup.byid",
    key: "id",
    byIds: (ids: string[]) => api.cupsSignups(ids),
  });

  const signups = cacheByParentId({
    name: "cups.signup",
    childKey: "id",
    parentKey: "cup_id",
    byParentId: (cupId: string) => api.cupsCupSignups(cupId),
  });

  const stageRankings = cacheById({
    name: "cups.stages.rankings",
    key: "stage_id",
    byId: (stageId: string) => api.cupsCupStageRankings(stageId),
  });

  const rankings = cacheById({
    name: "cups.rankings",
    key: "cup_id",
    byId: async (cupId: string) => ({
      cup_id: cupId,
      ...(await api.cupsCupRankings(cupId)),
    }),
  });

  const stagesById = cacheByIds({
    name: "cups.stagesById",
    key: "id",
    byIds: (stageIds: string[]) => api.cupsCupStagesByStageId(stageIds),
  });

  const stagesByCupId = cacheByParentId({
    name: "cups.stages",
    childKey: "stage_no",
    parentKey: "cup_id",
    byParentId: async (id: string) => {
      const stages = await api.cupsCupStages(id);
      for (const stage of stages) {
        stagesById.cache[stage.id] = Promise.resolve(stage);
      }
      return stages;
    },
  });

  const matches = cacheByParentId({
    name: "cups.stages.matches",
    parentKey: "cup_stage_id",
    childKey: "lid",
    byParentId: (stageId: string) =>
      api.cupsCupStageMatches2P(stageId) as CancelablePromise<CupMatch2pWithStage[]>,
  });

  const scorings = cacheById({
    name: "cups.scorings",
    key: "id",
    byId: (id: number) => api.cupsCupStageScoring(id),
  });

  const pickbans = cacheOnce(() => api.cupsPickbans());

  async function create(form: CupCreateForm): Promise<string> {
    const { cup } = await api.cupsCupCreate(form);
    cupsById.cache[cup.id] = Promise.resolve(cup);
    return cup.id;
  }

  async function dropdownOptions(page: number): Promise<[PagedForUuid, DropdownOption<string>[]]> {
    const [paged, cups] = await cupsPages.page(page);
    return [
      paged as PagedForUuid,
      cups.map((c) => ({
        title: () => c.title,
        key: c.id,
      })),
    ];
  }

  function stageDropdownOptions(cupId: string | undefined): typeof dropdownOptions {
    return async function dropdownOptions() {
      if (cupId === undefined) return [unpaged, []];
      const stages = await stagesByCupId.byParentId(cupId);
      return [
        unpaged,
        stages.array.map((s) => ({
          title: s.title,
          key: s.id,
          extra: s.stage_no,
        })),
      ];
    };
  }

  return {
    byId: cupsById.byId,
    byIds: cupsById.byIds,
    cache: cupsById.cache,
    page: cupsPages.page,
    dropdownOptions,
    stages: {
      ...stagesByCupId,
      rankings: stageRankings,
      dropdownOptions: stageDropdownOptions,
    },
    stagesById,
    rankings,
    signups,
    signupsByIdOnly,
    matches,
    scorings,
    create,
    pickbans,
  };
}
