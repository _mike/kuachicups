export interface CacheOnce<T> {
  (): Promise<T>;
}

export function cacheOnce<T>(query: () => Promise<T>): CacheOnce<T> {
  let done = false;
  let val: T;
  return async () => {
    if (done) return val!;
    val = await query();
    done = true;
    return val;
  };
}
