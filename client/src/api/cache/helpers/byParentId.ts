import log from "loglevel";
import { ArrayMap, ByIdFromParentCache, CacheByParentId } from "./types";

export function cacheByParentId<
  T extends Record<ParentKey, string | number>,
  ParentKey extends string | number,
  Pid extends Extract<T[ParentKey], string | number>
>(props: {
  name: string;
  parentKey: ParentKey;
  childKey?: undefined;
  byParentId: (pid: Pid) => Promise<T[]>;
  cache?: CacheByParentId<T, Pid>;
}): {
  byParentId: (pid: Pid, refresh?: boolean) => Promise<{ readonly array: readonly T[] }>;
  cache: CacheByParentId<T, Pid>;
};

export function cacheByParentId<
  T extends Record<ParentKey | ChildKey, string | number>,
  ParentKey extends string | number,
  Pid extends Extract<T[ParentKey], string | number>,
  Cid extends Extract<T[ChildKey], string | number>,
  ChildKey extends string | number
>(props: {
  name: string;
  parentKey: ParentKey;
  childKey: ChildKey;
  byParentId: (pid: Pid) => Promise<T[]>;
  cache?: CacheByParentId<T, Pid, Cid>;
}): {
  byParentId: (pid: Pid, refresh?: boolean) => Promise<ArrayMap<T, Cid>>;
  cache: CacheByParentId<T, Pid, Cid>;
} & ByIdFromParentCache<T, Pid, Cid>;

export function cacheByParentId<
  T extends Record<ParentKey | ChildKey, string | number>,
  ParentKey extends string | number,
  Pid extends Extract<T[ParentKey], string | number>,
  Cid extends Extract<T[ChildKey], string | number> = never,
  ChildKey extends string | number = never
>(props: {
  name: string;
  parentKey: ParentKey;
  childKey?: ChildKey;
  byParentId: (pid: Pid) => Promise<T[]>;
  cache?: CacheByParentId<T, Pid, Cid>;
}): {
  byParentId: (pid: Pid, refresh?: boolean) => Promise<ArrayMap<T, Cid>>;
  cache: CacheByParentId<T, Pid, Cid>;
} & ByIdFromParentCache<T, Pid, Cid> {
  const { name, byParentId: __byParentId, childKey, cache = {} as CacheByParentId<T, Pid, Cid> } = props;

  const elementsById = childKey !== undefined ? makeElementsById(childKey) : (_arr: any[]) => ({});

  async function byId(pid: Pid, cid: Cid, refresh: boolean = false): Promise<T | undefined> {
    const { map } = await byParentId(pid, refresh);
    return map[cid];
  }

  async function byParentId(pid: Pid, refresh: boolean = false): Promise<ArrayMap<T, Cid>> {
    if (!refresh) {
      const cur = cache[pid]!;
      if (cur !== undefined) {
        return cur as any;
      }
    }
    if (refresh) {
      log.info(name, "not using cache", pid);
    }
    const getter = __byParentId(pid).then((array) => ({
      array: Object.freeze(array),
      map: childKey !== undefined ? elementsById(array) : {},
    }));
    cache[pid] = getter as any;
    return getter;
  }

  if (typeof childKey === "string") {
    return { byId, byParentId, cache } as any;
  }

  return { byParentId, cache } as any;
}

export function makeElementsById<Key extends string | number>(
  key: Key
): <T extends Record<Key, string | number>>(
  arr: T[]
) => {
  [k in Extract<T[Key], string | number>]?: T;
} {
  return (arr) => {
    const r: any = {};
    for (const obj of arr) {
      r[obj[key]] = obj;
    }
    return Object.freeze(r);
  };
}
