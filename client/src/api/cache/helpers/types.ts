import { Key } from "ts-toolbelt/out/Any/Key";

export type CacheById<Id extends string | number, T> = {
  [id in Id]?: Promise<T>;
};

export interface ArrayMap<T, Cid extends Key> {
  readonly array: readonly T[];
  readonly map: { readonly [cid in Cid]?: T };
}

export type CacheByParentId<T, Pid extends string | number, Cid = never> = {
  [id in Pid]?: Promise<ArrayMap<T, Extract<Cid, Key>>>;
};

export type ByIdFromParentCache<T, Pid, Cid> = Cid extends string | number
  ? {
      byId: (pid: Pid, id: Cid, refresh?: boolean) => Promise<T | undefined>;
    }
  : {};
