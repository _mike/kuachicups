import log from "loglevel";
import { CacheById } from "./types";

/**
 * Caching by one id at a time
 */
export function cacheById<
  T extends Record<Key, string | number>,
  Key extends string | number,
  Id extends Extract<T[Key], string | number>
>(props: {
  name: string;
  key: Key;
  byId: (id: Id) => Promise<T>;
  cache?: CacheById<Id, T>;
}): {
  byId(ids: Id, refresh?: boolean): Promise<T>;
  byIds(ids: Id[], refresh?: boolean): Promise<T[]>;
  cache: { [id in Id]?: Promise<T> };
} {
  const { byId: __byId, name, cache = {} as CacheById<Id, T> } = props;
  async function byId(id: Id, refresh: boolean = false): Promise<T> {
    log.debug("byId", name, id, refresh);
    const cached: Promise<T> | undefined = cache[id];
    if (!refresh) {
      if (cached !== undefined) {
        log.info(name, "used cached value for", id);
        return cached;
      }
    }
    log.info(name, "retrieving", id);
    return (cache[id] = __byId(id));
  }
  return {
    cache,
    byId,
    byIds: (ids: Id[], refresh: boolean = false): Promise<T[]> =>
      Promise.all(ids.map((id) => byId(id, refresh))),
  };
}

/**
 * Caching by multiple ids at once
 */
export function cacheByIds<
  T extends Record<Key, string | number>,
  Key extends string | number,
  Id extends Extract<T[Key], string | number>
>(props: {
  name: string;
  key: Key;
  byIds: (ids: Id[]) => Promise<T[]>;
  cache?: CacheById<Id, T>;
}): {
  byId(ids: Id, refresh?: boolean): Promise<T>;
  byIds(ids: Id[], refresh?: boolean): Promise<T[]>;
  cache: CacheById<Id, T>;
} {
  const { name, key, byIds: __byIds, cache = {} as CacheById<Id, T> } = props;
  const idKey = key as Key;

  async function byIds(ids: Id[], refresh: boolean = false): Promise<T[]> {
    log.debug("byIds", name, ids, refresh);
    if (ids.length === 0) {
      log.warn(name, "queried nothing");
      return [];
    }
    const ids2ix: Record<string | number, [number, number]> = {};
    const required: Id[] = [];
    const result: Promise<T>[] = new Array(ids.length);
    for (let i = 0; i < ids.length; i++) {
      const id: Id = ids[i];
      const g: Promise<T> | undefined = cache[id];
      if (g !== undefined && !refresh) {
        result[i] = g;
      } else {
        ids2ix[id as any] = [i, required.length];
        required.push(id);
      }
    }
    if (required.length > 0) {
      const gets = __byIds(required);
      for (const id of required) {
        const [resultIx, reqIx] = ids2ix[id as any];
        const g = gets.then((g) => g[reqIx]).then((t) => Object.freeze(t));
        cache[id] = g;
        result[resultIx] = g;
      }
    }
    return Promise.all(result);
  }

  const buffer: Id[] = [];
  const resolves: ((obj: { [id in Id]?: T }) => void)[] = [];
  function awaitBuffer(buffer: Id[]): Promise<{ [id in Id]?: T }> {
    return new Promise((resolve) => {
      if (buffer.length === 0) {
        resolve(empty);
        return;
      }
      const len0 = buffer.length;
      setTimeout(() => {
        // wait for the length to stabilise at 100ms intervals
        if (len0 === buffer.length) {
          const ids = Array.from(new Set(buffer.splice(0, buffer.length)));
          const ress = resolves.splice(0, resolves.length);
          const r: { [id in Id]?: T } = {};
          if (ids === undefined) {
            resolve(empty);
            return;
          }
          byIds(ids, true).then((vals) => {
            for (const val of vals) {
              if (val === undefined) {
                continue;
              }
              r[val[idKey] as Id] = val;
            }
            resolve(r);
            for (const res of ress) {
              res(r);
            }
          });
        } else {
          resolves.push(resolve);
        }
      }, 100);
    });
  }

  return {
    cache,
    byId: async (id: Id, refresh: boolean = false): Promise<T> => {
      const getter: Promise<T> | undefined = cache[id];
      if (!refresh && getter !== undefined) {
        return getter;
      }
      buffer.push(id);
      return (await awaitBuffer(buffer))[id]!;
    },
    byIds,
  };
}

const empty = {};
