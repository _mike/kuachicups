import { parseDate } from "@/shared/date";
import log from "loglevel";
import { Paged_for_Uuid } from "../../generated";

type UpdatedAtField = "updatedAt" | "updated_at";
type HasUpdatedAt = { [K in UpdatedAtField]?: Date | string | undefined };

export function getUpdatedAtTime(obj: HasUpdatedAt): Date | undefined {
  const date = obj?.updated_at ?? obj?.updatedAt;
  return parseDate(date);
}

export function cachePaginated<
  T extends Record<K, string | number> & HasUpdatedAt,
  K extends string | number
>(props: {
  name: string;
  key: K;
  byIds(ids: string[], refresh?: boolean): Promise<T[]>;
  page(begin: number, terms?: string): Promise<Paged_for_Uuid>;
  cache?: { [id in T[K]]?: Promise<T> };
}): {
  cache: { [id in T[K]]?: Promise<T> };
  page(begin: number, terms?: string): Promise<[Paged_for_Uuid, T[]]>;
} {
  const { name, key, cache = {} as { [id in T[K]]?: Promise<T> }, page, byIds } = props;
  return {
    cache,
    page: async (begin: number, terms?: string): Promise<[Paged_for_Uuid, T[]]> => {
      log.info(name, "request page", begin, terms);
      begin = begin !== undefined && isNaN(begin) ? 0 : begin ?? 0;
      const paged: Paged_for_Uuid = await page(begin, prepareTerms(terms));
      const items = paged.items;
      const required: string[] = [];
      const id2ix: { [k: string]: number } = {};
      const result: T[] = new Array(items.length);
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const { id } = item;
        const updatedAt = getUpdatedAtTime(item);
        const cached = cache[id as T[K]];
        const val = cached !== undefined ? await (cached as Promise<T>) : undefined;
        if (
          cached === undefined ||
          (updatedAt !== undefined && val !== undefined && updatedAt > getUpdatedAtTime(val)!)
        ) {
          id2ix[id] = i;
          required.push(id);
        } else {
          result[i] = val!; // cached !== undefined => val !== undefined
        }
      }
      const vals = await byIds(required, true);
      for (const val of vals) {
        const id = val[key];
        result[id2ix[id]] = val;
      }
      return [paged, result];
    },
  };
}

export function cachePaginatedLazy<
  T extends Record<K, string | number> & HasUpdatedAt,
  K extends string | number
>(props: {
  name: string;
  key: K;
  byId(id: string, refresh?: boolean): Promise<T>;
  page(begin: number, terms?: string): Promise<Paged_for_Uuid>;
  cache?: { [id in T[K]]?: Promise<T> };
}): {
  cache: { [id in T[K]]?: Promise<T> };
  page(begin: number, terms?: string): Promise<[Paged_for_Uuid, (() => Promise<T>)[]]>;
} {
  const { name, cache = {} as { [id in T[K]]?: Promise<T> }, page, byId } = props;
  return {
    cache,
    page: async (
      begin: number,
      terms?: string
    ): Promise<[Paged_for_Uuid, (() => Promise<T>)[]]> => {
      begin = begin !== undefined && isNaN(begin) ? 0 : begin ?? 0;
      log.info(name, "request page", begin, terms);
      const paged: Paged_for_Uuid = await page(begin, prepareTerms(terms));
      const items = paged.items;
      const required: [number, string][] = [];
      const result: (() => Promise<T>)[] = new Array(items.length);
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const { id } = items[i];
        const updatedAt = getUpdatedAtTime(item);
        const cached: Promise<T> | undefined = cache[id as T[K]];
        const val: T | undefined = cached !== undefined ? await cached : undefined;
        if (
          cached === undefined ||
          (updatedAt !== undefined && val !== undefined && updatedAt! > getUpdatedAtTime(val)!)
        ) {
          required.push([i, id]);
        } else {
          result[i] = () => Promise.resolve(cached);
        }
      }
      for (const [i, id] of required) {
        result[i] = () => byId(id, true);
      }
      return [paged, result];
    },
  };
}

function prepareTerms(terms?: string): undefined | string {
  if (typeof terms !== "string") return undefined;
  return terms
    .split(" ")
    .map((w) => w + ":*")
    .join(" ");
}
