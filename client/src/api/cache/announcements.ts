import { cacheByIds, cachePaginatedLazy } from "./helpers";
import { DefaultService as api } from "../generated";

export default function createAnnouncementsCache() {
  const posts = cacheByIds({
    name: "announcements",
    key: "id",
    byIds: (ids: string[]) => api.postsAnns(ids),
  });

  const postsPages = cachePaginatedLazy({
    name: "announcements",
    page: (page: number) => api.postsAnnsPage(page),
    key: "id",
    byId: posts.byId,
    cache: posts.cache,
  });

  const unpublishedPostsPages = cachePaginatedLazy({
    name: "announcements.unpublished",
    key: "id",
    page: (page: number) => api.postsAnnsUnpublishedPage(page),
    byId: posts.byId,
    cache: posts.cache,
  });

  return {
    ...posts,
    ...postsPages,
    unpublished: unpublishedPostsPages.page,
  };
}
