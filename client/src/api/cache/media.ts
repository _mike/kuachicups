import { DefaultService as api } from "../generated";
import { cacheByIds, cachePaginatedLazy } from "./helpers";

export default function createMediaCache() {
  const byId = cacheByIds({
    name: "medias",
    key: "id",
    byIds: api.postsMedias,
  });

  const pages = cachePaginatedLazy({
    name: "medias",
    key: "id",
    byId: byId.byId,
    page: api.postsMediasPage,
  });

  return { ...byId, ...pages };
}
