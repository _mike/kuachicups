import { DefaultService as api, Role } from "../generated";
import { cacheById, cacheByIds, cacheByParentId, cachePaginated } from "./helpers";

export interface PT {
  playerId: string;
  teamId: string;
  role: Role;
}

export default function createPlayersCache() {
  const playersById = cacheByIds({
    name: "players",
    key: "id",
    byIds: api.playerProfile,
  });

  const playersPages = cachePaginated({
    name: "players",
    key: "id",
    page: (page, terms) => api.playerPlayersPage(page, terms),
    byIds: playersById.byIds,
    cache: playersById.cache,
  });

  const teamRoles = cacheByParentId({
    name: "players.teamRoles",
    parentKey: "player_id",
    byParentId: async (playerId: string) =>
      (await api.playerTeams(playerId))
        .map((pt) => ({
          player_id: playerId,
          team_id: pt.team_id,
          role: pt.role,
        }))
        .sort((a, b) => (a.player_id > b.player_id ? 1 : -1)),
  });

  const playerResults = cacheById({
    name: "player.results",
    key: "player_id",
    byId: api.playerPastResults,
  });

  async function roleWithin(
    playerId: string,
    teamId: string,
    refresh?: boolean
  ): Promise<undefined | Role> {
    for (const pt of (await teamRoles.byParentId(playerId, refresh)).array) {
      if (pt.team_id === teamId) return pt.role;
    }
  }

  return {
    ...playersById,
    ...playersPages,
    results: playerResults,
    teamRoles,
    roleWithin,
  };
}

// /**
//  * Create an awaitable for a player
//  *
//  * If the id returned is "self" it attempts to use the current auth result to
//  * get the current player
//  */
// function usePlayer(getId: Accessor<string>) {
//   const auth = useAuth();
//   return useAsync<Player | undefined>(async (ref) => {
//     let id: string | undefined = getId();
//     if (id === "self") id = auth()?.login?.id;
//     if (id === undefined) return undefined;
//     return playersById.byId(id, ref);
//   });
// }

// export function useTeamsOf(playerId: () => Promise<string> | string) {
//   const api = useApi();
//   return useAsyncI(async () => {
//     const pid = await playerId();
//     const roles = await api.players.teamRoles.byParentId(pid);
//     return api.teams.byIds(roles.array.map((r) => r.teamId));
//   }, []);
// }
