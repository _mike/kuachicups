import * as t from 'io-ts';

export interface EmptyStringBrand {
  readonly EmptyString: unique symbol
}

export type EmptyString = t.Branded<string, EmptyStringBrand>

export interface EmptyStringC extends t.Type<EmptyString, string, unknown> {}

/**
 * A codec that succeeds if a string is empty
 */
export const EmptyString: EmptyStringC = t.brand(
  t.string,
  (s): s is EmptyString & '' => s.length === 0,
  'EmptyString',
)
