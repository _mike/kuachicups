import { useAsync } from "@/shared/async/Awaitable";

let blobCache: undefined | Map<string, Promise<string | undefined>>;

export default function useBlobCache(
  props: () => { id: string; src: undefined | string; fallback: undefined | string }
) {
  if (blobCache === undefined) {
    blobCache = new Map();
  }
  return useAsync(async () => {
    const { id, src, fallback } = props();
    if (typeof src === "string" && src !== "") {
      const cached = blobCache!.get(id);
      if (cached === undefined) {
        const getter = fetchObjectURL(src, fallback);
        blobCache!.set(id, getter);
        return getter;
      } else {
        return cached;
      }
    }
    return undefined;
  });
}

async function fetchObjectURL(src: string, fallback?: string): Promise<string | undefined> {
  let blob: Blob | undefined = await fetch(src).then((f) => f.blob());

  if (blob === undefined || blob.size === 0 || blob.type === "") {
    if (fallback !== undefined) {
      blob = await fetch(fallback).then((f) => f.blob());
    }
  }

  if (blob === undefined || blob.size === 0 || blob.type === "") {
    return undefined;
  }

  return URL.createObjectURL(blob);
}
