declare module "*?worker" {
  class WebpackWorker extends Worker {
    constructor();
  }
  export default WebpackWorker;
}

declare module "*?url" {
  const value: string;
  export = value;
}

declare module "*.png" {
  const value: string;
  export = value;
}

declare module "*.jpg" {
  const value: string;
  export = value;
}

declare module "*.jpeg" {
  const value: string;
  export = value;
}

declare module "*.webp" {
  const value: string;
  export = value;
}

declare module "*.svg" {
  const value: string;
  export = value;
}

declare module "*.webm" {
  const value: string;
  export = value;
}

declare module "*.png?*" {
  const value: string;
  export = value;
}

// seems like a limitation in what modules you can declare with wildcards
declare module "*.png?w=32&h=32" {
  const value: string;
  export = value;
}

declare module "*.jpg?*" {
  const value: string;
  export = value;
}

declare module "*.jpeg?*" {
  const value: string;
  export = value;
}

declare module "*.webp?*" {
  const value: string;
  export = value;
}

declare module "*.svg?*" {
  const value: string;
  export = value;
}

declare module "*.webm?*" {
  const value: string;
  export = value;
}

declare module "youtube-player" {
  const yt: any;
  export default yt;
}
