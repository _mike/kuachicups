declare module "json-url" {
  export type Algorithm = "lzma" | "lzstring" | "lzw" | "pack";
  export default function createClient(algorithm: Algorithm): {
    compress(json: unknown): Promise<string>;
    decompress(str: string): Promise<unknown>;
  };
}
