import * as s from "solid-js";

declare module "solid-js" {
  export * from s;
  export declare function Suspense(props: {
    fallback?: JSX.Element;
    children: JSX.Element;
  }): JSX.Element;
}
