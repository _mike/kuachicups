import {
  useContext,
  createContext,
  createEffect,
  createSignal,
  onCleanup,
  createMemo,
  JSX,
  createComputed,
  untrack,
} from "solid-js";
import log from "loglevel";
import { debounce } from "lodash";

export const MOBILE_WIDTH = 740;
export const SMALL_WIDTH = 980;

interface Size {
  width: number;
  height: number;
  small: boolean;
}

const computeSize = (
  width: number = document.documentElement.clientWidth,
  height: number = document.documentElement.clientHeight
) => {
  return {
    width,
    height,
    small: width <= SMALL_WIDTH,
    mobile: width <= MOBILE_WIDTH,
  };
};

const Context = createContext<{
  size(): Size;
  small(): boolean;
  mobile(): boolean;
}>({
  size: () => ({ width: 800, height: 600, small: false }),
  small: () => false,
  mobile: () => false,
});

export function Provider<T extends { children: any }>(props: T) {
  let lastSize = computeSize();
  const [size, setSize] = createSignal(lastSize);
  const [small, setSmall] = createSignal(lastSize.small, {
    equals: (a, b) => a === b,
  });
  const [mobile, setMobile] = createSignal(lastSize.mobile, {
    equals: (a, b) => a === b,
  });

  function update() {
    const width = innerWidth;
    const height = innerHeight;
    if (lastSize.width !== width || lastSize.height !== height) {
      const s = computeSize(width, height);
      log.info("resize", s);
      setSmall(s.small);
      setMobile(s.mobile);
      setSize(s);
    }
  }

  const listener = debounce(update, 50, {
    leading: false,
    trailing: true,
    maxWait: 150,
  });

  update();

  addEventListener("resize", listener);
  onCleanup(() => {
    log.info("cleaned up resize");
    removeEventListener("resize", listener);
  });

  return <Context.Provider value={{ size, small, mobile }}>{props.children}</Context.Provider>;
}

export const useSize = () => useContext(Context).size;
export const useSmall = () => useContext(Context).small;
export const useMobile = () => useContext(Context).mobile!;

interface ContentCtx {
  contentWidth(): number;
  fullscreen(): boolean;
  setFullscreen(fullscreen: boolean): void;
}

const ContentSizeContext = createContext<ContentCtx>({
  contentWidth: () => 800,
  fullscreen: () => false,
  setFullscreen: () => {},
});

export function ContentSizeProvider(props: { id?: string; children: JSX.Element }) {
  const size = useSize();
  const width = createMemo(() => size().width);
  const [contentWidth, setContentWidth] = createSignal(800);
  const [fullscreen, setFullscreen] = createSignal(false);
  let div = document.getElementById(untrack(() => props.id ?? "measure"));
  createEffect(() => {
    if (div === null) div = document.getElementById(untrack(() => props.id ?? "measure"));
    if (div === null) return 800;
    width();
    setContentWidth(div.offsetWidth);
  });
  return (
    <div id={props.id} class="container" data-fullscreen={fullscreen()}>
      <ContentSizeContext.Provider value={{ contentWidth, fullscreen, setFullscreen }}>
        {props.children}
      </ContentSizeContext.Provider>
    </div>
  );
}

export const useContentWidth = () => useContext(ContentSizeContext).contentWidth;

export function RequireFullscreenContent(props: { children: JSX.Element }) {
  const { fullscreen, setFullscreen } = useContext(ContentSizeContext);
  createComputed(() => {
    const prevState = untrack(fullscreen);
    setFullscreen(true);
    onCleanup(() => {
      setFullscreen(prevState);
    });
  });
  return props.children;
}

export function ClampY(props: { children: JSX.Element }) {
  // TODO
  return props.children;
}
