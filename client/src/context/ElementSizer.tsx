import { batch, createContext, createSignal, JSX, onCleanup, Setter, useContext } from "solid-js";

export interface IElementSizerContext {
  name: string;
  manage(element: HTMLElement): void;
  setWidth: Setter<number | undefined>;
  setHeight: Setter<number | undefined>;
  width(): number | undefined;
  height(): number | undefined;
}

export default function createElementSizerContext(name: string) {
  const ElementSizerContext = createContext<IElementSizerContext>();
  function ElementSizerProvider(props: { children: JSX.Element }) {
    const parent = useContext(ElementSizerContext);
    const [width, setWidth] = createSignal<number | undefined>(undefined);
    const [height, setHeight] = createSignal<number | undefined>(undefined);
    return (
      <ElementSizerContext.Provider
        value={{
          name,
          manage(element) {
            function observe() {
              const rect = element.getBoundingClientRect();
              batch(() => {
                const width = Math.max(rect.width, parent?.width() ?? 0);
                parent?.setWidth(width);
                setWidth(width);
                setHeight(rect.height);
              });
            }
            const observer = new MutationObserver(observe);
            observer.observe(element, { childList: true, subtree: true });
            onCleanup(() => observer.disconnect());
            setTimeout(() => observe(), 100);
          },
          width,
          height,
          setWidth,
          setHeight,
        }}
      >
        {props.children}
      </ElementSizerContext.Provider>
    );
  }
  function useElementSizer() {
    return useContext(ElementSizerContext);
  }
  return { ElementSizerProvider, useElementSizer };
}

export function createElementSizer() {
  const [width, setWidth] = createSignal<number | undefined>(undefined);
  const [height, setHeight] = createSignal<number | undefined>(undefined);
  return {
    manage(element: HTMLElement) {
      function observe() {
        const rect = element.getBoundingClientRect();
        batch(() => {
          setWidth(rect.width);
          setHeight(rect.height);
        });
      }
      const observer = new MutationObserver(observe);
      observer.observe(element, { childList: true, subtree: true });
      onCleanup(() => observer.disconnect());
      setTimeout(() => observe(), 100);
    },
    width,
    height,
  };
}
