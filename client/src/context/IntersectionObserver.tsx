import { createContext, createUniqueId, JSX, onCleanup, splitProps, useContext } from "solid-js";
import { Dynamic } from "solid-js/web";

const Ctx = createContext<{
  setRoot(root: HTMLElement): void;
  observe(target: HTMLElement, callback: IntersectionObserverProviderCallback): void;
}>();

export interface IntersectionObserverProviderCallback {
  (entry: HTMLElement, observer: IntersectionObserver): void;
}

export default function IntersectionRoot<K extends keyof JSX.IntrinsicElements>(
  props: {
    component: K;
    children: JSX.Element;
    rootMargin?: string;
    threshold?: number | number[];
  } & JSX.IntrinsicElements[K]
) {
  const [ownProps, compProps] = splitProps(props, [
    "component",
    "children",
    "rootMargin",
    "threshold",
  ]);
  return (
    <IntersectionObserverProvider rootMargin={ownProps.rootMargin} threshold={ownProps.threshold}>
      <IntersectionProviderRoot component={ownProps.component} {...compProps}>
        {ownProps.children}
      </IntersectionProviderRoot>
    </IntersectionObserverProvider>
  );
}

const idAttr = "intersectionobserver";

export function IntersectionObserverProvider(props: {
  children: JSX.Element;
  rootMargin?: string;
  threshold?: number | number[];
}) {
  const entryMap: { [obsId: string]: IntersectionObserverProviderCallback } = {};
  const callback: IntersectionObserverCallback = (entries, self) => {
    log.debug("IntersectionObserverCallback", entries, self);
    for (const entry of entries) {
      if (entry.target instanceof HTMLElement) {
        entryMap[entry.target.dataset[idAttr]!]?.(entry as any, self);
      }
    }
  };

  let observer: IntersectionObserver | undefined;

  let _root: HTMLElement | undefined;

  function setRoot(root: HTMLElement) {
    if (observer !== undefined) {
      if (root !== _root) {
        return;
      }
      log.error("called setRoot on two elements", { oldRoot: _root, newRoot: root });
      return;
    }
    log.debug("IntersectionObserverProvider.setRoot", root);
    _root = root;
    observer = new IntersectionObserver(callback, {
      root,
      rootMargin: props.rootMargin,
      threshold: props.threshold ?? [0.0, 0.5, 1.0],
    });
  }

  onCleanup(() => {
    observer?.disconnect();
  });

  function observe(target: HTMLElement, callback: IntersectionObserverProviderCallback) {
    if (observer === undefined) {
      log.error("cannot create intersectionobserver, no parent observer available");
      return;
    }
    log.debug("IntersectionObserverProvider.observe", target, callback);
    const id = createUniqueId();
    target.dataset[idAttr] = id;
    entryMap[id] = callback;
    observer.observe(target);
    onCleanup(() => {
      try {
        observer?.unobserve(target);
        delete entryMap[id];
        delete target.dataset[idAttr];
      } catch (err) {
        log.warn("error cleaning up intersection observer", err);
      }
    });
  }

  return <Ctx.Provider value={{ setRoot, observe }}>{props.children}</Ctx.Provider>;
}

const D = Dynamic as any;
export function IntersectionProviderRoot<K extends keyof JSX.IntrinsicElements>(
  props: JSX.IntrinsicElements[K] & {
    children?: JSX.Element;
    component: K;
  }
) {
  const ctx = useContext(Ctx)!;
  return <D {...props} ref={(el: HTMLElement) => ctx.setRoot(el)} />;
}

export function useIntersectionObserver() {
  return useContext(Ctx)!.observe;
}
