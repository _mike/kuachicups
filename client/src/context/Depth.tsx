import { createContext, JSX, useContext } from "solid-js";

const DepthCtx = createContext<() => number>(() => 0);

export function useDepth() {
  return useContext(DepthCtx);
}

export function DepthProvider(props: { children: JSX.Element }) {
  const depth = useDepth();
  return (
    <DepthCtx.Provider value={() => depth() + 1}>
      {props.children}
    </DepthCtx.Provider>
  );
}

const DepthValueCtx = createContext<() => any[]>(() => []);

export function useDepthValue() {
  return useContext(DepthValueCtx);
}

export function DepthValueProvider(props: {
  value: any;
  children: JSX.Element;
}) {
  const dv = useDepthValue();
  return (
    <DepthValueCtx.Provider value={() => [...dv(), props.value]}>
      <DepthProvider>{props.children}</DepthProvider>
    </DepthValueCtx.Provider>
  );
}
