import { createContext, createEffect, createMemo, JSX, useContext } from "solid-js";
import { Player, api } from "../api";
import { usePromise } from "../shared/async/Awaitable";
import { IfThenElse } from "../shared/Case";
import { csrfInit } from "@/api/csrf";

const useAuth = () => useContext(Context)!;
export default useAuth;

export type IAuthCtx = IAuthState & {
  then: Promise<IAuthState>;
};

export type IAuthState = Readonly<
  | {
    loggedIn: undefined | false;
    login: undefined;
    admin: false;
  }
  | {
    loggedIn: true;
    login: Player;
    admin: boolean;
  }
>;

export type Auth = () => IAuthCtx;

export const Context = createContext<Auth>();

const defaultAuthState: IAuthState = Object.freeze({
  loggedIn: undefined,
  login: undefined,
  admin: false,
});

async function getAuth(): Promise<IAuthState> {
  log.debug("getAuth");
  try {
    const _csrf = await csrfInit;
    const login = await api.playerCurrentSession();
    const admin = (await api.playerCurrentSessionIsAdmin()).is_admin;
    log.debug("getAuth/done", { login, admin });
    return Object.freeze({
      loggedIn: true,
      login,
      admin,
    });
  } catch (err) {
    log.debug("getAuth/error", err);
    return Object.freeze({
      loggedIn: false,
      login: undefined,
      admin: false,
    });
  }
}

const authPromise = getAuth();

export function Provider<T extends { children: any }>(props: T) {
  const authState = usePromise(authPromise);
  const authCtx = createMemo<IAuthCtx>(() => ({
    ...(authState() ?? defaultAuthState),
    then: authPromise,
  }));
  //
  // authPromise.then((auth) => {
  //   if (auth.login !== undefined) {
  //     try {
  //       tracker.identify(auth.login.discord_id);
  //     } catch (e) { }
  //   }
  // });
  //
  return <Context.Provider value={authCtx}>{props.children}</Context.Provider>;
}

export function WithLogin(props: {
  children: (player: Player) => JSX.Element;
  fallback?: JSX.Element;
}) {
  const auth = useAuth();
  return (
    <IfThenElse data={auth()} key="loggedIn" fallback={() => props.fallback}>
      {(s) => props.children(s.login)}
    </IfThenElse>
  );
}

export function RequireLogin(props: { children: JSX.Element; fallback?: JSX.Element }) {
  const auth = useAuth();
  return (
    <IfThenElse data={auth()} key="loggedIn" fallback={() => props.fallback}>
      {() => props.children}
    </IfThenElse>
  );
}
