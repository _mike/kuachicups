import { number, refine } from "superstruct";

export const Nat = refine(
  number(),
  "Nat",
  (v) => Number.isSafeInteger(v) && v >= 0
);

export const Pos = refine(
  number(),
  "Pos",
  (v) => Number.isSafeInteger(v) && v > 0
);

export const Odd = refine(
  number(),
  "Odd",
  (v) => Number.isSafeInteger(v) && v % 2 === 1
);

export const Even = refine(
  number(),
  "Odd",
  (v) => Number.isSafeInteger(v) && v % 2 === 0
);
