#!/usr/bin/env bash
wget 127.0.0.1:3000/api/openapi.json -O src/openapi.json
rm -r src/api/generated
npx openapi \
    -i ./src/openapi.json \
    -o ./src/api/generated \
    --useUnionTypes \
    --exportSchemas=true
