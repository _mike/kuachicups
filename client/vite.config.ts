import * as path from "path";
import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";
import solidPlugin from "vite-plugin-solid";
import { imagetools } from "vite-imagetools";

export default defineConfig((env) => ({
  plugins: [tsconfigPaths({ loose: true }), imagetools(), solidPlugin()],
  cacheDir: ".vite", // keep out of nix-managed node_modules
  resolve: {
    alias: [
      { find: /^@\/(.*)/, replacement: "src/$1" },
      { find: "@", replacement: "./src" },
    ],
  },
  build: {
    target: "es6",
    rollupOptions: {
      input: {
        main: path.resolve(__dirname, "index.html"),
        overlay: path.resolve(__dirname, "stream/overlay/index.html"),
        overlay_remote: path.resolve(__dirname, "stream/remote/index.html"),
      },
    },
  },
  server: {
    host: "0.0.0.0",
    port: 8000,
    proxy: {
      "/logged_in": "http://127.0.0.1:3000",
      "/reset_csrf": "http://127.0.0.1:3000",
      "/api": "http://127.0.0.1:3000",
      "/swagger": "http://127.0.0.1:3000",
      // "/stream/remote": "http://127.0.0.1:8000/stream/remote/index.html",
      // "/stream/overlay": "http://127.0.0.1:8000/stream/overlay/index.html",
    },
    watch: {
      ignored: ["**/.direnv", "**/.direnv/**", "**/build/**"],
    },
  },
  define: {
    "process.env": process.env,
  },
}));
