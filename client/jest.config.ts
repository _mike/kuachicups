import * as path from "path";
import { pathsToModuleNameMapper } from "ts-jest/utils";
import type { Config } from "@jest/types";

const config: Config.InitialOptions = {
  moduleFileExtensions: ["ts", "tsx", "js", "css"],
  preset: "ts-jest/presets/default-esm",
  extensionsToTreatAsEsm: [".ts", ".tsx"],
  transform: {
    ".d.spec.tsx?$": "dts-jest/transform",
    ".spec.tsx?$": "ts-jest",
  },
  roots: ["<rootDir>"],
  modulePaths: ["<rootDir>"],
  moduleDirectories: [".", "src", "node_modules"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "..css$": "<rootDir>/src/stub.css.ts",
  },
  setupFiles: ["./setupJest.ts"],
  globals: {
    "ts-jest": {
      useESM: true,
    },
    _dts_jest_: {
      // https://github.com/ikatyang/dts-jest/issues/407
      compiler_options: path.resolve(__dirname, "./tsconfig.json"),
    },
  },
};

export default config;
