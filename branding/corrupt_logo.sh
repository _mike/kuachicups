#!/usr/bin/env zsh
#

zmodload zsh/mathfunc

stridemin=0.1
stridemax=0.4
striderange=$((stridemax - stridemin))

bheightmin=10
bheightmax=30
bheightrange=$((bheightmax - bheightmin))

magmin=0
magmax=2
magrange=$((magmax - magmin))

corr() {
  local input="${1}"
  local time="${2}.0"
  local i=$(( 0.5 * (cos(0.5 * time) + 1.0) ))
  local j=$(( 0.5 * (sin(0.75 * time) + 1.0) ))
  local k=$(( 0.5 * (sin(0.05 * time) + 1.0) ))
  local stride=$(( stridemin + (i * striderange) ))
  local stride=$(printf %.4f $stride)
  local bheight=$(( bheightmin + (j * bheightrange) ))
  local bheight=$(printf %.0f $bheight)
  local mag=$(( magmin + (k * magmax) ))
  local mag=$(printf %.0f $mag)
  stdbuf -o0 cat "$input" | stdbuf -i0 -o0 corrupter -seed "$2" \
    -add 0 \
    -stride $stride \
    -lb 0.0 -lg 0.0 -lr 0.0 \
    -mag $mag \
    -meanabber 10 \
    -bheight $bheight \
    -boffset 1 \
    -lag 0.005 \
    -
}

asdf=0
x=450
img="./aqlunused.png"
rm aqlunused.mp4
for _j in $(seq $x); do
  asdf=$((asdf + 1))
  corr "$img" "$asdf"
done | stdbuf -i0 ffmpeg -f image2pipe -i - aqlunused-glitch.mp4

wait
