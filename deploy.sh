#!/usr/bin/env bash
set -e
set -x

docker build . -t quasimal/kuachi.gg:latest
docker push quasimal/kuachi.gg:latest

# https://github.com/docker/compose/issues/8544
dockerssh() {
  rm -f /tmp/docker.sock
  cleanup() {
    ssh -q -S docker-ctrl-socket -O exit mike@kuachi.gg
    rm -f /tmp/docker.sock
  }
  trap "cleanup" EXIT
  ssh -M -S docker-ctrl-socket -fnNT -L /tmp/docker.sock:/var/run/docker.sock mike@kuachi.gg
  DOCKER_HOST=unix:///tmp/docker.sock eval "$*"
}

dockerssh docker-compose pull --no-parallel
dockerssh docker-compose up --remove-orphans -d
