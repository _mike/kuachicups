use poem::web::Query;
use poem_openapi::OpenApi;

struct Api;

// api! {
//
//   POST /cup/:cup_id/add_stage => add_cup_stage(id: i32, xxx) -> Result<()> {
//     _
//   }
// }
//
// create_api_macros! API;
//
// api! {
//   GET /cup/:id/add_stage => cup::add_stage;
//   GET /cup/:foobar/asdf => xx
// }
//
//
// api! POST

// GET /cup/:id/create
// cup_create(id: ...) -> Result<()> {
// }

// #[OpenApi]
// impl Api {
//     #[oai(path = "/logged_in", method = "get")]
//     async fn auth_logged_in(&self, code: Query<String>, state: Query<String>) -> () {}
// }

// (accept_invite) POST /api/team/<team_id>/accept_invite
// (ads_txt) GET /ads.txt
// (anns) GET /api/anns/<ids>
// (anns_page) GET /api/anns/page?<page>
// (anns_unpublished_page) GET /api/anns/unpublished/page?<page>
// (blank) GET /api/blank
// (client_index_anywhere) GET /<path..> [30] text/html; charset=utf-8
// (create) POST /api/team/create application/json
// (create_ann) POST /api/posts/new application/json
// (cup_create) POST /api/cup/create application/json
// (cup_current_stage) GET /api/cup/<cup_id>/current_stage
// (cup_delete) POST /api/cup/<id>/delete
// (cup_finished_matches_for_players) GET /api/cup_finished_matches_for_players/<pla
// (cup_finished_matches_for_signups) GET /api/cup_finished_matches_for_signups/<sig
// (cup_match_pickban_state) GET /api/cup_match/<match_id>/pickbans/state
// (cup_match_pickbans) GET /api/cup_match/<match_id>/pickbans
// (cup_matches) GET /api/cup/<cup_id>/matches
// (cup_own_pending_matches) GET /api/cup/<cup_id>/own_pending_matches
// (cup_pickban_action) POST /api/cup_match/<match_id>/pickbans/action application/j
// (cup_rankings) GET /api/cup/<cup_id>/rankings
// (cup_reset) POST /api/cup/<id>/reset
// (cup_signup_checkin) POST /api/cup/<cup_id>/checkin
// (cup_signup_checkout) POST /api/cup/<cup_id>/checkout
// (cup_signup_forfeit) POST /api/cup/<cup_id>/forfeit
// (cup_signup_leave) POST /api/cup/<cup_id>/leave
// (cup_signup_solo) POST /api/cup/<cup_id>/solo_signup?<team_id>
// (cup_signup_team) POST /api/cup/<cup_id>/team_signup?<team_id>
// (cup_signups) GET /api/cup/<id>/signups
// (cup_signups_self) GET /api/cup/<cup_id>/signups_self
// (cup_stage_advance) POST /api/cup_stage/<stage_id>/advance
// (cup_stage_create) POST /api/cup/<cup_id>/<stage_no>/create application/json
// (cup_stage_delete) POST /api/cup_stage/<stage_id>/delete
// (cup_stage_initialise) POST /api/cup_stage/<stage_id>/initialise
// (cup_stage_matches_2p) GET /api/cup_stage/<stage_id>/matches_2p
// (cup_stage_pending_matches) GET /api/cup_stage/<stage_id>/pending_matches
// (cup_stage_pending_matches_for) GET /api/cup_stage/<stage_id>/pending_matches/<si
// (cup_stage_rankings) GET /api/cup_stage/<stage_id>/rankings
// (cup_stage_restart) POST /api/cup_stage/<stage_id>/restart
// (cup_stage_scoring) GET /api/cup_stage_scoring/<scoring_id>
// (cup_stage_scorings) GET /api/cup_stage/<stage_id>/scorings
// (cup_stage_update) POST /api/cup_stage/<stage_id>/update application/json
// (cup_stages) GET /api/cup/<id>/stages
// (cup_stages_by_stage_id) GET /api/cup_stage/<stage_ids>
// (cup_update) POST /api/cup/<id>/update application/json
// (cup_update_signups) POST /api/cup/<id>/update_signups application/json
// (cups) GET /api/cups/<ids>
// (cups_page) GET /api/cups/page?<page>
// (cups_unpublished_page) GET /api/cups/unpublished_page?<page>
// (current_session) GET /api/session/user
// (current_session_is_admin) GET /api/session/is_admin
// (current_session_team_invites) GET /api/session/team_invites
// (delete) POST /api/team/<id>/delete
// (delete_ann) POST /api/posts/delete/<post_id>
// (game_mode_by_id) GET /api/games/modes/<id>
// (game_modes) GET /api/games/<id>/modes
// (games) GET /api/games/<ids> [2]
// (games_page) GET /api/games?<page>
// (get_oauth2_challenge) GET /api/oauth2_challenge
// (index) GET / [10] text/html; charset=utf-8
// (invites) GET /api/team/<id>/invites
// (leave) POST /api/team/<id>/leave
// (link_media) POST /api/media/<id>/link application/json
// (logged_in) GET /logged_in?<code>&<state> text/html; charset=utf-8
// (logout) GET /api/logout
// (make_invite) POST /api/team/make_invite application/json
// (match_override_report_2p) POST /api/cup_match/<match_id>/override_report applica
// (match_report_2p) POST /api/cup_match/<match_id>/report application/json
// (match_reset_report_2p) POST /api/cup_match/<match_id>/reset_report
// (match_result_2p) GET /api/cup_match/<match_id>/result
// (matches_) GET /api/matches/<match_ids>
// (matches_query) GET /api/matches?<is_scored>&<cup_id>&<stage_id>&<player_ids>&<te
// (medias) GET /api/media/<ids>
// (medias_page) GET /api/media/page?<page>
// (medias_query) GET /api/media/query?<page>&<cup_id>&<stage_id>&<match_id>
// (override_member) POST /api/team/override_member application/json
// (past_results) GET /api/profile/<player_id>/past_results?<solo>
// (pickbans) POST /api/pickbans
// (player_teams_page) GET /api/teams/<player_id>/page?<page>&<terms>
// (players) GET /api/team/<id>/players
// (players_page) GET /api/players/page?<page>&<terms>
// (profile) GET /api/profile/<ids>
// (publish_ann) POST /api/posts/<post_id>/publish_ann?<uri>
// (reject_invite) POST /api/team/<team_id>/reject_invite
// (reset_csrf) GET /reset_csrf text/html; charset=utf-8
// (set_member) POST /api/team/set_member application/json
// (signups) GET /api/cup_signups/<ids>
// (teams) GET /api/profile/<id>/teams
// (teams) GET /api/teams/<ids>
// (teams_page) GET /api/teams/page?<page>&<terms>
// (test_csrf) GET /api/test_csrf
// (update) POST /api/team/update application/json
// (update_ann) POST /api/posts/update application/json
