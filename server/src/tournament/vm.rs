use serde::{Deserialize, Serialize};

use super::{Focus, Player, Points, Slot, SortMethod, Standings};
use std::cmp::max;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Op {
    Match(Slot, Slot),
    PerformSorting(Focus, SortMethod),
    BeginRound,
    EndRound,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum Trace {
    Overlay(Vec<Trace>),
    MatchSlots(Slot, Slot),
    Match(Box<Trace>, Box<Trace>),
    Sort(Focus, SortMethod, Box<Trace>),
    Slot(Slot),
    BeginRound(Box<Trace>),
    EndRound(Box<Trace>),
}

pub struct State {
    ip: usize,
    code: Vec<Op>,
    pending_matches: Vec<(Slot, Slot)>,
    pending_results: Vec<((Slot, Points), (Slot, Points))>,
    seeding: Standings,
    history: Vec<Standings>,
}

impl State {
    pub fn new(code: Vec<Op>, seeding: Standings) -> Self {
        State {
            ip: 0,
            code,
            pending_matches: vec![],
            pending_results: vec![],
            seeding,
            history: vec![],
        }
    }

    pub fn current_standings(&self) -> &Standings {
        self.history.last().unwrap_or(&self.seeding)
    }
}
