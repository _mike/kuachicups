use serde::{Deserialize, Serialize};
use std::cmp::max;
use std::iter::zip;
use std::marker::PhantomData;
use std::ops;
pub mod unified;
pub mod vm;

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Slot(pub usize);

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct PlayerCount(pub usize);

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Player(pub usize);

#[derive(Debug, Serialize, Deserialize)]
pub struct Standings(pub Vec<usize>);

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Match(pub Slot, pub Slot);

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone, Copy)]
pub enum SortMethod {
    WinnerTakesHigh,
    Points,
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Focus {
    start: Slot,
    end: Slot,
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Serialize, Deserialize, Clone)]
pub struct Points(Vec<u32>);

impl Points {
    pub fn zero() -> Points {
        Points(vec![])
    }
}

impl ops::Add<Points> for Points {
    type Output = Points;
    fn add(self, other: Points) -> Points {
        let Points(a) = self;
        let Points(b) = other;
        let len = max(a.len(), b.len());
        let mut next = Vec::with_capacity(len);
        for i in 0..len {
            match (i < a.len(), i < b.len()) {
                (true, true) => next.push(a[i] + b[i]),
                (true, false) => next.push(a[i]),
                (false, true) => next.push(b[i]),
                (false, false) => next.push(0),
            }
        }
        Points(next)
    }
}
