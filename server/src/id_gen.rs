use std::cell::RefCell;
use std::rc::Rc;

#[derive(Clone, Debug)]
pub struct IdGen {
    next: Rc<RefCell<u32>>,
}

impl IdGen {
    pub fn next_id(&self) -> u32 {
        let mut next = self.next.borrow_mut();
        let id = *next;
        *next += 1;
        id
    }

    pub fn new() -> IdGen {
        IdGen {
            next: Rc::new(RefCell::new(0)),
        }
    }
}
