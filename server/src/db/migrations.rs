use super::DB;
use crate::config::Config;
use crate::db::tables::{elo_update, map_winloss_for, START_ELO};
use crate::db::{ById, Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, HasParent, Rankings};
use futures::future::BoxFuture;
use itertools::Itertools;
use sqlx::types::BigDecimal;
use sqlx::{query, Acquire, PgConnection, PgPool};
use std::convert::TryFrom;
use std::error::Error;
use std::str::FromStr;
use uuid::Uuid;

macro_rules! migration {
    ($migr:expr, $db:expr) => {
        _migration(stringify!($migr), |db| Box::pin($migr(db)), $db).await?
    };
}

pub async fn run_migrations(config: &Config) -> Result<(), Box<dyn Error>> {
    let db = PgPool::connect(&config.db_uri).await?;
    let mut db = db.acquire().await?;
    let db: &mut PgConnection = &mut *db;
    migration!(add_stage_results, db);
    migration!(add_elo_again20, db);
    Ok(())
}

async fn _migration(
    name: &'static str,
    runner: for<'r> fn(&'r mut PgConnection) -> BoxFuture<Result<(), Box<dyn Error>>>,
    db: &mut PgConnection,
) -> Result<(), Box<dyn Error>> {
    if !migration_is_ran(name, &mut *db).await? {
        log::debug!("{:?}: Running migration", name);
        let r = runner(&mut *db).await?;
        migration_end(name, &mut *db).await?;
        log::debug!("{:?}: Done", name);
        Ok(r)
    } else {
        Ok(())
    }
}

async fn migration_is_ran(
    name: &'static str,
    db: &mut PgConnection,
) -> Result<bool, Box<dyn Error>> {
    let have_migr = query!("SELECT * FROM data_migration WHERE name = $1", name)
        .fetch_optional(&mut *db)
        .await?
        .is_some();
    log::debug!("have_migr for {:?}: {:?}", name, have_migr);
    Ok(have_migr)
}

async fn migration_end(name: &'static str, db: &mut PgConnection) -> Result<(), Box<dyn Error>> {
    query!("INSERT INTO data_migration(name) VALUES($1)", name)
        .execute(&mut *db)
        .await?;
    Ok(())
}

////////////////////////////////////////////////////////////////////////////////

/// add stage results for any stages that dont have them already
async fn add_stage_results(db: &mut PgConnection) -> Result<(), Box<dyn Error>> {
    let mut tx = db.begin().await?;

    let have_stage_migration = query!(
        "
            SELECT *
            FROM movine_migrations
            WHERE name = '2022-06-10-044651_cup_stage_seeding'
        "
    )
    .fetch_optional(&mut tx)
    .await?;

    if !have_stage_migration.is_some() {
        log::debug!("Do not have the correct database migrations to run the stage data migration");
        return Ok(());
    }

    let cups = query!("SELECT id,title FROM cup")
        .fetch_all(&mut tx)
        .await?;

    'cups: for r in cups.into_iter() {
        log::debug!("adding results to cup ({}, {:?})", r.id, r.title);
        let cup_id = r.id;
        let stages = CupStage::by_parent_id(&mut tx, cup_id).await?;
        let num_stages = stages.len();
        for (i, stage) in stages.into_iter().enumerate() {
            log::debug!("adding results to stage ({}, {})", stage.stage_no, stage.id);
            if stage.is_started {
                let Rankings {
                    is_complete,
                    rankings,
                    ..
                } = stage.rankings(None, &mut tx).await?;
                if is_complete {
                    for (index, rank) in rankings.into_iter().enumerate() {
                        query!(
                            "
                            INSERT INTO cup_stage_final_ranking(
                                cup_stage_id, signup_id, index, placement,
                                score, wins, losses, group_no)
                            VALUES($1, $2, $3, $4, $5, $6, $7, $8)
                            ",
                            stage.id,
                            rank.signup_id,
                            index as i32,
                            rank.placement,
                            rank.score,
                            rank.wins,
                            rank.losses,
                            rank.group_no,
                        )
                        .execute(&mut tx)
                        .await?;
                    }
                } else {
                    log::debug!(
                        "skipping stage due to rankings saying it's incomplete, stage=({}, {})",
                        stage.stage_no,
                        stage.id
                    );
                    if i + 1 < num_stages {
                        log::debug!("skipping further stages as well");
                    }
                    continue 'cups;
                }
            } else {
                log::debug!(
                    "skipping stage due to not being started stage=({}, {})",
                    stage.stage_no,
                    stage.id
                );
                if i + 1 < num_stages {
                    log::debug!("skipping further stages as well");
                }
                continue 'cups;
            }
        }
    }
    tx.commit().await?;
    Ok(())
}

////////////////////////////////////////////////////////////////////////////////

struct KnownGames {
    quake_champions_duel: Uuid,
    quake_champions_tdm: Uuid,
    quake_live_duel: Uuid,
    quake_live_tdm: Uuid,
    diabotical_duel: Uuid,
}

async fn get_known_games(db: &mut PgConnection) -> Result<KnownGames, Box<dyn Error>> {

    let quake_champions_duel = sqlx::query!(
        "
        SELECT game_mode.id FROM game_mode
        WHERE game_mode.game_id = (SELECT game.id FROM game WHERE game.name = 'Quake Champions')
          AND game_mode.name = 'Duel'
        "
    ).fetch_one(&mut *db).await?;

    let quake_champions_tdm = sqlx::query!(
        "
        SELECT game_mode.id FROM game_mode
        WHERE game_mode.game_id = (SELECT game.id FROM game WHERE game.name = 'Quake Champions')
          AND game_mode.name = 'Duel'
        "
    ).fetch_one(&mut *db).await?;

    let quake_live_duel = sqlx::query!(
        "
        SELECT game_mode.id FROM game_mode
        WHERE game_mode.game_id = (SELECT game.id FROM game WHERE game.name = 'Quake Live')
          AND game_mode.name = 'Duel'
        "
    ).fetch_one(&mut *db).await?;

    let quake_live_tdm = sqlx::query!(
        "
        SELECT game_mode.id FROM game_mode
        WHERE game_mode.game_id = (SELECT game.id FROM game WHERE game.name = 'Quake Live')
          AND game_mode.name = 'TDM'
        "
    ).fetch_one(&mut *db).await?;

    let diabotical_duel = sqlx::query!(
        "
        SELECT game_mode.id FROM game_mode
        WHERE game_mode.game_id = (SELECT game.id FROM game WHERE game.name = 'Diabotical')
          AND game_mode.name = 'Duel'
        "
    ).fetch_one(&mut *db).await?;

    Ok(KnownGames {
        quake_champions_duel: quake_champions_duel.id,
        quake_champions_tdm: quake_champions_tdm.id,
        quake_live_duel: quake_live_duel.id,
        quake_live_tdm: quake_live_tdm.id,
        diabotical_duel: diabotical_duel.id,
    })
}

/// Retroactively compute ELO using the whole match history so far
async fn add_elo_again20(db: &mut PgConnection) -> Result<(), Box<dyn Error>> {
    let games = get_known_games(&mut *db).await?;
    let mut tx = db.begin().await?;
    let db = ();
    query!("DELETE FROM elo_update").execute(&mut *tx).await?;
    let matches = sqlx::query_as!(
        CupMatch2pRow,
        "
            SELECT m.*
            FROM cup_match_2p m
            WHERE is_scored
            ORDER BY updated_at ASC
        "
    )
    .fetch_all(&mut *tx)
    .await?
    .into_iter()
    .map(CupMatch2p::from_row)
    .collect_vec();

    for (discord_username, game_mode, elo_init) in &[
        ("moistynz", games.quake_champions_duel, 600),
        ("fragincarnate", games.quake_champions_duel, 700),
        ("dandaking", games.quake_champions_duel, 2200),
        ("myztro | RAISY", games.quake_champions_duel, 2400),
        ("myztro | ZenAku", games.quake_champions_duel, 2300),
        ("cha1n", games.quake_champions_duel, 2100),
        ("rapha", games.quake_champions_duel, 2450),
        ("steej0", games.quake_champions_duel, 2150),
    ] {
        query!("
            INSERT INTO elo_update(game_mode_id,player_id,elo,elo_diff) 
            SELECT $1, player.id, $3, 0
            FROM player WHERE player.discord_username = $2
            ",
            game_mode,
            discord_username,
            BigDecimal::from((*elo_init) as i32)
        ).execute(&mut *tx).await?;
    }

    
    for m in matches {
        let cup_id = query!(
            "SELECT s.cup_id FROM cup_stage s WHERE s.id = $1",
            m.cup_stage_id
        ).fetch_one(&mut *tx)
        .await?.cup_id;
        let cup = Cup::by_id(&mut *tx, cup_id).await?;
        let game_mode_id = cup.game_mode_id;
        let cup_start_elo: Option<f32> = match cup_title_division(&cup.title) {
            CupTitleDiv::Div1 => Some(1700.0),
            CupTitleDiv::Div2 => Some(1500.0),
            CupTitleDiv::Div3 => Some(1200.0),
            CupTitleDiv::Div4 => Some(1000.0),
            CupTitleDiv::Div5 => Some(900.0),
            CupTitleDiv::Div6 => Some(800.0),
            _ => None,
        };

        if let (Some(high_id), Some(low_id), Some(_), Some(_)) = (m.high_id, m.low_id, &m.low_report, &m.high_report) {

            match m.id {
                12459|12463|12464|12469|12471|11265|11272|11281|11290|11286|11279|11271 => { continue; },
                _ => {}
            };

            let h_cs = CupSignup::by_id(&mut *tx, high_id).await?;
            let l_cs = CupSignup::by_id(&mut *tx, low_id).await?;

            let h_elo = query!(
                r#"
                SELECT e.elo::float4 AS "elo!"
                FROM elo_update e
                WHERE e.game_mode_id = $3 AND (team_id = $1 OR player_id = $2)
                ORDER BY e.id DESC LIMIT 1
                "#,
                h_cs.team_id,
                h_cs.player_id,
                game_mode_id
            )
            .fetch_optional(&mut *tx)
            .await?.map(|r| r.elo);

            let l_elo = query!(
                r#"
                SELECT e.elo::float4 AS "elo!"
                FROM elo_update e
                WHERE e.game_mode_id = $3 AND (team_id = $1 OR player_id = $2)
                ORDER BY e.id DESC LIMIT 1
                "#,
                l_cs.team_id,
                l_cs.player_id,
                game_mode_id
            )
            .fetch_optional(&mut *tx)
            .await?.map(|r| r.elo);

            let (mut h_elo, mut l_elo) = match (h_elo, l_elo) {
                (Some(h), Some(l)) => (h, l),
                (Some(h), None) => (h, cup_start_elo.unwrap_or(h - 50.0)),
                (None, Some(l)) => (cup_start_elo.unwrap_or(l + 50.0), l),
                (None, None) => (cup_start_elo.unwrap_or(1200.0), cup_start_elo.unwrap_or(1200.0))
            };

            let report = &m.low_report.or(m.high_report);
            for (game, r) in report.iter().flatten().enumerate() {
                let (h_score, l_score) = (r.high, r.low);
                if h_score != l_score {
                    let (h_next, l_next) = elo_update((h_elo, h_score), (l_elo, l_score));
                    let h_diff = h_next - h_elo;
                    let l_diff = l_next - l_elo;
                    for (cs, elo, elo_diff) in &[(&h_cs, h_next, h_diff), (&l_cs, l_next, l_diff)] {
                        log::debug!("elo={:?} elo_diff={:?}", elo, elo_diff);
                        query!(
                            "
                            INSERT INTO 
                                elo_update(
                                    game_mode_id, 
                                    team_id, 
                                    player_id, 
                                    source_match, 
                                    source_match_game, 
                                    elo,
                                    elo_diff,
                                    created_at
                                )
                                VALUES($1, $2, $3, $4, $5, $6, $7, COALESCE($8, now()))
                            ",
                            game_mode_id,
                            cs.team_id,
                            cs.player_id,
                            m.id,
                            game as i32,
                            BigDecimal::try_from(*elo).unwrap(),
                            BigDecimal::try_from(*elo_diff).unwrap(),
                            m.updated_at
                        )
                        .execute(&mut *tx)
                        .await?;
                        h_elo = h_next;
                        l_elo = l_next;
                    }
                }
            }
        }
    }
    tx.commit().await?;
    Ok(())
}

enum CupTitleDiv {
    Div1,
    Div2,
    Div3,
    Div4,
    Div5,
    Div6,
    Unknown
}


fn cup_title_division(title: &str) -> CupTitleDiv {
    let title = title.to_ascii_lowercase();
    if title.contains("division 1") || title.contains("div 1") || title.contains("div1") {
        CupTitleDiv::Div1
    } else if title.contains("division 2") || title.contains("div 2") || title.contains("div2") {
        CupTitleDiv::Div2
    } else if title.contains("division 3") || title.contains("div 3") || title.contains("div3") {
        CupTitleDiv::Div3
    } else if title.contains("division 3") || title.contains("div 3") || title.contains("div3") {
        CupTitleDiv::Div3
    } else if title.contains("division 4") || title.contains("div 4") || title.contains("div4") {
        CupTitleDiv::Div4
    } else if title.contains("division 5") || title.contains("div 5") || title.contains("div5") {
        CupTitleDiv::Div5
    } else if title.contains("division 6") || title.contains("div 6") || title.contains("div6") {
        CupTitleDiv::Div6
    } else {
        CupTitleDiv::Unknown
    }
}


