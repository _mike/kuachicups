use crate::db::Result;
use async_trait::async_trait;
use sqlx::{Executor, Postgres};
use uuid::Uuid;

#[async_trait]
pub trait HasParent<Parent, ParentId = Uuid>: Sized {
    fn parent_id(&self) -> ParentId;

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>>;
}
