use crate::db::Result;
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{Executor, Postgres};
use std::collections::HashMap;
use std::hash::Hash;
use uuid::Uuid;

pub trait HasId: Sized {
    type Id: std::fmt::Debug + Send + Copy + Clone + JsonSchema + Serialize + Eq + PartialEq + Hash;
    fn id(&self) -> Self::Id;
}

impl HasId for Uuid {
    type Id = Uuid;
    fn id(&self) -> Self::Id {
        *self
    }
}

#[async_trait]
pub trait ById: Sized + HasId {
    async fn by_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: <Self as HasId>::Id,
    ) -> Result<Self>;
    async fn by_ids<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: &[<Self as HasId>::Id],
    ) -> Result<Vec<Self>>;
}

pub fn group_by_id<'a, T: ById>(vec: &'a Vec<T>) -> HashMap<T::Id, &'a T> {
    let mut r = HashMap::new();
    for x in vec.iter() {
        r.insert(x.id(), x);
    }
    r
}

#[macro_export]
macro_rules! by_id_impl {
    ($type:ident, $table:literal, $idtype:ty) => {
        #[allow(dead_code)]
        impl crate::db::helpers::by_id::HasId for $type {
            type Id = $idtype;
            #[inline(always)]
            fn id(&self) -> Self::Id {
                self.id
            }
        }

        #[allow(dead_code)]
        #[async_trait::async_trait]
        impl crate::db::helpers::by_id::ById for $type {
            async fn by_id<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
                db: E,
                id: Self::Id,
            ) -> crate::db::Result<Self> {
                Ok(sqlx::query_as!(
                    $type,
                    "SELECT " + $table + ".* FROM " + $table + " WHERE id = $1",
                    id,
                )
                .fetch_one(db)
                .await?)
            }

            async fn by_ids<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
                db: E,
                ids: &[Self::Id],
            ) -> crate::db::Result<Vec<Self>> {
                Ok(sqlx::query_as!(
                    $type,
                    "SELECT t.* FROM "
                        + $table
                        + " AS t WHERE id = ANY($1) ORDER BY array_position($1, t.id)",
                    ids,
                )
                .fetch_all(db)
                .await?)
            }
        }
    };
}

#[macro_export]
macro_rules! by_id_json_impl {
    ($type:ident, $table:literal, $idtype:ty) => {
        #[allow(dead_code)]
        impl crate::db::helpers::by_id::HasId for $type {
            type Id = $idtype;
            #[inline(always)]
            fn id(&self) -> Self::Id {
                self.id
            }
        }

        #[allow(dead_code)]
        #[async_trait::async_trait]
        impl crate::db::helpers::by_id::ById for $type {
            async fn by_id<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
                db: E,
                id: Self::Id,
            ) -> crate::db::Result<Self> {
                use crate::db::from_json::{JsonRow, JsonToDbResult};
                sqlx::query_as!(
                    JsonRow,
                    "SELECT to_jsonb((r.*)) AS obj FROM " + $table + " AS r WHERE id = $1",
                    id,
                )
                .fetch_one(db)
                .await?
                .from_json()
            }

            async fn by_ids<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
                db: E,
                ids: &[Self::Id],
            ) -> crate::db::Result<Vec<Self>> {
                use crate::db::from_json::{JsonRow, JsonToDbResult};
                sqlx::query_as!(
                    JsonRow,
                    "SELECT to_jsonb((r.*)) AS obj FROM "
                        + $table
                        + " AS r WHERE id = ANY($1) ORDER BY array_position($1, r.id)",
                    ids,
                )
                .fetch_all(db)
                .await?
                .from_json()
            }
        }
    };
}
