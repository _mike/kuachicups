use super::model::{Cup, CupMatch2p, CupStage};
use crate::db::{
    tables::player::{Player, Role},
    Error, Executor, MatchReporter, Result, DB,
};
use sqlx::{query, PgConnection};
use uuid::Uuid;

impl CupStage {
    pub async fn require_ownership(
        cup_stage_id: Uuid,
        player_id: Uuid,
        db: &mut PgConnection,
    ) -> Result<()> {
        let team_id = query!(
            "
                SELECT cup.owner_team_id
                FROM cup JOIN cup_stage ON cup_stage.cup_id = cup.id
                WHERE cup_stage.id = $1
                GROUP BY cup.id
            ",
            cup_stage_id,
        )
        .fetch_one(&mut *db)
        .await?
        .owner_team_id;
        if Player::has_role_within(db, player_id, team_id, Role::Captain).await {
            Ok(())
        } else {
            Err(Error::AuthError("Need captain role within team"))
        }
    }
}

impl Cup {
    pub async fn require_ownership(
        cup_id: Uuid,
        player_id: Uuid,
        db: &mut PgConnection,
    ) -> Result<()> {
        let team_id = query!(
            "SELECT cup.owner_team_id FROM cup WHERE cup.id = $1",
            cup_id,
        )
        .fetch_one(&mut *db)
        .await?
        .owner_team_id;
        if Player::has_role_within(db, player_id, team_id, Role::Captain).await {
            Ok(())
        } else {
            Err(Error::AuthError("Need captain role within team"))
        }
    }

    pub async fn require_solo_entrant_type(cup_id: Uuid, db: &mut PgConnection) -> Result<()> {
        let is_solo_entrant_type = query!(
            "SELECT (entrant_type = 'player') OR (entrant_type IS NULL) AS is_solo
            FROM cup JOIN game_mode ON cup.game_mode_id = game_mode.id
            WHERE cup.id = $1 ",
            cup_id
        )
        .fetch_one(db)
        .await?;
        if is_solo_entrant_type.is_solo.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require solo entrant type"))
        }
    }

    pub async fn require_team_entrant_type(cup_id: Uuid, db: &mut PgConnection) -> Result<()> {
        let is_team_entrant_type = query!(
            "
            SELECT (entrant_type = 'team') OR (entrant_type IS NULL) AS is_team
            FROM cup JOIN game_mode ON cup.game_mode_id = game_mode.id
            WHERE cup.id = $1 ",
            cup_id
        )
        .fetch_one(db)
        .await?;
        if is_team_entrant_type.is_team.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require solo entrant type"))
        }
    }

    pub async fn require_checkin_available(cup_id: Uuid, db: &mut PgConnection) -> Result<()> {
        let r = query!("
            SELECT NOT cup.is_finished AND (now() >= checkintime) AND (now() <= starttime) AS can_checkin
            FROM cup JOIN cup_stage ON cup.current_stage = cup_stage.stage_no AND cup.id = cup_stage.cup_id
            WHERE cup.id = $1", cup_id).fetch_one(db).await?;
        if r.can_checkin.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require checkin available"))
        }
    }

    pub async fn require_signup_available(cup_id: Uuid, db: &mut PgConnection) -> Result<()> {
        let r = query!("
            SELECT NOT cup.is_finished AND NOT cup.is_signups_closed AND (now() <= starttime) AS can_signup
            FROM cup JOIN cup_stage ON cup.current_stage = cup_stage.stage_no AND cup.id = cup_stage.cup_id
            WHERE cup.id = $1 ", cup_id).fetch_one(db).await?;
        if r.can_signup.unwrap_or(false) {
            Ok(())
        } else {
            Err(Error::SomeError("require signup available"))
        }
    }

    pub async fn require_started(cup_id: Uuid, db: &mut PgConnection) -> Result<(Uuid, i32)> {
        let r = query!("
            SELECT stage.id, stage.stage_no, stage.is_started
            FROM cup JOIN cup_stage stage ON stage.cup_id = cup.id AND cup.current_stage = stage.stage_no
            WHERE cup.id = $1 AND NOT cup.is_finished
            ",
            cup_id
        ).fetch_one(db).await?;
        if r.is_started {
            Ok((r.id, r.stage_no))
        } else {
            Err(Error::SomeError(
                "require that the cup is started (its current stage is started)",
            ))
        }
    }
}

impl CupMatch2p {
    pub async fn require_participant_of(
        match_id: i32,
        player_id: Uuid,
        db: &mut PgConnection,
    ) -> Result<(Uuid, MatchReporter)> {
        let result = query!(
            "
                SELECT s.id, m.low_id = s.id AS is_low
                FROM cup_match_2p m JOIN cup_signup s ON (m.low_id = s.id OR m.high_id = s.id)
                WHERE m.id = $1 AND s.player_id = $2
            ",
            match_id,
            player_id
        )
        .fetch_one(db)
        .await?;
        let reporter = if let Some(is_low) = result.is_low {
            Ok(if is_low {
                MatchReporter::Low
            } else {
                MatchReporter::High
            })
        } else {
            Err(Error::SomeError("must be participant of that match"))
        }?;
        Ok((result.id, reporter))
    }
}
