use super::model::{Cup, CupMatch2p, CupMatch2pRow, CupSignup};
use crate::db::{Executor, Result, DB};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, PgConnection};
use uuid::Uuid;

/// Queries
///
///

impl Cup {
    pub async fn make_signup_solo(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_signup_available(cup_id, &mut *transaction).await?;
        Cup::require_solo_entrant_type(cup_id, &mut *transaction).await?;
        let result = query!(
            "INSERT INTO cup_signup(cup_id, player_id) VALUES($1, $2)",
            cup_id,
            player_id,
        )
        .execute(&mut *transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn make_signup_solo_under_team(
        db: &DB,
        cup_id: Uuid,
        player_id: Uuid,
        team_id: Uuid,
    ) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_signup_available(cup_id, &mut *transaction).await?;
        Cup::require_solo_entrant_type(cup_id, &mut *transaction).await?;
        let result = query!(
            "INSERT INTO cup_signup(cup_id, player_id, team_id) VALUES($1, $2, $3)",
            cup_id,
            player_id,
            team_id,
        )
        .execute(&mut *transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn make_signup_team(
        db: &DB,
        cup_id: Uuid,
        player_id: Uuid,
        team_id: Uuid,
    ) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_signup_available(cup_id, &mut *transaction).await?;
        Cup::require_team_entrant_type(cup_id, &mut *transaction).await?;
        let result = query!(
            "INSERT INTO cup_signup(cup_id, player_id, team_id) VALUES($1, $2, $3)",
            cup_id,
            player_id,
            team_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_leave(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        let result = query!(
            "DELETE FROM cup_signup WHERE cup_id = $1 AND player_id = $2",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_checkin(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_checkin_available(cup_id, &mut *transaction).await?;
        let result = query!(
            "
            UPDATE cup_signup
            SET checkedin_time = now()
            WHERE cup_id = $1 AND player_id = $2
            ",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_checkout(db: &DB, cup_id: Uuid, player_id: Uuid) -> Result<bool> {
        let mut transaction = db.begin().await?;
        Cup::require_checkin_available(cup_id, &mut *transaction).await?;
        let result = query!(
            "
            UPDATE cup_signup
            SET checkedin_time = NULL
            WHERE cup_id = $1 AND player_id = $2 ",
            cup_id,
            player_id,
        )
        .execute(&mut transaction)
        .await?
        .rows_affected()
            > 0;
        transaction.commit().await?;
        Ok(result)
    }

    pub async fn player_signups_self(
        db: impl Executor<'_>,
        cup_id: Uuid,
        player_id: Uuid,
    ) -> Result<Vec<CupSignup>> {
        Ok(query_as!(
            CupSignup,
            "SELECT * FROM cup_signup WHERE cup_id = $1 AND player_id = $2",
            cup_id,
            player_id,
        )
        .fetch_all(db)
        .await?)
    }

    pub async fn add_signup(
        cup_id: Uuid,
        add: AddSignup,
        db: &mut sqlx::PgConnection,
    ) -> Result<bool> {
        query!(
            "
            INSERT INTO cup_signup(
                cup_id,
                player_id,
                team_id,
                seed_value,
                checkedin_time,
                add_to_stage_no
            )
            VALUES($1, $2, $3, $4, CASE
                WHEN ($5 IS true) THEN now()
                WHEN ($5 IS false) THEN null
                ELSE null
            END, $6) ",
            cup_id,
            add.player_id,
            add.team_id,
            add.seed_value,
            add.checkin,
            add.stage_no,
        )
        .execute(&mut *db)
        .await?;
        Ok(true)
    }

    pub async fn set_signup(set: SetSignup, db: &mut sqlx::PgConnection) -> Result<bool> {
        query!(
            "
            UPDATE
                cup_signup
            SET
                seed_value = $2,
                checkedin_time = (CASE
                    WHEN ($3 IS true) AND (checkedin_time IS NULL) THEN now()
                    WHEN ($3 IS false) AND (checkedin_time IS NOT NULL) THEN null
                    ELSE checkedin_time
                END),
                player_id = $4,
                team_id = $5
            WHERE id = $1
            ",
            set.signup_id,
            set.seed_value,
            set.checkin,
            set.player_id,
            set.team_id,
            // set.add_to_stage_no,
        )
        .execute(&mut *db)
        .await?;
        Ok(true)
    }

    pub async fn forfeit_player(
        cup_id: Uuid,
        player_id: Uuid,
        db: &mut PgConnection,
    ) -> Result<bool> {
        let mut tx = db.begin().await?;
        let signup_id = query!(
            "SELECT id FROM cup_signup WHERE player_id = $1 AND cup_id = $2",
            player_id,
            cup_id
        )
        .fetch_one(&mut *tx)
        .await?
        .id;
        let result = Cup::forfeit_signup(cup_id, signup_id, &mut *tx).await?;
        tx.commit().await?;
        Ok(result)
    }

    pub async fn forfeit_signup(
        cup_id: Uuid,
        signup_id: Uuid,
        db: &mut PgConnection,
    ) -> Result<bool> {
        log::debug!("forfeit_signup cup_id={cup_id} signup_id={signup_id}");
        let mut tx = db.begin().await?;
        let (stage_id, stage_no) = Cup::require_started(cup_id, &mut *tx).await?;
        let result = query!(
            "
            UPDATE cup_signup
            SET forfeit_stage_no = $1
            WHERE id = $2
            ",
            stage_no,
            signup_id,
        )
        .execute(&mut tx)
        .await?
        .rows_affected()
            > 0;
        log::debug!("set forfeit stage to {}", stage_no);
        // Now update the matches
        // Where matches have no reports, give the other signup the win, i.e.
        // 1) Where matches have me as the high_id, give low_id the win
        // 2) Where matches have me as the low_id, give high_id the win
        // 3) Advance the stage around those matches that we've just updated
        let mut rows = Vec::<CupMatch2pRow>::new();
        rows.append(
            &mut query_as!(
                CupMatch2pRow,
                "
                    UPDATE cup_match_2p
                    SET winner_id = high_id, loser_id = low_id
                    WHERE NOT is_scored AND cup_stage_id = $1 AND low_id = $2
                    RETURNING *
                ",
                stage_id,
                signup_id
            )
            .fetch_all(&mut tx)
            .await?,
        );
        // 2)
        rows.append(
            &mut query_as!(
                CupMatch2pRow,
                "
                    UPDATE cup_match_2p
                    SET winner_id = low_id, loser_id = high_id
                    WHERE NOT is_scored AND cup_stage_id = $1 AND high_id = $2
                    RETURNING *
                ",
                stage_id,
                signup_id
            )
            .fetch_all(&mut tx)
            .await?,
        );
        log::debug!(
            "advancing forfeit matches {:?}",
            rows.iter().map(|m| m.id).collect::<Vec<_>>()
        );
        for m in rows.into_iter().map(CupMatch2p::from_row) {
            m.advance_around(&mut *tx).await?;
        }
        // We could go further and update other matches...
        tx.commit().await?;
        Ok(result)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, JsonSchema)]
pub struct AddSignup {
    player_id: Uuid,
    team_id: Option<Uuid>,
    seed_value: Option<i32>,
    stage_no: Option<i32>,
    checkin: Option<bool>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct SetSignup {
    signup_id: Uuid,
    player_id: Uuid,
    team_id: Option<Uuid>,
    seed_value: Option<i32>,
    checkin: Option<bool>,
}
