use super::model::{Cup, CupSignup};
use crate::db::{Executor, Result, DB};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as};
use uuid::Uuid;

impl Cup {
    /// Get all the signups ordered by their seed value
    pub async fn get_all_signups_seeded(
        db: impl Executor<'_>,
        cup_id: Uuid,
    ) -> Result<Vec<CupSignup>> {
        Ok(query_as!(
            CupSignup,
            "
                SELECT * FROM cup_signup
                WHERE cup_id = $1
                ORDER BY add_to_stage_no ASC NULLS FIRST, seed_value ASC NULLS LAST
            ",
            cup_id,
        )
        .fetch_all(db)
        .await?)
    }

    /// Get all checked-in signups, ordered by their seed value
    pub async fn get_checkedin_signups_seeded(
        db: impl Executor<'_>,
        cup_id: Uuid,
    ) -> Result<Vec<CupSignup>> {
        Ok(query_as!(
            CupSignup,
            "
                SELECT * FROM cup_signup
                WHERE cup_id = $1 AND checked_in AND (add_to_stage_no IS NULL OR add_to_stage_no = 0)
                ORDER BY seed_value ASC NULLS LAST
            ",
            cup_id,
        )
        .fetch_all(db)
        .await?)
    }
}
