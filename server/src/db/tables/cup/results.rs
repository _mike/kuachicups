use super::model::{Cup, CupSignup, CupSignupResult, CupStage};
use crate::db::{
    paged1, ById, Error, Executor, Page, PageItem, Paged, Ranking, Rankings, Result, DB, PAGE_LEN,
};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, PgConnection, Postgres};
use uuid::Uuid;

impl CupSignupResult {
    pub async fn by_signup<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        signup_id: Uuid,
    ) -> Result<CupSignupResult> {
        let row = sqlx::query_as!(
            CupSignupResult,
            "
                SELECT r.*
                FROM cup_signup_result r
                WHERE id = $1
                ORDER BY updated_at DESC
            ",
            signup_id,
        )
        .fetch_one(db)
        .await?;
        Ok(row)
    }

    pub async fn by_player<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        player_id: Uuid,
    ) -> Result<Vec<CupSignupResult>> {
        let rows = sqlx::query_as!(
            CupSignupResult,
            "
                SELECT r.*
                FROM cup_signup_result r
                WHERE player_id = $1
                ORDER BY updated_at DESC
            ",
            player_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows)
    }

    pub async fn by_player_solo<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        player_id: Uuid,
    ) -> Result<Vec<CupSignupResult>> {
        let rows = sqlx::query_as!(
            CupSignupResult,
            "
                SELECT r.*
                FROM cup_signup_result r
                WHERE player_id = $1 AND team_id IS NULL
                ORDER BY updated_at DESC
            ",
            player_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows)
    }

    pub async fn by_team<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        team_id: Uuid,
    ) -> Result<Vec<CupSignupResult>> {
        let rows = sqlx::query_as!(
            CupSignupResult,
            "
                SELECT r.*
                FROM cup_signup_result r
                WHERE team_id = $1
                ORDER BY updated_at DESC
            ",
            team_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows)
    }
}

impl Cup {
    pub async fn is_complete(cup_id: Uuid, db: &mut PgConnection) -> Result<Option<CupStage>> {
        let cur_stage = CupStage::current(cup_id, &mut *db)
            .await?
            .ok_or(Error::SomeError("no current stage"))?;

        let next_stage_id = query!(
            "SELECT id FROM cup_stage WHERE cup_id = $1 AND stage_no = $2",
            cup_id,
            cur_stage.stage_no + 1
        )
        .fetch_optional(&mut *db)
        .await?;

        match next_stage_id {
            None => {
                if CupStage::is_complete(cur_stage.id, &mut *db).await? {
                    Ok(Some(cur_stage))
                } else {
                    Ok(None)
                }
            }

            // there is another stage, this cup can't be complete
            Some(_) => Ok(None),
        }
    }

    pub async fn add_cup_results_for_complete_cup(
        stage: &CupStage,
        db: &mut PgConnection,
    ) -> Result<()> {
        let existing = query!(
            "SELECT true AS ok FROM cup_signup_result WHERE cup_id = $1 LIMIT 1",
            stage.cup_id
        )
        .fetch_optional(&mut *db)
        .await?;
        match existing {
            Some(e) if e.ok == Some(true) => {
                return Err(Error::SomeError(
                    "cannot add results to stage with results already",
                ));
            }
            _ => {}
        }
        let rankings = stage.rankings(None, &mut *db).await?.rankings;

        let mut tx = db.begin().await?;
        for Ranking {
            signup_id,
            score,
            placement,
            ..
        } in rankings
        {
            let signup = CupSignup::by_id(&mut *tx, signup_id).await?;
            query!(
                "
                INSERT INTO cup_signup_result (id, team_id, player_id, cup_id, score, placement)
                VALUES ($1, $2, $3, $4, $5, $6)
                ",
                signup.id,
                signup.team_id,
                signup.player_id,
                signup.cup_id,
                score,
                placement,
            )
            .execute(&mut tx)
            .await?;
        }
        tx.commit().await?;

        Ok(())
    }
}
