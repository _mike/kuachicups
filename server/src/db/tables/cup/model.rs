use crate::db::{
    tables::{player::Player, team::Role},
    Authorize, Error, Executor, HasParent, JsonRow, JsonToDbResult, Result,
};
use crate::{by_id_impl, by_id_json_impl, paginated_impl};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use num_derive::{FromPrimitive, ToPrimitive};
use num_traits::{FromPrimitive, ToPrimitive};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, PgConnection, Postgres};
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct Cup {
    pub id: Uuid,
    pub owner_team_id: Uuid,
    pub game_mode_id: Uuid,
    pub slug: String,
    pub title: String,
    pub description_md: String,
    pub description_html: String,
    pub current_stage: Option<i32>,
    pub is_signups_closed: bool,
    pub is_finished: bool,
    pub is_published: bool,
    pub overlay_theme: Option<String>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, JsonSchema, sqlx::Type, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum CupStageFormat {
    DoubleElim,
    SingleElim,
    Groups,
    Ladder,
}

impl CupStageFormat {
    pub fn is_elim(&self) -> bool {
        match self {
            Self::DoubleElim => true,
            Self::SingleElim => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupStage {
    pub id: Uuid,
    pub stage_no: i32,
    pub division: i32,
    pub cup_id: Uuid,
    pub title: String,
    pub slug: String,
    pub start_immediately: bool,
    pub is_started: bool,
    pub checkintime: Option<DateTime<Utc>>,
    pub starttime: Option<DateTime<Utc>>,
    pub format: CupStageFormat,
    pub max_participants: Option<i32>,
    pub group_size: Option<i32>,
    pub group_rematches: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
#[serde(rename_all = "snake_case")]
pub enum PickbanStepKind {
    LowBan,
    LowPick,
    HighBan,
    HighPick,
    LowLose,
    Reset,
}

/// A pickban step _description_ (not an actual step done on a match)
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct PickbanStep {
    pub pool_name: String,
    pub kind: PickbanStepKind,
    pub parent_index: Option<i32>,
}

impl PickbanStepKind {
    pub fn is_pick(&self) -> bool {
        use PickbanStepKind::*;
        match &self {
            HighPick | LowPick => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder, PartialEq, Eq, Hash)]
pub struct PickbanPoolItem {
    pub name: String,
    pub slug: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct PickbanPool {
    pub name: String,
    pub slug: String,
    pub items: Vec<PickbanPoolItem>,
}

/// a pickban system
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct Pickbans {
    pub id: i32,
    pub name: String,
    pub slug: String,
    pub game_mode: Option<Uuid>,
    pub bestof: i32,
    pub pool: Vec<PickbanPool>,
    pub steps: Vec<PickbanStep>,
}

pub type PoolName = String;
pub type PoolItemName = String;
pub type PoolSlug = String;
pub type PoolItemSlug = String;

/// NOTE Available items are ones that are _not_ in the pool filter
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub enum PoolItemState {
    Picked,
    Banned,
}

/// NOTE Available items are ones that are _not_ in the pool filter
pub type PoolFilter = HashMap<PoolName, HashMap<PoolItemName, PoolItemState>>;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupMatch2pPickbanStep {
    pub cup_match_id: i32,
    /// who did this step
    pub actor: Uuid,
    /// the index of this pickban step
    pub pickban_index: i32,
    /// the pickbans system used
    pub pickbans: i32,
    /// what pool (by name) the step was on
    pub pool_name: String,
    /// the item (by name) of the pool that was actioned
    pub item_name: String,
    /// what the kind (high/low pick/ban) of step was
    pub kind: PickbanStepKind,
    /// the pool filter _after_ this step
    pub pool_filter: PoolFilter,
    /// parent step, if any
    pub parent_index: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

/// DEPRECATED
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageScoring {
    pub id: i32,
    pub cup_stage_id: Option<Uuid>,
    pub bestof: i32,
    pub pickbans: Option<i32>,
    pub elim_round: Option<i32>,
    pub is_lb: Option<bool>,
    pub groups: bool,
    pub is_point_score: Option<bool>,
    pub point_score_greatest_is_winner: Option<bool>,
    pub is_time_score: Option<bool>,
    pub is_time_score_race: Option<bool>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupSignup {
    pub id: Uuid,
    pub player_id: Uuid,
    pub team_id: Option<Uuid>,
    pub cup_id: Uuid,
    pub checked_in: bool,
    pub signup_time: DateTime<Utc>,
    pub checkedin_time: Option<DateTime<Utc>>,
    pub seed_value: Option<i32>,
    pub add_to_stage_no: Option<i32>,
    pub forfeit_stage_no: Option<i32>,
}

/// Final result for a signup in a cup
///
/// Used for the final results in a cup
///
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupSignupResult {
    /// links to CupSignup(id)
    pub id: Uuid,
    pub player_id: Uuid,
    pub team_id: Option<Uuid>,
    pub cup_id: Uuid,
    // ^ all fields from CupSignup
    pub score: i32,
    pub placement: i32,
    // there's no created_at because it'd be the same
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupStageFinalRanking {
    pub cup_stage_id: Uuid,
    pub signup_id: Uuid,
    pub index: i32,
    pub placement: i32,
    pub score: i32,
    pub wins: i32,
    pub losses: i32,
    pub map_wins: Option<i32>,
    pub map_losses: Option<i32>,
    pub group_no: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

pub use super::stage::ElimMatchType;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupMatch2p {
    pub id: i32,
    pub lid: i32,
    pub cup_stage_id: Option<Uuid>,
    pub group_no: Option<i32>,
    pub group_round: Option<i32>,
    pub elim_round: Option<i32>,
    pub elim_logical_round: Option<i32>,
    pub elim_index: Option<i32>,
    pub elim_type: Option<ElimMatchType>,
    pub elim_winner_match_id: Option<i32>,
    pub elim_winner_to_high: Option<bool>,
    pub elim_loser_match_id: Option<i32>,
    pub elim_loser_to_high: Option<bool>,
    pub elim_low_match_id: Option<i32>,
    pub elim_high_match_id: Option<i32>,
    pub scoring_id: i32,
    pub low_id: Option<Uuid>,
    pub high_id: Option<Uuid>,
    pub low_report: Option<Vec<ScoreReport>>,
    pub high_report: Option<Vec<ScoreReport>>,
    pub is_scored: bool,
    pub winner_id: Option<Uuid>,
    pub loser_id: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

/// Raw data version of 'CupMatch2p'
#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct CupMatch2pRow {
    pub id: i32,
    pub lid: i32,
    pub cup_stage_id: Option<Uuid>,
    pub group_no: Option<i32>,
    pub group_round: Option<i32>,
    pub elim_round: Option<i32>,
    pub elim_logical_round: Option<i32>,
    pub elim_index: Option<i32>,
    pub elim_type: Option<i16>,
    pub elim_winner_match_id: Option<i32>,
    pub elim_winner_to_high: Option<bool>,
    pub elim_loser_match_id: Option<i32>,
    pub elim_loser_to_high: Option<bool>,
    pub elim_low_match_id: Option<i32>,
    pub elim_high_match_id: Option<i32>,
    pub scoring_id: i32,
    pub low_id: Option<Uuid>,
    pub high_id: Option<Uuid>,
    pub low_report_low: Option<Vec<i32>>,
    pub low_report_high: Option<Vec<i32>>,
    pub high_report_low: Option<Vec<i32>>,
    pub high_report_high: Option<Vec<i32>>,
    pub is_scored: bool,
    pub winner_id: Option<Uuid>,
    pub loser_id: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder, PartialEq, Eq)]
pub struct ScoreReport {
    pub high: i32,
    pub low: i32,
}

// Basic queries
by_id_impl!(Cup, "cup", Uuid);
by_id_impl!(CupStageScoring, "cup_stage_scoring", i32);
by_id_json_impl!(CupStage, "cup_stage", Uuid);
by_id_json_impl!(Pickbans, "pickbans", i32);
by_id_impl!(CupSignup, "cup_signup", Uuid);
by_id_impl!(CupSignupResult, "cup_signup_result", Uuid);

paginated_impl!(
    Cup,
    "cup",
    16,
    "WHERE is_published ORDER BY created_at DESC"
);

paginated_impl!(CupStageScoring, "cup_stage_scoring");

#[async_trait]
impl HasParent<Cup> for CupStage {
    fn parent_id(&self) -> Uuid {
        self.cup_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>> {
        query_as!(
            JsonRow,
            "
            SELECT to_jsonb((cup_stage.*)) as obj
            FROM cup_stage
            WHERE cup_id = $1
            ORDER BY stage_no ASC ",
            id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }
}

#[async_trait]
impl HasParent<CupStage, Option<Uuid>> for CupStageScoring {
    fn parent_id(&self) -> Option<Uuid> {
        self.cup_stage_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>> {
        Ok(query_as!(
            CupStageScoring,
            "SELECT * FROM cup_stage_scoring WHERE cup_stage_id = $1",
            id
        )
        .fetch_all(db)
        .await?)
    }
}

#[async_trait]
impl HasParent<CupStage> for CupStageFinalRanking {
    fn parent_id(&self) -> Uuid {
        self.cup_stage_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>> {
        Ok(query_as!(
            CupStageFinalRanking,
            "
            SELECT *
            FROM cup_stage_final_ranking
            WHERE cup_stage_id = $1
            ORDER BY index ASC
            ",
            id
        )
        .fetch_all(db)
        .await?)
    }
}

impl Cup {
    pub async fn current_stage(cup_id: Uuid, db: &mut PgConnection) -> Result<Option<Uuid>> {
        let r = query!(
            "
            SELECT s.id AS cup_stage_id
            FROM cup c JOIN cup_stage s ON (c.id = s.cup_id AND c.current_stage = s.stage_no)
            WHERE c.id = $1
            ",
            cup_id,
        )
        .fetch_optional(&mut *db)
        .await?;
        match r {
            Some(r) => Ok(Some(r.cup_stage_id)),
            None => Ok(None),
        }
    }
}
