use itertools::Itertools;
use std::iter::{empty, once, repeat, Iterator};

/// Create an array of matches between signup indices according to slaughter
/// seeding
pub fn slaughter_seeding(signups: u32) -> Vec<(Option<u32>, Option<u32>)> {
    match signups {
        0 => {
            return vec![];
        }
        1 => {
            return vec![(Some(0), None)];
        }
        2 => {
            return vec![(Some(0), Some(1))];
        }
        _ => {}
    };
    let max_depth = (signups as f32).log2().ceil() as u32;
    let winners = slaughter_seeding_flat(max_depth);
    let mut r = Vec::new();
    let check_signup = |i: u32| -> Option<u32> {
        if i < signups {
            Some(i)
        } else {
            None
        }
    };
    for i in 0..(winners.len() / 2) {
        let high = winners[i * 2].expect("no high seed index");
        let low = winners[i * 2 + 1].expect("no low seed index");
        r.push((check_signup(high), check_signup(low)));
    }
    r
}

/// Compute a array of logical pairs of winner, loser, for the nearest above
/// or equal power of 2 to signups
///
/// This is then processed below according to how many signups there actually
/// are to effectively add byes
///
/// Think of the algorithm below as walking backward from the grand final of
/// a single elimination bracket, from the grand final to the round 0 games.
/// If the seeding is correct, then what we expect is like:
///
/// - Grand final (round R) has s0 win against s1
///
/// - round (R - 1) has these players win: s0 against s3, s1 against s2
///
/// - round (R - 2) has these players win:
///   - s0 against s7
///   - s1 against s6
///   - s2 against s5
///   - s3 against s4
///
/// etc, all the way down to R=0. When we do this backwards like this the
/// interleaving of matches that ensures basically the most lopsided matches
/// possible per round happen given the seeds available
pub fn slaughter_seeding_flat(max_depth: u32) -> Vec<Option<u32>> {
    let mut winners = vec![Some(0)];
    for depth in 1..=max_depth {
        let next_matches = 2_usize.pow(depth);
        let mut next_winners = repeat(None as Option<u32>).take(next_matches).collect_vec();
        assert_eq!(winners.len(), 2_usize.pow(depth - 1));
        let mut losing_entrants = (winners.len() as u32..winners.len() as u32 * 2).collect_vec();
        let indexed_winners = winners.iter().enumerate().sorted_by_key(|s| s.1);
        for (index, seed) in indexed_winners {
            let loser = losing_entrants.pop();
            next_winners[index * 2] = Some(seed.expect("no winner!"));
            next_winners[index * 2 + 1] = loser;
        }
        winners = next_winners;
    }
    winners
}

#[cfg(test)]
mod test {
    use super::slaughter_seeding;

    #[test]
    fn slaughter_seeding_2() {
        assert_eq!(slaughter_seeding(2), vec![(Some(0), Some(1)),]);
    }

    #[test]
    fn slaughter_seeding_3() {
        assert_eq!(
            slaughter_seeding(3),
            vec![(Some(0), None), (Some(1), Some(2)),]
        );
    }

    #[test]
    fn slaughter_seeding_4() {
        assert_eq!(
            slaughter_seeding(4),
            vec![(Some(0), Some(3)), (Some(1), Some(2)),]
        );
    }

    #[test]
    fn slaughter_seeding_5() {
        assert_eq!(
            slaughter_seeding(5),
            vec![
                (Some(0), None),
                (Some(3), Some(4)),
                (Some(1), None),
                (Some(2), None),
            ]
        );
    }

    #[test]
    fn slaughter_seeding_6() {
        assert_eq!(
            slaughter_seeding(6),
            vec![
                (Some(0), None),
                (Some(3), Some(4)),
                (Some(1), None),
                (Some(2), Some(5)),
            ]
        );
    }

    #[test]
    fn slaughter_seeding_7() {
        assert_eq!(
            slaughter_seeding(7),
            vec![
                (Some(0), None),
                (Some(3), Some(4)),
                (Some(1), Some(6)),
                (Some(2), Some(5)),
            ]
        );
    }

    #[test]
    fn slaughter_seeding_8() {
        assert_eq!(
            slaughter_seeding(8),
            vec![
                (Some(0), Some(7)),
                (Some(3), Some(4)),
                (Some(1), Some(6)),
                (Some(2), Some(5)),
            ]
        );
    }
}
