// ElimNode algorithms inspired by 'Double-Elimination Tournaments: Counting and
// Calculating' https://www.jstor.org/stable/2685040
use itertools::Itertools;
use recur_fn::{recur_fn, RecurFn};
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::cmp::max;
use std::collections::{BTreeSet, VecDeque};
use std::iter::{empty, once, repeat, Iterator};
use std::rc::Rc;
use std::sync::{Arc, RwLock};

mod link_fun;
use link_fun::LinkFun;

mod node;
pub use node::{ElimContestent, ElimMatchType, ElimNode};
use ElimContestent::{Loser, Seed};
use ElimNode::{Bye, In, Match, Output};

use crate::db::tables::cup::stage::slaughter_seeding::*;
use crate::id_gen::IdGen;

/// A tournament structure for either single or double elimination
#[derive(Clone, Debug)]
pub struct ElimBracket {
    pub root: Arc<RwLock<ElimNode>>,
    id_gen: IdGen,
}

impl ElimNode {
    pub fn bracket(self) -> ElimBracket {
        ElimBracket::new(self)
    }
}

impl ElimBracket {
    pub fn new(node: ElimNode) -> ElimBracket {
        ElimBracket {
            root: Arc::new(RwLock::new(node)),
            id_gen: IdGen::new(),
        }
    }

    pub fn height(&self) -> usize {
        self.root.read().unwrap().height()
    }

    /// List all contestents
    pub fn contestents(&self) -> Vec<Arc<RwLock<ElimContestent>>> {
        let mut r: Vec<Arc<RwLock<ElimContestent>>> = Vec::new();
        let mut queue: Vec<Arc<RwLock<ElimNode>>> = Vec::new();
        queue.push(self.root.clone());
        while let Some(here) = queue.pop() {
            match &*here.read().unwrap() {
                Match { high, low, .. } => {
                    queue.push(high.clone());
                    queue.push(low.clone());
                }
                In(c) => {
                    r.push(c.clone());
                }
                Bye => {}
                Output { .. } => {}
            }
        }
        r
    }

    pub fn name(&self) -> String {
        match &*self.root.read().unwrap() {
            In(_) => format!("{:?}", self),
            Bye => "Bye".to_string(),
            Match { id, match_type, .. } => format!("Match({}, {:?})", id, match_type),
            Output { id, match_type, .. } => format!("Output({}, {:?})", id, match_type),
        }
    }

    pub fn pprint(&self) {
        println!("");
        fn go(node: Arc<RwLock<ElimNode>>, prefix: String, last: bool) {
            let prefix_current = if last { "`- " } else { "|- " };
            let prefix_child = if last { "   " } else { "|  " };
            let prefix = prefix + prefix_child;
            let node = node.read().unwrap();
            println!("{}{}{}", prefix, prefix_current, node.name());
            if let Some(high) = node.high() {
                go(high.clone(), prefix.to_string(), false);
            }
            if let Some(low) = node.low() {
                go(low.clone(), prefix.to_string(), false);
            }
        }
        go(self.root.clone(), "".to_string(), true)
    }

    /// Collect all the matches and group them into rounds
    pub fn levels(&self) -> Vec<Vec<Arc<RwLock<ElimNode>>>> {
        let mut result: Vec<Rc<RefCell<Vec<Arc<RwLock<ElimNode>>>>>> = Vec::new();
        let mut queue: Vec<(usize, Arc<RwLock<ElimNode>>)> = Vec::new();
        queue.push((0, self.root.clone()));
        while let Some((height, node)) = queue.pop() {
            if let Match { high, low, .. } = &*node.clone().read().unwrap() {
                queue.push((height + 1, low.clone()));
                queue.push((height + 1, high.clone()));
                let level = result.get_mut(height).cloned();
                let level = if level.is_none() && result.len() == height {
                    let r = Rc::new(RefCell::new(Vec::new()));
                    result.push(r.clone());
                    r
                } else if let Some(level) = level {
                    level
                } else {
                    panic!("cannot flatten bracket")
                };
                level.borrow_mut().push(node);
            }
        }
        result.reverse();
        result.iter().map(|round| round.take()).collect()
    }

    pub fn single_elim(signups: u32) -> ElimBracket {
        let mut wb = Self::winner_bracket(signups, true);
        wb.simplify();
        wb
    }

    pub fn winner_bracket(signups: u32, has_gf: bool) -> ElimBracket {
        if signups == 0 {
            return Bye.bracket();
        } else if signups == 1 {
            let seed = Seed { rank: 0 }.new();
            return Match {
                id: 0,
                round: 0,
                match_type: ElimMatchType::WB,
                high: In(seed).new(),
                low: Bye.new(),
                drop: None,
            }
            .bracket();
        } else if signups == 2 {
            let high = Seed { rank: 0 }.new();
            let low = Seed { rank: 1 }.new();
            return Match {
                id: 0,
                round: 0,
                match_type: ElimMatchType::WB,
                high: In(high).new(),
                low: In(low).new(),
                drop: None,
            }
            .bracket();
        }
        let mut seeding = slaughter_seeding(signups);
        seeding.reverse();
        let seeding = Rc::new(RefCell::new(seeding));
        let id_gen = IdGen::new();
        let matches = seeding.borrow().len() as u32;
        let rounds = ((2 * matches) as f32).log2().floor() as u32;
        let go = recur_fn(|go, depth: u32| -> Arc<RwLock<ElimNode>> {
            let round = rounds - depth - 1;
            match round {
                0 => {
                    let mut seeding = seeding.borrow_mut();
                    let seeds = seeding.pop();
                    match seeds {
                        Some((high, low)) => Match {
                            id: id_gen.next_id(),
                            round,
                            match_type: ElimMatchType::WB,
                            high: match high {
                                Some(high) => In(Seed { rank: high }.new()),
                                None => Bye,
                            }
                            .new(),
                            low: match low {
                                Some(low) => In(Seed { rank: low }.new()),
                                None => Bye,
                            }
                            .new(),
                            drop: None,
                        },
                        None => Bye,
                    }
                    .new()
                }
                _ => Match {
                    id: id_gen.next_id(),
                    round,
                    match_type: if round >= rounds - 1 && has_gf {
                        ElimMatchType::GF1
                    } else {
                        ElimMatchType::WB
                    },
                    high: go(depth + 1),
                    low: go(depth + 1),
                    drop: None,
                }
                .new(),
            }
        });
        ElimBracket {
            root: go.call(0),
            id_gen,
        }
    }

    /// Add a loser bracket
    pub fn add_loser_bracket(self) -> ElimBracket {
        let wb_matches: Vec<Vec<Arc<RwLock<ElimNode>>>> = self.levels();
        let wb_rounds = wb_matches.len();

        let wb_drops: Vec<(usize, Vec<Arc<RwLock<ElimNode>>>)> = wb_matches
            .iter()
            .enumerate()
            .map(|(round, matches)| {
                (
                    round,
                    LinkFun::optimal_linking(round as u32, wb_rounds as u32, matches.clone()),
                )
            })
            .collect::<Vec<_>>();

        let rounds = wb_rounds * 2;
        let lb_rounds = rounds - 2; // the LB rounds = the rounds minus the two
                                    // special grand finals

        let wb_drops = Arc::new(RwLock::new(wb_drops));
        let make_loser_bracket = recur_fn(|go, depth: usize| -> Arc<RwLock<ElimNode>> {
            let round = lb_rounds - depth;
            if round == 0 {
                // this is the initial loser bracket round where there is nobody
                // here and we have to seed the lb
                let mut wb_drops = wb_drops.write().unwrap();
                let wb_round_0 = &mut wb_drops.get_mut(0).expect("no round 0").1;
                if let Some(d) = wb_round_0.pop() {
                    let loser = In(Loser(d.clone()).new()).new();
                    {
                        let mut d = d.write().unwrap();
                        match *d {
                            Match { ref mut drop, .. } => {
                                *drop = Some(loser.clone());
                            }
                            _ => {}
                        }
                    };
                    loser
                } else {
                    Bye.new()
                }
            } else if round % 2 == 0 {
                // this is a round where we have to add losers to the existing
                // lb the high seed is the one who dropped from the WB. the low
                // seed is already in the LB, and we have to generate the rest
                // of the LB that they come from as well
                let wb_round = round / 2;
                let wb_match = wb_drops
                    .write()
                    .unwrap()
                    .get_mut(wb_round)
                    .expect(&format!("no wb_round {}", round))
                    .1
                    .pop();
                if let Some(wb_match) = wb_match {
                    let loser = In(Loser(wb_match.clone()).new()).new();
                    match *wb_match.write().unwrap() {
                        Match { ref mut drop, .. } => {
                            *drop = Some(loser.clone());
                        }
                        _ => {}
                    };
                    Match {
                        id: self.id_gen.next_id(),
                        round: (round - 1) as u32,
                        match_type: ElimMatchType::LB,
                        high: loser.clone(),
                        low: go(depth + 1),
                        drop: None,
                    }
                    .new()
                } else {
                    log::warn!("a wb_match at round {} didn't exist", round);
                    go(depth + 1)
                }
            } else {
                // this is a round where losers face off with eachother
                Match {
                    id: self.id_gen.next_id(),
                    round: (round - 1) as u32,
                    match_type: ElimMatchType::LB,
                    high: go(depth + 1),
                    low: go(depth + 1),
                    drop: None,
                }
                .new()
            }
        });

        let gf1 = Match {
            id: self.id_gen.next_id(),
            round: (rounds - 2) as u32,
            match_type: ElimMatchType::GF1,
            high: self.root.clone(),
            low: make_loser_bracket.call(0),
            drop: None,
        }
        .new();

        let gf1_loser = In(Loser(gf1.clone()).new()).new();

        let gf2 = Match {
            id: self.id_gen.next_id(),
            round: (rounds - 1) as u32,
            match_type: ElimMatchType::GF2,
            high: gf1.clone(),
            low: gf1_loser.clone(),
            drop: None,
        }
        .new();

        match *gf1.write().unwrap() {
            Match { ref mut drop, .. } => {
                *drop = Some(gf1_loser);
            }
            _ => {}
        }

        ElimBracket {
            id_gen: self.id_gen,
            root: gf2,
        }
    }

    /// Generate a double elimination bracket for the given number of signups
    pub fn double_elim(signups: u32) -> ElimBracket {
        let winner_bracket = ElimBracket::winner_bracket(signups, false);
        let mut de = winner_bracket.add_loser_bracket();
        de.simplify();
        de.pprint();
        de
    }

    /// Simplify a bracket
    ///
    /// Removes redundant Bye games and contestents
    fn simplify(&mut self) {
        #[derive(Debug)]
        struct SimplifyElimNode {
            node: Arc<RwLock<ElimNode>>,
            byes: Rc<RefCell<(bool, bool)>>,
            parent: Option<usize>,
            is_high: Option<bool>,
        }

        let mut queue = vec![(None, None, self.root.clone())];
        let mut back = vec![];
        while let Some((is_high, parent, node)) = queue.pop() {
            let i = back.len();
            back.push(SimplifyElimNode {
                parent,
                is_high,
                node: node.clone(),
                byes: Rc::new(RefCell::new((false, false))),
            });
            let node = node.write().unwrap();
            match &*node {
                Bye => {}
                Match { high, low, .. } => {
                    queue.push((Some(true), Some(i), high.clone()));
                    queue.push((Some(false), Some(i), low.clone()));
                }
                _ => {}
            }
        }

        let mut back = back.iter().collect::<Vec<_>>();
        let parents = back.clone();
        back.reverse();

        for SimplifyElimNode {
            node,
            byes,
            parent,
            is_high,
        } in back
        {
            let parent: Option<&SimplifyElimNode> = match parent {
                Some(parent) => parents.get(*parent).map(|p| *p),
                None => None,
            };
            let (high_bye, low_bye) = *byes.borrow();
            if high_bye && low_bye {
                let mut node = node.write().unwrap();
                // both children are byes
                match *node {
                    Match {
                        drop: Some(ref mut drop),
                        ..
                    } => {
                        *drop.write().unwrap() = Bye;
                    }
                    _ => {}
                }
                *node = Bye;
            } else if high_bye || low_bye {
                // one of the child matches are byes
                let (repl, drop) = match &*node.read().unwrap() {
                    Match { high, drop, .. } if low_bye => (Some(high.clone()), drop.clone()),
                    Match { low, drop, .. } if high_bye => (Some(low.clone()), drop.clone()),
                    _ => (None, None),
                };
                match repl {
                    Some(repl) => {
                        match drop {
                            Some(drop) => {
                                *drop.write().unwrap() = Bye;
                            }
                            None => {}
                        }
                        *node.write().unwrap() = repl.read().unwrap().clone();
                    }
                    None => {}
                }
            }

            match &*node.read().unwrap() {
                Bye => match parent {
                    Some(SimplifyElimNode { byes, .. }) => {
                        let mut byes = (&*byes).borrow_mut();
                        if *is_high == Some(true) {
                            byes.0 = true;
                        } else if *is_high == Some(false) {
                            byes.1 = true;
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }
    }

    /// Truncate the bracket to have only the given number of rounds
    pub fn truncate(&mut self, max_round: u32) {
        let mut queue: Vec<(u32, Arc<RwLock<ElimNode>>)> = Vec::new();
        queue.push((0, self.root.clone()));
        while let Some((depth, here)) = queue.pop() {
            let mut here = here.write().unwrap();
            match &*here {
                Match {
                    id,
                    round,
                    match_type,
                    high,
                    low,
                    ..
                } => {
                    if *round >= max_round {
                        *here = Output {
                            id: *id,
                            round: *round,
                            match_type: *match_type,
                        };
                    } else {
                        queue.push((depth + 1, high.clone()));
                        queue.push((depth + 1, low.clone()));
                    }
                }
                _ => {}
            }
        }
    }
}
