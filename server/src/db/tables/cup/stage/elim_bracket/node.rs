use num_derive::{FromPrimitive, ToPrimitive};
use rocket_okapi::JsonSchema;
use serde::{Deserialize, Serialize};
use std::cmp::max;
use std::sync::{Arc, RwLock};

/// Node in a bracket
#[derive(Clone, Debug)]
pub enum ElimNode {
    Match {
        id: u32,
        round: u32,
        match_type: ElimMatchType,
        /** High seed */
        high: Arc<RwLock<ElimNode>>,
        /** Low seed */
        low: Arc<RwLock<ElimNode>>,
        drop: Option<Arc<RwLock<ElimNode>>>,
    },
    In(Arc<RwLock<ElimContestent>>),
    Bye,
    Output {
        id: u32,
        round: u32,
        match_type: ElimMatchType,
    },
}

#[derive(
    JsonSchema,
    Serialize,
    Deserialize,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Debug,
    FromPrimitive,
    ToPrimitive,
)]
pub enum ElimMatchType {
    WB = 0,
    LB = 1,
    GF1 = 2,
    GF2 = 3,
}

/// Identifier for a contestent somewhere in the bracket
/// Either a specific seed, or a loser of some match
#[derive(Clone, Debug)]
pub enum ElimContestent {
    Seed {
        /**
         * NOTE this is *not* the seed value -- the seed values are used to
         * sort players, and their rank is their index in the final sorted Vec
         * of players
         */
        rank: u32,
    },
    /// The loser of this specific match
    Loser(Arc<RwLock<ElimNode>>),
}

use ElimContestent::{Loser, Seed};
use ElimNode::{Bye, In, Match, Output};

impl ElimNode {
    pub fn new(self) -> Arc<RwLock<Self>> {
        Arc::new(RwLock::new(self))
    }

    pub fn name(&self) -> String {
        self.name_recur(true)
    }

    pub fn name_recur(&self, cont: bool) -> String {
        if !cont {
            match self {
                In(contestent) => format!(
                    "In({})",
                    match &*contestent.read().unwrap() {
                        Seed { rank } => format!("Seed({})", rank),
                        Loser(_lb) => format!("Loser(_)"),
                    }
                ),
                Bye => "Bye".to_string(),
                Match {
                    id,
                    match_type,
                    drop,
                    ..
                } => format!(
                    "Match({}, {:?}{})",
                    id,
                    match_type,
                    if let Some(_d) = drop {
                        format!(" drop to __")
                    } else {
                        "".to_string()
                    }
                ),
                Output {
                    id,
                    round,
                    match_type,
                } => format!("Output({}, {}, {:?})", id, round, match_type),
            }
        } else {
            match self {
                In(contestent) => format!(
                    "In({})",
                    match &*contestent.read().unwrap() {
                        Seed { rank } => format!("Seed({})", rank),
                        Loser(lb) => format!("Loser({})", lb.read().unwrap().name_recur(false)),
                    }
                ),
                Bye => "Bye".to_string(),
                Match {
                    id,
                    match_type,
                    drop,
                    ..
                } => format!(
                    "Match({}, {:?}{})",
                    id,
                    match_type,
                    if let Some(d) = drop {
                        format!(" drop to {}", d.read().unwrap().name_recur(false))
                    } else {
                        "".to_string()
                    }
                ),
                Output {
                    id,
                    round,
                    match_type,
                } => format!("Output({}, {}, {:?})", id, round, match_type),
            }
        }
    }

    pub fn match_id(&self) -> Option<u32> {
        match self {
            Match { id, .. } => Some(*id),
            _ => None,
        }
    }

    pub fn high(&self) -> Option<&Arc<RwLock<ElimNode>>> {
        match self {
            Match { high, .. } => Some(high),
            _ => None,
        }
    }

    pub fn low(&self) -> Option<&Arc<RwLock<ElimNode>>> {
        match self {
            Match { low, .. } => Some(low),
            _ => None,
        }
    }

    pub fn children(&self) -> Option<(&Arc<RwLock<ElimNode>>, &Arc<RwLock<ElimNode>>)> {
        match self {
            Match { high, low, .. } => Some((high, low)),
            _ => None,
        }
    }

    pub fn height(&self) -> usize {
        match self {
            Match { high, low, .. } => {
                let height_high = high.read().unwrap().height();
                let height_low = low.read().unwrap().height();
                1 + max(height_high, height_low)
            }
            _ => 0,
        }
    }

    pub fn is_empty(&self) -> bool {
        match self {
            Bye => true,
            In(input) => match &*input.read().unwrap() {
                Seed { .. } => false,
                Loser(loser) => loser.read().unwrap().is_empty(),
            },
            Match { high, low, .. } => {
                high.read().unwrap().is_empty() && low.read().unwrap().is_empty()
            }
            Output { .. } => false,
        }
    }
}

impl ElimContestent {
    pub fn new(self) -> Arc<RwLock<Self>> {
        Arc::new(RwLock::new(self))
    }
}
