use std::iter::{empty, once, repeat, Iterator};
use itertools::Itertools;

#[derive(Debug, Clone, Copy)]
pub enum LinkFun {
    Reverse,
    Swap,
}

impl LinkFun {
    pub fn swap<T>(mut top: Vec<T>) -> (Vec<T>, Vec<T>) {
        let k = top.len();
        assert!(k.is_power_of_two());
        let bot = top.split_off(k / 2);
        (bot, top)
    }

    pub fn reverse<T>(mut top: Vec<T>) -> (Vec<T>, Vec<T>) {
        let k = top.len();
        assert!(k.is_power_of_two());
        let mut bot = top.split_off(k / 2);
        top.reverse();
        bot.reverse();
        (top, bot)
    }

    pub fn interpret<T>(input: Vec<T>, commands: &[LinkFun]) -> Vec<T> {
        let mut stack: Vec<(usize, Vec<T>)> = Vec::new();
        let mut output: Vec<T> = Vec::with_capacity(input.len());
        // log::info!("input = {:?}, commands = {:?}", input, commands);
        // assert!(commands.len() <= ((input.len() as f32).log2().floor() as usize));
        stack.push((0, input));
        while let Some((i, mut input)) = stack.pop() {
            if i < commands.len() && input.len() > 1 {
                let cmd = commands[i];
                match cmd {
                    LinkFun::Reverse => {
                        let (left, right) = LinkFun::reverse(input);
                        stack.push((i + 1, right));
                        stack.push((i + 1, left));
                    }
                    LinkFun::Swap => {
                        let (left, right) = LinkFun::swap(input);
                        stack.push((i + 1, right));
                        stack.push((i + 1, left));
                    }
                }
            } else {
                output.append(&mut input);
            }
        }
        output
    }

    /// The ith string in the language of strings lexicographically sorted
    /// P={s,r}^(n - log(n))
    pub fn lexi_pattern(num_rounds: u32, i: u32, l: u32) -> Vec<LinkFun> {
        let len = (num_rounds - l) as usize;
        let mut lang = repeat(LinkFun::Swap).take(len).collect_vec();
        let mut bit = 0_usize;
        let mut i = i;
        while bit < len {
            lang[bit] = if i % 2 == 0 {
                LinkFun::Swap
            } else {
                LinkFun::Reverse
            };
            bit = bit + 1;
            i = i / 2;
        }
        lang
    }

    pub fn optimal_pattern(round: u32, num_rounds: u32) -> Vec<LinkFun> {
        let l = ((num_rounds as f32) - (num_rounds as f32).log2()).ceil() as u32;
        if round <= l {
            LinkFun::lexi_pattern(num_rounds, round, l)
        } else {
            let len = num_rounds - round;
            repeat(LinkFun::Swap).take(len as usize).collect()
        }
    }

    pub fn optimal_linking<T>(round: u32, num_rounds: u32, input: Vec<T>) -> Vec<T> {
        let pat = LinkFun::optimal_pattern(round, num_rounds);
        LinkFun::interpret(input, &pat)
    }
}
