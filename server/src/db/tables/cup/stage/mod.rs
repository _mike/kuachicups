mod advance;
mod create;
mod elim_bracket;
mod group_schedule;
mod old_scoring_config;
mod pickban_query;
mod populate_matches;
mod rankings;
mod seeding;
pub mod slaughter_seeding;

pub use advance::*;
pub use create::*;
pub use elim_bracket::*;
pub use group_schedule::*;
pub use old_scoring_config::*;
pub use pickban_query::*;
pub use populate_matches::*;
pub use rankings::*;
pub use seeding::*;
pub use slaughter_seeding::*;

use crate::db::{ById, CupStage, Result};
use sqlx::{query, PgConnection};

impl CupStage {
    /// Retrieve the next stage after this one
    pub async fn next(&self, db: &mut PgConnection) -> Result<Option<CupStage>> {
        // TODO this could be much simpler if postgres enums are supported by sqlx
        let r = query!(
            "SELECT id FROM cup_stage WHERE cup_id = $1 AND stage_no = $2",
            self.cup_id,
            self.stage_no + 1,
        )
        .fetch_optional(&mut *db)
        .await?;
        match r {
            Some(r) => Ok(Some(CupStage::by_id(&mut *db, r.id).await?)),
            None => Ok(None),
        }
    }

    /// Retrieve the previous stage before this one
    pub async fn previous(&self, db: &mut PgConnection) -> Result<Option<CupStage>> {
        if self.stage_no <= 0 {
            return Ok(None);
        }
        let previous_stage = query!(
            "
                SELECT id
                FROM cup_stage
                WHERE stage_no = $1 AND cup_id = $2
            ",
            self.stage_no - 1,
            self.cup_id,
        )
        .fetch_one(&mut *db)
        .await?;
        Ok(Some(CupStage::by_id(&mut *db, previous_stage.id).await?))
    }
}
