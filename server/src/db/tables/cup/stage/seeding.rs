use super::super::model::{
    Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, CupStageFinalRanking, CupStageFormat,
    ElimMatchType, ScoreReport,
};
use crate::db::{
    group_by_id, ById, Error, Executor, HasId, HasParent, JsonRow, JsonToDbResult, Player, Result,
    DB,
};
use itertools::Itertools;
use num_rational::Ratio;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use std::cell::RefCell;
use std::cmp::{max, min, Ordering, Reverse};
use std::collections::{HashMap, HashSet};
use std::rc::Rc;
use uuid::Uuid;

use super::rankings::{Ranking, Rankings};

impl CupStage {
    /// Get the seeding **for** this stage, as in the ranks of the players prior
    /// to creating the bracket/groups/... in this stage
    ///
    /// Requires the previous stage to be finished!
    #[async_recursion::async_recursion]
    pub async fn seeding(&self, db: &mut sqlx::PgConnection) -> Result<Vec<CupSignup>> {
        let signups = if self.stage_no > 0 {
            log::debug!("getting rankings of previous stage to {:#?}", self);
            let previous_stage = self.previous(&mut *db).await?.expect("no previous stage");
            let prev_stage_signups_sorted_by_rank = {
                let all = previous_stage
                    .final_rankings(&mut *db)
                    .await?
                    .ok_or(Error::SomeError("previous stage must be finished"))?
                    .rankings
                    .into_iter()
                    .map(|r| r.signup_id)
                    .collect::<Vec<_>>();

                all.as_slice()[0..self.max_participants.map_or(all.len(), |i| i as usize)].to_vec()
            };
            let ids = self
                .process_signup_changes(prev_stage_signups_sorted_by_rank, &mut *db)
                .await?;
            let ids = match previous_stage.format {
                // rearrange placements by group
                CupStageFormat::Groups if self.format.is_elim() => {
                    log::debug!("rearranging seeds to avoid group match repeats");
                    let signup_to_group = query!(
                        "
                            SELECT DISTINCT ON (signup_id)
                                group_no,
                                unnest(array[high_id, low_id]) signup_id
                            FROM cup_match_2p
                            WHERE group_no IS NOT NULL AND cup_stage_id = $1
                        ",
                        previous_stage.id
                    )
                    .fetch_all(&mut *db)
                    .await?;
                    let signup_to_group = signup_to_group
                        .into_iter()
                        .flat_map(|s| match (s.signup_id, s.group_no) {
                            (Some(id), Some(g)) => Some((id, g)),
                            _ => None,
                        })
                        .collect::<HashMap<Uuid, i32>>();
                    let mut group_ranks: HashMap<i32, Vec<Uuid>> = HashMap::new();
                    let mut leftover: Vec<Uuid> = Vec::new();
                    for id in ids {
                        if let Some(group_no) = signup_to_group.get(&id) {
                            let group_ids = group_ranks.entry(*group_no).or_default();
                            group_ids.push(id);
                        } else {
                            leftover.push(id);
                        }
                    }
                    let group_ranks: Vec<Vec<Uuid>> = group_ranks
                        .into_iter()
                        .sorted_by_key(|k| k.0)
                        .map(|k| k.1)
                        .collect::<Vec<_>>();
                    let mut result = Vec::new();
                    let largest_group_len = group_ranks.iter().map(|m| m.len()).max().unwrap_or(0);
                    for j in 0..largest_group_len {
                        for i in 0..group_ranks.len() {
                            if let Some(id) = group_ranks[i].get(j) {
                                result.push(*id);
                            }
                        }
                    }
                    result.extend_from_slice(&leftover);
                    result
                }
                _ => ids,
            };
            CupSignup::by_ids(&mut *db, &ids).await?
        } else {
            Cup::get_checkedin_signups_seeded(&mut *db, self.cup_id).await?
        };
        // if true {
        //     for (i, s) in signups.iter().enumerate() {
        //         let p = Player::by_id(&mut *db, s.player_id).await?;
        //         log::debug!("seed[{i}] = {}", p.discord_username);
        //     }
        // }
        Ok(signups)
    }

    pub async fn prior_forfeits(&self, db: &mut sqlx::PgConnection) -> Result<HashSet<Uuid>> {
        let forfeits = query!(
            "
                SELECT id
                FROM cup_signup
                WHERE forfeit_stage_no <= $1 AND cup_id = $2
            ",
            self.stage_no,
            self.cup_id,
        )
        .fetch_all(&mut *db)
        .await?;
        Ok(forfeits.into_iter().map(|r| r.id).collect())
    }

    pub async fn added_signups(&self, db: &mut sqlx::PgConnection) -> Result<Vec<Uuid>> {
        let additional_ids = query!(
            "
                SELECT id
                FROM cup_signup
                WHERE add_to_stage_no <= $1 AND cup_id = $2
                ORDER BY seed_value ASC NULLS LAST
            ",
            self.stage_no,
            self.cup_id,
        )
        .fetch_all(&mut *db)
        .await?;
        Ok(additional_ids.into_iter().map(|r| r.id).collect())
    }

    pub async fn process_signup_changes(
        &self,
        seeding: Vec<Uuid>,
        db: &mut sqlx::PgConnection,
    ) -> Result<Vec<Uuid>> {
        let forfeit_ids = self.prior_forfeits(&mut *db).await?;
        let additional_ids = self.added_signups(&mut *db).await?;
        Ok(seeding
            .into_iter()
            .chain(additional_ids.into_iter())
            .filter(|cs| !forfeit_ids.contains(cs))
            .collect::<Vec<_>>())
    }
}
