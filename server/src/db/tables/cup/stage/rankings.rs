use super::super::model::{
    Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, CupStageFinalRanking, CupStageFormat,
    ElimMatchType, ScoreReport,
};
use crate::db::{
    group_by_id, ById, Error, Executor, HasId, HasParent, JsonRow, JsonToDbResult, Result, DB,
};
use crate::handlers::api::cups::cup_signups_self;
use indexmap::Equivalent;
use itertools::Itertools;
use num_rational::Ratio;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use std::cell::RefCell;
use std::cmp::{max, min, Ordering, Reverse};
use std::collections::{BTreeSet, HashMap, HashSet, VecDeque};
use std::rc::Rc;
use uuid::Uuid;

#[derive(Clone, Serialize, Deserialize, JsonSchema)]
pub struct Ranking {
    pub signup_id: Uuid,
    pub placement: i32,
    pub score: i32,
    pub wins: i32,
    pub losses: i32,
    pub map_wins: Option<i32>,
    pub map_losses: Option<i32>,
    pub group_no: Option<i32>,
    pub last_round_alive: Option<i32>,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct Rankings {
    pub stage_id: Uuid,
    pub rankings: Vec<Ranking>,
    pub is_complete: bool,
}

impl CupStage {
    pub async fn final_rankings(&self, db: &mut sqlx::PgConnection) -> Result<Option<Rankings>> {
        let rankings = CupStageFinalRanking::by_parent_id(&mut *db, self.id).await?;
        if rankings.len() > 0 {
            Ok(Some(CupStageFinalRanking::to_rankings(self.id, rankings)))
        } else {
            Ok(None)
        }
    }

    pub async fn rankings(
        &self,
        cutoff: Option<usize>,
        db: &mut sqlx::PgConnection,
    ) -> Result<Rankings> {
        log::debug!("rankings({cutoff:?})");
        let rankings = CupStageFinalRanking::by_parent_id(&mut *db, self.id).await?;
        if rankings.len() > 0 {
            log::info!("using pre-computed rankings for {}", self.id);
            return Ok(CupStageFinalRanking::to_rankings(self.id, rankings));
        }
        log::info!("computing rankings for {}", self.id);
        let matches = query_as!(
            CupMatch2pRow,
            "SELECT * FROM cup_match_2p WHERE cup_stage_id = $1",
            self.id
        )
        .fetch_all(&mut *db)
        .await?
        .iter()
        .map(|m| CupMatch2p::from_row(m))
        .collect::<Vec<_>>();
        let is_complete = matches.iter().all(|m| m.is_scored);
        let signups = self.seeding(&mut *db).await?;
        let signups_by_id = group_by_id(&signups);
        let rankings = match self.format {
            CupStageFormat::Groups => {
                let scoring = CupStage::scoring_group(self.id, &mut *db).await?;
                group_rankings(
                    &matches,
                    &signups_by_id,
                    cutoff,
                    (scoring.bestof / 2) + (scoring.bestof % 2),
                )
            }
            CupStageFormat::SingleElim => elim_rankings(&matches, &signups_by_id, cutoff),
            CupStageFormat::DoubleElim => elim_rankings(&matches, &signups_by_id, cutoff),
            _ => vec![],
        };
        Ok(Rankings {
            stage_id: self.id,
            is_complete,
            rankings,
        })
    }
}

#[derive(Clone, Copy, Default, Debug)]
struct WinLoss {
    pub wins: i32,
    pub losses: i32,
    pub map_wins: i32,
    pub map_losses: i32,
}

pub fn map_winloss_for(report: &Vec<ScoreReport>, am_high: bool) -> (i32, i32) {
    let mut wins = 0_i32;
    let mut losses = 0_i32;
    for r in report.iter() {
        if am_high && r.high > r.low || !am_high && r.low > r.high {
            wins += 1;
        }
        if am_high && r.low > r.high || !am_high && r.high > r.low {
            losses += 1;
        };
    }
    (wins, losses)
}

pub fn map_winloss_for_maybe(report: &Option<Vec<ScoreReport>>, am_high: bool) -> (i32, i32) {
    match report {
        Some(r) => map_winloss_for(&r, am_high),
        None => (0, 0),
    }
}

fn group_rankings(
    matches: &Vec<CupMatch2p>,
    signups_by_id: &HashMap<Uuid, &CupSignup>,
    cutoff: Option<usize>,
    bye_map_win_value: i32,
) -> Vec<Ranking> {
    let groups = matches
        .into_iter()
        .sorted_by_key(|m| m.group_no)
        .group_by(|c| {
            c.group_no
                .expect("no group_no even though using group_rankings!")
        })
        .into_iter()
        .map(|(k, g)| (k, g.collect_vec()))
        .collect_vec();

    let matchup_winner_ids = Rc::new(RefCell::new(HashMap::<(Uuid, Uuid), Uuid>::new()));

    fn winloss_sort_key(wl: &WinLoss) -> (Reverse<i32>, Reverse<i32>, Reverse<i32>, i32) {
        let total_maps = wl.map_losses + wl.map_wins;
        (
            Reverse(wl.wins),
            Reverse(
                ((if total_maps > 0 {
                    wl.map_wins as f32 / total_maps as f32
                } else {
                    0.0
                }) * 1e6)
                    .round() as i32,
            ),
            Reverse(wl.map_wins),
            wl.map_losses,
        )
    }

    let placements_by_wins = groups
        .into_iter()
        .map(|(group_no, group)| {
            //
            // let group_size = group.len(); NOTE group sizes now all equalized
            // with explicit byes.
            //
            // signup id -> number of wins (starting at zero)
            //
            let mut signup_winloss = group
                .iter()
                .map(|m| m.low_id)
                .chain(group.iter().map(|m| m.high_id))
                .filter_map(|s| s)
                .map(|s| (s, WinLoss::default()))
                .collect::<HashMap<Uuid, WinLoss>>();
            for m in group.iter() {
                if m.is_scored && m.high_id.is_some() && m.low_id.is_some() {
                    // if either high_id or low_id aren't there but the match is scored...
                    {
                        let mut wlh = signup_winloss.entry(m.high_id.unwrap()).or_default();
                        let (mw, ml) = map_winloss_for_maybe(&m.low_report, true);
                        wlh.map_wins += mw;
                        wlh.map_losses += ml;
                    };
                    {
                        let mut wll = signup_winloss.entry(m.low_id.unwrap()).or_default();
                        let (mw, ml) = map_winloss_for_maybe(&m.low_report, false);
                        wll.map_wins += mw;
                        wll.map_losses += ml;
                    };
                    // store matchup winners in both orders (high, low)->winner and (low,high)->winner
                    {
                        let mup1 = (m.high_id.unwrap(), m.low_id.unwrap());
                        let mup2 = (m.low_id.unwrap(), m.high_id.unwrap());
                        let mut matchup_winner_ids = matchup_winner_ids.borrow_mut();
                        (matchup_winner_ids).insert(mup1, m.winner_id.unwrap());
                        (matchup_winner_ids).insert(mup2, m.winner_id.unwrap());
                    };
                } else if m.is_scored {
                    // if either high_id or low_id aren't there but the match is scored...
                    if let Some(winner_id) = m.winner_id {
                        let mut wlh = signup_winloss.entry(winner_id).or_default();
                        wlh.map_wins += bye_map_win_value;
                    }
                }

                if let Some(signup_id) = m.winner_id {
                    let mut wl = signup_winloss.entry(signup_id).or_default();
                    wl.wins += 1;
                }
                if let Some(signup_id) = m.loser_id {
                    let mut wl = signup_winloss.entry(signup_id).or_default();
                    wl.losses += 1;
                }
            }
            signup_winloss.into_iter().map(move |(signup_id, wl)| {
                let signup = signups_by_id.get(&signup_id);
                signup.map(|s| (s, wl, group_no))
            })
        })
        .flatten()
        .flatten()
        .sorted_by(|a, b| {
            let sort_key_a = winloss_sort_key(&a.1);
            let sort_key_b = winloss_sort_key(&b.1);
            match sort_key_a.cmp(&sort_key_b) {
                Ordering::Equal => {
                    // find their matchup if it exists, use that for ordering
                    // otherwise use their group number, seed
                    let matchup_winner_ids = matchup_winner_ids.borrow();
                    let winner_id = matchup_winner_ids.get(&(a.0.id, b.0.id));
                    if winner_id == Some(&a.0.id) {
                        Ordering::Less
                    } else if winner_id == Some(&b.0.id) {
                        Ordering::Greater
                    } else {
                        (
                            a.2, // group number
                            a.0.seed_value,
                        )
                            .cmp(&(
                                b.2,
                                b.0.seed_value, // seed)
                            ))
                    }
                }
                ltgt => ltgt,
            }
        })
        .group_by(|(_signup, wl, _group_no)| winloss_sort_key(wl));

    #[derive(Debug)]
    struct Placement<'s> {
        placement: usize,
        wins: i32,
        losses: i32,
        map_wins: i32,
        map_losses: i32,
        group_no: i32,
        signup: &'s CupSignup,
    }

    let mut cutoff_placements: Vec<Placement> = vec![];
    for (placement, (_wins, plg)) in placements_by_wins.into_iter().enumerate() {
        if let Some(cutoff) = cutoff {
            if cutoff_placements.len() >= cutoff {
                break;
            }
        }
        for (signup, wl, group_no) in plg.into_iter() {
            cutoff_placements.push(Placement {
                placement,
                signup,
                wins: wl.wins,
                losses: wl.losses,
                map_wins: wl.map_wins,
                map_losses: wl.map_losses,
                group_no,
            });
        }
    }

    cutoff_placements
        .into_iter()
        .map(|p| Ranking {
            signup_id: p.signup.id,
            placement: p.placement as i32,
            score: p.wins,
            wins: p.wins,
            losses: p.losses,
            map_wins: Some(p.map_wins),
            map_losses: Some(p.map_losses),
            last_round_alive: None,
            group_no: Some(p.group_no),
        })
        .collect_vec()
}

#[derive(Clone, Default, Debug)]
struct WinLossRound {
    pub wins: i32,
    pub losses: i32,
    pub map_wins: i32,
    pub map_losses: i32,
    pub depth: i32,
    pub lb: bool,
    pub history: Vec<(ElimMatchType, bool)>,
}

impl WinLossRound {
    pub fn sort_key(&self) -> impl Ord + Eq {
        (
            self.depth,
            Reverse(self.wins),
            self.losses,
            Reverse(self.map_wins),
            self.map_losses,
        )
    }

    pub fn compare_history(a: &Self, b: &Self) -> Ordering {
        for i in (0..max(a.history.len(), b.history.len())).rev() {
            let a_depth = a.history.get(i);
            let b_depth = b.history.get(i);
            match (a_depth, b_depth) {
                (Some((_, true)), None | Some((_, false))) => return Ordering::Less,
                (Some((_, false)) | None, Some((_, true))) => return Ordering::Greater,
                // (Some(a_depth), Some(b_depth)) if a_depth != b_depth => {
                //     return a_depth.cmp(b_depth)
                // }
                // (Some(_), None) => return Ordering::Less,
                // (None, Some(_)) => return Ordering::Greater,
                _ => {}
            }
        }
        Ordering::Equal
    }
}

fn elim_rankings(
    matches: &Vec<CupMatch2p>,
    signups_by_id: &HashMap<Uuid, &CupSignup>,
    cutoff: Option<usize>,
) -> Vec<Ranking> {
    let mut signup_to_wlr: HashMap<Uuid, WinLossRound> = HashMap::new();

    let mut matches: Vec<&CupMatch2p> = matches.iter().collect_vec();
    matches.sort_by_key(|m| {
        (
            Reverse(m.elim_type),
            Reverse(m.elim_round),
            m.elim_index,
            m.lid,
        )
    });

    let mut matches_by_id: HashMap<i32, &CupMatch2p> = HashMap::new();
    let mut links: HashMap<i32, BTreeSet<i32>> = HashMap::new();
    let mut gf: Option<&CupMatch2p> = None;
    for m in matches {
        matches_by_id.insert(m.id, m);
        let mut parent_ids = [m.elim_low_match_id, m.elim_high_match_id]
            .into_iter()
            .flatten()
            .cloned()
            .collect::<BTreeSet<_>>();
        let child_ids = [m.elim_winner_match_id, m.elim_winner_match_id]
            .into_iter()
            .flatten()
            .cloned()
            .collect::<Vec<_>>();
        if child_ids.len() == 0 {
            gf = Some(m)
        }
        links.insert(m.id, parent_ids);
    }

    log::debug!("grand final = {:#?}", gf);

    // breadth first search down from GF downward

    let mut queue: VecDeque<(i32, &CupMatch2p)> = VecDeque::new();
    let mut max_depth: i32 = 0;
    if let Some(m) = gf {
        queue.push_back((0, m));
    }

    let mut visited: HashMap<i32, bool> = HashMap::new();

    while let Some((depth, m)) = queue.pop_front() {
        log::debug!("match {} at depth {}", m.id, depth);
        max_depth = max(depth, max_depth);
        visited.insert(m.id, true);
        let parent_ids = links.get(&m.id).unwrap();
        for id in parent_ids {
            let m = matches_by_id.get(id).unwrap();
            if !visited.contains_key(&m.id) {
                if depth == 0 {
                    log::debug!("going from depth=0");
                }
                queue.push_back((depth + 1, m));
            }
        }
        for signup_id in [m.high_id, m.low_id].into_iter().flatten() {
            let signup_id = *signup_id;
            let already_present = signup_to_wlr.contains_key(&signup_id);
            let mut ent = signup_to_wlr.entry(signup_id).or_default();
            let is_winner = m.winner_id == Some(signup_id);
            let is_loser = m.loser_id == Some(signup_id);
            let am_high = m.high_id == Some(signup_id);
            if m.is_scored {
                let (map_wins, map_losses) = map_winloss_for_maybe(&m.low_report, am_high);
                ent.map_wins += map_wins;
                ent.map_losses += map_losses;
            }

            if is_winner {
                ent.wins += 1;
            }

            if is_loser {
                ent.losses += 1;
            }

            ent.history.push((m.elim_type.unwrap(), is_winner));

            log::debug!(
                "ent.depth: {:?} :: {}=>{}",
                already_present,
                ent.depth,
                depth
            );

            let lb = m.elim_type == Some(ElimMatchType::LB);
            if !already_present {
                if m.elim_type == Some(ElimMatchType::GF2) && is_winner {
                    ent.depth = depth - 1;
                } else {
                    ent.depth = depth;
                }
            }
            if !ent.lb && lb && is_loser {
                ent.depth = depth;
                ent.lb = true;
            }
        }
    }

    for (_id, wlr) in signup_to_wlr.iter_mut() {
        wlr.history.reverse();
    }

    let mut rankings: Vec<Ranking> = Vec::new();

    log::debug!("{:#?}", signup_to_wlr);

    for (placement, (_group_round, placement_group)) in signup_to_wlr
        .into_iter()
        .sorted_by_key(|a| (a.1.depth, a.0))
        .group_by(|a| a.1.depth)
        // .sorted_by(|a, b| {
        //     let hist_cmp = WinLossRound::compare_history(&a.1, &b.1);
        //     if hist_cmp == Ordering::Equal {
        //         // sort by sort_key first then by the signup_id last --
        //         // signup_id just to ensure deterministic result. it shouldn't
        //         // affect the grouping
        //         (a.0).cmp(&b.0)
        //     } else {
        //         hist_cmp
        //     }
        // })
        // .group_by(|r| (r.1.history.clone()))
        .into_iter()
        .enumerate()
    {
        if let Some(cutoff) = cutoff {
            if rankings.len() >= cutoff {
                break;
            }
        }
        for (signup_id, wlr) in placement_group {
            let last_round_alive = max_depth - wlr.depth;
            rankings.push(Ranking {
                signup_id,
                group_no: None,
                placement: placement as i32,
                score: last_round_alive,
                wins: wlr.wins,
                losses: wlr.losses,
                map_wins: Some(wlr.map_wins),
                map_losses: Some(wlr.map_losses),
                last_round_alive: Some(last_round_alive),
            });
        }
    }

    rankings
}

impl CupStageFinalRanking {
    pub fn to_rankings(stage_id: Uuid, vec: Vec<CupStageFinalRanking>) -> Rankings {
        Rankings {
            is_complete: true,
            stage_id,
            rankings: vec
                .into_iter()
                .map(|r| Ranking {
                    group_no: r.group_no,
                    losses: r.losses,
                    placement: r.placement,
                    score: r.score,
                    signup_id: r.signup_id,
                    wins: r.wins,
                    map_wins: r.map_wins,
                    map_losses: r.map_losses,
                    last_round_alive: None,
                })
                .collect(),
        }
    }
}

// NOTE: cutoffs
//
// This allows more signups than exactly 'cutoff' specifies. This is so that
// when there are ties within the top N placements, the whole placement gets in
// rather than just the first 'cutoff' ones.
//
