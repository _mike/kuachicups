use super::super::model::{CupStage, CupStageFormat, CupStageScoring};
use crate::db::{Executor, Result};
use chrono::{DateTime, Utc};
use serde_json::{from_value, to_value};
use slug::slugify;
use sqlx::{query, Acquire, PgConnection, Postgres, Transaction};
use uuid::Uuid;

impl CupStage {
    // queries via jsonb so that the postgres enum works
    pub async fn create(
        cup_id: Uuid,
        title: &str,
        start_immediately: bool,
        checkintime: Option<DateTime<Utc>>,
        starttime: Option<DateTime<Utc>>,
        stage_no: i32,
        format: CupStageFormat,
        max_participants: Option<i32>,
        group_size: Option<i32>,
        group_rematches: Option<i32>,
        db: impl Executor<'_>,
    ) -> Result<CupStage> {
        let result = query!(
            r#"
            INSERT INTO cup_stage(
                cup_id,
                title,
                slug,
                start_immediately,
                checkintime,
                starttime,
                stage_no,
                format,
                max_participants,
                group_size,
                group_rematches
            )
            VALUES(
                $1,
                $2,
                $3,
                $4,
                $5,
                $6,
                $7,
                (($8::jsonb)->>0)::cup_stage_format,
                $9,
                $10,
                $11)
            RETURNING to_jsonb(cup_stage.*) AS value
            "#,
            cup_id,
            title,
            slugify(title),
            start_immediately,
            checkintime,
            starttime,
            stage_no,
            to_value(&format).expect("cup stage format to_value"),
            max_participants,
            group_size,
            group_rematches,
        )
        .fetch_one(db)
        .await?;
        Ok(from_value(result.value.expect("invalid jsonb")).expect("invalid cup stage"))
    }

    pub async fn delete(id: Uuid, db: &mut PgConnection) -> Result<bool> {
        let info = query!("SELECT cup_id, stage_no FROM cup_stage WHERE id = $1", id)
            .fetch_one(&mut *db)
            .await?;
        let cup_id = info.cup_id;
        let stage_no = info.stage_no;

        let mut tx = db.begin().await?;

        let r = query!("DELETE FROM cup_stage WHERE id = $1", id)
            .execute(&mut tx)
            .await?;

        query!(
            "UPDATE cup_stage SET stage_no = stage_no - 1 WHERE cup_id = $1 AND stage_no > $2",
            cup_id,
            stage_no
        )
        .execute(&mut tx)
        .await?;

        let last_stage = query!(
            "SELECT stage_no FROM cup_stage WHERE cup_id = $1 ORDER BY stage_no DESC LIMIT 1",
            cup_id
        )
        .fetch_optional(&mut tx)
        .await?;

        if let Some(last_stage) = last_stage {
            query!(
                "UPDATE cup SET current_stage = $3 WHERE current_stage >= $1 AND id = $2",
                stage_no,
                id,
                last_stage.stage_no
            )
            .execute(&mut tx)
            .await?;
        }

        tx.commit().await?;

        Ok(r.rows_affected() == 1)
    }

    pub async fn update(
        id: Uuid,
        title: Option<String>,
        cup_id: Option<Uuid>,
        checkintime: Option<DateTime<Utc>>,
        starttime: Option<DateTime<Utc>>,
        stage_no: Option<i32>,
        format: Option<CupStageFormat>,
        max_participants: Option<i32>,
        group_size: Option<i32>,
        group_rematches: Option<i32>,
        db: &mut PgConnection,
    ) -> Result<CupStage> {
        let result = query!(
            r#"
            UPDATE cup_stage SET
              cup_id = coalesce($1, cup_id),
              title = coalesce($2, title),
              slug = coalesce($3, slug),
              checkintime = coalesce($4, checkintime),
              starttime = coalesce($5, starttime),
              stage_no = coalesce($6, stage_no),
              format = coalesce((($7::jsonb)->>0)::cup_stage_format, format),
              max_participants = $8,
              group_size = $9,
              group_rematches = $10,
              updated_at = now()
            WHERE id = $11
            RETURNING to_jsonb(cup_stage.*) AS value
            "#,
            cup_id,
            title.clone(),
            title.map(slugify),
            checkintime,
            starttime,
            stage_no,
            to_value(&format).expect("cup stage format to_value"),
            max_participants,
            group_size,
            group_rematches,
            id,
        )
        .fetch_one(db)
        .await?;
        Ok(from_value(result.value.expect("invalid jsonb")).expect("invalid cup stage"))
    }
}
