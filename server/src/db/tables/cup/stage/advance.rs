use super::super::model::{
    Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, CupStageFormat, ElimMatchType,
};
use crate::db::{ById, Error, Executor, JsonRow, JsonToDbResult, Rankings, Result, DB};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, PgConnection, Postgres, Transaction};
use std::cmp::max;
use std::collections::HashMap;
use uuid::Uuid;

impl CupStage {
    pub async fn current(cup_id: Uuid, db: &mut PgConnection) -> Result<Option<CupStage>> {
        let stage_id = Cup::current_stage(cup_id, &mut *db).await?;
        match stage_id {
            Some(stage_id) => Ok(Some(CupStage::by_id(&mut *db, stage_id).await?)),
            None => Ok(None),
        }
    }

    pub async fn is_complete(stage_id: Uuid, db: &mut PgConnection) -> Result<bool> {
        let all_is_scored = query!(
            "
                SELECT bool_and(m.is_scored) AS val
                FROM cup_match_2p m
                WHERE m.cup_stage_id = $1
            ",
            stage_id
        )
        .fetch_one(&mut *db)
        .await?;
        Ok(all_is_scored.val == Some(true))
    }

    pub async fn advance(cup_stage_id: Uuid, db: &mut PgConnection) -> Result<()> {
        let stage_is_complete = CupStage::is_complete(cup_stage_id, &mut *db).await?;
        log::debug!(
            "CupStage::advance {}, stage_is_complete = {}",
            cup_stage_id,
            stage_is_complete
        );
        if stage_is_complete {
            // add final rankings for this stage
            let Rankings {
                is_complete,
                rankings,
                ..
            } = CupStage::by_id(&mut *db, cup_stage_id)
                .await?
                .rankings(None, &mut *db)
                .await?;
            if !is_complete {
                return Err(Error::SomeError("is_complete state mismatch"));
            }
            let mut tx = db.begin().await?;
            for (index, rank) in rankings.into_iter().enumerate() {
                query!(
                    "
                        INSERT INTO cup_stage_final_ranking(
                            cup_stage_id, signup_id, index, placement,
                            score, wins, losses, group_no, map_wins, map_losses)
                        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
                    ",
                    cup_stage_id,
                    rank.signup_id,
                    index as i32,
                    rank.placement,
                    rank.score,
                    rank.wins,
                    rank.losses,
                    rank.group_no,
                    rank.map_wins,
                    rank.map_losses,
                )
                .execute(&mut *tx)
                .await?;
            }
            let stage = CupStage::by_id(&mut *tx, cup_stage_id).await?;
            let next = stage.next(&mut *tx).await?;
            if let Some(next) = next {
                log::info!("got next stage = {:?}", next);
                let ranks = stage
                    .rankings(next.max_participants.map(|m| m as usize), &mut *tx)
                    .await?;
                if ranks.is_complete != stage_is_complete {
                    log::error!("mismatched is_complete state!");
                    return Err(Error::SomeError("mismatched is_complete state"));
                }
                if next.can_start() {
                    CupStage::start_stage(next.id, &mut *tx).await?;
                } else {
                    log::error!("cannot start the next stage");
                    return Err(Error::SomeError("cannot start the next stage"));
                }
            } else {
                log::info!("no next stage available");
                if query!("SELECT is_finished FROM cup WHERE id = $1", stage.cup_id)
                    .fetch_one(&mut *tx)
                    .await?
                    .is_finished
                {
                    return Err(Error::SomeError("already completed that cup"));
                }
                let mut tx1 = tx.begin().await?;
                Cup::add_cup_results_for_complete_cup(&stage, &mut *tx1).await?;
                query!(
                    "
                        UPDATE cup
                        SET current_stage = null, updated_at = now()
                        WHERE id = $1
                    ",
                    stage.cup_id
                )
                .execute(&mut *tx1)
                .await?;
                tx1.commit().await?;
            }
            tx.commit().await?;
        }
        Ok(())
    }
}
