use super::super::model::{
    CupMatch2p, CupStage, CupStageScoring, ElimMatchType, PickbanPool, PickbanPoolItem,
    PickbanStep, PickbanStepKind, Pickbans, PoolFilter,
};
use super::old_scoring_config;
use crate::db::{ById, Error, Executor, JsonRow, JsonToDbResult, Result, DB};
use itertools::{any, Itertools};
use num_rational::Ratio;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, PgConnection, Postgres, Transaction};
use std::cmp::{max, min, Reverse};
use std::collections::{HashMap, HashSet};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PickbanName {
    id: i32,
    name: String,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, JsonSchema)]
pub struct ElimQuery {
    pub round: i32,
    pub lower_bracket: bool,
}

impl Pickbans {
    pub async fn all(db: &mut PgConnection) -> Result<Vec<Pickbans>> {
        query_as!(JsonRow, "SELECT to_jsonb((p.*)) AS obj FROM pickbans p")
            .fetch_all(db)
            .await?
            .from_json()
    }

    pub async fn query(
        pickbans: Option<i32>,
        bestof: i32,
        db: &mut PgConnection,
    ) -> Result<Pickbans> {
        let pickbans = match pickbans {
            Some(pid) => query_as!(
                JsonRow,
                "SELECT to_jsonb((p.*)) AS obj FROM pickbans p WHERE p.id = $1",
                pid
            )
            .fetch_one(&mut *db)
            .await?
            .from_json()?,

            //
            // fall back to using the old scoring config
            //
            // we're still using the cup_stage_scoring object, but we're
            // querying for a Generic BO[X] row now
            //
            None => query_as!(
                JsonRow,
                "
                    SELECT to_jsonb((p.*)) AS obj
                    FROM pickbans p
                    WHERE game_mode IS NULL AND bestof = $1
                ",
                bestof
            )
            .fetch_one(&mut *db)
            .await?
            .from_json()?,
        };

        Ok(pickbans)
    }

    pub async fn names(db: &mut PgConnection) -> Result<Vec<PickbanName>> {
        let ps = query_as!(PickbanName, "SELECT p.id, p.name FROM pickbans p")
            .fetch_all(db)
            .await?;
        Ok(ps)
    }

    pub fn filter_items(&self, filter: &Vec<Vec<bool>>) -> Option<Vec<PickbanPool>> {
        if any(filter, |pf| any(pf, |pfi| *pfi)) {
            Some(
                self.pool
                    .iter()
                    .zip(filter)
                    .map(|(pp, pf)| PickbanPool {
                        name: pp.name.clone(),
                        slug: pp.slug.clone(),
                        // take only items that are still true in the truth 'matrix' / filter param
                        items: pp
                            .items
                            .iter()
                            .zip(pf)
                            .filter(|(_pi, test)| **test)
                            .map(|p| p.0)
                            .cloned()
                            .collect_vec(),
                    })
                    .collect_vec(),
            )
        } else {
            None
        }
    }

    pub fn default_pool_filter() -> PoolFilter {
        HashMap::new()
    }
}
