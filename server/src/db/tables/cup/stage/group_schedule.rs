/// Circle method for round-robin / groups scheduling
///
/// https://en.wikipedia.org/wiki/Round-robin_tournament#Scheduling_algorithm
/// adapted from https://gist.github.com/ih84ds/be485a92f334c293ce4f1c84bfba54c9
/// minus flipping the "home"/"away" players
pub fn group_schedule(signups: usize, rematches: usize) -> Vec<Vec<(usize, usize, usize)>> {
    let mut rounds = Vec::new();
    let n = signups + signups % 2;
    let mut map = (0..n).collect::<Vec<_>>();
    let mid = n / 2;
    for _i in 0..n - 1 {
        let mut round: Vec<(usize, usize, usize)> = Vec::new();
        let up = (&map[..mid]).to_vec();
        let down = {
            let mut v = (&map[mid..]).to_vec();
            v.reverse();
            v
        };
        for j in 0..mid {
            let s1 = up[j];
            let s2 = down[j];
            for r in 0..=rematches {
                round.push((s1, s2, r));
            }
        }
        rounds.push(round);
        // map = map[mid:-1] + map[:mid] + map[-1:]
        map = [&map[mid..n - 1], &map[..mid], &[map[n - 1]]].concat();
    }
    rounds
}
