use super::super::model::{
    Cup, CupMatch2p, CupMatch2pRow, CupSignup, CupStage, CupStageFormat, ElimMatchType,
};
use super::elim_bracket::{ElimBracket, ElimContestent, ElimNode};
use super::group_schedule::group_schedule;
use crate::db::{group_by_id, ById, Error, Executor, JsonRow, JsonToDbResult, Result, DB};
use comprehension::iter;
use sqlx::{query, query_as, Acquire, PgConnection, Postgres, Transaction};
use std::borrow::Borrow;
use std::cell::RefCell;
use std::cmp::max;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::iter::{once, repeat};
use std::ops::Range;
use std::sync::{Arc, RwLock};
use uuid::Uuid;

impl CupStage {
    pub fn can_start(&self) -> bool {
        use chrono::offset::Utc;
        let now = Utc::now();
        self.start_immediately || Some(now) >= self.starttime
    }

    pub async fn ideal_participant_div_slice(
        &self,
        db: &mut PgConnection,
    ) -> Result<Option<Range<i64>>> {
        let prev_div_mparts: Option<i64> = query!(
            "
                SELECT SUM(max_participants) mparts
                FROM cup_stage
                WHERE cup_id = $1 AND stage_no = $2 AND division < $3
            ",
            self.cup_id,
            self.stage_no,
            self.division
        )
        .fetch_optional(&mut *db)
        .await?
        .map_or(Some(0), |r| r.mparts);
        match (prev_div_mparts, self.max_participants) {
            (Some(mparts), Some(max_parts)) => Ok(Some(mparts..((max_parts as i64) + mparts))),
            _ => Ok(None),
        }
    }

    pub async fn start_stage(stage_id: Uuid, db: &mut PgConnection) -> Result<bool> {
        let q = query!(
            "
                SELECT cup_id, is_started
                FROM cup_stage
                WHERE id = $1
            ",
            stage_id,
        )
        .fetch_one(&mut *db)
        .await?;
        if q.is_started {
            Err(Error::SomeError("stage is already started"))?;
        }
        let cup_id = q.cup_id;
        let stage: CupStage = CupStage::by_id(&mut *db, stage_id).await?;
        if !stage.can_start() {
            Err(Error::SomeError("cannot start this stage yet"))?;
        }
        let next_signups = stage.seeding(&mut *db.acquire().await?).await?;
        log::info!("beginning {:?}", stage.format);
        let mut tx = db.begin().await?;
        match stage.format {
            CupStageFormat::Groups => stage.populate_groups_2p(next_signups, &mut *tx).await?,
            CupStageFormat::SingleElim => stage.populate_elim_2p(next_signups, &mut *tx).await?,
            CupStageFormat::DoubleElim => stage.populate_elim_2p(next_signups, &mut *tx).await?,
            _ => {}
        };
        query!(
            "UPDATE cup_stage SET is_started = true WHERE id = $1",
            stage_id
        )
        .execute(&mut *tx)
        .await?;
        CupStage::run_byes(stage_id, &mut *tx).await?;
        query!(
            "UPDATE cup SET current_stage = $1, updated_at = now() WHERE id = $2",
            stage.stage_no,
            cup_id
        )
        .execute(&mut *tx)
        .await?;
        tx.commit().await?;
        Ok(true)
    }

    pub async fn run_byes(stage_id: Uuid, db: &mut PgConnection) -> Result<()> {
        let match_ids_by_round = query!(
            "
            SELECT array_agg(id) AS ids
            FROM cup_match_2p
            WHERE cup_stage_id = $1
            GROUP BY elim_round
            ORDER BY elim_round ",
            stage_id
        )
        .fetch_all(&mut *db)
        .await?;
        let mut transaction = db.begin().await?;
        for match_ids in match_ids_by_round {
            for match_id in match_ids.ids.iter().flatten() {
                CupMatch2p::set_winner(transaction.begin().await?, *match_id).await?;
            }
        }
        transaction.commit().await?;
        Ok(())
    }

    pub async fn populate_elim_2p(
        &self,
        signups: Vec<CupSignup>,
        db: &mut PgConnection,
    ) -> Result<()> {
        log::info!("populating elim with {:#?}", signups);
        /*
         * In general the brackets don't have a very rigid structure due to
         * simplifying and byes, so we can't rely on easily being able to count
         * the indices of each match in a round across different branches of the
         * tree. Instead we just assign an index per round that gets incremented
         * whenever we make a match insert
         */
        let mut lengths: Vec<i32> = vec![];
        let mut get_next_index = |depth: i32| -> i32 {
            if let Some(ix) = lengths.get_mut(depth as usize) {
                let here = *ix;
                *ix += 1;
                log::debug!("lengths = {:?}", lengths);
                here
            } else {
                for _ in lengths.len()..=(depth as usize) {
                    lengths.push(0);
                }
                // increment our specific index
                let ix = lengths.get_mut(depth as usize).expect("wat");
                let here = *ix;
                *ix += 1;
                here
            }
        };

        #[derive(Copy, Clone, Debug)]
        enum Cmd {
            InsMatch {
                lid: u32,
                round: i32,
                logical_round: i32,
                index: i32,
                position: Option<Position>,
                match_type: ElimMatchType,
                parent_lid: Option<u32>,
            },
            AddBye {
                lid: u32,
                position: Position,
            },
            AddSeed {
                lid: u32,
                position: Position,
                rank: u32,
            },
            DropLoser {
                origin_lid: u32,
                dest_lid: u32,
                dest_position: Position,
            },
        }

        /*
         * Why all this song and dance creating a little interpreter? well,
         * mainly because I can't figure out how to make bracket.rs stuff Send
         * i.e. how to interleave sqlx 'await's in the bracket creation.
         */

        let cmds = {
            // these are just here for ordering the commands
            let mut cmds0 = Vec::new();
            let mut cmds1 = Vec::new();
            let mut cmds2 = Vec::new();

            let bracket = match self.format {
                CupStageFormat::SingleElim => Ok(ElimBracket::single_elim(signups.len() as u32)),
                CupStageFormat::DoubleElim => Ok(ElimBracket::double_elim(signups.len() as u32)),
                _ => Err(Error::SomeError("invalid format for populate_elim_2p")),
            }?;

            bracket.pprint();

            let rounds = bracket.height();

            #[derive(Clone)]
            struct N {
                index: i32,
                depth: i32,
                node: Arc<RwLock<ElimNode>>,
                parent_lid: Option<u32>,
                is_high: Option<bool>,
            }

            let mut queue = vec![N {
                index: 0,
                depth: 0,
                node: bracket.root,
                parent_lid: None,
                is_high: None,
            }];

            while let Some(n) = queue.pop() {
                let N {
                    index,
                    depth,
                    node,
                    parent_lid,
                    is_high,
                } = n;
                let round = (rounds as i32) - depth - 1;
                let position = is_high;
                match &*node.read().unwrap() {
                    ElimNode::Match {
                        id,
                        high,
                        low,
                        match_type,
                        round: logical_round,
                        ..
                    } => {
                        let lid = *id;
                        let index_2 = get_next_index(depth);
                        let index_1 = get_next_index(depth);
                        cmds0.push(Cmd::InsMatch {
                            lid,
                            round,
                            logical_round: (*logical_round) as i32,
                            index,
                            match_type: *match_type,
                            parent_lid,
                            position,
                        });
                        queue.push(N {
                            index: index_1,
                            depth: depth + 1,
                            node: low.clone(),
                            parent_lid: Some(lid),
                            is_high: Some(false),
                        });
                        queue.push(N {
                            index: index_2,
                            depth: depth + 1,
                            node: high.clone(),
                            parent_lid: Some(lid),
                            is_high: Some(true),
                        });
                    }

                    ElimNode::Bye => cmds1.push(Cmd::AddBye {
                        lid: parent_lid.expect("no parent"),
                        position: position.expect("no position"),
                    }),

                    ElimNode::In(contestent) => match &*contestent.read().unwrap() {
                        ElimContestent::Loser(loser) => cmds1.push(Cmd::DropLoser {
                            origin_lid: loser
                                .read()
                                .unwrap()
                                .match_id()
                                .expect("no match dropped from"),
                            dest_lid: parent_lid.expect("no parent"),
                            dest_position: position.expect("no position"),
                        }),
                        // do at the very end after byes etc so that the code to
                        // advance players after byes works
                        ElimContestent::Seed { rank } => cmds2.push(Cmd::AddSeed {
                            lid: parent_lid.expect("no parent"),
                            position: position.expect("no position"),
                            rank: *rank,
                        }),
                    },

                    ElimNode::Output { .. } => {
                        unimplemented!("Truncated elimination stages not yet supported")
                    }
                };
            }

            [cmds0, cmds1, cmds2].concat()
        };

        log::info!("populating elim by {:#?}", cmds);

        let mut transaction: Transaction<Postgres> = db.begin().await?;

        for cmd in cmds {
            match cmd {
                Cmd::InsMatch {
                    lid,
                    round,
                    logical_round,
                    index,
                    match_type,
                    position,
                    parent_lid,
                } => {
                    let scoring_id = CupStage::scoring_elim(
                        self.id,
                        round,
                        match_type == ElimMatchType::LB,
                        &mut *transaction, // TODO see if this must use outer scope?
                    )
                    .await?
                    .id;
                    let new_match = query!(
                        "
                            INSERT INTO cup_match_2p(
                                cup_stage_id,
                                lid,
                                elim_round,
                                elim_logical_round,
                                elim_index,
                                elim_type,
                                scoring_id,
                                elim_winner_match_id,
                                elim_winner_to_high
                            )
                            SELECT
                                $1,
                                $2,
                                $3,
                                $4,
                                $5,
                                $6,
                                $7,
                                (SELECT m.id FROM cup_match_2p m WHERE m.cup_stage_id = $1 AND m.lid = $8),
                                $9
                            RETURNING id
                        ",
                        self.id,
                        lid as i32,
                        round,
                        logical_round,
                        index,
                        match_type as i16,
                        scoring_id,
                        parent_lid.map(|plid| plid as i32),
                        position.map(|pos| pos == POSITION_HIGH),
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    if let Some(parent_lid) = parent_lid {
                        query!(
                            "
                                UPDATE cup_match_2p
                                SET
                                    elim_high_match_id = coalesce($1, elim_high_match_id),
                                    elim_low_match_id = coalesce($2, elim_low_match_id)
                                WHERE
                                    cup_stage_id = $3 AND lid = $4
                            ",
                            (position == Some(POSITION_HIGH)).then_some(new_match.id),
                            (position == Some(POSITION_LOW)).then_some(new_match.id),
                            self.id,
                            parent_lid as i32,
                        )
                        .execute(&mut transaction)
                        .await?;
                    }
                }

                Cmd::AddSeed {
                    lid,
                    position,
                    rank,
                } => {
                    let signup = signups
                        .get(rank as usize)
                        .expect(&format!("signup {} didn't exist", rank));
                    query!(
                        "
                            UPDATE cup_match_2p
                            SET high_id = coalesce($1, high_id), low_id = coalesce($2, low_id)
                            WHERE cup_stage_id = $3 AND lid = $4
                        ",
                        (position == POSITION_HIGH).then_some(signup.id),
                        (position == POSITION_LOW).then_some(signup.id),
                        self.id,
                        lid as i32,
                    )
                    .execute(&mut transaction)
                    .await?;
                }

                Cmd::AddBye { lid, position } => {
                    // update the match that has this bye
                    let dest = query!(
                        "
                            UPDATE cup_match_2p
                            SET winner_id = (CASE WHEN $1 THEN high_id ELSE low_id END)
                            WHERE cup_stage_id = $2 AND lid = $3
                            RETURNING winner_id, elim_winner_match_id, elim_winner_to_high
                        ",
                        position == POSITION_HIGH,
                        self.id,
                        lid as i32
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    // advance the winner if there was one
                    query!(
                        "
                            UPDATE cup_match_2p
                            SET high_id = coalesce($1, high_id), low_id = coalesce($2, low_id)
                            WHERE cup_stage_id = $3 AND lid = $4
                        ",
                        (position == POSITION_HIGH)
                            .then_some(dest.winner_id)
                            .flatten(),
                        (position == POSITION_LOW)
                            .then_some(dest.winner_id)
                            .flatten(),
                        self.id,
                        lid as i32
                    )
                    .execute(&mut transaction)
                    .await?;
                }

                Cmd::DropLoser {
                    origin_lid,
                    dest_lid,
                    dest_position,
                } => {
                    // this is where the drop goes to
                    let dest = query!(
                        "
                            SELECT id FROM cup_match_2p WHERE cup_stage_id = $1 AND lid = $2
                        ",
                        self.id,
                        dest_lid as i32
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    // set the loser destination for our origin
                    let origin = query!(
                        "
                            UPDATE cup_match_2p
                            SET elim_loser_match_id = $1, elim_loser_to_high = $2
                            WHERE cup_stage_id = $3 AND lid = $4
                            RETURNING id
                        ",
                        dest.id,
                        dest_position == POSITION_HIGH,
                        self.id,
                        origin_lid as i32,
                    )
                    .fetch_one(&mut transaction)
                    .await?;

                    // now set either elim_low_match_id or elim_high_match_id on
                    // the destination to the origin match
                    query!(
                        "
                            UPDATE cup_match_2p
                            SET elim_low_match_id = coalesce($1, elim_low_match_id),
                                elim_high_match_id = coalesce($2, elim_high_match_id)
                            WHERE id = $3
                        ",
                        (dest_position == POSITION_LOW).then_some(origin.id),
                        (dest_position == POSITION_HIGH).then_some(origin.id),
                        dest.id,
                    )
                    .execute(&mut transaction)
                    .await?;
                }
            }
        }

        transaction.commit().await?;

        Ok(())
    }

    pub async fn populate_groups_2p(
        &self,
        signups: Vec<CupSignup>,
        db: &mut PgConnection,
    ) -> Result<()> {
        assert_eq!(self.format, CupStageFormat::Groups);

        let group_rematches = self.group_rematches.unwrap_or(0);
        if group_rematches < 0 {
            return Err(Error::SomeError("negative group_matches"));
        }
        let group_rematches = group_rematches as usize;

        // The number of players in each group
        let group_size = self
            .group_size
            .map(|g| g as usize)
            .unwrap_or(guess_decent_group_size(signups.len()));

        // Signups put into their respective groups, but not paired into matches
        let group_draws: Vec<Vec<&CupSignup>> = {
            let num_groups = ((signups.len() as f64) / (group_size as f64)).ceil() as usize;
            log::info!(
                "num_groups={}; group_size={}; signups={}",
                num_groups,
                group_size,
                signups.len()
            );
            let grouped: Vec<(&CupSignup, usize)> = signups
                .iter()
                .zip(group_indices(signups.len(), group_size, num_groups))
                .collect();
            assert_eq!(grouped.len(), signups.len());
            let mut draws: Vec<Vec<&CupSignup>> = (0..num_groups).map(|_| vec![]).collect();
            for (signup, group_ix) in grouped.iter() {
                let group = draws.get_mut(*group_ix).expect("group didn't exist");
                group.push(signup);
            }
            draws
        };

        // Groups -> Round -> Matchup pairs
        // /* groups */ Vec<
        //   /* rounds */ Vec<
        //     /* matchups */ Vec<
        //       /* individual match */
        //       (option<signup>, option<signup>)
        //     >
        //   >
        // >
        let mut group_matchups_by_round: Vec<
            Vec<Vec<(Option<&&CupSignup>, Option<&&CupSignup>, usize)>>,
        > = Vec::with_capacity(group_draws.len() * (1 + group_rematches));

        // let mut group_sched_cache = BTreeMap::new();
        let group_sched = group_schedule(group_size, group_rematches);

        for group in group_draws.iter() {
            // let size = group.len(); NOTE allocation of byes is
            // important in determining the final ranking of the group! so we
            // can't really just have an optimal schedule for every group
            //
            // let group_sched = group_sched_cache
            //   .entry(size)
            //   .or_insert_with(|| round_robin_schedule(size));
            group_matchups_by_round.push(
                group_sched
                    .iter()
                    .map(|round| {
                        round
                            .iter()
                            .map(|(s1, s2, r)| (group.get(*s1), group.get(*s2), *r))
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>(),
            );
        }

        let scoring_id = CupStage::scoring_group(self.id, &mut *db.acquire().await?)
            .await?
            .id;

        let mut transaction = db.begin().await?;
        let mut next_lid = 0;

        for (group_no, group_rounds) in group_matchups_by_round.iter().enumerate() {
            for (group_round, group) in group_rounds.iter().enumerate() {
                for (s1, s2, r) in group.iter() {
                    let (low_id, high_id) = match (s1, s2) {
                        (Some(s1), Some(s2)) => {
                            if s1.seed_value >= s2.seed_value {
                                (Some(s1.id), Some(s2.id))
                            } else {
                                (Some(s2.id), Some(s1.id))
                            }
                        }
                        (None, Some(s2)) => (None, Some(s2.id)),
                        (Some(s1), None) => (None, Some(s1.id)),
                        (None, None) => (None, None),
                    };
                    let (low_id, high_id) = if r % 2 == 0 {
                        (low_id, high_id)
                    } else {
                        (high_id, low_id)
                    };
                    let winner_id = if low_id.is_none() { high_id } else { None };
                    let lid = next_lid;
                    next_lid += 1;
                    query!(
                        "
                            INSERT INTO cup_match_2p(
                                lid,
                                cup_stage_id,
                                group_no,
                                group_round,
                                low_id,
                                high_id,
                                winner_id,
                                scoring_id)
                            VALUES($1, $2, $3, $4, $5, $6, $7, $8)
                        ",
                        lid,
                        self.id,
                        group_no as i32,
                        group_round as i32,
                        low_id,
                        high_id,
                        winner_id,
                        scoring_id,
                    )
                    .execute(&mut transaction)
                    .await?;
                }
            }
        }
        transaction.commit().await?;
        Ok(())
    }
}

type Position = bool;
const POSITION_HIGH: bool = true;
const POSITION_LOW: bool = false;

pub fn guess_decent_group_size(num_signups: usize) -> usize {
    if num_signups <= 5 {
        num_signups
    } else {
        // want to minimise:
        // 1. oddness of groups
        // 2. number of byes
        // 3. num_signups % num_groups
        2_f64
            .powf((num_signups as f64).log2().ceil() as f64)
            .sqrt()
            .ceil() as usize
    }
}

// num_groups=4 => 0,1,2,3,3,2,1,0,0,1,2,3,3,2,1,0,...
pub fn group_indices(
    _num_signups: usize,
    _group_size: usize,
    num_groups: usize,
) -> impl Iterator<Item = usize> {
    let ascent = 0..num_groups;
    let descent = (0..num_groups).rev();
    ascent.chain(descent).cycle()
}

#[cfg(test)]
mod test {
    use super::guess_decent_group_size;
    #[test]
    fn group_sizes() {
        assert_eq!(guess_decent_group_size(1), 1);
        assert_eq!(guess_decent_group_size(8), 3);
        assert_eq!(guess_decent_group_size(9), 4);
        assert_eq!(guess_decent_group_size(16), 4);
        assert_eq!(guess_decent_group_size(17), 6);
        assert_eq!(guess_decent_group_size(32), 6);
        assert_eq!(guess_decent_group_size(33), 8);
    }
}
