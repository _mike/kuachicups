use super::super::{CupMatch2p, CupStage, CupStageFormat, CupStageScoring, ElimMatchType};
use crate::db::{helpers::ById, Error, Executor, Result};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Postgres, Transaction};
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageScoringForm {
    pub elim_round: Option<i32>,
    pub is_lb: Option<bool>,
    pub groups: bool,
    pub bestof: i32,
    pub pickbans: Option<i32>,
    pub is_point_score: Option<bool>,
    pub point_score_greatest_is_winner: Option<bool>,
    pub is_time_score: Option<bool>,
    pub is_time_score_race: Option<bool>,
}

impl CupStage {
    pub async fn set_scoring_config(
        cup_stage_id: Uuid,
        scorings: &Vec<CupStageScoringForm>,
        mut db: Transaction<'_, Postgres>,
    ) -> Result<()> {
        // first unlink any existing scorings. this means any matches already
        // made on the stage are not going to have their scoring overridden
        query!(
            "UPDATE cup_stage_scoring SET cup_stage_id = null WHERE cup_stage_id = $1",
            cup_stage_id
        )
        .execute(&mut db)
        .await?;
        for scoring in scorings {
            query!(
                "
                INSERT INTO cup_stage_scoring(
                    cup_stage_id,
                    elim_round,
                    is_lb,
                    groups,
                    bestof,
                    pickbans,
                    is_point_score,
                    point_score_greatest_is_winner,
                    is_time_score,
                    is_time_score_race)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
                ",
                cup_stage_id,
                scoring.elim_round,
                scoring.is_lb,
                scoring.groups,
                scoring.bestof,
                scoring.pickbans,
                scoring.is_point_score,
                scoring.point_score_greatest_is_winner,
                scoring.is_time_score,
                scoring.is_time_score_race,
            )
            .execute(&mut db)
            .await?;
        }

        let stage = CupStage::by_id(&mut db, cup_stage_id).await?;

        let matches = CupMatch2p::by_cup_stage_id(&mut db, cup_stage_id).await?;

        for m in matches {
            if !m.is_scored {
                let new_scoring = match stage.format {
                    CupStageFormat::Groups => CupStage::scoring_group(stage.id, &mut *db).await?,
                    CupStageFormat::DoubleElim | CupStageFormat::SingleElim => {
                        CupStage::scoring_elim(
                            stage.id,
                            m.elim_round
                                .ok_or(Error::SomeError("could not get elim_round from match"))?,
                            m.elim_type
                                .ok_or(Error::SomeError("could not get elim_is_lb from match"))?
                                == ElimMatchType::LB,
                            &mut *db,
                        )
                        .await?
                    }
                    f => panic!("unsupported stage format {:?}", f),
                };
                query!(
                    "UPDATE cup_match_2p SET scoring_id = $1 WHERE id = $2",
                    new_scoring.id,
                    m.id
                )
                .execute(&mut db)
                .await?;
            }
        }

        db.commit().await?;
        Ok(())
    }

    pub async fn scoring_elim(
        cup_stage_id: Uuid,
        elim_logical_round: i32,
        is_lb: bool,
        db: &mut sqlx::PgConnection,
    ) -> Result<CupStageScoring> {
        if query!(
            "
            SELECT format = 'single_elim' OR format = 'double_elim' AS is_elim
            FROM cup_stage
            WHERE id = $1",
            cup_stage_id,
        )
        .fetch_one(&mut *db)
        .await?
        .is_elim
            != Some(true)
        {
            return Err(Error::SomeError("not a elim bracket"));
        }
        let scoring = query_as!(
            CupStageScoring,
            // find the scoring_round with the greatest elim_round less than the input elim_logical_ruond
            "
            SELECT *
            FROM cup_stage_scoring
            WHERE cup_stage_id = $1 AND elim_round <= $2 AND is_lb = $3
            ORDER BY elim_round DESC
            LIMIT 1 ",
            cup_stage_id,
            elim_logical_round,
            is_lb,
        )
        .fetch_one(&mut *db)
        .await?;
        log::info!("got scoring_elim = {:#?}", scoring);
        Ok(scoring)
    }

    pub async fn scoring_group(
        cup_stage_id: Uuid,
        db: &mut sqlx::PgConnection,
    ) -> Result<CupStageScoring> {
        if query!(
            "
            SELECT format = 'groups' AS is_groups
            FROM cup_stage
            WHERE id = $1",
            cup_stage_id,
        )
        .fetch_one(&mut *db)
        .await?
        .is_groups
            != Some(true)
        {
            return Err(Error::SomeError("not a group bracket"));
        }
        let scoring = query_as!(
            CupStageScoring,
            "
            SELECT *
            FROM cup_stage_scoring
            WHERE cup_stage_id = $1 AND groups = true
            LIMIT 1",
            cup_stage_id,
        )
        .fetch_one(&mut *db)
        .await?;
        Ok(scoring)
    }
}
