use super::super::model::*;
use crate::db;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use uuid::Uuid;

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct PlayerPendingMatch {
    signup: CupSignup,
    match_: CupMatch2p,
}

impl CupMatch2p {
    pub async fn pending_matches(db: &db::DB, stage_id: Uuid) -> db::Result<Vec<CupMatch2p>> {
        let rows = query_as!(
            CupMatch2pRow,
            "
                SELECT m.*
                FROM cup_match_2p m
                WHERE m.cup_stage_id = $1 AND NOT m.is_scored
                ORDER BY lid ASC
            ",
            stage_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }

    pub async fn pending_matches_for_signups(
        db: &db::DB,
        stage_id: Uuid,
        signup_ids: Vec<Uuid>,
    ) -> db::Result<Vec<CupMatch2p>> {
        let rows = query_as!(
            CupMatch2pRow,
            "
                SELECT m.*
                FROM cup_match_2p m
                WHERE m.cup_stage_id = $1
                 AND (m.low_id = ANY($2) OR m.high_id = ANY($2))
                 AND NOT m.is_scored
                ORDER BY lid ASC
            ",
            stage_id,
            &signup_ids,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }
}
