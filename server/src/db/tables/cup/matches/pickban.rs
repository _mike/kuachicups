use super::super::model::*;
use crate::db::{
    ById, Error, JsonRow, JsonToDbResult, MatchReporter, PoolFilter, PoolItemState, PoolSlug,
    Result,
};
use num_traits::FromPrimitive;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, query_unchecked, Acquire, PgConnection, Postgres, Transaction};
use std::collections::{HashMap, HashSet};
use uuid::Uuid;

/// NOTE client must check if the pickban_index is valid! If it isn't, that
/// means that the pickbans are done
#[derive(Clone, Debug, Deserialize, Serialize, JsonSchema)]
#[serde(tag = "type")]
pub struct NextPickbanStep {
    pickban_index: i32,
    pool_filter: PoolFilter,
}

#[derive(Clone, Deserialize, Serialize, JsonSchema)]
pub struct PickbanActionKey {
    /// What pool they're actioning (picking or banning depending on the step)
    pool_name: String,
    /// What item in the pool they're actioning
    item_name: String,
}

pub enum PickbanActionCommand {
    Pick(PickbanActionKey),
    Ban(PickbanActionKey),
}

impl CupMatch2p {
    /// Get the pickbans object for the given match
    pub async fn pickbans(match_id: i32, db: &mut PgConnection) -> Result<Pickbans> {
        let info = query!(
            "
                SELECT s.pickbans, s.bestof
                FROM cup_match_2p m
                JOIN cup_stage_scoring s ON m.scoring_id = s.id
                WHERE m.id = $1
            ",
            match_id
        )
        .fetch_one(&mut *db)
        .await?;
        log::debug!("pickbans info is {:?}", info);
        Pickbans::query(info.pickbans, info.bestof, &mut *db).await
    }

    /// Get the next step in the pickban process
    pub async fn next_pickban_step(
        match_id: i32,
        db: &mut PgConnection,
    ) -> Result<Option<NextPickbanStep>> {
        let info = query!(
            "
                SELECT pickban_index, pool_filter
                FROM cup_match_2p_pickban_step ps
                WHERE cup_match_id = $1
                ORDER BY pickban_index DESC
                LIMIT 1
            ",
            match_id,
        )
        .fetch_optional(&mut *db)
        .await?;
        // where there is no pickban_index, we are at the start of the pickban process, so give 0 as the index
        let next_pickban_index = info.as_ref().map(|r| r.pickban_index + 1).unwrap_or(0);
        let pool_filter = if let Some(info) = info {
            serde_json::from_value::<PoolFilter>(info.pool_filter)
                .expect("Cannot parse pool_filter")
        } else {
            Pickbans::default_pool_filter()
        };
        Ok(Some(NextPickbanStep {
            pickban_index: next_pickban_index,
            pool_filter,
        }))
    }

    /// Perform 1 step in pickbans for the given match
    pub async fn perform_pickban(
        match_id: i32,
        action: PickbanActionKey,
        auth: &(Uuid, MatchReporter),
        db: &mut PgConnection,
    ) -> Result<()> {
        let pickbans = CupMatch2p::pickbans(match_id, &mut *db).await?;
        let next_step = CupMatch2p::next_pickban_step(match_id, &mut *db).await?;
        let next_step = next_step.filter(|s| (s.pickban_index as usize) < pickbans.steps.len());
        if let Some(next_step) = next_step {
            let next_pickban_index = next_step.pickban_index as usize;
            let pickban_index = next_pickban_index;
            let pool_filter = next_step.pool_filter;
            let step = pickbans
                .steps
                .get(pickban_index)
                .expect(format!("pickban step {} not available", pickban_index).as_str());
            if Some(auth.1) != step.kind.reporter() {
                return Err(Error::AuthError("invalid auth to perform pickban"));
            }
            // let pool_index = action.pool_index as usize;
            // let item_index = action.item_index as usize;
            let already_gone = pool_filter.get(&action.pool_name).and_then(|pool| {
                match pool.get(&action.item_name) {
                    None => Some(false),
                    _ => Some(true),
                }
            });
            if already_gone == Some(true) {
                return Err(Error::SomeError("item is already banned or picked"));
            }
            let mut next_pool_filter = pool_filter.clone();
            next_pool_filter
                .entry(action.pool_name.clone())
                .or_insert_with(|| HashMap::new())
                .insert(
                    action.item_name.clone(),
                    if step.kind.is_pick() {
                        PoolItemState::Picked
                    } else {
                        PoolItemState::Banned
                    },
                );
            query!(
                "
                    INSERT INTO cup_match_2p_pickban_step(
                        cup_match_id,
                        actor,
                        pickban_index,
                        pickbans,
                        pool_name,
                        item_name,
                        kind,
                        pool_filter
                    )
                    VALUES(
                        $1,
                        $2,
                        $3,
                        $4,
                        $5,
                        $6,
                        (($7::jsonb)->>0)::pickban_step_kind,
                        $8
                    )
                    ",
                match_id,
                auth.0,
                pickban_index as i32,
                pickbans.id,
                action.pool_name,
                action.item_name,
                serde_json::to_value(&step.kind).expect("step.kind to_value"),
                serde_json::to_value(&next_pool_filter).expect("pool_filter to_value failed"),
            )
            .execute(&mut *db)
            .await?;
            // not supported yet
            // perform_pickban_reset(match_id, db).await?;
            Ok(())
        } else {
            Err(Error::SomeError("next pickban not available"))
        }
    }
}

pub async fn perform_pickban_reset(match_id: i32, db: &mut PgConnection) -> Result<bool> {
    if let Some(next_step) = CupMatch2p::next_pickban_step(match_id, &mut *db).await? {
        let pickbans = CupMatch2p::pickbans(match_id, &mut *db).await?;
        let pickban_index = next_step.pickban_index as usize;
        let step = pickbans
            .steps
            .get(pickban_index)
            .expect(format!("pickban step {} not available", pickban_index).as_str());
        match step.kind {
            PickbanStepKind::Reset => {
                let mut next_pool_filter = next_step.pool_filter.clone();
                next_pool_filter.remove(&step.pool_name);
                query!(
                    "
                    INSERT INTO cup_match_2p_pickban_step(
                        cup_match_id,
                        pickban_index,
                        pickbans,
                        pool_name,
                        kind,
                        pool_filter
                    )
                    VALUES(
                        $1,
                        $2,
                        $3,
                        $4,
                        (($5::jsonb)->>0)::pickban_step_kind,
                        $6
                    )
                    ",
                    match_id,
                    pickban_index as i32,
                    pickbans.id,
                    step.pool_name,
                    serde_json::to_value(&step.kind).expect("step.kind to_value"),
                    serde_json::to_value(&next_pool_filter).expect("pool_filter to_value failed"),
                )
                .execute(&mut *db)
                .await?;
                Ok(true)
            }
            _ => Ok(false),
        }
    } else {
        Ok(false)
    }
}

impl PickbanStepKind {
    pub fn reporter(&self) -> Option<MatchReporter> {
        match self {
            PickbanStepKind::HighBan | PickbanStepKind::HighPick | PickbanStepKind::LowLose => Some(MatchReporter::High),
            PickbanStepKind::LowBan | PickbanStepKind::LowPick => Some(MatchReporter::Low),
            PickbanStepKind::Reset => None,
        }
    }
}
