use super::super::model::*;
use crate::db;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use uuid::Uuid;

impl db::HasId for CupMatch2p {
    type Id = i32;

    fn id(&self) -> Self::Id {
        self.id
    }
}

#[async_trait::async_trait]
impl db::ById for CupMatch2p {
    async fn by_ids<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        ids: &[Self::Id],
    ) -> db::Result<Vec<Self>> {
        let rows = query_as!(
            CupMatch2pRow,
            "SELECT * FROM cup_match_2p cm WHERE cm.id = ANY ($1) ORDER BY array_position($1, cm.id)",
            ids,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }

    async fn by_id<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        id: Self::Id,
    ) -> db::Result<Self> {
        let row = query_as!(
            CupMatch2pRow,
            "SELECT * FROM cup_match_2p cm WHERE cm.id = $1 ",
            id,
        )
        .fetch_one(db)
        .await?;
        Ok(CupMatch2p::from_row(row))
    }
}

impl CupMatch2p {
    pub async fn by_cup_stage_id<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        stage_id: Uuid,
    ) -> db::Result<Vec<CupMatch2p>> {
        let rows = query_as!(
            CupMatch2pRow,
            "
                SELECT * FROM cup_match_2p
                WHERE cup_stage_id = $1
                ORDER BY elim_type, elim_round, elim_index, group_no, group_round, lid
            ",
            stage_id,
        )
        .fetch_all(db)
        .await?;
        Ok(rows.iter().map(CupMatch2p::from_row).collect())
    }

    // boilerplate
    pub fn from_row(row: impl AsRef<CupMatch2pRow>) -> Self {
        let row = row.as_ref();
        CupMatch2p {
            id: row.id,
            lid: row.lid,
            scoring_id: row.scoring_id,
            cup_stage_id: row.cup_stage_id,
            group_no: row.group_no,
            group_round: row.group_round,
            elim_round: row.elim_round,
            elim_logical_round: row.elim_logical_round,
            elim_index: row.elim_index,
            elim_type: row.elim_type.and_then(num_traits::FromPrimitive::from_i16),
            elim_winner_match_id: row.elim_winner_match_id,
            elim_winner_to_high: row.elim_winner_to_high,
            elim_loser_match_id: row.elim_loser_match_id,
            elim_loser_to_high: row.elim_loser_to_high,
            elim_low_match_id: row.elim_low_match_id,
            elim_high_match_id: row.elim_high_match_id,
            low_id: row.low_id,
            high_id: row.high_id,
            low_report: {
                match &row.low_report_low {
                    Some(ll) => match &row.low_report_high {
                        Some(lh) if ll.len() == lh.len() => Some(
                            ll.iter()
                                .zip(lh.iter())
                                .map(|(low, high)| ScoreReport {
                                    low: *low,
                                    high: *high,
                                })
                                .collect::<Vec<ScoreReport>>(),
                        ),
                        _ => None,
                    },
                    None => None,
                }
            },
            high_report: {
                match &row.high_report_low {
                    Some(ll) => match &row.high_report_high {
                        Some(lh) if ll.len() == lh.len() => Some(
                            ll.iter()
                                .zip(lh.iter())
                                .map(|(low, high)| ScoreReport {
                                    low: *low,
                                    high: *high,
                                })
                                .collect::<Vec<ScoreReport>>(),
                        ),
                        _ => None,
                    },
                    None => None,
                }
            },
            winner_id: row.winner_id,
            loser_id: row.loser_id,
            is_scored: row.is_scored,
            created_at: row.created_at,
            updated_at: row.updated_at,
        }
    }
}

impl CupMatch2pRow {
    pub fn into(self) -> CupMatch2p {
        CupMatch2p::from_row(&self)
    }
}

impl AsRef<CupMatch2pRow> for CupMatch2pRow {
    fn as_ref(&self) -> &CupMatch2pRow {
        self
    }
}
