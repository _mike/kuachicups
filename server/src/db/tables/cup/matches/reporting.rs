use super::super::model::*;
use crate::db::{self, last_elo_for_gamemode};
use crate::db::{elo_update, ById};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::types::BigDecimal;
use sqlx::{query, query_as, Acquire, PgConnection, Postgres, Transaction};
use std::convert::TryFrom;
use std::error::Error;
use std::str::FromStr;
use uuid::Uuid;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MatchReporter {
    Low,
    High,
}

impl CupMatch2p {
    pub async fn set_winner(mut db: Transaction<'_, Postgres>, match_id: i32) -> db::Result<()> {
        let m = CupMatch2p::by_id(&mut db, match_id).await?;

        let bye_winner = if m.group_no == None {
            match (
                (m.elim_low_match_id, m.low_id),
                (m.elim_high_match_id, m.high_id),
            ) {
                ((None, None), (_, Some(w))) => Some(w),
                ((_, Some(w)), (None, None)) => Some(w),
                _ => None,
            }
        } else {
            match (m.low_id, m.high_id) {
                (Some(low), None) => Some(low),
                (None, Some(high)) => Some(high),
                _ => None,
            }
        };

        log::info!("bye_winner = {:#?}", bye_winner);

        let _result: Option<(Uuid, Option<Uuid>)> = if let Some(bye_winner) = bye_winner {
            query!(
                "UPDATE cup_match_2p SET winner_id = $1, updated_at = now() WHERE id = $2",
                bye_winner,
                match_id
            )
            .execute(&mut db)
            .await?;
            Some((bye_winner, None))
        } else if let (Some(low), Some(high)) = (m.low_report, m.high_report) {
            if low == high {
                // Reports match, so update the match winner_id
                let high_wins = low.iter().filter(|m| m.high > m.low).count();
                let low_wins = low.iter().filter(|m| m.low > m.high).count();
                if high_wins == low_wins {
                    return Err(db::Error::SomeError("tied results!"));
                }
                let high_id = m.high_id.expect("must have high_id");
                let low_id = m.low_id.expect("must have low_id");
                let (winner_id, loser_id) = if high_wins > low_wins {
                    query!(
                        "UPDATE cup_match_2p SET winner_id = high_id, loser_id = low_id, updated_at = now() WHERE id = $1",
                        match_id
                    )
                    .execute(&mut db)
                    .await?;
                    (high_id, low_id)
                } else {
                    query!(
                        "UPDATE cup_match_2p SET winner_id = low_id, loser_id = high_id, updated_at = now() WHERE id = $1",
                        match_id
                    )
                    .execute(&mut db)
                    .await?;
                    (low_id, high_id)
                };

                // Update Elo for players on this match
                let high_signup =
                    CupSignup::by_id(&mut db, m.high_id.expect("must have high_id")).await?;
                let low_signup =
                    CupSignup::by_id(&mut db, m.low_id.expect("must have low_id")).await?;
                let cup = Cup::by_id(&mut db, high_signup.cup_id).await?;
                let game_mode_id = cup.game_mode_id;

                let mut high_elo = last_elo_for_gamemode(
                    Some(high_signup.player_id),
                    high_signup.team_id,
                    game_mode_id,
                    &mut db,
                )
                .await?;
                let mut low_elo = last_elo_for_gamemode(
                    Some(low_signup.player_id),
                    low_signup.team_id,
                    game_mode_id,
                    &mut db,
                )
                .await?;

                for (game, r) in low.iter().enumerate() {
                    let (high_score, low_score) =
                        ((r.high > r.low) as i32, (r.low > r.high) as i32);
                    let (high_next_elo, low_next_elo) =
                        elo_update((high_elo, high_score as i32), (low_elo, low_score as i32));
                    let high_diff = high_next_elo - high_elo;
                    let low_diff = low_next_elo - low_elo;
                    for (cs, elo, elo_diff) in &[
                        (&high_signup, high_next_elo, high_diff),
                        (&low_signup, low_next_elo, low_diff),
                    ] {
                        query!(
                            "
                            INSERT INTO
                                elo_update(
                                    game_mode_id,
                                    team_id,
                                    player_id,
                                    source_match,
                                    source_match_game,
                                    elo,
                                    elo_diff
                                )
                                VALUES($1, $2, $3, $4, $5, $6, $7)
                            ",
                            game_mode_id,
                            cs.team_id,
                            cs.player_id,
                            m.id,
                            game as i32,
                            BigDecimal::try_from(*elo).unwrap(),
                            BigDecimal::try_from(*elo_diff).unwrap(),
                        )
                        .execute(&mut db)
                        .await?;
                    }
                    high_elo = high_next_elo;
                    low_elo = low_next_elo;
                }
                // Elos: Done

                Some((winner_id, Some(loser_id)))
            } else {
                None
            }
        } else {
            // Nothing to do
            return Ok(());
        };

        db.commit().await?;
        Ok(())
    }

    pub async fn require_valid_reports(
        match_id: i32,
        low_report: &[i32],
        high_report: &[i32],
        db: &mut PgConnection,
    ) -> db::Result<()> {
        if low_report.len() != high_report.len() {
            return Err(db::Error::SomeError("mismatch report lengths"));
        }
        let bestof = query!(
            "
            SELECT s.bestof
            FROM cup_stage_scoring s
            JOIN cup_match_2p m ON m.scoring_id = s.id
            WHERE m.id = $1 ",
            match_id
        )
        .fetch_one(db)
        .await?
        .bestof;
        let min_games = ((bestof as f32) / 2.0).ceil() as usize;
        let max_games = bestof as usize;
        let games = min_games..=max_games;
        let high_wins = low_report
            .iter()
            .zip(high_report.iter())
            .map(|(l, h)| {
                if l > h {
                    MatchReporter::Low
                } else {
                    MatchReporter::High
                }
            })
            .filter(|m| match m {
                MatchReporter::High => true,
                _ => false,
            })
            .count();
        let low_wins = low_report.len() - high_wins;
        log::debug!("games = {:?}", games);
        log::debug!("low_wins = {:?}", low_wins);
        log::debug!("high_wins = {:?}", high_wins);
        log::debug!("low_report = {:?}", low_report);
        log::debug!("high_report = {:?}", high_report);
        if !((games.contains(&low_wins) || games.contains(&high_wins))
            && low_report.len() <= max_games)
        {
            log::error!("invalid scores for bestof {}", bestof);
            return Err(db::Error::SomeError("invalid scores"));
        }
        Ok(())
    }

    pub async fn override_report(
        match_id: i32,
        low_report: &[i32],
        high_report: &[i32],
        db: &mut PgConnection,
    ) -> db::Result<Option<db::Error>> {
        CupMatch2p::require_valid_reports(match_id, low_report, high_report, &mut *db).await?;
        let mut transaction = db.begin().await?;
        let rows = query!(
            "
            UPDATE cup_match_2p
            SET
                low_report_low = $1,
                low_report_high = $2,
                high_report_low = $1,
                high_report_high = $2,
                updated_at = now()
            WHERE id = $3
            ",
            low_report,
            high_report,
            match_id,
        )
        .execute(&mut transaction)
        .await?;
        CupMatch2p::set_winner(transaction.begin().await?, match_id).await?;
        match rows.rows_affected() {
            0 => Err(db::Error::SomeError("match did not update, maybe wrong id")),
            1 => {
                let m = CupMatch2p::by_id(&mut transaction, match_id).await?;
                let result = if m.is_scored {
                    let advance_result = m.advance_around(&mut *transaction).await;
                    if let Err(err) = advance_result {
                        log::warn!("when advancing match, got error {:?}", err);
                        Some(err)
                    } else {
                        None
                    }
                } else {
                    None
                };
                transaction.commit().await?;
                Ok(result)
            }
            _ => Err(db::Error::SomeError(
                "match update affected more than one row",
            )),
        }
    }

    pub async fn reset_report(match_id: i32, db: &mut PgConnection) -> db::Result<()> {
        query!(
            "
            UPDATE cup_match_2p
            SET
                low_report_low = null,
                low_report_high = null,
                high_report_low = null,
                high_report_high = null,
                winner_id = null,
                loser_id = null,
                updated_at = now()
            WHERE id = $1
            ",
            match_id
        )
        .execute(db)
        .await?;
        Ok(())
    }

    pub async fn make_report(
        match_id: i32,
        reporter: MatchReporter,
        low_report: &[i32],
        high_report: &[i32],
        db: &mut PgConnection,
    ) -> db::Result<()> {
        CupMatch2p::require_valid_reports(match_id, low_report, high_report, &mut *db).await?;
        let mut transaction = db.begin().await?;
        let rows = if reporter == MatchReporter::Low {
            query!(
                "
                    UPDATE cup_match_2p
                    SET low_report_low = $1, low_report_high = $2, updated_at = now()
                    WHERE id = $3
                ",
                low_report,
                high_report,
                match_id
            )
            .execute(&mut transaction)
            .await?
        } else {
            query!(
                "
                    UPDATE cup_match_2p
                    SET high_report_low = $1, high_report_high = $2, updated_at = now()
                    WHERE id = $3
                ",
                low_report,
                high_report,
                match_id
            )
            .execute(&mut transaction)
            .await?
        };
        CupMatch2p::set_winner(transaction.begin().await?, match_id).await?;
        match rows.rows_affected() {
            0 => Err(db::Error::SomeError("match did not update, maybe wrong id")),
            1 => {
                let m = CupMatch2p::by_id(&mut transaction, match_id).await?;
                transaction.commit().await?;
                if m.is_scored {
                    m.advance_around(db).await?;
                }
                Ok(())
            }
            _ => Err(db::Error::SomeError(
                "match update affected more than one row",
            )),
        }
    }

    pub async fn advance_around(&self, db: &mut PgConnection) -> db::Result<()> {
        log::debug!("advance_around {}", self.id);

        let mut transaction = db.begin().await?;

        if let (Some(elim_next_match_id), Some(elim_next_match_is_high), Some(winner_id)) = (
            self.elim_winner_match_id,
            self.elim_winner_to_high,
            self.winner_id,
        ) {
            if elim_next_match_is_high {
                query!(
                    "UPDATE cup_match_2p SET high_id = $1, updated_at = now() WHERE id = $2",
                    winner_id,
                    elim_next_match_id
                )
                .execute(&mut transaction)
                .await?;
            } else {
                query!(
                    "UPDATE cup_match_2p SET low_id = $1, updated_at = now() WHERE id = $2",
                    winner_id,
                    elim_next_match_id
                )
                .execute(&mut transaction)
                .await?;
            }
        }

        if let (Some(elim_loser_match_id), Some(loser_id)) =
            (self.elim_loser_match_id, self.loser_id)
        {
            let loser_match = CupMatch2p::by_id(&mut transaction, elim_loser_match_id).await?;
            let mut set_loser = true;

            if self.elim_type == Some(ElimMatchType::GF1) {
                // only self.high_id can drop to the "lb" (the 2nd gf) in this case
                // if self.high_id wins then set gf2 to reflect high_id has won
                if Some(loser_id) != self.high_id {
                    set_loser = false;
                    if let Some(winner_id) = self.winner_id {
                        query!(
                            "
                                UPDATE cup_match_2p
                                SET winner_id = $1, updated_at = now()
                                WHERE id = $2
                            ",
                            winner_id,
                            loser_match.id
                        )
                        .execute(&mut transaction)
                        .await?;
                    } else {
                        return Err(db::Error::SomeError("no winner_id to give gf2 to"));
                    }
                }
            }

            if set_loser {
                if loser_match.elim_low_match_id == Some(self.id) {
                    query!(
                        "UPDATE cup_match_2p SET low_id = $1, updated_at = now() WHERE id = $2",
                        loser_id,
                        elim_loser_match_id,
                    )
                    .execute(&mut transaction)
                    .await?;
                } else if loser_match.elim_high_match_id == Some(self.id) {
                    query!(
                        "UPDATE cup_match_2p SET high_id = $1, updated_at = now() WHERE id = $2",
                        loser_id,
                        elim_loser_match_id,
                    )
                    .execute(&mut transaction)
                    .await?;
                } else {
                    return Err(db::Error::SomeError(
                        "no matching high/low of match to drop to",
                    ));
                }
            }
        }

        // if let Some(cup_stage_id) = self.cup_stage_id {
        //     CupStage::advance(cup_stage_id, &mut *transaction).await?;
        // }

        transaction.commit().await?;

        Ok(())
    }
}
