use sqlx;
use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use derive_builder::Builder;
use schemars::JsonSchema;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, sqlx::Type)]
pub enum PickbanAction {
    #[serde(rename="pick")] Pick,
    #[serde(rename="ban")] Ban,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct PickbanCfgPoolItem {
    pub key: i32,
    pub meta: String,
    pub name: String,
    pub description: String,
    pub image_uri: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct PickbanCfgStep {
    pub name: String,
    pub action: PickbanAction,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct GameModeMatchControlConfig {
    pub id: Uuid,
    pub game_mode_id: Uuid,
    pub name: String,
    pub description_md: String,
    pub steps: Vec<PickbanCfgStep>,
    pub pools: Vec<PickbanCfgPoolItem>,
}
