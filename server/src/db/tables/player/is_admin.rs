use super::model::Player;
use crate::db::{Executor, Result};
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::query;
use uuid::Uuid;

#[derive(Serialize, Deserialize, JsonSchema, Debug, Copy, Clone, PartialEq, Eq)]
pub struct IsAdmin {
    pub is_admin: bool,
}

impl Player {
    pub async fn is_admin(db: impl Executor<'_>, id: Uuid) -> Result<IsAdmin> {
        let r = query!("SELECT count(id) FROM site_admin_player WHERE id = $1", id)
            .fetch_optional(db)
            .await?;
        Ok(IsAdmin {
            is_admin: r.map_or(false, |ac| ac.count == Some(1)),
        })
    }
}
