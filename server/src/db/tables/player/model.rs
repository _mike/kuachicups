use crate::db::{Executor, Result};
use crate::{by_id_impl, paginated_impl};
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::query_as;
use uuid::Uuid;

// XXX actually use this within Player?
// serde I think should have some option to make it "transparent"
#[derive(Debug, Copy, Clone, Serialize, Deserialize, JsonSchema)]
pub struct PlayerId(pub Uuid);

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct Player {
    pub id: Uuid,
    pub discord_id: String,
    pub discord_username: String,
    pub discord_discriminator: Option<String>,
    pub discord_tag: String,
    pub discord_avatar: Option<String>,
    #[serde(skip_serializing)]
    pub discord_email: Option<String>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct PlayerSocial {
    player_id: Uuid,
    service: Social,
    uri: String,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, JsonSchema, sqlx::Type)]
#[serde(rename_all = "snake_case")]
pub enum Social {
    Twitch,
    Twitter,
    Facebook,
    Youtube,
    Website,
}

#[derive(Debug, Serialize, Deserialize, schemars::JsonSchema)]
pub struct PlayerTeam {
    pub role: Role,
    pub team_id: Uuid,
}

#[derive(
    Debug,
    Clone,
    Copy,
    Serialize,
    Deserialize,
    JsonSchema,
    sqlx::Type,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
)]
#[serde(rename_all = "snake_case")]
pub enum Role {
    Player,
    Captain,
    Admin,
}

by_id_impl!(Player, "player", uuid::Uuid);
paginated_impl!(Player, "player");

impl Player {
    pub async fn by_discord_id(db: impl Executor<'_>, id: &str) -> Result<Player> {
        Ok(query_as!(
            Player,
            "SELECT p.* FROM player p WHERE p.discord_id = $1",
            id
        )
        .fetch_one(db)
        .await?)
    }
}
