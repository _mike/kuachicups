use super::model::Player;
use crate::db::{Error, Executor, Result};
use sqlx::{query_as, PgConnection};
use twilight_model::{channel::message::Mention, user::CurrentUser};

impl Player {
    /// Add or update a player based on a discord mention
    pub async fn add_from_discord_mention(
        mention: &Mention,
        db: &mut PgConnection,
    ) -> Result<Player> {
        let discord_id = format!("{}", mention.id);
        let player = query_as!(
            Player,
            "SELECT * FROM player WHERE discord_id = $1",
            discord_id
        )
        .fetch_one(&mut *db)
        .await;
        let discord_name = mention.name.as_str();
        let discord_avatar = mention.avatar.map(|a| a.to_string());
        let discord_discriminator = format!("{:04}", mention.discriminator);
        if let Ok(player) = player {
            if player.discord_avatar == discord_avatar
                && player.discord_username.as_str() == discord_name
                // TODO this doesn't work due to discord username thing
                && player.discord_discriminator == Some(discord_discriminator.clone())
            {
                Ok(player)
            } else {
                // perform an update
                Ok(query_as!(
                    Player,
                    "
                        UPDATE player
                        SET
                            discord_avatar = $1,
                            discord_username = $2,
                            discord_discriminator = $3
                        WHERE discord_id = $4
                        RETURNING player.*
                    ",
                    discord_avatar,
                    discord_name,
                    discord_discriminator,
                    discord_id
                )
                .fetch_one(&mut *db)
                .await?)
            }
        } else {
            Ok(query_as!(
                Player,
                "
                    INSERT INTO player (
                        discord_id,
                        discord_username,
                        discord_discriminator,
                        discord_avatar)
                    VALUES($1, $2, $3, $4)
                    RETURNING player.*
                ",
                discord_id,
                discord_name,
                discord_discriminator,
                discord_avatar,
            )
            .fetch_one(&mut *db)
            .await?)
            // sign up a new player
        }
    }

    /// Add or update a player from discord CurrentUser (what you get from
    /// oauth2 login)
    pub async fn add_from_discord(user: &CurrentUser, db: &mut PgConnection) -> Result<Player> {
        log::debug!("add_from_discord {:#?}", user);
        let discord_id = user.id.to_string();
        let player = query_as!(
            Player,
            "SELECT * FROM player WHERE discord_id = $1",
            discord_id,
        )
        .fetch_one(&mut *db)
        .await;
        if let Ok(player) = player {
            // Return an existing player, but maybe update the discord info if
            // needed
            let user_avatar = user.avatar.map(|a| a.to_string());
            let user_discriminator = format!("{:04}", user.discriminator);
            if player.discord_avatar == user_avatar
                && player.discord_username == user.name
                // TODO this doesn't work due to discord username thing
                && player.discord_discriminator == Some(user_discriminator.clone())
                && player.discord_email == user.email
            {
                Ok(player)
            } else {
                // update the discord credentials for this player
                log::info!(
                    "updating discord credentials for {:#?} to match {:#?}",
                    player,
                    user
                );
                Ok(query_as!(
                    Player,
                    "
                        UPDATE player
                        SET
                            discord_avatar = $1,
                            discord_username = $2,
                            discord_discriminator = $3,
                            discord_email = $4,
                            updated_at = now()
                        WHERE discord_id = $5
                        RETURNING player.*
                    ",
                    user_avatar,
                    user.name,
                    user_discriminator,
                    user.email,
                    discord_id,
                )
                .fetch_one(&mut *db)
                .await?)
            }
        } else {
            if !user.verified.unwrap_or(false) {
                log::error!("discord user is not verified {}", user.id);
                return Err(Error::AuthError(
                    "Discord account is not verified. Check account settings in Discord.",
                ));
            }
            if user.bot {
                log::error!("discord user is a bot {}", user.id);
                return Err(Error::AuthError("Cannot log in a Discord bot."));
            }
            log::info!("creating player from discord user {:#?}", user);
            let user_avatar = user.avatar.map(|a| a.to_string());
            let user_discriminator = format!("{:04}", user.discriminator);
            let player = query_as!(
                Player,
                "
                    INSERT INTO player (
                        discord_id,
                        discord_username,
                        discord_discriminator,
                        discord_avatar,
                        discord_email)
                    VALUES($1, $2, $3, $4, $5)
                    RETURNING player.*
                ",
                discord_id,
                user.name,
                user_discriminator,
                user_avatar,
                user.email
            )
            .fetch_one(&mut *db)
            .await?;
            Ok(player)
        }
    }
}
