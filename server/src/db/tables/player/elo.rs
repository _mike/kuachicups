use crate::db;
use sqlx::{query, query_as, Acquire, PgConnection, Postgres, Transaction};
use uuid::Uuid;

pub const START_ELO: f32 = 1200.0;

pub async fn last_elo_for_gamemode(
    player_id: Option<Uuid>,
    team_id: Option<Uuid>,
    game_mode_id: Uuid,
    db: &mut PgConnection,
) -> db::Result<f32> {
    let elo = query!(
        r#"
        SELECT e.elo::float4 AS "elo"
        FROM elo_update e
        WHERE e.game_mode_id = $3 AND (player_id = $1 OR team_id = $2)
        ORDER BY e.id DESC LIMIT 1
        "#,
        player_id,
        team_id,
        game_mode_id,
    )
        .fetch_optional(&mut *db)
        .await?
        .map_or(START_ELO, |r| r.elo.expect("high elo was null somehow"));
    Ok(elo)
}

