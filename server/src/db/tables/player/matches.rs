use super::super::cup::CupMatch2p;
use super::model::*;
use crate::db::{
    paged1, ById, Executor, JsonRow, JsonToDbResult, Page, PageItem, Paged, Paginated,
    PaginatedSearch, Result, PAGE_LEN,
};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use sqlx::{query_as, Postgres};
use uuid::Uuid;

impl Player {
    pub async fn past_solo_matches(
        player_id: Uuid,
        page: Page,
        db: impl Executor<'_>,
    ) -> Result<Paged<i32>> {
        let rows = query_as!(
            PageItem,
            "
                SELECT m.id, m.updated_at
                FROM cup_signup c
                JOIN cup_match_2p m
                  ON m.is_scored AND (m.winner_id = c.id AND m.loser_id IS NOT NULL OR
                                      m.loser_id = c.id AND m.winner_id IS NOT NULL)
                WHERE c.player_id = $1
                ORDER BY m.updated_at DESC
                OFFSET $2 LIMIT $3
            ",
            player_id,
            (PAGE_LEN as i64) * page.0,
            (PAGE_LEN as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<i32>(rows, page, PAGE_LEN))
    }

    pub async fn past_team_matches(
        player_id: Uuid,
        team_id: Uuid,
        page: Page,
        db: impl Executor<'_>,
    ) -> Result<Paged<i32>> {
        let rows = query_as!(
            PageItem,
            "
                SELECT m.id, m.updated_at
                FROM team_member tm
                JOIN cup_signup c ON c.team_id = tm.team_id
                JOIN cup_match_2p m
                  ON m.is_scored AND (m.winner_id = c.id AND m.loser_id IS NOT NULL OR
                                      m.loser_id = c.id AND m.winner_id IS NOT NULL)
                WHERE c.player_id = $1 AND c.team_id = $2
                ORDER BY m.updated_at DESC
                OFFSET $3 LIMIT $4
            ",
            player_id,
            team_id,
            (PAGE_LEN as i64) * page.0,
            (PAGE_LEN as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<i32>(rows, page, PAGE_LEN))
    }
}
