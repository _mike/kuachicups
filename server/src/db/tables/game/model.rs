use crate::db::{Executor, HasParent, JsonRow, JsonToDbResult, Result};
use crate::{by_id_impl, by_id_json_impl, paginated_impl};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query_as, Postgres};
use uuid::Uuid;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, JsonSchema, sqlx::Type)]
#[serde(rename_all = "lowercase")]
pub enum EntrantType {
    Player,
    Team,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct Game {
    pub id: Uuid,
    pub name: String,
    pub slug: String,
    pub description_md: String,
    pub description_html: String,
    pub rules_md: String,
    pub rules_html: String,
    pub avatar: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct GameMode {
    pub id: Uuid,
    pub game_id: Uuid,
    pub name: String,
    pub slug: String,
    pub description_md: String,
    pub description_html: String,
    pub rules_md: String,
    pub rules_html: String,
    pub avatar: String,
    pub entrant_type: Option<EntrantType>,
    pub entrants_per_match_min: i32,
    pub entrants_per_match_max: Option<i32>,
    pub team_size_min: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

by_id_impl!(Game, "game", Uuid);
by_id_json_impl!(GameMode, "game_mode", Uuid);
paginated_impl!(Game, "game");

#[async_trait]
impl HasParent<Game> for GameMode {
    fn parent_id(&self) -> Uuid {
        self.game_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<GameMode>> {
        query_as!(
            JsonRow,
            "
            SELECT to_jsonb(game_mode.*) as obj
            FROM game_mode
            WHERE game_mode.game_id = $1
            ORDER BY game_mode.game_id, game_mode.name ",
            id,
        )
        .fetch_all(db)
        .await?
        .from_json()
    }
}

/*
async fn match_control_config(
    db: impl Executor<'_, Database=Postgres>,
    game_mode_id: &Uuid,
) -> Result<Vec<GameModeMatchControlConfig>> {
    query_as!(JsonRow, r#"
        SELECT to_jsonb(game_mode_pb_v1_match_control.*) as obj
        FROM game_mode_pb_v1_match_control
        WHERE game_mode_id = $1 "#,
        *game_mode_id,
    )
        .fetch_all(db)
        .await?
        .from_json()
}

impl GameMode {
    pub async fn match_control_config(
        db: impl Executor<'_, Database=Postgres>,
        game_mode_id: &Uuid,
    ) -> Result<Vec<GameModeMatchControlConfig>> {
        match_control_config(db, game_mode_id).await
    }
}

#[async_trait]
impl HasParent<GameMode> for GameModeMatchControlConfig {
    fn parent_id(&self) -> &Uuid {
        &self.game_mode_id
    }

    async fn by_parent_id<'e, E: Executor<'e, Database=Postgres>> (
        db: E,
        game_mode_id: &Uuid,
    ) -> Result<Vec<GameModeMatchControlConfig>> {
        match_control_config(db, game_mode_id).await
    }
}
*/
