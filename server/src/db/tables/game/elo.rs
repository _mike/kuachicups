use crate::db;
use crate::db::{ById, GameMode};
use crate::handlers::api::player::PlayerEloUpdate;
use bigdecimal::BigDecimal;
use chrono::{DateTime, Utc};
use itertools::Itertools;
use sqlx::{query, PgConnection};
use std::cmp::min;
use uuid::Uuid;

const K_FACTOR: f32 = 32.0;

pub fn elo_update(high: (f32, i32), low: (f32, i32)) -> (f32, f32) {
    let (h, h_score) = high;
    let (l, l_score) = low;
    let (h_expected, l_expected) = expected_scores(h, l);
    let h_outcome = ((h_score > l_score) as i32) as f32;
    let l_outcome = 1.0 - h_outcome;
    let h_next = K_FACTOR.mul_add(h_outcome - h_expected, h);
    let l_next = K_FACTOR.mul_add(l_outcome - l_expected, l);
    (h_next, l_next)
}

pub fn expected_scores(high: f32, low: f32) -> (f32, f32) {
    let exp_one = (1.0 + 10_f32.powf((low - high) / 400.0)).recip();
    let exp_two = 1.0 - exp_one;
    (exp_one, exp_two)
}

impl GameMode {
    pub async fn game_current_rankings(
        game_id: Uuid,
        mode_id: Uuid,
        num_players: Option<usize>,
        date_from: Option<DateTime<Utc>>,
        date_to: Option<DateTime<Utc>>,
        db: &mut PgConnection,
    ) -> db::Result<GameRankings> {
        let game_mode = GameMode::by_id(&mut *db, mode_id).await?;
        let is_team_game = game_mode.team_size_min.unwrap_or(0) > 0;
        let final_rankings = if is_team_game {
            query!(
                "
                    SELECT DISTINCT ON (e.team_id) e.id, e.team_id, e.created_at, e.elo
                    FROM elo_update e
                    WHERE ($1::timestamptz is null or e.created_at >= $1) and ($2::timestamptz is null or e.created_at <= $2)
                    ORDER BY e.team_id, e.id DESC
                    LIMIT coalesce($3, 100)
                ",
                date_from,
                date_to,
                num_players.map(|u| u as i32),
            )
            .fetch_all(&mut *db)
            .await?
                .into_iter()
                .map(|u| EloUpdate {
                    team_id: u.team_id,
                    player_id: None,
                    elo: u.elo,
                    created_at: u.created_at,
                })
                .collect_vec()
        } else {
            query!(
                "
                    SELECT DISTINCT ON (e.player_id) e.id, e.player_id, e.created_at, e.elo
                    FROM elo_update e
                    WHERE ($1::timestamptz is null or e.created_at >= $1) and ($2::timestamptz is null or e.created_at <= $2)
                    ORDER BY e.player_id, e.id DESC
                    LIMIT coalesce($3, 100)
                ",
                date_from,
                date_to,
                num_players.map(|u| u as i32),
            )
            .fetch_all(&mut *db)
            .await?
                .into_iter()
                .map(|u| EloUpdate {
                    team_id: None,
                    player_id: u.player_id,
                    elo: u.elo,
                    created_at: u.created_at,
                })
                .collect_vec()
        };
        Ok(GameRankings {
            game_id,
            mode_id,
            num_players: final_rankings.len(),
            date_from,
            date_to,
            final_rankings,
        })
    }
}

pub struct EloUpdate {
    pub team_id: Option<Uuid>,
    pub player_id: Option<Uuid>,
    pub created_at: DateTime<Utc>,
    pub elo: BigDecimal,
}

pub struct GameRankings {
    pub game_id: Uuid,
    pub mode_id: Uuid,
    pub date_from: Option<DateTime<Utc>>,
    pub date_to: Option<DateTime<Utc>>,
    pub num_players: usize,
    pub final_rankings: Vec<EloUpdate>,
}
