use super::model::*;
use crate::db::{
    paged1, HasId, JsonRow, JsonToDbResult, Page, PageItem, Paged, Paginated, PaginatedSearch,
    Result,
};
use async_trait::async_trait;
use sqlx::{Executor, PgConnection, Postgres};
use uuid::Uuid;

#[async_trait]
impl PaginatedSearch for Team {
    async fn page_search<'e, E: Executor<'e, Database = Postgres>>(
        db: E,
        page: Page,
        search: &str,
    ) -> Result<Paged<<Self as HasId>::Id>> {
        let rows: Vec<PageItem<<Self as HasId>::Id>> = sqlx::query_as!(
            PageItem,
            "
                SELECT id, updated_at
                FROM team
                WHERE to_tsvector(name || ' ' || blurb_md) @@ to_tsquery($1) AND NOT is_deleted
                ORDER BY name ASC OFFSET $2 LIMIT $3
            ",
            search,
            (Self::page_len() as i64) * page.0,
            (Self::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<<Self as HasId>::Id>(rows, page, Self::page_len()))
    }
}

impl Team {
    pub async fn page_search_by_player(
        player_id: Uuid,
        page: Page,
        search: &str,
        db: &mut PgConnection,
    ) -> Result<Paged<<Self as HasId>::Id>> {
        let rows: Vec<PageItem<<Self as HasId>::Id>> = sqlx::query_as!(
            PageItem,
            "
                SELECT t.id, t.updated_at
                FROM team t JOIN team_member mem ON t.id = mem.team_id
                WHERE mem.player_id = $1 AND NOT t.is_deleted
                  AND to_tsvector(t.name || ' ' || t.blurb_md) @@ to_tsquery($2)
                ORDER BY name ASC OFFSET $3 LIMIT $4
            ",
            player_id,
            search,
            (Self::page_len() as i64) * page.0,
            (Self::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<<Self as HasId>::Id>(rows, page, Self::page_len()))
    }

    pub async fn page_by_player(
        player_id: Uuid,
        page: Page,
        db: &mut PgConnection,
    ) -> Result<Paged<<Self as HasId>::Id>> {
        let rows: Vec<PageItem<<Self as HasId>::Id>> = sqlx::query_as!(
            PageItem,
            "
                SELECT t.id, t.updated_at
                FROM team t JOIN team_member mem ON t.id = mem.team_id
                WHERE mem.player_id = $1 AND NOT t.is_deleted
                ORDER BY name ASC OFFSET $2 LIMIT $3
            ",
            player_id,
            (Self::page_len() as i64) * page.0,
            (Self::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<<Self as HasId>::Id>(rows, page, Self::page_len()))
    }
}
