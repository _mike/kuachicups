pub use crate::db::tables::player::{PlayerTeam, Role, Social};
use crate::db::{HasParent, JsonRow, JsonToDbResult, Result};
use crate::{by_id_impl, paginated_impl};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use derive_builder::Builder;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::query_as;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct Team {
    pub id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub name: String,
    pub blurb_md: Option<String>,
    pub blurb_html: Option<String>,
    pub discord_server_id: Option<String>,
    pub discord_server_avatar: Option<String>,
    pub is_deleted: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct TeamMember {
    pub player_id: Uuid,
    pub player_role: Role,
    pub team_id: Uuid,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct TeamMemberInvite {
    pub invite_by_player_id: Uuid,
    pub player_id: Uuid,
    pub team_id: Uuid,
    pub created_at: DateTime<Utc>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema, Builder)]
pub struct TeamSocial {
    team_id: Uuid,
    service: Social,
    uri: String,
}

by_id_impl!(Team, "team", uuid::Uuid);
paginated_impl!(Team, "team", 16, "WHERE NOT is_deleted ORDER BY name ASC");

#[async_trait]
impl HasParent<Team> for TeamSocial {
    fn parent_id(&self) -> Uuid {
        self.team_id
    }
    async fn by_parent_id<'e, E: sqlx::Executor<'e, Database = sqlx::Postgres>>(
        db: E,
        id: Uuid,
    ) -> Result<Vec<Self>> {
        query_as!(
            JsonRow,
            "SELECT to_jsonb((s.*)) as obj FROM team_social s WHERE s.team_id = $1",
            id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }
}
