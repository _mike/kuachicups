use super::{PlayerTeam, Role, Team};
use crate::db::{Executor, JsonRow, JsonToDbResult, Result};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use sqlx::query_as;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, schemars::JsonSchema)]
pub struct TeamPlayer {
    pub role: Role,
    pub player_id: Uuid,
}

impl Team {
    pub async fn players(db: impl Executor<'_>, id: &Uuid) -> Result<Vec<TeamPlayer>> {
        query_as!(
            JsonRow,
            "
            SELECT jsonb_build_object('role', player_role, 'player_id', player_id) AS obj
            FROM team_member
            WHERE team_member.team_id = $1
            ",
            *id
        )
        .fetch_all(db)
        .await?
        .from_json()
    }
}
