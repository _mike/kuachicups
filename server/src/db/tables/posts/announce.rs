use super::model::*;
use crate::db::{Error, Result, DB};
use crate::discord_markdown::{discord_md, textual_md};
use crate::render_markdown::markdown_to_html;
use bigdecimal::{BigDecimal, ToPrimitive};
use chrono::{DateTime, Utc};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as};
use twilight_http::Client;
use twilight_model::{
    channel::message::Embed,
    id::{
        marker::{ChannelMarker, MessageMarker},
        Id,
    },
};
use twilight_util::builder::embed::{
    EmbedAuthorBuilder, EmbedBuilder, EmbedFooterBuilder, ImageSource,
};
use uuid::Uuid;

pub const ANN_CHANNEL: Id<ChannelMarker> = Id::new(649790432498417685);

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CreatePostAnn {
    pub author_id: Uuid,
    pub title_md: Option<String>,
    pub content_md: Option<String>,
    pub message_line_md: Option<String>,
    pub image_uri: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct UpdatePostAnn {
    pub id: Uuid,
    pub author_id: Uuid,
    pub title_md: Option<String>,
    pub content_md: Option<String>,
    pub message_line_md: Option<String>,
    pub image_uri: Option<String>,
}

impl PostAnn {
    pub async fn create(post: CreatePostAnn, db: &DB) -> Result<Self> {
        Ok(query_as!(
            PostAnn,
            r#"
                INSERT INTO post_ann(
                    author_id,
                    title_md,
                    title_html,
                    content_md,
                    content_html,
                    message_line_md,
                    message_line_html,
                    image_uri)
                VALUES($1, $2, $3, $4, $5, $6, $7, $8)
                RETURNING post_ann.*
            "#,
            post.author_id,
            post.title_md,
            post.title_md.as_deref().map(markdown_to_html),
            post.content_md,
            post.content_md.as_deref().map(markdown_to_html),
            post.message_line_md,
            post.message_line_md.as_deref().map(markdown_to_html),
            post.image_uri
        )
        .fetch_one(db)
        .await?)
    }

    pub async fn update(post: UpdatePostAnn, client: Client, db: &DB) -> Result<Self> {
        let next = query_as!(
            PostAnn,
            r#"
                UPDATE post_ann
                SET
                    author_id = $2,
                    title_md = $3,
                    title_html = $4,
                    content_md = $5,
                    content_html = $6,
                    message_line_md = $7,
                    message_line_html = $8,
                    image_uri = $9
                WHERE id = $1
                RETURNING post_ann.*
            "#,
            post.id,
            post.author_id,
            post.title_md,
            post.title_md.as_deref().map(markdown_to_html),
            post.content_md,
            post.content_md.as_deref().map(markdown_to_html),
            post.message_line_md,
            post.message_line_md.as_deref().map(markdown_to_html),
            post.image_uri,
        )
        .fetch_one(db)
        .await?;

        if let Some(discords) = query!(
            "SELECT discord_msg_id FROM post_ann_to_discord WHERE source_post_id = $1",
            post.id
        )
        .fetch_optional(db)
        .await?
        {
            let msg_id = Id::<MessageMarker>::new(
                discords
                    .discord_msg_id
                    .to_u64()
                    .expect("discord_id does not fit into u64"),
            );
            client
                .update_message(ANN_CHANNEL, msg_id)
                .embeds(Some(&vec![next.embed(next.uri.clone())?]))?
                .content(Some(&next.message_line_md.clone().unwrap_or_default()))?
                .exec()
                .await?;
        }
        Ok(next)
    }

    pub async fn delete(id: Uuid, client: Client, db: &DB) -> Result<()> {
        if let Some(discords) = query!(
            "
                DELETE FROM post_ann_to_discord
                WHERE source_post_id = $1
                RETURNING discord_msg_id
            ",
            id
        )
        .fetch_optional(db)
        .await?
        {
            let msg_id = Id::<MessageMarker>::new(
                discords
                    .discord_msg_id
                    .to_u64()
                    .expect("discord_id does not fit into u64"),
            );

            client.delete_message(ANN_CHANNEL, msg_id).exec().await?;
        }

        query!("DELETE FROM post_ann WHERE id = $1", id)
            .execute(db)
            .await?;

        Ok(())
    }

    pub async fn publish(&self, uri: String, discord: Client, db: &DB) -> Result<()> {
        let mut trans = db.begin().await?;
        let embed = self.embed(Some(uri))?;
        let msg = discord
            .create_message(ANN_CHANNEL)
            .embeds(&vec![embed])?
            .content(&self.message_line_md.clone().unwrap_or_default())?
            .await;
        if let Err(err) = msg {
            log::error!("twilight error when publishing [1]: {:#?}", err);
            return Err(Error::SomeTwilightHttpError);
        }
        let msg = msg?.model().await;
        if let Err(err) = msg {
            log::error!("twilight error when publishing [2]: {:#?}", err);
            return Err(Error::SomeTwilightHttpError);
        }
        let msg = msg?;
        query!(
            r#"
                UPDATE post_ann
                SET published_time = now()
                WHERE id = $1
            "#,
            self.id,
        )
        .execute(&mut trans)
        .await?;
        query!(
            r#"
                INSERT INTO post_ann_to_discord(source_post_id, discord_msg_id)
                VALUES ($1, $2)
            "#,
            self.id,
            <sqlx::types::BigDecimal as From<u64>>::from(msg.id.get()),
        )
        .execute(&mut trans)
        .await?;
        trans.commit().await?;
        Ok(())
    }

    pub fn embed(&self, uri: Option<String>) -> Result<Embed> {
        let mut eb = EmbedBuilder::new().color(0xff9409);
        if let Some(image_uri) = &self.image_uri {
            if let Ok(imgsrc) = ImageSource::url(image_uri) {
                eb = eb.image(imgsrc);
            }
        }
        if let Some(uri) = uri.or(self.uri.clone()) {
            eb = eb.url(uri);
        }
        if let Some(title_md) = &self.title_md {
            eb = eb.title(textual_md(&title_md));
        }
        if let Some(content_md) = &self.content_md {
            let desc = discord_md(&content_md);
            eb = eb.description(desc);
        }
        Ok(eb.build())
    }
}
