use super::model::*;
use crate::db::{paged1, Error, Page, PageItem, Paged, Paginated, Player, Result, DB};
use crate::discord_markdown::{discord_md, textual_md};
use bigdecimal::BigDecimal;
use chrono::{DateTime, Utc};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_json::json;
use sqlx::{query, query_as, PgConnection};
use twilight_http::Client;
use twilight_model::{
    channel::message::{embed::EmbedAuthor, Embed},
    channel::Message,
    id::{
        marker::{ChannelMarker, MessageMarker},
        Id,
    },
};
use twilight_util::builder::embed::{EmbedAuthorBuilder, EmbedBuilder, EmbedFooterBuilder};
use uuid::Uuid;

pub const MEDIA_CHANNEL: Id<ChannelMarker> = Id::new(649791122033344512);

// recommend monitor in portrait orientation...

impl PostMedia {
    pub async fn create(approver_id: Uuid, message: Message, db: &DB) -> Result<Self> {
        let e0: Option<&Embed> = message.embeds.get(0);
        Ok(query_as!(
            PostMedia,
            "
            INSERT INTO post_media(
                approver_id,
                discord_msg_id,
                discord_user_id,
                discord_name,
                discord_discriminator,
                discord_avatar,
                discord_timestamp,
                discord_content_md,
                discord_content_html,
                embed_url,
                embed_description,
                embed_title,
                embed_author_name,
                embed_author_url,
                embed_author_icon_url,
                embed_thumbnail_url,
                embed_thumbnail_width,
                embed_thumbnail_height,
                embed_image_width,
                embed_image_height,
                embed_image_url,
                embed_kind,
                embed_video_url,
                embed_video_width,
                embed_video_height,
                embed_provider_name,
                embed_provider_name_url
            )
            VALUES(
                $1, -- approver_id
                ($2::jsonb)->>'discord_msg_id',
                ($2::jsonb)->>'discord_user_id',
                ($2::jsonb)->>'discord_name',
                ($2::jsonb)->>'discord_discriminator',
                ($2::jsonb)->>'discord_avatar',
                ($2::jsonb)->>'discord_timestamp',
                ($2::jsonb)->>'discord_content_md',
                ($2::jsonb)->>'discord_content_html',
                ($2::jsonb)->>'embed_url',
                ($2::jsonb)->>'embed_description',
                ($2::jsonb)->>'embed_title',
                ($2::jsonb)->>'embed_author_name',
                ($2::jsonb)->>'embed_author_url',
                ($2::jsonb)->>'embed_author_icon_url',
                ($2::jsonb)->>'embed_thumbnail_url',
                (($2::jsonb)->>'embed_thumbnail_width')::integer,
                (($2::jsonb)->>'embed_thumbnail_height')::integer,
                (($2::jsonb)->>'embed_image_width')::integer,
                (($2::jsonb)->>'embed_image_height')::integer,
                ($2::jsonb)->>'embed_image_url',
                ($2::jsonb)->>'embed_kind',
                ($2::jsonb)->>'embed_video_url',
                (($2::jsonb)->>'embed_video_width')::integer,
                (($2::jsonb)->>'embed_video_height')::integer,
                ($2::jsonb)->>'embed_provider_name',
                ($2::jsonb)->>'embed_provider_name_url'
            )
            RETURNING *",
            approver_id, // approver_id
            // big jsonb blob to get around recursion limit in sqlx lol
            json!({
                "discord_msg_id": message.id,
                "discord_user_id": message.author.id,
                "discord_name": message.author.name,
                "discord_discriminator": message.author.discriminator,
                "discord_avatar": message.author.avatar,
                "discord_timestamp": message.timestamp,
                "discord_content_md": message.content,
                "discord_content_html": crate::render_markdown::markdown_to_html(&message.content),
                "embed_url": e0.and_then(|e| e.url.as_ref()),
                "embed_description": e0.and_then(|e| e.description.as_ref()),
                "embed_title": e0.and_then(|e| e.title.as_ref()),
                "embed_author_name": e0.and_then(|e| e.author.as_ref().and_then(|a| Some(&a.name))),
                "embed_author_url": e0.and_then(|e| e.author.as_ref().and_then(|a| a.url.as_ref())),
                "embed_author_icon_url": e0.and_then(|e| e.author.as_ref().and_then(|a| a.icon_url.as_ref())),
                "embed_thumbnail_url": e0.and_then(|e| e.thumbnail.as_ref().and_then(|t| Some(&t.url))),
                "embed_thumbnail_width": e0.and_then(|e| e.thumbnail.as_ref().and_then(|t| t.width.map(|u| u as i32))),
                "embed_thumbnail_height": e0.and_then(|e| e.thumbnail.as_ref().and_then(|t| t.height.map(|u| u as i32))),
                "embed_image_width": e0.and_then(|e| e.image.as_ref().and_then(|t| t.width.map(|u| u as i32))),
                "embed_image_height": e0.and_then(|e| e.image.as_ref().and_then(|t| t.height.map(|u| u as i32))),
                "embed_image_url": e0.and_then(|e| e.image.as_ref().and_then(|t| Some(&t.url))),
                "embed_kind": e0.and_then(|e| Some(&e.kind)),
                "embed_video_url": e0.and_then(|e| e.video.as_ref().and_then(|v| v.url.as_ref())),
                "embed_video_width": e0.and_then(|e| e.video.as_ref().and_then(|v| v.width.map(|u| u as i32))),
                "embed_video_height": e0.and_then(|e| e.video.as_ref().and_then(|v| v.height.map(|u| u as i32))),
                "embed_provider_name": e0.and_then(|e| e.provider.as_ref().and_then(|p| p.name.as_ref())),
                "embed_provider_name_url": e0.and_then(|e| e.provider.as_ref().and_then(|p| p.url.as_ref())),
            })
        )
        .fetch_one(db)
        .await?)
    }

    pub async fn delete(message: Message, db: &DB) -> Result<()> {
        query!(
            "DELETE FROM post_media WHERE discord_msg_id = $1",
            message.id.to_string()
        )
        .execute(db)
        .await?;
        Ok(())
    }

    pub async fn link_to_cup(message_id: Uuid, cup_id: Uuid, db: &mut PgConnection) -> Result<()> {
        query!(
            "INSERT INTO post_media_cup(media_id, cup_id) VALUES($1, $2)",
            message_id,
            cup_id
        )
        .execute(&mut *db)
        .await?;
        Ok(())
    }

    pub async fn link_to_cup_stage(
        message_id: Uuid,
        cup_stage_id: Uuid,
        db: &mut PgConnection,
    ) -> Result<()> {
        query!(
            "INSERT INTO post_media_stage(media_id, stage_id) VALUES($1, $2)",
            message_id,
            cup_stage_id
        )
        .execute(&mut *db)
        .await?;
        Ok(())
    }

    pub async fn link_to_match(
        message_id: Uuid,
        match_id: i32,
        db: &mut PgConnection,
    ) -> Result<()> {
        query!(
            "INSERT INTO post_media_match(media_id, match_id) VALUES($1, $2)",
            message_id,
            match_id
        )
        .execute(&mut *db)
        .await?;
        Ok(())
    }

    pub async fn query(media_query: MediaQuery, db: &mut PgConnection) -> Result<Paged<Uuid>> {
        let rows: Vec<PageItem<Uuid>> = sqlx::query_as!(
            PageItem,
            "SELECT m.id, m.updated_at
            FROM post_media m
            LEFT OUTER JOIN post_media_cup pc ON m.id = pc.media_id
            LEFT OUTER JOIN post_media_stage ps ON m.id = ps.media_id
            LEFT OUTER JOIN post_media_match pm ON m.id = pm.media_id
            WHERE
              (($1::uuid) IS NULL OR pc.cup_id = $1) AND
              (($2::uuid) IS NULL OR ps.stage_id = $2) AND
              (($3::integer) IS NULL OR pm.match_id = $3)
            ORDER BY created_at DESC
            OFFSET $4 LIMIT $5",
            media_query.cup_id,
            media_query.stage_id,
            media_query.match_id,
            (<Self as Paginated>::page_len() as i64) * media_query.page.0,
            (<Self as Paginated>::page_len() as i64) + 1,
        )
        .fetch_all(db)
        .await?;
        Ok(paged1::<Uuid>(
            rows,
            media_query.page,
            <Self as Paginated>::page_len(),
        ))
    }
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub struct MediaQuery {
    pub page: Page,
    pub cup_id: Option<Uuid>,
    pub stage_id: Option<Uuid>,
    pub match_id: Option<i32>,
}
