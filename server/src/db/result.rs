use crate::auto_froms;
use okapi::openapi3::Responses;
use rocket::{
    request::Request,
    response::{status::BadRequest, Responder},
};
use rocket_okapi::{
    gen::OpenApiGenerator,
    openapi,
    response::{OpenApiResponder, OpenApiResponderInner},
    Result as OkapiResult,
};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    JsonError(serde_json::error::Error),
    SqlxError(sqlx::Error),
    SomeError(&'static str),
    AuthError(&'static str),
    NoneError,
    SomeTwilightHttpError,
    SomeTwilightMessageValidationError,
    SomeTwilightEmbedValidationError,
}

auto_froms!(Error, {
    sqlx::Error = Error::SqlxError;
    serde_json::error::Error = Error::JsonError;
    twilight_http::Error = |_| Error::SomeTwilightHttpError;
    twilight_http::response::DeserializeBodyError = |_| Error::SomeTwilightHttpError;
    twilight_validate::embed::EmbedValidationError = |_| Error::SomeTwilightEmbedValidationError;
    twilight_validate::message::MessageValidationError =
        |_| Error::SomeTwilightMessageValidationError;
});

impl<'r, 'o: 'r> Responder<'r, 'o> for Error {
    fn respond_to(self, request: &'r Request) -> rocket::response::Result<'o> {
        BadRequest(Some(format!("{:#?}", self))).respond_to(request)
    }
}

impl OpenApiResponderInner for Error {
    fn responses(gen: &mut OpenApiGenerator) -> OkapiResult<Responses> {
        <BadRequest<String> as OpenApiResponder>::responses(gen)
    }
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "db::Error")
    }
}
