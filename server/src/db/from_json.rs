use super::result::{Error, Result};
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use serde_json::{from_value, Value};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JsonRow {
    pub obj: Option<Value>,
}

pub trait JsonToDbResult<T> {
    fn from_json(self) -> Result<T>;
}

impl<T: DeserializeOwned> JsonToDbResult<T> for Value {
    fn from_json(self) -> Result<T> {
        Ok(from_value(self)?)
    }
}

impl<T: DeserializeOwned> JsonToDbResult<T> for JsonRow {
    fn from_json(self) -> Result<T> {
        match self.obj {
            Some(obj) => obj.from_json(),
            None => Err(Error::NoneError),
        }
    }
}

impl<T: DeserializeOwned + std::fmt::Debug> JsonToDbResult<Vec<T>> for Vec<JsonRow> {
    fn from_json(self) -> Result<Vec<T>> {
        self.iter()
            .map(|v| <JsonRow as JsonToDbResult<T>>::from_json(v.clone()))
            .collect::<Result<Vec<T>>>()
    }
}

impl<T: DeserializeOwned + std::fmt::Debug> JsonToDbResult<Option<T>> for Option<JsonRow> {
    fn from_json(self) -> Result<Option<T>> {
        Ok(match self {
            Some(s) => Some(s.from_json()?),
            None => None,
        })
    }
}
