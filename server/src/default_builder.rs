#[macro_export]
macro_rules! default_builder {
    ( $($type:ident,)* ) => {
        $(
            impl $type {
                pub fn builder() -> concat_idents!($type, derive_builder::Builder) {
                    type B = concat_idents!($type, derive_builder::Builder);
                    B::default()
                }
            }
        )*
    }
}
