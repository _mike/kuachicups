use twilight_http::{client::Client, Error};

pub type DiscordClient = Client;
pub type DiscordError = Error;

#[derive(Debug, Clone)]
pub struct DiscordClientBuilder {
    // ratelimiter: Ratelimiter,
    bot_token: String,
}

impl DiscordClientBuilder {
    pub fn new(bot_token: String) -> Self {
        DiscordClientBuilder {
            // ratelimiter: Ratelimiter::new(),
            bot_token,
        }
    }

    pub fn client_bearer(&self, oauth2_bearer_token: &str) -> DiscordClient {
        Client::builder()
            .token(format!("Bearer {}", oauth2_bearer_token))
            // .ratelimiter(self.ratelimiter.clone())
            .build()
    }

    pub fn client(&self) -> DiscordClient {
        Client::builder()
            .token(self.bot_token.clone())
            // .ratelimiter(self.ratelimiter.clone())
            .build()
    }
}
