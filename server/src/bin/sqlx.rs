use clap3::Parser;
use sqlx_cli::{run, Opt};

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let opt = Opt::parse();
    run(opt).await.unwrap();
}
