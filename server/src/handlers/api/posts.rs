use chrono::{DateTime, Utc};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use slug::slugify;
use sqlx::{query, query_as};
use std::marker::PhantomData;
use uuid::Uuid;

use super::super::csrf::ProtectCsrf;
use super::super::uuids::Uuids;
use super::paginate::{paged1, Page, PageItem, Paged, PaginateApi, PAGE_LEN};
use super::player::AdminSession;
use crate::app::{DiscordClientBuilder, SettingCache};
use crate::db::{
    ById, CreatePostAnn, Error, MediaQuery, Paginated, PostAnn, PostAnnUnpublished, PostMedia,
    Result, UpdatePostAnn, DB,
};

#[openapi]
#[get("/anns/<ids>")]
pub async fn anns(ids: Uuids, db: &State<DB>) -> Result<Json<Vec<PostAnn>>> {
    Ok(Json(PostAnn::by_ids(db.inner(), &ids.0).await?))
}

#[openapi]
#[get("/anns/page?<page>")]
pub async fn anns_page(page: Option<i64>, db: &State<DB>) -> Option<Json<Paged<Uuid>>> {
    PostAnn::get_page(db.inner(), page).await
}

#[openapi]
#[get("/anns/unpublished/page?<page>")]
pub async fn anns_unpublished_page(
    page: Option<i64>,
    db: &State<DB>,
    _sess: AdminSession,
) -> Option<Json<Paged<Uuid>>> {
    PostAnnUnpublished::get_page(db.inner(), page).await
}

#[openapi]
#[post("/posts/new", format = "json", data = "<post>")]
pub async fn create_ann(
    post: Json<CreatePostAnn>,
    db: &State<DB>,
    sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<Json<PostAnn>> {
    assert_eq!(post.0.author_id, sess.0.user);
    Ok(Json(PostAnn::create(post.0, db.inner()).await?))
}

#[openapi]
#[post("/posts/update", format = "json", data = "<post>")]
pub async fn update_ann(
    post: Json<UpdatePostAnn>,
    db: &State<DB>,
    dcb: &State<DiscordClientBuilder>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<Json<PostAnn>> {
    // allows updates from other users
    let discord = dcb.inner().client();
    Ok(Json(PostAnn::update(post.0, discord, db.inner()).await?))
}

#[openapi]
#[post("/posts/delete/<post_id>")]
pub async fn delete_ann(
    post_id: Uuid,
    db: &State<DB>,
    dcb: &State<DiscordClientBuilder>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<()> {
    // allows deletes from other users
    let discord = dcb.inner().client();
    Ok(PostAnn::delete(post_id, discord, db.inner()).await?)
}

#[openapi]
#[post("/posts/<post_id>/publish_ann?<uri>")]
pub async fn publish_ann(
    post_id: Uuid,
    uri: String,
    db: &State<DB>,
    dcb: &State<DiscordClientBuilder>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<()> {
    let db = db.inner();
    let post = PostAnn::by_id(db, post_id).await?;
    let discord = dcb.inner().client();
    Ok(post.publish(uri, discord, db).await?)
}

////////////
//////////// MEDIA
////////////

#[openapi]
#[get("/media/page?<page>")]
pub async fn medias_page(page: Option<i64>, db: &State<DB>) -> Option<Json<Paged<Uuid>>> {
    PostMedia::get_page(db.inner(), page).await
}

#[openapi]
#[get("/media/query?<page>&<cup_id>&<stage_id>&<match_id>")]
pub async fn medias_query(
    page: Option<i64>,
    cup_id: Option<Uuid>,
    stage_id: Option<Uuid>,
    match_id: Option<i32>,
    db: &State<DB>,
) -> Result<Json<Paged<Uuid>>> {
    let mut db = db.inner().acquire().await?;
    let result = PostMedia::query(
        MediaQuery {
            page: page.map(|p| Page(p)).unwrap_or(Page(0)),
            cup_id,
            stage_id,
            match_id,
        },
        &mut *db,
    )
    .await?;
    Ok(Json(result))
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct MediaLinks {
    cup_id: Option<Uuid>,
    stage_id: Option<Uuid>,
    match_id: Option<i32>,
}

#[openapi]
#[post("/media/<id>/link", format = "json", data = "<links>")]
pub async fn link_media(
    id: Uuid,
    links: Json<MediaLinks>,
    db: &State<DB>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<()> {
    let mut db = db.inner().acquire().await?;
    if let Some(cup_id) = links.0.cup_id {
        PostMedia::link_to_cup(id, cup_id, &mut *db).await?;
    }
    if let Some(stage_id) = links.0.stage_id {
        PostMedia::link_to_cup_stage(id, stage_id, &mut *db).await?;
    }
    if let Some(match_id) = links.0.match_id {
        PostMedia::link_to_match(id, match_id, &mut *db).await?;
    }
    Ok(())
}

#[openapi]
#[get("/media/<ids>")]
pub async fn medias(ids: Uuids, db: &State<DB>) -> Result<Json<Vec<PostMedia>>> {
    Ok(Json(PostMedia::by_ids(db.inner(), &ids.0).await?))
}
