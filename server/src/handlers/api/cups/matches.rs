use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, PageItem, Paged, DB},
};
use crate::{
    db::{paged1, Page},
    handlers::{
        api::{
            paginate::PaginateApi,
            player::{AdminSession, UserSession},
        },
        csrf::ProtectCsrf,
        uuids::{Listi32, Uuids},
    },
};
use chrono::{DateTime, Utc};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

////////////////////////
// Matches
////////////////////////

#[openapi]
#[get("/cup_match/<match_id>/result")]
pub async fn match_result_2p(match_id: i32, db: &State<DB>) -> db::Result<Json<CupMatch2p>> {
    Ok(Json(CupMatch2p::by_id(db.inner(), match_id).await?))
}

#[openapi]
#[get("/matches/<match_ids>")]
pub async fn matches_(match_ids: Listi32, db: &State<DB>) -> db::Result<Json<Vec<CupMatch2p>>> {
    Ok(Json(CupMatch2p::by_ids(db.inner(), &match_ids.0).await?))
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupMatchReportForm2p {
    match_id: i32,
    low_report: Vec<i32>,
    high_report: Vec<i32>,
}

#[openapi]
#[post("/cup_match/<match_id>/report", format = "json", data = "<report>")]
pub async fn match_report_2p(
    match_id: i32,
    report: Json<CupMatchReportForm2p>,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> db::Result<Json<CupMatch2p>> {
    let mut db = db.inner().acquire().await?;
    let reporter = CupMatch2p::require_participant_of(match_id, sess.user, &mut *db).await?;
    log::debug!(
        "making report for {:?} ({:?}) on match {:?}",
        reporter,
        sess.user,
        match_id
    );
    CupMatch2p::make_report(
        match_id,
        reporter.1,
        &report.low_report,
        &report.high_report,
        &mut *db,
    )
    .await?;
    Ok(Json(CupMatch2p::by_id(&mut *db, match_id).await?))
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct MatchOverrideReportResult {
    match_now: CupMatch2p,
    error: Option<String>,
}

#[openapi]
#[post(
    "/cup_match/<match_id>/override_report",
    format = "json",
    data = "<report>"
)]
pub async fn match_override_report_2p(
    match_id: i32,
    report: Json<CupMatchReportForm2p>,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> db::Result<Json<MatchOverrideReportResult>> {
    let mut db = db.inner().acquire().await?;
    let cup_id = query!(
        "SELECT s.cup_id FROM cup_match_2p m JOIN cup_stage s ON s.id = m.cup_stage_id LIMIT 1"
    )
    .fetch_one(&mut *db)
    .await?
    .cup_id;
    Cup::require_ownership(cup_id, sess.user, &mut *db).await?;
    let result =
        CupMatch2p::override_report(match_id, &report.low_report, &report.high_report, &mut *db)
            .await?;
    Ok(Json(MatchOverrideReportResult {
        match_now: CupMatch2p::by_id(&mut *db, match_id).await?,
        error: result.map(|err| format!("{:?}", err)),
    }))
}

#[openapi]
#[post("/cup_match/<match_id>/reset_report")]
pub async fn match_reset_report_2p(
    match_id: i32,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> db::Result<Json<CupMatch2p>> {
    let mut db = db.inner().acquire().await?;
    let cup_id = query!(
        "SELECT s.cup_id FROM cup_match_2p m JOIN cup_stage s ON s.id = m.cup_stage_id LIMIT 1"
    )
    .fetch_one(&mut *db)
    .await?
    .cup_id;
    Cup::require_ownership(cup_id, sess.user, &mut *db).await?;
    CupMatch2p::reset_report(match_id, &mut *db).await?;
    Ok(Json(CupMatch2p::by_id(&mut *db, match_id).await?))
}

/// Get a list of matches under a cup, ordered by 1) being scored 2) update time
#[openapi]
#[get("/cup/<cup_id>/matches")]
pub async fn cup_matches(cup_id: Uuid, db: &State<DB>) -> db::Result<Json<Vec<CupMatch2p>>> {
    let mut db = db.inner().acquire().await?;
    let matches = query_as!(
        CupMatch2pRow,
        "
            SELECT m.*
            FROM cup_match_2p m JOIN cup_stage s ON s.id = m.cup_stage_id
            WHERE s.cup_id = $1
            ORDER BY
                s.is_started ASC,
                s.stage_no DESC,
                m.group_no ASC NULLS LAST,
                m.group_round ASC NULLS LAST,
                m.elim_logical_round DESC NULLS LAST,
                m.elim_round DESC NULLS LAST,
                m.is_scored DESC,
                m.low_id ASC NULLS LAST,
                m.high_id ASC NULLS LAST,
                m.updated_at DESC
        ",
        cup_id
    )
    .fetch_all(&mut *db)
    .await?;
    let matches = matches
        .into_iter()
        .map(CupMatch2p::from_row)
        .collect::<Vec<_>>();
    Ok(Json(matches))
}

/// Get a list of matches under a cup, ordered by 1) being scored 2) update time
#[openapi]
#[get("/cup/<cup_id>/own_pending_matches")]
pub async fn cup_own_pending_matches(
    cup_id: Uuid,
    sess: UserSession,
    db: &State<DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    let mut db = db.inner().acquire().await?;
    let matches = query_as!(
        CupMatch2pRow,
        "
            SELECT m.*
            FROM cup_match_2p m
              JOIN cup_stage stage ON stage.id = m.cup_stage_id
              JOIN cup_signup signup ON (signup.id = m.low_id OR signup.id = m.high_id)
            WHERE stage.cup_id = $1
              AND NOT m.is_scored
              AND signup.player_id = $2
            ORDER BY
                stage.is_started ASC,
                stage.stage_no DESC,
                m.group_no ASC NULLS LAST,
                m.group_round ASC NULLS LAST,
                m.elim_logical_round DESC NULLS LAST,
                m.elim_round DESC NULLS LAST,
                m.is_scored DESC,
                m.updated_at DESC
        ",
        cup_id,
        sess.user,
    )
    .fetch_all(&mut *db)
    .await?;
    let matches = matches
        .into_iter()
        .map(CupMatch2p::from_row)
        .collect::<Vec<_>>();
    Ok(Json(matches))
}

#[derive(Serialize, Deserialize, JsonSchema)]
struct MatchQuery {
    cup_id: Option<Uuid>,
    stage_id: Option<Uuid>,
    player_ids: Vec<Uuid>,
    team_ids: Vec<Uuid>,
}

use schemars::gen::SchemaGenerator;

/// Get a list of matches according to some query options
#[openapi]
#[get("/matches?<is_scored>&<cup_id>&<stage_id>&<player_ids>&<team_ids>&<page>")]
pub async fn matches_query(
    page: Option<i64>,
    is_scored: Option<bool>,
    cup_id: Option<Uuid>,
    stage_id: Option<Uuid>,
    player_ids: Uuids,
    team_ids: Uuids,
    db: &State<DB>,
) -> db::Result<Json<Paged<i32>>> {
    let mut db = db.inner().acquire().await?;
    let page_len = db::PAGE_LEN;
    let page = Page(page.unwrap_or(0));
    let matches = query_as!(
        PageItem,
        "
            SELECT m.id, m.updated_at
            FROM cup_match_2p m
            JOIN cup_signup s ON (s.id = m.low_id OR s.id = m.high_id)
            WHERE
                (
                    ($1::boolean) IS NULL OR
                    ($1 = m.is_scored) AND (
                        (($1 = false) AND low_report_low IS NOT NULL OR high_report_low IS NOT NULL) OR
                        (($1 = true) AND low_report_low IS NOT NULL AND high_report_low IS NOT NULL)
                    )
                )
                AND (($2::uuid) IS NULL OR s.cup_id = $2)
                AND (($3::uuid) IS NULL OR m.cup_stage_id = $3)
                AND (($4::uuid[]) IS NULL OR s.player_id = ANY($4))
                AND (($5::uuid[]) IS NULL OR s.team_id = ANY($5))
            ORDER BY
                m.updated_at DESC,
                m.created_at DESC,
                m.group_no ASC NULLS LAST,
                m.group_round ASC NULLS LAST,
                m.elim_logical_round DESC NULLS LAST,
                m.elim_round DESC NULLS LAST
            OFFSET $6 LIMIT $7
        ",
        is_scored,
        cup_id,
        stage_id,
        if player_ids.0.len() == 0 {
            None
        } else {
            Some(player_ids.0.as_slice())
        },
        if team_ids.0.len() == 0 {
            None
        } else {
            Some(team_ids.0.as_slice())
        },
        page.0 * (page_len as i64),
        (page_len as i64) + 1,
    )
    .fetch_all(&mut *db)
    .await?;
    Ok(Json(paged1(matches, page, page_len)))
}
