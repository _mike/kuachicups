use crate::handlers::{
    api::{
        paginate::PaginateApi,
        player::{AdminSession, UserSession},
    },
    csrf::ProtectCsrf,
    uuids::Uuids,
};
use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, PageItem, Paged, DB},
};
use chrono::{DateTime, Utc};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

/*
 * Cup creation and updating
 * =========================
 */

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct CupCreateForm {
    pub name: String,
    pub game_mode_id: Uuid,
    pub owner_team_id: Uuid,
    pub is_signups_closed: bool,
    pub is_published: bool,
    pub description: String,
    pub stages: Vec<CupStageCreateForm>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageCreateForm {
    pub title: String,
    pub start_immediately: bool,
    pub checkintime: Option<DateTime<Utc>>,
    pub starttime: Option<DateTime<Utc>>,
    pub format: CupStageFormat,
    pub max_participants: Option<i32>,
    pub group_size: Option<i32>,
    pub group_rematches: Option<i32>,
    pub scorings: Vec<CupStageScoringForm>,
}

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupCreate {
    pub cup: Cup,
    pub stages: Vec<CupStage>,
}

/// Create a cup. Must be an admin
#[openapi]
#[post("/cup/create", format = "json", data = "<cup>")]
pub async fn cup_create(
    cup: Json<CupCreateForm>,
    db: &State<DB>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> Result<Json<CupCreate>, db::Error> {
    let Json(cup) = cup;
    let db: &DB = db.inner();
    let mut transaction = db.begin().await?;
    let stages = cup.stages;
    let cup = Cup::create(
        cup.owner_team_id,
        cup.game_mode_id,
        cup.name,
        cup.description,
        cup.is_signups_closed,
        cup.is_published,
        &mut transaction,
    )
    .await?;
    let stages = {
        let result: Vec<CupStage> = vec![];
        for (i, stage) in stages.iter().enumerate() {
            let scorings = &stage.scorings;
            let stage = CupStage::create(
                cup.id,
                &stage.title,
                stage.start_immediately,
                stage.checkintime,
                stage.starttime,
                i as i32,
                stage.format,
                stage.max_participants,
                stage.group_size,
                stage.group_rematches,
                &mut transaction,
            )
            .await?;
            CupStage::set_scoring_config(stage.id, scorings, transaction.begin().await?).await?;
        }
        result
    };
    transaction.commit().await?;
    Ok(Json(CupCreate { cup, stages }))
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct CupUpdateForm {
    pub game_mode_id: Option<Uuid>,
    pub name: Option<String>,
    pub description: Option<String>,
    pub is_signups_closed: Option<bool>,
    pub is_published: Option<bool>,
}

/// Update a cup. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup/<id>/update", format = "json", data = "<cup>")]
pub async fn cup_update(
    id: Uuid,
    cup: Json<CupUpdateForm>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<Option<Cup>>, db::Error> {
    let mut db = db.inner().acquire().await?;
    Cup::require_ownership(id, sess.user, &mut *db).await?;
    let mut transaction = db.begin().await?;
    let Json(cup) = cup;
    let cup = Cup::update(
        id,
        cup.name,
        cup.description,
        cup.is_signups_closed,
        cup.is_published,
        cup.game_mode_id,
        &mut *transaction,
    )
    .await?;
    transaction.commit().await?;
    Ok(Json(cup))
}

////////////////////////////////////
// Cup stages
////////////////////////////////////

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
pub struct CupStageUpdateForm {
    /// None means this is a new stage
    pub stage_id: Option<Uuid>,
    pub title: Option<String>,
    pub checkintime: Option<DateTime<Utc>>,
    pub starttime: Option<DateTime<Utc>>,
    pub format: Option<CupStageFormat>,
    pub max_participants: Option<i32>,
    pub group_size: Option<i32>,
    pub group_rematches: Option<i32>,
    pub scorings: Vec<CupStageScoringForm>,
}

/// Update a cup stage. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup_stage/<stage_id>/update", format = "json", data = "<stage>")]
pub async fn cup_stage_update(
    stage_id: Uuid,
    stage: Json<CupStageUpdateForm>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<CupStage>, db::Error> {
    let mut db = db.inner().acquire().await?;
    CupStage::require_ownership(stage_id, sess.user, &mut *db).await?;
    let mut transaction = db.begin().await?;
    let Json(sf) = stage;
    let stage = CupStage::update(
        stage_id,
        sf.title,
        None, // cup_id cannot change
        sf.checkintime,
        sf.starttime,
        None, // no changing of stage_no as requires reordering the other stages
        sf.format,
        sf.max_participants,
        sf.group_size,
        sf.group_rematches,
        &mut *transaction,
    )
    .await?;
    CupStage::set_scoring_config(stage.id, &sf.scorings, transaction.begin().await?).await?;
    transaction.commit().await?;
    Ok(Json(stage))
}

/// Create an extra cup stage. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup/<cup_id>/<stage_no>/create", format = "json", data = "<stage>")]
pub async fn cup_stage_create(
    cup_id: Uuid,
    stage_no: i32,
    stage: Json<CupStageCreateForm>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<CupStage>, db::Error> {
    let mut db = db.inner().acquire().await?;
    let Json(sf) = stage;
    Cup::require_ownership(cup_id, sess.user, &mut *db).await?;
    let mut transaction = db.begin().await?;
    let stage = CupStage::create(
        cup_id,
        &sf.title,
        sf.start_immediately,
        sf.checkintime,
        sf.starttime,
        stage_no, // no changing of stage_no as requires reordering the other stages
        sf.format,
        sf.max_participants,
        sf.group_size,
        sf.group_rematches,
        &mut *transaction,
    )
    .await?;
    CupStage::set_scoring_config(stage.id, &sf.scorings, transaction.begin().await?).await?;
    transaction.commit().await?;
    Ok(Json(stage))
}
