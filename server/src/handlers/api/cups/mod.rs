use super::{
    paginate::PaginateApi,
    player::{AdminSession, UserSession},
};
use crate::handlers::{
    csrf::ProtectCsrf,
    uuids::Uuids,
};
use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, PageItem, Paged, DB},
};
use chrono::{DateTime, Utc};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

mod forms;
mod matches;
mod pickbans_;
mod signup;
mod stages;
pub use forms::*;
pub use matches::*;
pub use pickbans_::*;
pub use signup::*;
pub use stages::*;

/*
 * Basic cup queries
 * =================
 * */

/// Retrieve cups by ids
#[openapi]
#[get("/cups/<ids>")]
pub async fn cups(ids: Uuids, db: &State<DB>) -> Option<Json<Vec<Cup>>> {
    Cup::get_by_ids(db.inner(), &ids.0).await
}

/// Retrieve a page of cups
#[openapi]
#[get("/cups/page?<page>")]
pub async fn cups_page(page: Option<i64>, db: &State<DB>) -> Option<Json<Paged<Uuid>>> {
    Cup::get_page(db.inner(), page).await
}

/// Retrieve unpublished cups
#[openapi]
#[get("/cups/unpublished_page?<page>")]
pub async fn cups_unpublished_page(
    page: Option<i64>,
    db: &State<DB>,
    sess: AdminSession,
) -> db::Result<Json<Paged<Uuid>>> {
    use crate::db::helpers::by_id::HasId;
    use crate::db::helpers::paginate::{paged1, Page, PageItem, Paginated};
    let db = db.inner();
    let page = Page(page.unwrap_or(0));
    let rows: Vec<PageItem<<Cup as HasId>::Id>> = sqlx::query_as!(
        PageItem,
        "SELECT id, updated_at FROM cup WHERE NOT is_published ORDER BY updated_at OFFSET $1 LIMIT $2",
        (Cup::page_len() as i64) * page.0,
        (Cup::page_len() as i64) + 1,
    )
    .fetch_all(db)
    .await?;
    let page = paged1::<<Cup as HasId>::Id>(rows, page, Cup::page_len());
    Ok(Json(page))
}

/// Retrieve the stages of a cup
#[openapi]
#[get("/cup/<id>/stages")]
pub async fn cup_stages(id: Uuid, db: &State<DB>) -> Option<Json<Vec<CupStage>>> {
    CupStage::by_parent_id(db.inner(), id)
        .await
        .ok()
        .map(Json)
}

/// Update a cup. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup/<id>/reset")]
pub async fn cup_reset(
    id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<(), db::Error> {
    let mut db = db.inner().acquire().await?;
    Cup::require_ownership(id, sess.user, &mut *db).await?;
    Cup::reset(id, &mut *db).await?;
    Ok(())
}

#[openapi]
#[post("/cup/<id>/delete")]
pub async fn cup_delete(
    id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    let mut db = db.inner().acquire().await?;
    Cup::require_ownership(id, sess.user, &mut *db).await?;
    Ok(Json(Cup::delete(id, &mut *db).await?))
}

#[openapi]
#[get("/cup/<cup_id>/rankings")]
pub async fn cup_rankings(cup_id: Uuid, db: &State<DB>) -> db::Result<Json<Rankings>> {
    let db = db.inner();
    let stage_id = query!(
        "
            SELECT s.id FROM cup_stage s
            WHERE s.cup_id = $1 AND s.is_started
            ORDER BY s.stage_no DESC LIMIT 1
        ",
        cup_id,
    )
    .fetch_one(db)
    .await?
    .id;
    let stage = CupStage::by_id(db, stage_id).await?;
    let rankings = stage.rankings(None, &mut *db.acquire().await?).await?;
    Ok(Json(rankings))
}
