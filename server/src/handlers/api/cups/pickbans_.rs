use crate::handlers::{
    api::{
        paginate::PaginateApi,
        player::{AdminSession, UserSession},
    },
    csrf::ProtectCsrf,
    uuids::Uuids,
};
use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, JsonRow, JsonToDbResult, PageItem, Paged, DB},
};
use chrono::{DateTime, Utc};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

////////////////////////
// Pickbans
////////////////////////

/// Get a list of all the pickban systems available
#[openapi]
#[post("/pickbans")]
pub async fn pickbans(db: &State<DB>) -> db::Result<Json<Vec<Pickbans>>> {
    let mut db = db.inner().acquire().await?;
    let ps = Pickbans::all(&mut *db).await?;
    Ok(Json(ps))
}

/// Get the pickban system used for a particular match
#[openapi]
#[get("/cup_match/<match_id>/pickbans")]
pub async fn cup_match_pickbans(match_id: i32, db: &State<DB>) -> db::Result<Json<Pickbans>> {
    let mut db = db.inner().acquire().await?;
    let pb = CupMatch2p::pickbans(match_id, &mut *db).await?;
    Ok(Json(pb))
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub struct CupMatch2pPickbanStepAbbrev {
    pub cup_match_id: i32,
    /// who did this step
    /// None indicates the step happened automatically
    pub actor: Option<Uuid>,
    /// what pool (by name) the step was on
    pub pool_name: String,
    /// the item (by name) of the pool that was actioned
    /// None indicates that the step was a reset
    pub item_name: Option<String>,
    /// what the kind (high/low pick/ban) of step was
    pub kind: PickbanStepKind,
    /// parent step, if any
    pub parent_index: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Debug, Serialize, Deserialize, JsonSchema)]
pub struct CupMatch2pPickbanState {
    steps: Vec<CupMatch2pPickbanStepAbbrev>,
    next_step: Option<NextPickbanStep>,
    is_done: bool,
}

#[openapi]
#[get("/cup_match/<match_id>/pickbans/state")]
pub async fn cup_match_pickban_state(
    match_id: i32,
    db: &State<DB>,
) -> db::Result<Json<CupMatch2pPickbanState>> {
    let mut db = db.inner().acquire().await?;
    let steps: Vec<CupMatch2pPickbanStepAbbrev> = query_as!(
        JsonRow,
        "
            SELECT jsonb_build_object(
                'cup_match_id', cup_match_id,
                'actor', actor,
                'pool_name', pool_name,
                'item_name', item_name,
                'kind', kind,
                'parent_index', parent_index,
                'created_at', created_at,
                'updated_at', updated_at
            ) AS obj
            FROM cup_match_2p_pickban_step s
            WHERE cup_match_id = $1
            ORDER BY pickban_index ASC
        ",
        match_id
    )
    .fetch_all(&mut *db)
    .await?
    .from_json()?;
    log::debug!("steps = {:?}", steps);
    let num_steps = query!(
        "
            SELECT array_length(pickbans.steps, 1) AS num_steps
            FROM cup_match_2p m
            JOIN cup_stage_scoring s ON m.scoring_id = s.id
            JOIN pickbans ON s.pickbans = pickbans.id
            WHERE m.id = $1
        ",
        match_id
    )
    .fetch_one(&mut *db)
    .await?
    .num_steps;
    log::debug!("num_steps = {:?}", num_steps);
    let is_done = if let Some(num_steps) = num_steps {
        !((steps.len() as i32) < num_steps)
    } else {
        // if there are no steps, it is counted as done
        true
    };
    let next_step = if is_done {
        None
    } else {
        CupMatch2p::next_pickban_step(match_id, &mut *db).await?
    };
    Ok(Json(CupMatch2pPickbanState {
        steps,
        next_step,
        is_done,
    }))
}

#[openapi]
#[post(
    "/cup_match/<match_id>/pickbans/action",
    format = "json",
    data = "<action>"
)]
pub async fn cup_pickban_action(
    match_id: i32,
    action: Json<PickbanActionKey>,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> db::Result<()> {
    let mut db = db.inner().acquire().await?;
    let auth = CupMatch2p::require_participant_of(match_id, sess.user, &mut *db).await?;
    CupMatch2p::perform_pickban(match_id, action.0, &auth, &mut *db).await?;
    Ok(())
}
