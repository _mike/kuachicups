use crate::handlers::{
    api::{
        paginate::PaginateApi,
        player::{AdminSession, UserSession},
    },
    csrf::ProtectCsrf,
    uuids::Uuids,
};
use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, PageItem, Paged, DB},
};
use chrono::{DateTime, Utc};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct CurrentStage {
    pub stage_id: Option<Uuid>,
}

#[openapi]
#[get("/cup/<cup_id>/current_stage")]
pub async fn cup_current_stage(cup_id: Uuid, db: &State<DB>) -> db::Result<Json<CurrentStage>> {
    let mut db = db.inner().acquire().await?;
    let stage_id = Cup::current_stage(cup_id, &mut *db).await?;
    Ok(Json(CurrentStage { stage_id }))
}

/// Retrieve the scorings of a stage
#[openapi]
#[get("/cup_stage/<stage_id>/scorings")]
pub async fn cup_stage_scorings(
    stage_id: Uuid,
    db: &State<DB>,
) -> Option<Json<Vec<CupStageScoring>>> {
    CupStageScoring::by_parent_id(db.inner(), stage_id)
        .await
        .ok()
        .map(Json)
}

#[openapi]
#[get("/cup_stage/<stage_ids>")]
pub async fn cup_stages_by_stage_id(
    stage_ids: Uuids,
    db: &State<DB>,
) -> db::Result<Json<Vec<CupStage>>> {
    let stages = CupStage::by_ids(db.inner(), &stage_ids.0).await?;
    Ok(Json(stages))
}

/// Restart a cup stage. Must be a captain of the team that owns the cup.
#[openapi]
#[post("/cup_stage/<stage_id>/restart")]
pub async fn cup_stage_restart(
    stage_id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<(), db::Error> {
    let mut db = db.inner().acquire().await?;
    CupStage::require_ownership(stage_id, sess.user, &mut *db).await?;
    let cur = query!(
        "
            SELECT c.current_stage, s.id
            FROM cup c
            JOIN cup_stage s ON s.cup_id = c.id AND c.current_stage = s.stage_no
            WHERE s.id = $1
        ",
        stage_id
    )
    .fetch_one(&mut *db)
    .await?;
    if cur.id == stage_id {
        query!(
            "DELETE FROM cup_match_2p WHERE cup_stage_id = $1",
            stage_id
        )
        .execute(&mut *db)
        .await?;
        query!(
            "UPDATE cup_stage SET is_started = false WHERE id = $1",
            stage_id
        )
        .execute(&mut *db)
        .await?;
        CupStage::start_stage(stage_id, &mut *db).await?;
    } else {
        log::warn!("cannot restart that stage, it is not the current stage");
    }
    Ok(())
}

/// Force advance() to be called on a cup stage. Should never need to be used
/// *except* for adding the results to cups that are finished but were made
/// before cup_signup_result was added
#[openapi]
#[post("/cup_stage/<stage_id>/advance")]
pub async fn cup_stage_advance(
    stage_id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<(), db::Error> {
    let mut db = db.inner().acquire().await?;
    CupStage::require_ownership(stage_id, sess.user, &mut *db).await?;
    CupStage::advance(stage_id, &mut *db).await?;
    Ok(())
}

#[openapi]
#[post("/cup_stage/<stage_id>/delete")]
pub async fn cup_stage_delete(
    stage_id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    let mut db = db.inner().acquire().await?;
    Cup::require_ownership(stage_id, sess.user, &mut *db).await?;
    Ok(Json(CupStage::delete(stage_id, &mut *db).await?))
}

#[openapi]
#[post("/cup_stage/<stage_id>/initialise")]
pub async fn cup_stage_initialise(
    stage_id: Uuid,
    sess: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> Result<Json<bool>, db::Error> {
    let mut db = db.inner().acquire().await?;
    CupStage::require_ownership(stage_id, sess.user, &mut *db).await?;
    Ok(Json(CupStage::start_stage(stage_id, &mut *db).await?))
}

#[openapi]
#[get("/cup_stage/<stage_id>/matches_2p")]
pub async fn cup_stage_matches_2p(
    stage_id: Uuid,
    db: &State<DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    Ok(Json(
        CupMatch2p::by_cup_stage_id(db.inner(), stage_id).await?,
    ))
}

#[openapi]
#[get("/cup_stage/<stage_id>/pending_matches/<signup_ids>")]
pub async fn cup_stage_pending_matches_for(
    stage_id: Uuid,
    signup_ids: Uuids,
    db: &State<DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    let signup_ids = signup_ids.0;
    let matches = if signup_ids.len() > 0 {
        CupMatch2p::pending_matches_for_signups(db.inner(), stage_id, signup_ids).await?
    } else {
        CupMatch2p::pending_matches(db.inner(), stage_id).await?
    };
    Ok(Json(matches))
}

#[openapi]
#[get("/cup_stage/<stage_id>/pending_matches")]
pub async fn cup_stage_pending_matches(
    stage_id: Uuid,
    db: &State<DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    let matches = CupMatch2p::pending_matches(db.inner(), stage_id).await?;
    Ok(Json(matches))
}

#[openapi]
#[get("/cup_stage_scoring/<scoring_id>")]
pub async fn cup_stage_scoring(
    scoring_id: i32,
    db: &State<DB>,
) -> db::Result<Json<CupStageScoring>> {
    let mut db = db.inner().acquire().await?;
    Ok(Json(CupStageScoring::by_id(&mut *db, scoring_id).await?))
}

#[openapi]
#[get("/cup_stage/<stage_id>/rankings")]
pub async fn cup_stage_rankings(stage_id: Uuid, db: &State<DB>) -> db::Result<Json<Rankings>> {
    let mut db = db.inner().acquire().await?;
    let stage = CupStage::by_id(&mut *db, stage_id).await?;
    let rankings = stage.rankings(None, &mut *db.acquire().await?).await?;
    Ok(Json(rankings))
}
