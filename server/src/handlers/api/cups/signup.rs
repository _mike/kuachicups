use crate::handlers::{
    api::{
        paginate::PaginateApi,
        player::{AdminSession, UserSession},
    },
    csrf::ProtectCsrf,
    uuids::Uuids,
};
use crate::{
    db,
    db::tables::*,
    db::{ById, HasParent, PageItem, Paged, DB},
};
use chrono::{DateTime, Utc};
use itertools::Itertools;
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, Acquire, Postgres, Transaction};
use std::marker::PhantomData;
use uuid::Uuid;

// use super::super::model::*;
// use crate::db;
// use schemars::JsonSchema;
// use serde::{Deserialize, Serialize};
// use sqlx::{query, query_as, Acquire, Postgres, Transaction};
// use uuid::Uuid;

/*
 * Signup operations
 * =================
 */

/// Retrieve signups
#[openapi]
#[get("/cup_signups/<ids>")]
pub async fn signups(ids: Uuids, db: &State<DB>) -> Result<Json<Vec<CupSignup>>, db::Error> {
    Ok(Json(CupSignup::by_ids(db.inner(), &ids.0).await?))
}

/// Retrieve the signups of a cup, ordered by seed value
#[openapi]
#[get("/cup/<id>/signups")]
pub async fn cup_signups(id: Uuid, db: &State<DB>) -> Result<Json<Vec<CupSignup>>, db::Error> {
    Ok(Json(Cup::get_all_signups_seeded(db.inner(), id).await?))
}

/// Retrieve signups that match the current session
#[openapi]
#[get("/cup/<cup_id>/signups_self")]
pub async fn cup_signups_self(
    cup_id: Uuid,
    session: UserSession,
    db: &State<DB>,
) -> Result<Json<Vec<CupSignup>>, db::Error> {
    let signups = Cup::player_signups_self(db.inner(), cup_id, session.user).await?;
    Ok(Json(signups))
}

/// Checkin solo as the current user
#[openapi]
#[post("/cup/<cup_id>/solo_signup?<team_id>")]
pub async fn cup_signup_solo(
    cup_id: Uuid,
    team_id: Option<Uuid>,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = match team_id {
        Some(tid) => {
            Cup::make_signup_solo_under_team(db.inner(), cup_id, session.user, tid).await?
        }
        None => Cup::make_signup_solo(db.inner(), cup_id, session.user).await?,
    };
    Ok(Json(ok))
}

/// Checkin solo as the current user with a specified team
#[openapi]
#[post("/cup/<cup_id>/team_signup?<team_id>")]
pub async fn cup_signup_team(
    cup_id: Uuid,
    team_id: Option<Uuid>,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> Result<Json<bool>, db::Error> {
    match team_id {
        Some(tid) => Ok(Json(
            Cup::make_signup_team(db.inner(), cup_id, session.user, tid).await?,
        )),
        None => Err(db::Error::SomeError("must have team")),
    }
}

/// Leave whatever signup has the current user session player id
#[openapi]
#[post("/cup/<cup_id>/leave")]
pub async fn cup_signup_leave(
    cup_id: Uuid,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = Cup::player_leave(db.inner(), cup_id, session.user).await?;
    Ok(Json(ok))
}

/// Checkin whatever signup we have under this session
#[openapi]
#[post("/cup/<cup_id>/checkin")]
pub async fn cup_signup_checkin(
    cup_id: Uuid,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = Cup::player_checkin(db.inner(), cup_id, session.user).await?;
    Ok(Json(ok))
}

/// Checkout whatever signup we have under this session
#[openapi]
#[post("/cup/<cup_id>/checkout")]
pub async fn cup_signup_checkout(
    cup_id: Uuid,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> Result<Json<bool>, db::Error> {
    let ok = Cup::player_checkout(db.inner(), cup_id, session.user).await?;
    Ok(Json(ok))
}

/// Checkout whatever signup we have under this session
#[openapi]
#[post("/cup/<cup_id>/forfeit")]
pub async fn cup_signup_forfeit(
    cup_id: Uuid,
    session: UserSession,
    _csrf: ProtectCsrf,
    db: &State<DB>,
) -> db::Result<Json<bool>> {
    let mut db = db.inner().acquire().await?;
    let ok = Cup::forfeit_player(cup_id, session.user, &mut *db).await?;
    Ok(Json(ok))
}

////////////////////////////////////////////////////////////////////////////////
// Signup controls, for admins etc

#[derive(Debug, Clone, Serialize, Deserialize, JsonSchema)]
#[serde(tag = "tag", content = "content")]
pub enum UpdateSignup {
    /// Update the player, team, or seed value, for existing signups
    SetSignup(SetSignup),
    /// Add some new signups
    AddSignup(AddSignup),
    /// Drop a signup that has had no matches yet
    /// Throws an error if there are matches completed already
    DropSignup(Uuid),
    /// Forfeit a signup in a running cup
    /// Drops them if the cup is not running
    ForfeitSignup(Uuid),
}

/// Set the signups
#[openapi]
#[post("/cup/<id>/update_signups", format = "json", data = "<updates>")]
pub async fn cup_update_signups(
    id: Uuid,
    updates: Json<Vec<UpdateSignup>>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> Result<Json<bool>, db::Error> {
    type US = UpdateSignup;
    let mut db = db.inner().acquire().await?;
    Cup::require_ownership(id, sess.user, &mut *db).await?;
    let mut tx = db.begin().await?;
    for u in updates.0 {
        log::debug!("performing signup update {:?}", u);
        match u {
            US::SetSignup(set) => {
                Cup::set_signup(set, &mut *tx).await?;
            }
            US::AddSignup(add) => {
                Cup::add_signup(id, add, &mut *tx).await?;
            }
            US::DropSignup(signup_id) => {
                // will error out if the delete fails, i.e. matches are
                // connected to this signup
                query!("DELETE FROM cup_signup WHERE id = $1", signup_id)
                    .execute(&mut tx)
                    .await?;
            }
            US::ForfeitSignup(signup_id) => {
                Cup::forfeit_signup(id, signup_id, &mut *tx).await?;
            }
        }
    }
    tx.commit().await?;
    Ok(Json(true))
}

/// Find what matches are finished for the given cup and signups
#[openapi]
#[get("/cup_finished_matches_for_signups/<signup_ids>")]
pub async fn cup_finished_matches_for_signups(
    signup_ids: Uuids,
    db: &State<DB>,
) -> db::Result<Json<Vec<CupMatch2p>>> {
    let rows = query_as!(
        CupMatch2pRow,
        "
            SELECT m.*
            FROM cup_match_2p m
            WHERE (m.low_id = ANY($1) OR m.high_id = ANY($1)) AND m.is_scored
            ORDER BY updated_at DESC
        ",
        &signup_ids.0,
    )
    .fetch_all(db.inner())
    .await?;
    Ok(Json(rows.iter().map(CupMatch2p::from_row).collect()))
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct PlayerFinishedMatch {
    match_: CupMatch2p,
    am_high: bool,
}

/// Find what matches are finished for the given cup and players
#[openapi]
#[get("/cup_finished_matches_for_players/<player_ids>")]
pub async fn cup_finished_matches_for_players(
    player_ids: Uuids,
    db: &State<DB>,
) -> db::Result<Json<Vec<PlayerFinishedMatch>>> {
    let player_ids = player_ids.0;
    let db = db.inner();
    let rows = query!(
        "
            SELECT to_jsonb((m.*)) AS m, (m.high_id = cs.id) AS am_high
            FROM cup_match_2p m JOIN cup_signup cs
            ON (m.low_id = cs.id OR m.high_id = cs.id)
            WHERE cs.player_id = ANY($1) AND m.is_scored AND m.low_id IS NOT NULL AND m.high_id IS NOT NULL
            ORDER BY updated_at DESC
            LIMIT 8
        ",
        &player_ids,
    )
    .fetch_all(db)
    .await?;
    let rows = rows
        .into_iter()
        .map(|f| PlayerFinishedMatch {
            match_: CupMatch2p::from_row(
                serde_json::from_value::<CupMatch2pRow>(f.m.unwrap()).unwrap(),
            ),
            am_high: f.am_high.unwrap(),
        })
        .collect_vec();
    Ok(Json(rows))
}
