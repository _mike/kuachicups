use super::paginate::{Paged, PaginateApi};
use super::player::AdminSession;
use crate::db;
use crate::db::{ById, Game, GameMode, HasParent, Result, DB};
use crate::handlers::csrf::ProtectCsrf;
use crate::handlers::uuids::Uuids;
use itertools::Itertools;
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_json::to_value;
use sqlx::{query, query_as, Acquire};
use std::marker::PhantomData;
use uuid::Uuid;

#[openapi]
#[get("/games?<page>")]
pub async fn games_page(page: Option<i64>, db: &State<DB>) -> Option<Json<Paged<Uuid>>> {
    Game::get_page(db.inner(), page).await
}

#[openapi]
#[get("/games?all")]
pub async fn games_all(db: &State<DB>) -> Option<Json<Vec<Game>>> {
    Game::get_all(db.inner()).await
}

#[openapi]
#[get("/games/<ids>", rank = 2)]
pub async fn games(ids: Uuids, db: &State<DB>) -> Option<Json<Vec<Game>>> {
    Game::get_by_ids(db.inner(), &ids.0).await
}

#[openapi]
#[get("/games/<id>/modes", rank = 1)]
pub async fn game_modes(id: Uuid, db: &State<DB>) -> Result<Json<Vec<GameMode>>> {
    GameMode::by_parent_id(db.inner(), id).await.map(Json)
}

#[openapi]
#[get("/games/modes/<id>", rank = 0)]
pub async fn game_mode_by_id(id: Uuid, db: &State<DB>) -> Result<Json<GameMode>> {
    GameMode::by_id(db.inner(), id).await.map(Json)
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct KDCSplitGeneratorForm {
    pub split_number: i32,
    pub maps: Vec<KDCSplitMap>,
}

#[derive(Serialize, Deserialize, JsonSchema)]
pub struct KDCSplitMap {
    pub title: String,
    pub slug: String,
}

#[openapi]
#[post("/games/kdc-split-pickban-generate", format = "json", data = "<form>")]

pub async fn kdc_split_pickban_generate(
    form: Json<KDCSplitGeneratorForm>,
    db: &State<DB>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> db::Result<()> {
    let form = form.0;
    let db: &DB = db.inner();
    let mut transaction = db.begin().await?;
    let title_prefix = format!("KDC Y1-S{}", form.split_number);
    let slug_prefix = format!("kdc-y1-s{}", form.split_number);
    let mk_title = |bestof: u32| format!("{} BO{}", title_prefix, bestof);
    let mk_slug = |bestof: u32| format!("{}-bo{}", slug_prefix, bestof);
    let maps: Vec<serde_json::Value> = form
        .maps
        .into_iter()
        .map(|m| to_value(m).unwrap())
        .collect_vec();
    query!(
        r#"
        INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
          SELECT
          ($1::text) AS "name",
          ($2::text) AS "slug",
          m.id AS "game_mode",
          7,
          array[
            ('Map', 'map', (
              SELECT
                array_agg((r->>'title',r->>'slug')::pickban_pool_item)
              FROM
                unnest($3::jsonb[]) AS r
            ))::pickban_pool,
            ('Champion', 'champ', array[
              ('Anarki', 'anarki')::pickban_pool_item,
              ('Athena', 'athena')::pickban_pool_item,
              ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
              ('Clutch', 'clutch')::pickban_pool_item,
              ('Death Knight', 'dk')::pickban_pool_item,
              ('Doom Slayer', 'doom')::pickban_pool_item,
              ('Eisen', 'eisen')::pickban_pool_item,
              ('Galena', 'galena')::pickban_pool_item,
              ('Keel', 'keel')::pickban_pool_item,
              ('Nyx', 'nyx')::pickban_pool_item,
              ('Ranger', 'ranger')::pickban_pool_item,
              ('Scalebearer', 'scale')::pickban_pool_item,
              ('Slash', 'slash')::pickban_pool_item,
              ('Sorlag', 'sorlag')::pickban_pool_item,
              ('Strogg & Peeker', 'strogg')::pickban_pool_item,
              ('Visor', 'visor')::pickban_pool_item
            ])::pickban_pool
          ]::pickban_pool[] AS "pool",
          array[
            ('Map', 'high_pick', null), -- 0 - map in hand, result entered manually
            ('Map', 'low_pick', null),  -- 1 - first actual map pick is low player
            ('Map', 'high_pick', null), -- 2
            ('Map', 'low_pick', null),  -- 3
            ('Map', 'high_pick', null), -- 4
            ('Map', 'low_pick', null),  -- 5
            ('Map', 'high_pick', null), -- 6
            -- Map 1
            ('Champion', 'low_ban', 1),
            ('Champion', 'high_pick', 1),
            ('Champion', 'low_pick', 1),
            -- Map 2
            ('Champion', 'high_ban', 2),
            ('Champion', 'low_pick', 2),
            ('Champion', 'high_pick', 2),
            -- Map 3
            ('Champion', 'low_ban', 3),
            ('Champion', 'high_pick', 3),
            ('Champion', 'low_pick', 3),
            -- Map 4
            ('Champion', 'high_ban', 4),
            ('Champion', 'low_pick', 4),
            ('Champion', 'high_pick', 4),
            -- Map 5
            ('Champion', 'low_pick', 5),
            ('Champion', 'high_pick', 5),
            -- Map 6
            ('Champion', 'high_pick', 6),
            ('Champion', 'low_pick', 6)
          ]::pickban_step[] AS "steps"
          FROM game g JOIN game_mode m ON m.game_id = g.id
          WHERE g.name = 'Quake Champions' AND m.name = 'Duel'
       "#,
        mk_slug(7),
        mk_title(7),
        &maps,
    )
    .execute(&mut *transaction)
    .await?;

    query!(
        r#"
        INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
          SELECT
          ($1::text) AS "name",
          ($2::text) AS "slug",
          m.id AS "game_mode",
          1,
          array[
            ('Map', 'map', (
              SELECT
                array_agg((r->>'title',r->>'slug')::pickban_pool_item)
              FROM
                unnest($3::jsonb[]) AS r
            ))::pickban_pool,
            ('Champion', 'champ', array[
              ('Anarki', 'anarki')::pickban_pool_item,
              ('Athena', 'athena')::pickban_pool_item,
              ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
              ('Clutch', 'clutch')::pickban_pool_item,
              ('Death Knight', 'dk')::pickban_pool_item,
              ('Doom Slayer', 'doom')::pickban_pool_item,
              ('Eisen', 'eisen')::pickban_pool_item,
              ('Galena', 'galena')::pickban_pool_item,
              ('Keel', 'keel')::pickban_pool_item,
              ('Nyx', 'nyx')::pickban_pool_item,
              ('Ranger', 'ranger')::pickban_pool_item,
              ('Scalebearer', 'scale')::pickban_pool_item,
              ('Slash', 'slash')::pickban_pool_item,
              ('Sorlag', 'sorlag')::pickban_pool_item,
              ('Strogg & Peeker', 'strogg')::pickban_pool_item,
              ('Visor', 'visor')::pickban_pool_item
            ])::pickban_pool
          ]::pickban_pool[] AS "pool",
          array[
            ('Map', 'low_ban', null),                           -- 0
            ('Map', 'high_ban', null),                          -- 1
            ('Map', 'low_ban', null),                           -- 2
            ('Map', 'high_ban', null),                          -- 3
            ('Map', 'low_ban', null),                           -- 4
            ('Map', 'high_pick', null),                         -- 5
            ('Champion', 'high_ban', 5),
            ('Champion', 'low_ban', 5),
            ('Champion', 'high_pick', 5),
            ('Champion', 'low_pick', 5)
          ]::pickban_step[] AS "steps"
          FROM game g JOIN game_mode m ON m.game_id = g.id
          WHERE g.name = 'Quake Champions' AND m.name = 'Duel'
          "#,
        mk_slug(1),
        mk_title(1),
        &maps,
    )
    .execute(&mut *transaction)
    .await?;

    query!(
        r#"
        INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
          SELECT
          ($1::text) AS "name",
          ($2::text) AS "slug",
          m.id AS "game_mode",
          3,
          array[
            ('Map', 'map', (
              SELECT
                array_agg((r->>'title',r->>'slug')::pickban_pool_item)
              FROM
                unnest($3::jsonb[]) AS r
            ))::pickban_pool,
            ('Champion', 'champ', array[
              ('Anarki', 'anarki')::pickban_pool_item,
              ('Athena', 'athena')::pickban_pool_item,
              ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
              ('Clutch', 'clutch')::pickban_pool_item,
              ('Death Knight', 'dk')::pickban_pool_item,
              ('Doom Slayer', 'doom')::pickban_pool_item,
              ('Eisen', 'eisen')::pickban_pool_item,
              ('Galena', 'galena')::pickban_pool_item,
              ('Keel', 'keel')::pickban_pool_item,
              ('Nyx', 'nyx')::pickban_pool_item,
              ('Ranger', 'ranger')::pickban_pool_item,
              ('Scalebearer', 'scale')::pickban_pool_item,
              ('Slash', 'slash')::pickban_pool_item,
              ('Sorlag', 'sorlag')::pickban_pool_item,
              ('Strogg & Peeker', 'strogg')::pickban_pool_item,
              ('Visor', 'visor')::pickban_pool_item
            ])::pickban_pool
          ]::pickban_pool[] AS "pool",
          array[
            ('Map', 'high_ban', null),                          -- 
            ('Map', 'low_ban', null),                           -- 
            ('Map', 'high_pick', null),                         -- 2
            ('Map', 'low_pick', null),                          -- 3
            ('Map', 'high_ban', null),                          -- 
            ('Map', 'low_pick', null),                          -- 5
            ('Champion', 'high_ban', 2),
            ('Champion', 'low_pick', 2),
            ('Champion', 'high_pick', 2),
            ('Champion', 'low_ban', 3),
            ('Champion', 'high_pick', 3),
            ('Champion', 'low_pick', 3),
            ('Champion', 'high_ban', 5),
            ('Champion', 'low_pick', 5),
            ('Champion', 'high_pick', 5)
          ]::pickban_step[] AS "steps"
          FROM game g JOIN game_mode m ON m.game_id = g.id
          WHERE g.name = 'Quake Champions' AND m.name = 'Duel'
          "#,
        mk_slug(3),
        mk_title(3),
        &maps,
    )
    .execute(&mut *transaction)
    .await?;

    query!(
        r#"
        INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
          SELECT
          ($1::text) AS "name",
          ($2::text) AS "slug",
          m.id AS "game_mode",
          5,
          array[
            ('Map', 'map', (
              SELECT
                array_agg((r->>'title',r->>'slug')::pickban_pool_item)
              FROM
                unnest($3::jsonb[]) AS r
            ))::pickban_pool,
            ('Champion', 'champ', array[
              ('Anarki', 'anarki')::pickban_pool_item,
              ('Athena', 'athena')::pickban_pool_item,
              ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
              ('Clutch', 'clutch')::pickban_pool_item,
              ('Death Knight', 'dk')::pickban_pool_item,
              ('Doom Slayer', 'doom')::pickban_pool_item,
              ('Eisen', 'eisen')::pickban_pool_item,
              ('Galena', 'galena')::pickban_pool_item,
              ('Keel', 'keel')::pickban_pool_item,
              ('Nyx', 'nyx')::pickban_pool_item,
              ('Ranger', 'ranger')::pickban_pool_item,
              ('Scalebearer', 'scale')::pickban_pool_item,
              ('Slash', 'slash')::pickban_pool_item,
              ('Sorlag', 'sorlag')::pickban_pool_item,
              ('Strogg & Peeker', 'strogg')::pickban_pool_item,
              ('Visor', 'visor')::pickban_pool_item
            ])::pickban_pool
          ]::pickban_pool[] AS "pool",
          array[
            ('Map', 'high_ban', null),                          --
            ('Map', 'low_ban', null),                           --
            ('Map', 'high_pick', null),                         -- 2
            ('Map', 'low_pick', null),                          -- 3
            ('Map', 'high_pick', null),                         -- 4
            ('Map', 'low_pick', null),                          -- 5
            ('Map', 'high_pick', null),                         -- 6
            ('Champion', 'high_ban', 2),
            ('Champion', 'low_pick', 2),
            ('Champion', 'high_pick', 2),
            ('Champion', 'low_ban', 3),
            ('Champion', 'high_pick', 3),
            ('Champion', 'low_pick', 3),
            ('Champion', 'high_ban', 4),
            ('Champion', 'low_pick', 4),
            ('Champion', 'high_pick', 4),
            ('Champion', 'low_ban', 5),
            ('Champion', 'high_pick', 5),
            ('Champion', 'low_pick', 5),
            ('Champion', 'high_ban', 6),
            ('Champion', 'low_pick', 6),
            ('Champion', 'high_pick', 6)
          ]::pickban_step[] AS "steps"
          FROM game g JOIN game_mode m ON m.game_id = g.id
          WHERE g.name = 'Quake Champions' AND m.name = 'Duel'
          "#,
        mk_slug(5),
        mk_title(5),
        &maps,
    )
    .execute(&mut *transaction)
    .await?;

    transaction.commit().await?;
    Ok(())
}

/*
#[openapi]
#[get("/games/<id>/mc_pb_cfg_v1")]
pub async fn game_mode_match_control_cfg_v1(
    id: Uuid,
    db: &State<DB>,
) -> Result<Json<Vec<GameModeMatchControlConfig>>> {
    GameModeMatchControlConfig::by_parent_id(db.inner(), &id)
        .await
        .map(Json)
}
*/
