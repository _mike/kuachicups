pub mod cups;
pub mod game;
pub mod paginate;
pub mod player;
pub mod posts;
pub mod teams;

use crate::handlers::csrf::{ProtectCsrf, ProvideCsrf};
use rocket::get;
use rocket_okapi::openapi;

#[openapi]
#[get("/blank")]
pub fn blank(_csrf: ProvideCsrf) {}

#[openapi]
#[get("/test_csrf")]
pub fn test_csrf(_csrf: ProtectCsrf) {}
