use super::{
    paginate::{Paged, PaginateApi, PaginateSearchApi},
    player::{AdminSession, UserSession},
};
use crate::{
    db,
    db::{
        AcceptInviteTeamMember, InviteTeamMember, Page, Player, Role, SetTeamMember, Team,
        TeamCreateForm, TeamMember, TeamMemberInvite, TeamPlayer, TeamUpdateForm, DB,
    },
    handlers::{
        csrf::ProtectCsrf,
        uuids::Uuids,
    },
};
use rocket::{get, post, serde::json::Json, State};
use rocket_okapi::openapi;
use std::marker::PhantomData;
use uuid::Uuid;

#[openapi]
#[get("/teams/<ids>")]
pub async fn teams(ids: Uuids, db: &State<DB>) -> Option<Json<Vec<Team>>> {
    Team::get_by_ids(db.inner(), &ids.0).await
}

#[openapi]
#[get("/teams/page?<page>&<terms>")]
pub async fn teams_page(
    page: Option<i64>,
    terms: Option<String>,
    db: &State<DB>,
) -> Option<Json<Paged<Uuid>>> {
    if let Some(terms) = terms {
        Team::get_page_search(db.inner(), page, &terms).await
    } else {
        Team::get_page(db.inner(), page).await
    }
}

/// search for teams under a certain player
#[openapi]
#[get("/teams/<player_id>/page?<page>&<terms>")]
pub async fn player_teams_page(
    player_id: Uuid,
    page: Option<i64>,
    terms: Option<String>,
    db: &State<DB>,
) -> db::Result<Json<Paged<Uuid>>> {
    let mut db = db.inner().acquire().await?;
    let rows = if let Some(terms) = terms {
        Team::page_search_by_player(player_id, Page::new(page), &terms, &mut *db).await?
    } else {
        Team::page_by_player(player_id, Page::new(page), &mut *db).await?
    };
    Ok(Json(rows))
}

#[openapi]
#[get("/team/<id>/players")]
pub async fn players(id: Uuid, db: &State<DB>) -> Option<Json<Vec<TeamPlayer>>> {
    Some(Json(Team::players(db.inner(), &id).await.ok()?))
}

#[openapi]
#[get("/team/<id>/invites")]
pub async fn invites(
    id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<Vec<TeamMemberInvite>>> {
    let db = db.inner();
    sess.require_team_role(db, id, Role::Admin).await?;
    Ok(Json(Team::invites(id, db).await?))
}

#[openapi]
#[post("/team/<id>/delete")]
pub async fn delete(
    id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<()> {
    let mut db = db.inner().acquire().await?;
    sess.require_team_role(&mut *db, id, Role::Admin).await?;
    Team::delete(id, &mut *db).await?;
    Ok(())
}

#[openapi]
#[post("/team/create", data = "<team>", format = "json")]
pub async fn create(
    team: Json<TeamCreateForm>,
    db: &State<DB>,
    _sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<Team>> {
    Ok(Json(Team::create(team.0, db.inner()).await?))
}

#[openapi]
#[post("/team/update", data = "<team>", format = "json")]
pub async fn update(
    team: Json<TeamUpdateForm>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<Team>> {
    let db = db.inner();
    sess.require_team_role(db, team.0.id, Role::Admin).await?;
    Ok(Json(Team::update(team.0, db).await?))
}

#[openapi]
#[post("/team/set_member", data = "<member>", format = "json")]
pub async fn set_member(
    member: Json<SetTeamMember>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    sess.require_team_role(db, member.0.team_id, Role::Admin)
        .await?;
    Ok(Json(Team::set_member(member.0, false, db).await?))
}

#[openapi]
#[post("/team/<id>/leave")]
pub async fn leave(
    id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    sess.require_team_role(db, id, Role::Player).await?;
    Ok(Json(
        Team::set_member(
            SetTeamMember {
                team_id: id,
                player_id: sess.user,
                player_role: None,
            },
            false,
            db,
        )
        .await?,
    ))
}

#[openapi]
#[post("/team/override_member", data = "<member>", format = "json")]
pub async fn override_member(
    member: Json<SetTeamMember>,
    db: &State<DB>,
    _sess: AdminSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    Ok(Json(Team::set_member(member.0, true, db).await?))
}

#[openapi]
#[post("/team/make_invite", data = "<invite>", format = "json")]
pub async fn make_invite(
    invite: Json<InviteTeamMember>,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    if sess.user != invite.0.invite_by_player_id {
        return Err(db::Error::SomeError("must own invite"));
    }
    let db = db.inner();
    sess.require_team_role(db, invite.0.team_id, Role::Admin)
        .await?;
    Ok(Json(Team::make_invite(invite.0, db).await?))
}

#[openapi]
#[post("/team/<team_id>/reject_invite")]
pub async fn reject_invite(
    team_id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    Ok(Json(
        Team::reject_invite(
            AcceptInviteTeamMember {
                player_id: sess.user,
                team_id: team_id,
            },
            db,
        )
        .await?,
    ))
}

#[openapi]
#[post("/team/<team_id>/accept_invite")]
pub async fn accept_invite(
    team_id: Uuid,
    db: &State<DB>,
    sess: UserSession,
    _csrf: ProtectCsrf,
) -> db::Result<Json<u64>> {
    let db = db.inner();
    Ok(Json(
        Team::accept_invite(
            AcceptInviteTeamMember {
                player_id: sess.user,
                team_id: team_id,
            },
            db,
        )
        .await?,
    ))
}
