use async_trait::async_trait;
use cookie::Expiration;
use csrf::{ChaCha20Poly1305CsrfProtection, CsrfProtection};
// pub use csrf::{CSRF_COOKIE_NAME, CSRF_HEADER};
use data_encoding::BASE64;
use rocket::{
    fairing,
    fairing::Fairing,
    http::{Cookie, CookieJar, Method, SameSite, Status},
    outcome::Outcome::*,
    request::{FromRequest, Outcome, Request},
    State,
};
use rocket_okapi::OpenApiFromRequest;
use std::borrow::Cow;

pub const CSRF_COOKIE_NAME: &'static str = "csrf";
pub const CSRF_HEADER_NAME: &'static str = "X-CSRF-Token";
pub const CSRF_TTL_SECS: i64 = 60 * 60 * 24 * 7;

#[derive(Debug, OpenApiFromRequest)]
pub struct ProvideCsrf;

#[async_trait]
impl<'r> FromRequest<'r> for ProvideCsrf {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let csrf = <&'r State<ChaCha20Poly1305CsrfProtection>>::from_request(request).await;
        log::debug!("ProvideCsrf/existing csrf check {:?}", csrf);
        match csrf {
            Success(csrf) => {
                provide_csrf(&csrf, request.cookies());
                Success(ProvideCsrf)
            }
            Failure(err) => Failure(err),
            Forward(err) => Forward(err),
        }
    }
}

pub fn csrf_cookie<'c>(cookie: impl Into<Cow<'c, str>>) -> Cookie<'c> {
    Cookie::build(CSRF_COOKIE_NAME, cookie)
        .path("/")
        .same_site(SameSite::Lax)
        .expires(Expiration::Session)
        .finish()
}

pub fn csrf_header<'c>(token: impl Into<Cow<'c, str>>) -> Cookie<'c> {
    Cookie::build(CSRF_HEADER_NAME, token)
        .path("/")
        .same_site(SameSite::Strict)
        .expires(Expiration::Session)
        .finish()
}

pub fn provide_csrf(csrf: &ChaCha20Poly1305CsrfProtection, cookies: &CookieJar) {
    let (token, cookie) = csrf.generate_token_pair(None, CSRF_TTL_SECS).unwrap();
    let token = token.b64_string();
    let cookie = cookie.b64_string();
    cookies.add(csrf_cookie(cookie));
    cookies.add(csrf_header(token));
}

#[derive(Debug, OpenApiFromRequest)]
pub struct ProtectCsrf;

#[async_trait]
impl<'r> FromRequest<'r> for ProtectCsrf {
    type Error = ();

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        use rocket::outcome::Outcome::*;
        let csrf = <&'r State<ChaCha20Poly1305CsrfProtection>>::from_request(request).await;
        match csrf {
            Success(csrf) => {
                let cookies = request.cookies();
                let headers = request.headers();
                let token = headers.get(CSRF_HEADER_NAME).last();
                let cookie = &cookies.get(CSRF_COOKIE_NAME).map(|c| c.value().to_string());
                match (cookie, token) {
                    (Some(cookie), Some(token)) => {
                        if validate_csrf(csrf.inner(), token, cookie) {
                            log::info!("csrf ok!\ncookie={:?}\ntoken={:?}", cookie, token);
                            Success(ProtectCsrf)
                        } else {
                            log::error!(
                                "csrf exists but is invalid\ncookie={:?}\ntoken={:?}",
                                cookie,
                                token
                            );
                            Failure((Status::Forbidden, ()))
                        }
                    }
                    _ => {
                        log::error!(
                            "missing csrf parameter\ncookie={:?}\ntoken={:?}",
                            cookie,
                            token
                        );
                        Failure((Status::Forbidden, ()))
                    }
                }
            }
            Failure(err) => {
                log::error!("csrf error {:#?}", err);
                Failure(err)
            }
            Forward(err) => {
                log::error!("csrf forward error {:#?}", err);
                Failure((Status::Forbidden, err))
            }
        }
    }
}

pub fn validate_csrf(csrf: &ChaCha20Poly1305CsrfProtection, token: &str, cookie: &str) -> bool {
    let token = BASE64
        .decode(token.as_bytes())
        .ok()
        .and_then(|t| csrf.parse_token(&t).ok());
    let cookie = BASE64
        .decode(cookie.as_bytes())
        .ok()
        .and_then(|c| csrf.parse_cookie(&c).ok());
    match (token, cookie) {
        (Some(token), Some(cookie)) => csrf.verify_token_pair(&token, &cookie),
        _ => false,
    }
}
