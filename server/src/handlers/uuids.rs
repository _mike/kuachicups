use crate::auto_froms;
use async_trait::async_trait;
use okapi::openapi3::{Parameter, ParameterValue};
use rocket::form::{FromFormField, ValueField};
use rocket::http::RawStr;
use rocket::request::FromParam;
use rocket::serde::json::Json;
use rocket_okapi::gen::OpenApiGenerator;
use rocket_okapi::request::{OpenApiFromFormField, OpenApiFromParam};
use rocket_okapi::Result as OResult;
use schemars::{gen::SchemaGenerator, schema::Schema, JsonSchema};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

pub struct List<T>(pub Vec<T>);
pub type Uuids = List<Uuid>;
pub type Listi32 = List<i32>;
auto_froms!(Uuids, { Vec<Uuid> = List; });

impl<T: JsonSchema> JsonSchema for List<T> {
    fn schema_name() -> String {
        Vec::<T>::schema_name()
    }
    fn json_schema(gen: &mut SchemaGenerator) -> Schema {
        Vec::<T>::json_schema(gen)
    }
    fn is_referenceable() -> bool {
        false
    }
}

impl<'r> FromParam<'r> for Uuids {
    type Error = &'r str;
    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        let uuids: Result<Vec<Uuid>, Self::Error> = String::from_param(param)
            .unwrap() // <String as FromParam>::Error is Infallible
            .split(',')
            .map(|u| Uuid::parse_str(u).or(Err(param)))
            .collect();
        Ok(List(uuids?))
    }
}

impl<'r> FromParam<'r> for Listi32 {
    type Error = &'r str;
    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        let uuids: Result<Vec<i32>, Self::Error> = String::from_param(param)
            .unwrap() // <String as FromParam>::Error is Infallible
            .split(',')
            .map(|u| i32::from_str_radix(u, 10).or(Err(param)))
            .collect();
        Ok(List(uuids?))
    }
}

#[async_trait]
impl<'v, T: Sized + Send> FromFormField<'v> for List<T>
where
    List<T>: FromParam<'v, Error = &'v str>,
{
    fn from_value(field: ValueField<'v>) -> rocket::form::Result<'v, Self> {
        match <List<T> as FromParam<'v>>::from_param(field.value) {
            Ok(v) => Ok(v),
            Err(err) => {
                let mut errs = rocket::form::Errors::new();
                errs.push(
                    rocket::form::Error::validation(err)
                        .with_name(field.name)
                        .with_value(field.value),
                );
                Err(errs)
            }
        }
    }

    fn default() -> Option<Self> {
        Some(List(Vec::new()))
    }
}
