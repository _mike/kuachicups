use super::csrf::{
    csrf_cookie, csrf_header, ProtectCsrf, ProvideCsrf, CSRF_COOKIE_NAME, CSRF_HEADER_NAME,
};
use crate::app::{DiscordClientBuilder, SettingCache};
use crate::db::{Player, DB};
use oauth2::PkceCodeVerifier;
use rocket::{
    fs::NamedFile,
    get,
    http::uri::Segments,
    http::{Cookie, CookieJar, SameSite, Status},
    request::FromSegments,
    response::Redirect,
    uri, State,
};
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::ops::Add;
use std::time::Duration as StdDuration;
use time::{Duration, OffsetDateTime};
use twilight_model::id::marker::UserMarker;
use twilight_model::id::Id;

use super::api::player::{
    UserSession, OAUTH2_DEFAULT_EXPIRY, OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME,
    USER_SESSION_COOKIE_NAME,
};

#[derive(Serialize, Deserialize)]
struct OAuth2State {
    dest: String,
    csrf: String,
}

#[derive(Debug)]
pub enum ClientIndexPath {
    Index,
    StreamOverlay,
    StreamRemote,
}

impl FromSegments<'_> for ClientIndexPath {
    type Error = ();
    #[inline(always)]
    fn from_segments(
        segments: Segments<'_, rocket::http::uri::fmt::Path>,
    ) -> Result<Self, Self::Error> {
        use std::ffi::OsStr;
        let pb = segments.to_path_buf(false).map_err(|_| ())?;
        if pb.starts_with("api") || pb.extension().is_some() {
            return Err(());
        } else if pb == OsStr::new("stream/overlay") {
            log::debug!("{:?} => ClientIndexPath::StreamOverlay", pb);
            Ok(ClientIndexPath::StreamOverlay)
        } else if pb == OsStr::new("stream/remote") {
            log::debug!("{:?} => ClientIndexPath::StreamRemote", pb);
            Ok(ClientIndexPath::StreamRemote)
        } else {
            log::debug!("{:?} => ClientIndexPath::Index", pb);
            Ok(ClientIndexPath::Index)
        }
    }
}

#[get("/", rank = 10, format = "html")]
pub async fn index(
    settings: &State<SettingCache>,
    _csrf: ProvideCsrf,
) -> std::io::Result<NamedFile> {
    log::debug!("serving index file {:?}", settings.index_file);
    let file = NamedFile::open(settings.index_file.clone()).await?;
    Ok(file)
}

#[get("/<path..>", rank = 30, format = "html")]
pub async fn client_index_anywhere(
    path: ClientIndexPath,
    settings: &State<SettingCache>,
    csrf: ProvideCsrf,
) -> std::io::Result<NamedFile> {
    log::debug!("client_index_anywhere serving {:?}", path);
    match path {
        ClientIndexPath::Index => index(settings, csrf).await,
        ClientIndexPath::StreamOverlay => NamedFile::open(&settings.overlay_index_file).await,
        ClientIndexPath::StreamRemote => NamedFile::open(&settings.overlay_remote_index_file).await,
    }
}

#[get("/ads.txt")]
pub async fn ads_txt(settings: &State<SettingCache>) -> Option<NamedFile> {
    NamedFile::open(settings.ads_txt.clone()).await.ok()
}

#[get("/reset_csrf", format = "html")]
pub async fn reset_csrf(cookies: &CookieJar<'_>) -> Redirect {
    // cookies.remove(mut cookie: Cookie<'static>)
    let c = csrf_cookie("");
    let h = csrf_header("");
    cookies.remove(c);
    cookies.remove(h);
    cookies.remove_private(UserSession::cookie(None));
    Redirect::to(uri!("/"))
}

#[get("/logged_in?<code>&<state>", format = "html")]
pub async fn logged_in(
    code: String,
    state: String,
    cookies: &CookieJar<'_>,
    db: &State<DB>,
    client: &State<oauth2::basic::BasicClient>,
    protect: &State<csrf::ChaCha20Poly1305CsrfProtection>,
    dcb: &State<DiscordClientBuilder>,
    settings: &State<SettingCache>,
) -> Result<NamedFile, (Status, NamedFile)> {
    let r = logged_in_simple_err(code, state, cookies, db, client, protect, dcb, settings).await;
    match r {
        Ok(r) => Ok(r),
        Err((status, err)) => {
            let c = csrf_cookie("");
            let h = csrf_header("");
            cookies.remove(c);
            cookies.remove(h);
            cookies.remove_private(UserSession::cookie(None));
            let now = rocket::time::OffsetDateTime::now_utc().add(StdDuration::from_secs(5));
            log::error!("logged_in error status={:?} err={:#?}", status, err);
            cookies.add(
                Cookie::build("error", err)
                    .expires(cookie::Expiration::DateTime(now))
                    .http_only(false)
                    .finish(),
            );
            Err((
                status,
                index(settings, ProvideCsrf)
                    .await
                    .expect("Could not get index!"),
            ))
        }
    }
}

pub async fn logged_in_simple_err(
    code: String,
    state: String,
    cookies: &CookieJar<'_>,
    db: &State<DB>,
    client: &State<oauth2::basic::BasicClient>,
    protect: &State<csrf::ChaCha20Poly1305CsrfProtection>,
    dcb: &State<DiscordClientBuilder>,
    settings: &State<SettingCache>,
) -> Result<NamedFile, (Status, String)> {
    log::info!("got auth code {:#?}", code);

    use super::csrf::{provide_csrf, validate_csrf};
    use oauth2::{
        basic::BasicTokenResponse, reqwest::async_http_client, AuthorizationCode, CodeTokenRequest,
        TokenResponse,
    };

    fn forbidden(s: &'static str) -> (Status, String) {
        (Status::Forbidden, s.to_string())
    }

    // 1. Verify CSRF
    let csrf_cookie = &cookies
        .get(CSRF_COOKIE_NAME)
        .map_or(Err(forbidden("No CSRF cookie")), |c| {
            Ok(c.value().to_string())
        })?;

    log::info!("got csrf cookie {:#?}", csrf_cookie);

    if !validate_csrf(&protect, &state, csrf_cookie) {
        return Err(forbidden("Invalid CSRF cookie"));
    }

    // 2. Get the PKCE verifier which we stored as an encrypted cookie on the client
    let pkce_verifier = PkceCodeVerifier::new(
        cookies
            .get_private(OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME)
            .ok_or(forbidden("No PKCE verifier cookie"))?
            .value()
            .to_string(),
    );

    log::info!("got pkce verifier");

    // 3. Perform OAuth2 token exchange
    let token: BasicTokenResponse = client
        .exchange_code(AuthorizationCode::new(code))
        .set_pkce_verifier(pkce_verifier)
        .request_async(async_http_client)
        .await
        .map_err(|err| {
            log::error!("Cannot create BasicTokenResponse due to\n{:#?}", err);
            forbidden("Could not perform OAuth2 token exchange")
        })?;

    log::info!("got token");

    // 4. Remove the PKCE verifier cookie
    cookies.remove_private(
        Cookie::build(OAUTH2_PKCE_CODE_VERIFIER_COOKIE_NAME, "")
            .path("/logged_in")
            .same_site(SameSite::Lax)
            .finish(),
    );

    log::info!("removed pkce");

    // 10. Provide new CSRF cookie/header pair for the client app to use
    provide_csrf(&protect, cookies);

    log::info!("provided csrf");

    let max_age: rocket::time::Duration = {
        let std: StdDuration = token.expires_in().unwrap_or(OAUTH2_DEFAULT_EXPIRY);
        // When upgrading to time 0.2 use
        std.try_into()
            .ok()
            .ok_or(forbidden("Could not create max_age"))?
    };

    log::info!("made max_age {:#?}", max_age);

    // Now we actually create a user!
    //
    // 11. Get the user info from discord
    let discord = dcb.client_bearer(token.access_token().secret());

    let user = discord
        .current_user()
        .await
        .or(Err(forbidden("Could not get current discord user")))?
        .model()
        .await
        .or(Err(forbidden("Could deserialize the current discord user")))?;

    if is_special_fella(user.id) {
        log::debug!(
            "special fella discord info:
            discord user: {user:#?}
            token: {token:#?}"
        );
    }

    log::debug!("user: {user:#?}");

    let player = Player::add_from_discord(
        &user,
        &mut *db
            .acquire()
            .await
            .or(Err(forbidden("could not acquire database")))?,
    )
    .await
    .map_err(|err| (Status::Forbidden, format!("{:?}", err)))?;

    log::info!("player: {player:#?}");

    let session = UserSession {
        response: token,
        user: player.id,
    };

    // 12. Set the session!
    cookies.add_private(
        Cookie::build(
            USER_SESSION_COOKIE_NAME,
            serde_json::to_string(&session)
                .ok()
                .ok_or(forbidden("Cannot create session value"))?,
        )
        .path("/")
        .http_only(true)
        .same_site(SameSite::Strict)
        .max_age(max_age)
        .finish(),
    );

    // 13. Now just serve the client app
    // Note no ProvideCsrf here!
    NamedFile::open(settings.index_file.clone())
        .await
        .ok()
        .ok_or((Status::NotFound, "logged_in: No index".to_string()))
}

pub fn is_special_fella(id: Id<UserMarker>) -> bool {
    id == Id::new(115109323352440840) || // mordie
    id == Id::new(293892996154392579) || // redoxide
    id == Id::new(156626228319748096)
}
