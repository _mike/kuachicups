use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug, Clone, Serialize, Deserialize)]
pub struct UpdateTime {
    pub updated_at: OffsetDateTime,
    pub created_at: OffsetDateTime,
}

impl UpdateTime {
    pub fn new() -> Self {
        let now = OffsetDateTime::now_utc();
        UpdateTime {
            updated_at: now,
            created_at: now,
        }
    }
}
