use crate::config::Config;
use crate::db;
use crate::db::{Player, PostMedia, DB, MEDIA_CHANNEL};
use futures::stream::StreamExt;
use sqlx::postgres::PgPool;
use std::collections::HashMap;
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;
use twilight_gateway::{Event, Intents, Shard, ShardId};
use twilight_http::Client;
use twilight_model::channel::message::{Message, Reaction, ReactionType};
use twilight_model::channel::Channel;
use twilight_model::gateway::payload::incoming::{
    GuildCreate, MemberAdd, ReactionAdd, ReactionRemove,
};
use twilight_model::gateway::payload::incoming::{MemberRemove, MemberUpdate, MessageCreate};
use twilight_model::gateway::GatewayReaction;
use twilight_model::guild::{Guild, Member};
use twilight_model::id::marker::{
    ChannelMarker, EmojiMarker, GuildMarker, MessageMarker, RoleMarker,
};
use twilight_model::id::Id;
use twilight_standby::Standby;

const GUILD: Id<GuildMarker> = Id::new(649789878925525013);
const ADD_EMOJI: Id<EmojiMarker> = Id::new(767932236497551442);
const ADD_EMOJI_ANIMATED: bool = false;
const ADD_EMOJI_NAME: &'static str = "gg";
const ADMIN_ROLE: Id<RoleMarker> = Id::new(649792108831703041);

pub async fn main(config: Config) -> Result<(), Box<dyn Error>> {
    let db = PgPool::connect(&config.db_uri).await?;
    let intents = Intents::GUILD_MESSAGES
        | Intents::MESSAGE_CONTENT
        | Intents::GUILD_MESSAGE_REACTIONS
        // | Intents::GUILD_EMOJIS
        | Intents::GUILD_MEMBERS
        | Intents::GUILDS;

    let client = Arc::new(Client::new(config.discord_bot_token.clone()));
    let mut shard = Shard::new(ShardId::ONE, config.discord_bot_token, intents);

    let mut channel_names = HashMap::<Id<ChannelMarker>, String>::new();

    loop {
        if let Ok(event) = shard.next_event().await {
            match &event {
                Event::GuildCreate(guildcreate) => {
                    let Guild {
                        channels, members, ..
                    } = &guildcreate.0;
                    for channel in channels {
                        if let Some(name) = &channel.name {
                            channel_names.insert(channel.id, name.clone());
                        }
                    }
                    // Update player list in database
                    for member in members {
                        log::debug!("member {:?}", member);
                    }
                }

                // Event::MemberAdd(box MemberAdd {
                //     member,
                //     guild_id: _,
                // }) => {
                //     log::debug!("add member {:?}", member);
                // }
                Event::MemberRemove(member) => {
                    log::debug!("remove member {:?}", member);
                }

                // Event::MemberUpdate(box member) => {
                //     log::debug!("update member {:?}", member);
                // }
                Event::ReactionAdd(reaction_add) => match &reaction_add.0 {
                    GatewayReaction {
                        channel_id,
                        message_id,
                        emoji:
                            ReactionType::Custom {
                                animated: emoji_animated,
                                id: emoji_id,
                                name: Some(emoji_name),
                            },
                        member: Some(Member { roles, user, .. }),
                        ..
                    } if *channel_id == MEDIA_CHANNEL
                        && *emoji_animated == ADD_EMOJI_ANIMATED
                        && *emoji_id == ADD_EMOJI
                        && &*emoji_name == ADD_EMOJI_NAME
                        && roles.iter().any(|r| *r == ADMIN_ROLE) =>
                    {
                        tokio::spawn(add_media_msg(
                            user.id.to_string(),
                            *channel_id,
                            *message_id,
                            db.clone(),
                            client.clone(),
                        ));
                    }
                    _ => {}
                },

                // Event::ReactionRemove(box ReactionRemove(GatewayReaction {
                //     channel_id,
                //     message_id,
                //     user_id,
                //     emoji:
                //         ReactionType::Custom {
                //             animated: emoji_animated,
                //             id: emoji_id,
                //             name: Some(emoji_name),
                //         },
                //     ..
                // })) if *channel_id == MEDIA_CHANNEL
                //     && *emoji_animated == ADD_EMOJI_ANIMATED
                //     && *emoji_id == ADD_EMOJI
                //     && &*emoji_name == ADD_EMOJI_NAME
                //     && client
                //         .guild_member(GUILD, *user_id)
                //         .exec()
                //         .await?
                //         .model()
                //         .await?
                //         .roles
                //         .iter()
                //         .any(|r| *r == ADMIN_ROLE) =>
                // {
                //     tokio::spawn(remove_media_msg(
                //         user_id.to_string(),
                //         *channel_id,
                //         *message_id,
                //         db.clone(),
                //         client.clone(),
                //     ));
                // }

                // Event::MessageCreate(box MessageCreate(Message {
                //     channel_id,
                //     author,
                //     content,
                //     timestamp,
                //     member,
                //     mentions,
                //     ..
                // })) => {
                //     let chan = &channel_names.get(channel_id);
                //     if let Some(channel_name) = chan {
                //         log::debug!(
                //             "#{} {} <@{}#{:04}> {}",
                //             channel_name,
                //             timestamp.iso_8601(),
                //             author.name,
                //             author.discriminator,
                //             content
                //         );
                //         // TODO bot commands implemented here
                //         //
                //         // Possibilities:
                //         // - Site signups
                //         // - Pickups
                //         // - Site + player + ELO-tracked pickups
                //         // - Quick oneoff templates for admins, like
                //         //   !create groups:4 single_elim:4 This is the title @mention1 @mention2 @mention3 @mention4
                //         //
                //     }

                //     // !signup command
                //     // Syntax: !signup @user @user2 @user3
                //     if member
                //         .iter()
                //         .map(|m| m.roles.iter())
                //         .flatten()
                //         .any(|r| *r == ADMIN_ROLE)
                //         && content.starts_with("!signup")
                //     {
                //         for mention in mentions.into_iter() {
                //             Player::add_from_discord_mention(mention, &mut *db.acquire().await?)
                //                 .await?;
                //         }
                //     }
                // }
                _ => {}
            }
        }
    }
}

async fn add_media_msg(
    user_id: String,
    channel_id: Id<ChannelMarker>,
    message_id: Id<MessageMarker>,
    db: DB,
    client: Arc<Client>,
) -> Result<(), db::Error> {
    log::info!("adding media {}", message_id);
    let player_id = Player::by_discord_id(&db, &user_id).await?.id;
    let is_admin = Player::is_admin(&db, player_id).await?.is_admin;
    if is_admin {
        if let Ok(message) = client
            .message(channel_id, message_id)
            .exec()
            .await?
            .model()
            .await
        {
            let post = PostMedia::create(player_id, message, &db).await?;
            log::info!("added media {:#?}", post);
        }
    }
    Ok(())
}

async fn remove_media_msg(
    user_id: String,
    channel_id: Id<ChannelMarker>,
    message_id: Id<MessageMarker>,
    db: DB,
    client: Arc<Client>,
) -> Result<(), db::Error> {
    log::info!("removing media {}", message_id);
    let player_id = Player::by_discord_id(&db, &user_id).await?.id;
    let is_admin = Player::is_admin(&db, player_id).await?.is_admin;
    if is_admin {
        if let Ok(message) = client
            .message(channel_id, message_id)
            .exec()
            .await?
            .model()
            .await
        {
            PostMedia::delete(message, &db).await?;
            log::info!("removed media {}", message_id);
        }
    }
    Ok(())
}
