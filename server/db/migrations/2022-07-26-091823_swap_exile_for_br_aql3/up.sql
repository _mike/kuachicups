
UPDATE pickbans
  SET pool = array [
     ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,          -- 0
      ('Blood Covenant', 'bc')::pickban_pool_item,      -- 1
      ('Blood Run', 'br')::pickban_pool_item,            -- 2
      ('Corrupted Keep', 'ck')::pickban_pool_item,      -- 3
      ('Deep Embrace', 'de')::pickban_pool_item,        -- 4
      ('Insomnia', 'insomnia')::pickban_pool_item,      -- 5
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item, -- 6
      ('Molten Falls', 'mf')::pickban_pool_item,        -- 7
      ('Vale of Pnath', 'vale')::pickban_pool_item      -- 8
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[]
  WHERE slug LIKE 'aql3-%';
