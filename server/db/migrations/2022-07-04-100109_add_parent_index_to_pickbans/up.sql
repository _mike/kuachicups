
ALTER TABLE cup_match_2p_pickban_step
    ADD CONSTRAINT cup_match_2p_pickban_step_unique_pickban_index UNIQUE (cup_match_id, pickban_index);

ALTER TABLE cup_match_2p_pickban_step
    ADD COLUMN parent_index integer;

-- require that whatever parent_index refers to (if non-null) is accounted for
ALTER TABLE cup_match_2p_pickban_step
    ADD CONSTRAINT cup_match_2p_pickban_step_parent_index_fk
    FOREIGN KEY (cup_match_id, parent_index) REFERENCES cup_match_2p_pickban_step(cup_match_id, pickban_index);
