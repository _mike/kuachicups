ALTER TYPE pickban_step_kind ADD VALUE IF NOT EXISTS 'low_lose' AFTER 'high_pick';
ALTER TYPE pickban_step_kind ADD VALUE IF NOT EXISTS 'reset' AFTER 'low_lose';
