
CREATE TABLE post_media_match(
  media_id uuid NOT NULL REFERENCES post_media(id),
  match_id integer NOT NULL REFERENCES cup_match_2p(id),
  UNIQUE (media_id, match_id)
);

CREATE TABLE post_media_stage(
  media_id uuid NOT NULL REFERENCES post_media(id),
  stage_id uuid NOT NULL REFERENCES cup_stage(id),
  UNIQUE (media_id, stage_id)
);

CREATE TABLE post_media_cup(
  media_id uuid NOT NULL REFERENCES post_media(id),
  cup_id uuid NOT NULL REFERENCES cup(id),
  UNIQUE (media_id, cup_id)
);
