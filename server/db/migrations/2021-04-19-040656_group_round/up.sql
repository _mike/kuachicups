-- integer starting at 0 for the round within the group for the match
ALTER TABLE cup_match_2p ADD COLUMN group_round integer;
UPDATE cup_match_2p SET group_round = 0 WHERE group_no IS NOT NULL;
