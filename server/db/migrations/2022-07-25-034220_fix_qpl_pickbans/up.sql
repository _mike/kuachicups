

UPDATE pickbans
  SET steps =
    array[
        ('Map', 'high_ban', null), -- 0
        ('Map', 'low_ban', null), -- 1
        ('Map', 'high_ban', null), -- 2
        ('Map', 'low_ban', null), -- 3
        ('Map', 'high_ban', null), -- 4
        ('Map', 'low_ban', null), -- 5
        ('Map', 'high_pick', null), -- 6
        ('Champion', 'high_ban', 6),
        ('Champion', 'low_ban', 6),
        ('Champion', 'high_pick', 6),
        ('Champion', 'low_pick', 6)
    ]::pickban_step[]
  WHERE slug = 'qc-tld-s1-bo1'
    ;

UPDATE pickbans
  SET steps =
    array[
        ('Map', 'high_ban', null), -- 0
        ('Map', 'low_ban', null), -- 1
        ('Map', 'high_pick', null), -- 2
        ('Map', 'low_pick', null), -- 3
        ('Map', 'high_ban', null), -- 4
        ('Map', 'low_ban', null), -- 5
        ('Map', 'high_pick', null), -- 6
        ('Champion', 'high_ban', 2),
        ('Champion', 'low_pick', 2),
        ('Champion', 'high_pick', 2),
        ('Champion', 'low_ban', 3),
        ('Champion', 'high_pick', 3),
        ('Champion', 'low_pick', 3),
        ('Champion', 'high_ban', 6),
        ('Champion', 'low_pick', 6),
        ('Champion', 'high_pick', 6)
    ]::pickban_step[]
  WHERE slug = 'qc-tld-s1-bo3'
    ;

UPDATE pickbans
  SET steps = array[
    ('Map', 'high_ban', null), -- 0
    ('Map', 'low_ban', null), -- 1
    ('Map', 'high_pick', null), -- 2
    ('Map', 'low_pick', null), -- 3
    ('Map', 'high_pick', null), -- 4
    ('Map', 'low_pick', null), -- 5
    ('Map', 'high_pick', null), -- 6
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6)
  ]::pickban_step[]
  WHERE slug = 'qc-tld-s1-bo5'
;

UPDATE pickbans
  SET steps =
    array[
        ('Map', 'high_ban', null), -- 0
        ('Map', 'low_ban', null), -- 1
        ('Map', 'high_ban', null), -- 2
        ('Map', 'low_ban', null), -- 3
        ('Map', 'high_ban', null), -- 4
        ('Map', 'low_ban', null), -- 5
        ('Map', 'high_pick', null), -- 6
        ('Champion', 'high_ban', 6),
        ('Champion', 'low_ban', 6),
        ('Champion', 'high_pick', 6),
        ('Champion', 'low_pick', 6)
    ]::pickban_step[]
  WHERE slug = 'qc-tld-s3-bo1'
    ;

UPDATE pickbans
  SET steps =
    array[
        ('Map', 'high_ban', null), -- 0
        ('Map', 'low_ban', null), -- 1
        ('Map', 'high_pick', null), -- 2
        ('Map', 'low_pick', null), -- 3
        ('Map', 'high_ban', null), -- 4
        ('Map', 'low_ban', null), -- 5
        ('Map', 'high_pick', null), -- 6
        ('Champion', 'high_ban', 2),
        ('Champion', 'low_pick', 2),
        ('Champion', 'high_pick', 2),
        ('Champion', 'low_ban', 3),
        ('Champion', 'high_pick', 3),
        ('Champion', 'low_pick', 3),
        ('Champion', 'high_ban', 6),
        ('Champion', 'low_pick', 6),
        ('Champion', 'high_pick', 6)
    ]::pickban_step[]
  WHERE slug = 'qc-tld-s3-bo3'
    ;

UPDATE pickbans
  SET steps = array[
    ('Map', 'high_ban', null), -- 0
    ('Map', 'low_ban', null), -- 1
    ('Map', 'high_pick', null), -- 2
    ('Map', 'low_pick', null), -- 3
    ('Map', 'high_pick', null), -- 4
    ('Map', 'low_pick', null), -- 5
    ('Map', 'high_pick', null), -- 6
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6)
  ]::pickban_step[]
  WHERE slug = 'qc-tld-s3-bo5'
;
