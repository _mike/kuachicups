CREATE EXTENSION IF NOT EXISTS moddatetime;

CREATE TABLE cup_stage_final_ranking (
  cup_stage_id uuid NOT NULL REFERENCES cup_stage(id),
  signup_id uuid NOT NULL REFERENCES cup_signup(id),
  index integer NOT NULL,
  placement integer NOT NULL,
  score integer NOT NULL,
  wins integer NOT NULL,
  losses integer NOT NULL,
  group_no integer,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (cup_stage_id, signup_id),
  UNIQUE (cup_stage_id, index)
);

CREATE TRIGGER cup_stage_final_ranking_updated_at
  BEFORE UPDATE ON cup_stage_final_ranking
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
