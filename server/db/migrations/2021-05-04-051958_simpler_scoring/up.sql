ALTER TABLE cup_match_2p ADD COLUMN elim_logical_round integer;

CREATE TYPE pickban_step_kind AS ENUM (
  'low_ban', 'low_pick', 'high_ban', 'high_pick' );

CREATE TYPE pickban_step AS (
  pool_name text,
  kind pickban_step_kind,
  parent_index integer
);

CREATE TYPE pickban_pool_item AS (
  name text,
  slug text
);

CREATE TYPE pickban_pool AS (
  name text,
  slug text,
  items pickban_pool_item[]
);

CREATE TABLE pickbans (
  id serial PRIMARY KEY,
  name text NOT NULL UNIQUE,
  slug text NOT NULL UNIQUE,
  game_mode uuid REFERENCES game_mode(id),
  bestof integer NOT NULL,
  CONSTRAINT bestof_pos_odd CHECK ((bestof > 0) AND ((bestof % 2) <> 0)),
  pool pickban_pool[] NOT NULL DEFAULT array[]::pickban_pool[],
  steps pickban_step[] NOT NULL DEFAULT array[]::pickban_step[]
);

---------------------------
-- Generic BO1/BO3/BO5/BO7
---------------------------

INSERT INTO pickbans(name, slug, bestof) VALUES('Generic BO1', 'bo1', 1);
INSERT INTO pickbans(name, slug, bestof) VALUES('Generic BO3', 'bo3', 3);
INSERT INTO pickbans(name, slug, bestof) VALUES('Generic BO5', 'bo5', 5);
INSERT INTO pickbans(name, slug, bestof) VALUES('Generic BO7', 'bo7', 7);

---------------------------
-- QPL Season 1
---------------------------

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Champions TLD - QPL Season 1 Maps - BO1' AS "name",
  'qc-tld-s1-bo1' AS "slug",
  m.id AS "game_mode",
  1,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Blood Covenant', 'bc')::pickban_pool_item,
      ('Blood Run', 'br')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Molten Falls', 'mf')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null),
    ('Champion', 'high_ban', 7),
    ('Champion', 'low_ban', 7),
    ('Champion', 'high_pick', 7),
    ('Champion', 'low_pick', 7)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Champions TLD - QPL Season 1 Maps - BO3' AS "name",
  'qc-tld-s1-bo3' AS "slug",
  m.id AS "game_mode",
  3,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Blood Covenant', 'bc')::pickban_pool_item,
      ('Blood Run', 'br')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Molten Falls', 'mf')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null),
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Champions TLD - QPL Season 1 Maps - BO5' AS "name",
  'qc-tld-s1-bo5' AS "slug",
  m.id AS "game_mode",
  5,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Blood Covenant', 'bc')::pickban_pool_item,
      ('Blood Run', 'br')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Molten Falls', 'mf')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null), -- 2
    ('Map', 'low_pick', null), -- 3
    ('Map', 'high_pick', null), -- 4
    ('Map', 'low_pick', null), -- 5
    ('Map', 'high_pick', null), -- 6
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

-- ---------------------------
-- -- QPL Season 3
-- --
-- -- Not bothering with 2 because lol koth
-- ---------------------------

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Champions TLD - QPL Season 3 Maps - BO1' AS "name",
  'qc-tld-s3-bo1' AS "slug",
  m.id AS "game_mode",
  1,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Deep Embrace', 'de')::pickban_pool_item,
      ('Exile', 'exile')::pickban_pool_item,
      ('Molten Falls', 'mf')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null), -- 7
    ('Champion', 'high_ban', 7),
    ('Champion', 'low_ban', 7),
    ('Champion', 'high_pick', 7),
    ('Champion', 'low_pick', 7)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Champions TLD - QPL Season 3 Maps - BO3' AS "name",
  'qc-tld-s3-bo3' AS "slug",
  m.id AS "game_mode",
  3,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Deep Embrace', 'de')::pickban_pool_item,
      ('Exile', 'exile')::pickban_pool_item,
      ('Molten Falls', 'mf')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null), -- 3
    ('Map', 'low_pick', null), -- 4
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null), -- 7
    ('Champion', 'high_ban', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_ban', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_ban', 7),
    ('Champion', 'low_pick', 7),
    ('Champion', 'high_pick', 7)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Champions TLD - QPL Season 3 Maps - BO5' AS "name",
  'qc-tld-s3-bo5' AS "slug",
  m.id AS "game_mode",
  5,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Deep Embrace', 'de')::pickban_pool_item,
      ('Exile', 'exile')::pickban_pool_item,
      ('Molten Falls', 'mf')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null), -- 3
    ('Map', 'low_pick', null), -- 4
    ('Map', 'high_pick', null), -- 5
    ('Map', 'low_pick', null), -- 6
    ('Map', 'high_pick', null), -- 7
    ('Champion', 'high_ban', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_ban', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6),
    ('Champion', 'high_ban', 7),
    ('Champion', 'low_pick', 7),
    ('Champion', 'high_pick', 7)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

---------------------------
-- Diabotical
---------------------------

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Diabotical - Duel - BO1' AS "name",
  'dbt-kuachi-bo1' AS "slug",
  m.id AS "game_mode",
  1,
  array[
    ('Map', 'map', array[
      ('Amberfall', 'amberfall')::pickban_pool_item,
      ('Bioplant', 'bioplant')::pickban_pool_item,
      ('Kasbah', 'kasbah')::pickban_pool_item,
      ('Raya', 'raya')::pickban_pool_item,
      ('Restless', 'restless')::pickban_pool_item,
      ('Sanctum', 'sanctum')::pickban_pool_item,
      ('Skybreak', 'skybreak')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[],
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Diabotical' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Diabotical - Duel - BO3' AS "name",
  'dbt-kuachi-bo3' AS "slug",
  m.id AS "game_mode",
  3,
  array[
    ('Map', 'map', array[
      ('Amberfall', 'amberfall')::pickban_pool_item,
      ('Bioplant', 'bioplant')::pickban_pool_item,
      ('Kasbah', 'kasbah')::pickban_pool_item,
      ('Raya', 'raya')::pickban_pool_item,
      ('Restless', 'restless')::pickban_pool_item,
      ('Sanctum', 'sanctum')::pickban_pool_item,
      ('Skybreak', 'skybreak')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[],
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Diabotical' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Diabotical - Duel - BO5' AS "name",
  'dbt-kuachi-bo5' AS "slug",
  m.id AS "game_mode",
  5,
  array[
    ('Map', 'map', array[
      ('Amberfall', 'amberfall')::pickban_pool_item,
      ('Bioplant', 'bioplant')::pickban_pool_item,
      ('Kasbah', 'kasbah')::pickban_pool_item,
      ('Raya', 'raya')::pickban_pool_item,
      ('Restless', 'restless')::pickban_pool_item,
      ('Sanctum', 'sanctum')::pickban_pool_item,
      ('Skybreak', 'skybreak')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[],
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_pick', null)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Diabotical' AND m.name = 'Duel';

---------------------------

ALTER TABLE cup_stage_scoring ADD COLUMN pickbans integer REFERENCES pickbans(id);

CREATE TABLE cup_match_2p_pickban_step (
  cup_match_id integer NOT NULL REFERENCES cup_match_2p(id),
  actor uuid NOT NULL REFERENCES cup_signup(id),
  pickban_index integer NOT NULL,
  pickbans integer NOT NULL REFERENCES pickbans(id),
  UNIQUE (cup_match_id, pickban_index),
  pool_name text NOT NULL,
  pool_slug text NOT NULL,
  kind pickban_step_kind NOT NULL,
  item pickban_pool_item NOT NULL,
  pool_filter bool[][] NOT NULL,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now()
);

