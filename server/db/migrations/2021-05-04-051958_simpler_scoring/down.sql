ALTER TABLE cup_match_2p DROP COLUMN IF EXISTS elim_logical_round;
DROP TABLE IF EXISTS cup_match_2p_pickban_step CASCADE;
DROP TABLE IF EXISTS cup_stage_ruleset CASCADE;
DROP TYPE elim_cfg;
DROP TABLE IF EXISTS pickbans CASCADE;
DROP TYPE pickban_pool ;
DROP TYPE pickban_pool_item ;
DROP TYPE pickban_step ;
DROP TYPE pickban_step_kind;
