
CREATE TABLE data_migration(
    id serial NOT NULL PRIMARY KEY,
    name text NOT NULL UNIQUE,
    created_at timestamp DEFAULT now()
);
