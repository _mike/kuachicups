
INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'kuachicon BO1' AS "name",
  'kuachicon-bo1' AS "slug",
  m.id AS "game_mode",
  1,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,          -- 0
      ('Blood Covenant', 'bc')::pickban_pool_item,      -- 1
      ('Corrupted Keep', 'ck')::pickban_pool_item,      -- 2
      ('Deep Embrace', 'de')::pickban_pool_item,        -- 3
      ('Insomnia', 'insomnia')::pickban_pool_item,      -- 4
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item, -- 5
      ('Vale of Pnath', 'vale')::pickban_pool_item      -- 6
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'low_ban', null),                           -- 0
    ('Map', 'high_ban', null),                          -- 1
    ('Map', 'low_ban', null),                           -- 2
    ('Map', 'high_ban', null),                          -- 3
    ('Map', 'low_ban', null),                           -- 4
    ('Map', 'high_pick', null),                         -- 5
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_ban', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'low_pick', 5)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'kuachicon BO3' AS "name",
  'kuachicon-bo3' AS "slug",
  m.id AS "game_mode",
  3,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,          -- 0
      ('Blood Covenant', 'bc')::pickban_pool_item,      -- 1
      ('Corrupted Keep', 'ck')::pickban_pool_item,      -- 2
      ('Deep Embrace', 'de')::pickban_pool_item,        -- 3
      ('Insomnia', 'insomnia')::pickban_pool_item,      -- 4
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item, -- 5
      ('Vale of Pnath', 'vale')::pickban_pool_item      -- 6
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),                          -- 0
    ('Map', 'low_ban', null),                           -- 1
    ('Map', 'high_pick', null),                         -- 2
    ('Map', 'low_pick', null),                          -- 3
    ('Map', 'high_ban', null),                          -- 4
    ('Map', 'low_pick', null),                          -- 5
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'kuachicon BO5' AS "name",
  'kuachicon-bo5' AS "slug",
  m.id AS "game_mode",
  5,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,          -- 0
      ('Blood Covenant', 'bc')::pickban_pool_item,      -- 1
      ('Corrupted Keep', 'ck')::pickban_pool_item,      -- 2
      ('Deep Embrace', 'de')::pickban_pool_item,        -- 3
      ('Insomnia', 'insomnia')::pickban_pool_item,      -- 4
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item, -- 5
      ('Vale of Pnath', 'vale')::pickban_pool_item      -- 6
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'high_ban', null),                          -- 0
    ('Map', 'low_ban', null),                           -- 1
    ('Map', 'high_pick', null),                         -- 2
    ('Map', 'low_pick', null),                          -- 3
    ('Map', 'high_pick', null),                         -- 4
    ('Map', 'low_pick', null),                          -- 5
    ('Map', 'high_pick', null),                         -- 6
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'low_ban', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6),
    ('Champion', 'high_ban', 7),
    ('Champion', 'low_pick', 7),
    ('Champion', 'high_pick', 7),
    ('Champion', 'high_ban', 8),
    ('Champion', 'low_pick', 8),
    ('Champion', 'high_pick', 8)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'kuachicon BO7 map in hand' AS "name",
  'kuachicon-bo7-gf' AS "slug",
  m.id AS "game_mode",
  7 as "bestof",
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,
      ('Blood Covenant', 'bc')::pickban_pool_item,
      ('Corrupted Keep', 'ck')::pickban_pool_item,
      ('Deep Embrace', 'de')::pickban_pool_item,
      ('Insomnia', 'insomnia')::pickban_pool_item,
      ('Ruins of Sarnath', 'ruins')::pickban_pool_item,
      ('Vale of Pnath', 'vale')::pickban_pool_item
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",
  array[
    ('Map', 'low_lose', null), -- Pick an empty "first" map that the high seed has in hand
    ('Map', 'high_ban', null),
    ('Map', 'low_pick', null), -- 2
    ('Map', 'high_pick', null), -- 3
    ('Map', 'low_pick', null), -- 4
    ('Map', 'high_pick', null), -- 5
    ('Map', 'low_pick', null), -- 6
    ('Map', 'high_pick', null), -- 7
    -- Reset after the 4th map
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'high_ban', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'reset', null),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6),
    ('Champion', 'high_ban', 7),
    ('Champion', 'low_pick', 7),
    ('Champion', 'high_pick', 7)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';
