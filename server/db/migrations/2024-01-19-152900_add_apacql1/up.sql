
INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Live - Duel - BO1' AS "name",
  'apacql1-bo1' AS "slug",
  m.id AS "game_mode",
  1,
  array[
    ('Map', 'map', array[
      ('Aerowalk', 'aerowalk')::pickban_pool_item,
      ('Blood Run', 'ztn')::pickban_pool_item,
      ('Cure', 'cure')::pickban_pool_item,
      ('Furious Heights', 't7')::pickban_pool_item,
      ('Toxicity', 'tox')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[],
  array[
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Live' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Live - Duel - BO3' AS "name",
  'apacql1-bo3' AS "slug",
  m.id AS "game_mode",
  3,
  array[
    ('Map', 'map', array[
      ('Aerowalk', 'aerowalk')::pickban_pool_item,
      ('Blood Run', 'ztn')::pickban_pool_item,
      ('Cure', 'cure')::pickban_pool_item,
      ('Furious Heights', 't7')::pickban_pool_item,
      ('Toxicity', 'tox')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[],
  array[
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_ban', null),
    ('Map', 'low_ban', null),
    ('Map', 'high_pick', null)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Live' AND m.name = 'Duel';

INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'Quake Live - Duel - BO5' AS "name",
  'apacql1-bo5' AS "slug",
  m.id AS "game_mode",
  5,
  array[
    ('Map', 'map', array[
      ('Aerowalk', 'aerowalk')::pickban_pool_item,
      ('Blood Run', 'ztn')::pickban_pool_item,
      ('Cure', 'cure')::pickban_pool_item,
      ('Furious Heights', 't7')::pickban_pool_item,
      ('Toxicity', 'tox')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[],
  array[
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_pick', null),
    ('Map', 'low_pick', null),
    ('Map', 'high_pick', null)
  ]::pickban_step[] AS "steps"
  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Live' AND m.name = 'Duel';
