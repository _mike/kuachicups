#!/usr/bin/env bash

tables=(
  cup
  cup_match_2p
  cup_match_2p_pickban_step
  cup_signup_result
  cup_stage
  cup_stage_scoring
  game
  game_mode
  movine_migrations
  player
  post_ann
  post_media
  scoring
  team_member
  team
)


echo "##### UP ######"
for table in "${tables[@]}"; do
  envsubst <<EOF
CREATE TRIGGER ${table}_updated_at
  BEFORE UPDATE ON ${table}
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
EOF
done

echo "##### DOWN ######"
for table in "${tables[@]}"; do
  envsubst <<EOF
DROP TRIGGER ${table}_updated_at;
EOF
done
