
CREATE TRIGGER cup_updated_at
  BEFORE UPDATE ON cup
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER cup_match_2p_updated_at
  BEFORE UPDATE ON cup_match_2p
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER cup_match_2p_pickban_step_updated_at
  BEFORE UPDATE ON cup_match_2p_pickban_step
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER cup_signup_result_updated_at
  BEFORE UPDATE ON cup_signup_result
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER cup_stage_updated_at
  BEFORE UPDATE ON cup_stage
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER cup_stage_scoring_updated_at
  BEFORE UPDATE ON cup_stage_scoring
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER game_updated_at
  BEFORE UPDATE ON game
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER game_mode_updated_at
  BEFORE UPDATE ON game_mode
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER movine_migrations_updated_at
  BEFORE UPDATE ON movine_migrations
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER player_updated_at
  BEFORE UPDATE ON player
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER post_ann_updated_at
  BEFORE UPDATE ON post_ann
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER post_media_updated_at
  BEFORE UPDATE ON post_media
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER scoring_updated_at
  BEFORE UPDATE ON scoring
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER team_member_updated_at
  BEFORE UPDATE ON team_member
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
CREATE TRIGGER team_updated_at
  BEFORE UPDATE ON team
  FOR EACH ROW
  EXECUTE PROCEDURE moddatetime(updated_at);
