
ALTER TABLE player ALTER COLUMN discord_discriminator DROP NOT NULL;

DROP INDEX player_fts_idx;
ALTER TABLE player DROP COLUMN discord_tag CASCADE;
ALTER TABLE player ADD COLUMN discord_tag text NOT NULL GENERATED ALWAYS AS (
    COALESCE(
        ('@' || discord_username || '#' || discord_discriminator),
        ('@' || discord_username)
    )
) STORED;

CREATE INDEX player_fts_idx ON player USING GIN (to_tsvector('english', discord_tag));

CREATE VIEW site_admin_player AS
    SELECT player.*
    FROM player
    JOIN team_member ON player.id = team_member.player_id
    JOIN admin_team ON admin_team.team_id = team_member.team_id;
