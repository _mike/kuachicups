CREATE TABLE elo_update(
  id serial PRIMARY KEY NOT NULL,
  game_mode_id uuid NOT NULL REFERENCES game_mode(id),
  team_id uuid,
  player_id uuid,
  source_match integer NOT NULL REFERENCES cup_match_2p(id),
  elo numeric(6,2) NOT NULL,
  created_at timestamptz NOT NULL DEFAULT now(),
  UNIQUE (source_match, player_id)
);
