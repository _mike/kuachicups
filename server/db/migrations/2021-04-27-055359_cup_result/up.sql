
CREATE TABLE cup_signup_result (
  id uuid NOT NULL PRIMARY KEY REFERENCES cup_signup(id),
  -- invariant: cup_id, team_id and player_id MUST be the same as what you'd get
  -- from the signup with id signup_id
  team_id uuid REFERENCES team(id),
  player_id uuid NOT NULL REFERENCES player(id),
  cup_id uuid NOT NULL REFERENCES cup(id),
  -- higher is better
  score integer NOT NULL,
  updated_at timestamptz NOT NULL DEFAULT now(),
  UNIQUE(cup_id, id)
);
