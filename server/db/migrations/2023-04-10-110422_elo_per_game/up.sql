
DELETE FROM elo_update;

-- cause the migration that computes elo to run again
DELETE FROM data_migration WHERE name = 'add_elo';

ALTER TABLE elo_update ADD COLUMN source_match_game integer NOT NULL DEFAULT 0;
ALTER TABLE elo_update ADD COLUMN elo_diff numeric(6,2) NOT NULL;
ALTER TABLE elo_update DROP CONSTRAINT elo_update_source_match_player_id_key;
ALTER TABLE elo_update 
    ADD CONSTRAINT elo_update_source_match_source_match_game_player_id_key 
    UNIQUE(source_match, source_match_game, player_id);
