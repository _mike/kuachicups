INSERT INTO pickbans(name, slug, game_mode, bestof, pool, steps)
  SELECT
  'KDC Y1-S2 BO7' AS "name",
  'kdc-y1-s2-bo7' AS "slug",
  m.id AS "game_mode",
  7,
  array[
    ('Map', 'map', array[
      ('Awoken', 'awoken')::pickban_pool_item,          -- 0
      ('Blood Covenant', 'bc')::pickban_pool_item,      -- 1
      ('Corrupted Keep', 'ck')::pickban_pool_item,      -- 2
      ('Deep Embrace', 'de')::pickban_pool_item,        -- 3
      ('Insomnia', 'insomnia')::pickban_pool_item,      -- 4
      ('Molten Falls', 'mf')::pickban_pool_item,        -- 5
      ('Vale of Pnath', 'vale')::pickban_pool_item      -- 6
    ])::pickban_pool,
    ('Champion', 'champ', array[
      ('Anarki', 'anarki')::pickban_pool_item,
      ('Athena', 'athena')::pickban_pool_item,
      ('B.J. Blazkowicz', 'bj')::pickban_pool_item,
      ('Clutch', 'clutch')::pickban_pool_item,
      ('Death Knight', 'dk')::pickban_pool_item,
      ('Doom Slayer', 'doom')::pickban_pool_item,
      ('Eisen', 'eisen')::pickban_pool_item,
      ('Galena', 'galena')::pickban_pool_item,
      ('Keel', 'keel')::pickban_pool_item,
      ('Nyx', 'nyx')::pickban_pool_item,
      ('Ranger', 'ranger')::pickban_pool_item,
      ('Scalebearer', 'scale')::pickban_pool_item,
      ('Slash', 'slash')::pickban_pool_item,
      ('Sorlag', 'sorlag')::pickban_pool_item,
      ('Strogg & Peeker', 'strogg')::pickban_pool_item,
      ('Visor', 'visor')::pickban_pool_item
    ])::pickban_pool
  ]::pickban_pool[] AS "pool",

  array[
    ('Map', 'high_pick', null), -- 0 - map in hand, result entered manually
    ('Map', 'low_pick', null),  -- 1 - first actual map pick is low player
    ('Map', 'high_pick', null), -- 2
    ('Map', 'low_pick', null),  -- 3
    ('Map', 'high_pick', null), -- 4
    ('Map', 'low_pick', null),  -- 5
    ('Map', 'high_pick', null), -- 6

    -- Map 1
    ('Champion', 'low_ban', 1),
    ('Champion', 'high_pick', 1),
    ('Champion', 'low_pick', 1),

    -- Map 2
    ('Champion', 'high_ban', 2),
    ('Champion', 'low_pick', 2),
    ('Champion', 'high_pick', 2),

    -- Map 3
    ('Champion', 'low_ban', 3),
    ('Champion', 'high_pick', 3),
    ('Champion', 'low_pick', 3),

    -- Map 4
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),

    -- Map 5
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_pick', 5),

    -- Map 6
    ('Champion', 'high_pick', 6),
    ('Champion', 'low_pick', 6)
  ]::pickban_step[] AS "steps"

  FROM game g JOIN game_mode m ON m.game_id = g.id
  WHERE g.name = 'Quake Champions' AND m.name = 'Duel';
