
UPDATE pickbans
  SET steps = array[
    ('Map', 'high_ban', null),                          -- 0
    ('Map', 'low_ban', null),                           -- 1
    ('Map', 'high_ban', null),                          -- 2
    ('Map', 'low_ban', null),                           -- 3
    ('Map', 'high_pick', null),                         -- 4
    ('Map', 'low_pick', null),                          -- 5
    ('Map', 'high_pick', null),                         -- 6
    ('Map', 'low_pick', null),                          -- 7
    ('Map', 'high_pick', null),                         -- 8
    ('Champion', 'high_ban', 4),
    ('Champion', 'low_pick', 4),
    ('Champion', 'high_pick', 4),
    ('Champion', 'low_ban', 5),
    ('Champion', 'high_pick', 5),
    ('Champion', 'low_pick', 5),
    ('Champion', 'high_ban', 6),
    ('Champion', 'low_pick', 6),
    ('Champion', 'high_pick', 6),
    ('Champion', 'low_ban', 7),
    ('Champion', 'high_pick', 7),
    ('Champion', 'low_pick', 7),
    ('Champion', 'high_ban', 8),
    ('Champion', 'low_pick', 8),
    ('Champion', 'high_pick', 8)
  ]::pickban_step[]
  WHERE slug = 'aql3-bo5';
