
ALTER TABLE cup_stage ADD COLUMN division integer NOT NULL DEFAULT 0;
ALTER TABLE cup_stage ADD CONSTRAINT uniq_cup_stage_div UNIQUE (cup_id, stage_no, division);
