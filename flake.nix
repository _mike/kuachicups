{
  description = "A Rust web server including a NixOS module";

  # Nixpkgs / NixOS version to use.
  inputs = {
    nixpkgs.url = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    npmlock2nix-src = {
      url = "github:nix-community/npmlock2nix";
      flake = false;
    };
  };

  outputs =
    { self, nixpkgs, npmlock2nix-src, flake-utils, crane
    }:
    let
      # maybe use flake-utils.lib.eachDefaultSystem
      system = "x86_64-linux";
      npmlock2nix = pkgs.callPackage npmlock2nix-src { };
      craneLib = crane.lib.${system};
      pkgs = import nixpkgs {
        inherit system;
        overlays = self.overlays;
      };
    in {
      overlays = [
        (final: prev: {
          kuachicups-server = craneLib.buildPackage {
            src = craneLib.cleanCargoSource (craneLib.path ./server);
            buildInputs = with pkgs; [ pkg-config openssl.dev openssl ];
          };
          kuachicups-client = npmlock2nix.build {
            src = ./client;
            installPhase = "cp -r dist $out";
            buildCommands = [ "npm run build" ];
          };
        })
      ];

      defaultPackage.${system} = pkgs.kuachicups-server;
      devShells.${system} = {
        kuachicups-server = pkgs.mkShell {
          inputsFrom = [ pkgs.kuachicups-server ];
          nativeBuildInputs = with pkgs; [
            rustc
            rustfmt
            rust-analyzer
            cargo
            cargo-watch
          ];
        };
        kuachicups-client = npmlock2nix.shell { src = ./client; };
      };
    };
}
