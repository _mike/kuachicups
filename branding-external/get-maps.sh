#!/usr/bin/env zsh

declare -A maps
maps+=(
    ['Awoken']="https://mapban.gg/images/maps/quake-champions/awoken.jpg"
    ['Blood Covenant']="https://mapban.gg/images/maps/quake-champions/blood_covenant.jpg"
    ['Blood Run']="https://mapban.gg/images/maps/quake-champions/blood_run.jpg"
    ['Burial Chamber']="https://mapban.gg/images/maps/quake-champions/burial_chamber.jpg"
    ['Church of Azathoth']="https://mapban.gg/images/maps/quake-champions/church_of_azathoth.jpg"
    ['Corrupted Keep']="https://mapban.gg/images/maps/quake-champions/corrupted_keep.jpg"
    ['Deep Embrace']="https://mapban.gg/images/maps/quake-champions/deep_embrace.jpg"
    ['Lockbox']="https://mapban.gg/images/maps/quake-champions/lockbox.jpg"
    ['Longest Yard']="https://mapban.gg/images/maps/quake-champions/longest_yard.jpg"
    ['Molten Falls']="https://mapban.gg/images/maps/quake-champions/molten_falls.jpg"
    ['Ruins of Sarnath']="https://mapban.gg/images/maps/quake-champions/ruins_of_sarnath.jpg"
    ['Tempest Shrine']="https://mapban.gg/images/maps/quake-champions/tempest_shrine.jpg"
    ['Tower of Koth']="https://mapban.gg/images/maps/quake-champions/tower_of_koth.jpg"
    ['Vale of Pnath']="https://mapban.gg/images/maps/quake-champions/vale_of_pnath.jpg"
    ['Insomnia']="https://churchofquake.com/wp-content/uploads/2022/02/Loading-Screen-Insomnia-768x324.png"
    ['Exile']="https://images.ctfassets.net/rporu91m20dc/5pXFKsivPzMnGbnZZlM46S/566253399cd9ba5eb6a34d90b79d7ca3/quake_exile.jpg"
    ['Crucible']="https://churchofquake.com/wp-content/uploads/2022/12/Crucible_Arena_Final-1024x576.png"
)

mkdir -p "Quake Champions/maps"

for map url in "${(@kv)maps}"; do
  ext="${url##*/}"
  ext="${ext#*.}"
  out="Quake Champions/maps/${map}.jpg"
  echo "map: $map"
  echo "url: $url"
  echo "ext: $ext"
  echo "out: $out"
  if [ ! -f "$out" ]; then 
    wget "$url" -q -O - | convert "${ext}:-" "$out"
  fi
done
