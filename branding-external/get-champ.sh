#!/usr/bin/env zsh

declare -A champs
champs+=(
    ['Nyx']="https://images.ctfassets.net/rporu91m20dc/4bzuYGRazuOC4I6Wow6osu/328645182d4538df46f0f866ad2afeb7/nyx-thumb.jpg"
    ['Scalebearer']="https://images.ctfassets.net/rporu91m20dc/3Z1Y24gjpYQqu04Uoq8MqK/1b65975fb4d746d934cae7967d83c316/scalebearer-thumb.jpg"
    ['Anarki']="https://images.ctfassets.net/rporu91m20dc/1zXXCxkzfSAYScuUqkE0uu/796e9baa3aed00bffcec1b03134691c5/champ5-thumb.jpg"
    ['Slash']="https://images.ctfassets.net/rporu91m20dc/1o0Zv6k3bK8Suksec2QKue/caa8c70394dcb596c4a78503c9a76537/champion-thumbs_SLASH.png"
    ['Clutch']="https://images.ctfassets.net/rporu91m20dc/6riOh6Hne8iOyeKeAG8oY/3b43ba1385848fd682c23eb9efd02ac2/champ7-thumb.jpg"
    ['Galena']="https://images.ctfassets.net/rporu91m20dc/2GdiTkazXaCaekMGCmc6Q4/7e111097309b096532e865bc7d58c7cf/champ6-thumb.jpg"
    ['Ranger']="https://images.ctfassets.net/rporu91m20dc/6HZPxtOE7YqK2IGWKc2kaU/9d20ae73ca14ca069bdae25b29573518/ranger-thumb.jpg"
    ['Visor']="https://images.ctfassets.net/rporu91m20dc/2lxtK2vYMEYA2SIAM48MUs/70c1de87ffe4b06215b482cac54f765d/visor-thumb.jpg"
    ['Sorlag']="https://images.ctfassets.net/rporu91m20dc/2hzDQg4aYISugiygsKOeKO/ff45399cede2e63acf8858675d7eb9fb/champ8-thumb.jpg"
    ['BJ Blazkowicz']="https://images.ctfassets.net/rporu91m20dc/1V0TBPe6sImOSa8AIEsGmE/50f63e9f17cdae2f98356733e6f2e55c/BJ-thumbs.png"
    ['Doom Slayer']="https://images.ctfassets.net/rporu91m20dc/bccvhdkBhuqUKWe8U0O86/7eef3608289aef9e21055847239963fd/Doom-thumbs.jpg"
    ['Keel']="https://images.ctfassets.net/rporu91m20dc/4HzQBK0AJOmAescG0You4/654a513fe00fb996d91349f0fbf4cf50/keel-thumb.jpg"
    ['Strogg & Peeker']="https://images.ctfassets.net/rporu91m20dc/577WeDHf9uY0EciWAK2cyE/36e478e43e14c2789fc7853f0d3af891/STROGG_thumbnail.jpg"
    ['Death Knight']="https://images.ctfassets.net/rporu91m20dc/GQhbUPHfki22KyeiIacgy/0c3baaa00bf20d9f2e4719d144908cfe/QC-DeathKnight-684x380-03.jpg"
    ['Athena']="https://images.ctfassets.net/rporu91m20dc/2rvsofLWdCqG4ComkUKC0U/81da29dd2ac6142629bd284f75cf0ce9/QC-Athena-684x380-03__002_.jpg"
    ['Eisen']="https://images.ctfassets.net/rporu91m20dc/x5MnQAHnDaKcc8eOGWQWe/8ee12050324bab153f1d7ab4ccee35e7/QC-Eisen-684x380-01.jpg"
)

mkdir -p "Quake Champions/champs"

for champ url in "${(@kv)champs}"; do
  echo "champ: $champ"
  echo "url: $url"
  wget "$url" -O "Quake Champions/champs/${champ}.jpg"
done
