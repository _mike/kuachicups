FROM rust:1.71.1-slim-bookworm AS server-builder
    WORKDIR /server
    RUN USER=root apt-get update && apt-get install -y apt-utils \
      && apt-get install -y libssl-dev pkg-config sqlite3 libsqlite3-dev \
      && apt-get clean && rm -rf /var/lib/apt/lists/*
    RUN USER=root cargo init && cargo install movine --version=^0.8
    COPY ./server/Cargo.toml ./server/Cargo.lock ./
    RUN mkdir -p ./src/bin && \
        echo 'fn main() {}' > ./src/bin/sqlx.rs && \
        echo 'fn main() {}' > ./src/main.rs && \
        cargo build --release && \
        rm -Rvf src && \
        rm ./target/release/deps/kuachicups*
    COPY ./server/sqlx-data.json ./sqlx-data.json
    COPY ./server/src ./src
    RUN SQLX_OFFLINE=true RUST_LOG=info cargo build --release

FROM node:21-bookworm-slim AS client-node-cache
    WORKDIR /client
    RUN apt-get update && apt-get -y install build-essential python3
    COPY ./client/package*.json ./
    RUN npm install --no-audit --verbose

FROM node:21-bookworm-slim AS client-builder
    WORKDIR /client
    RUN apt-get update && apt-get -y install build-essential python3
    COPY --from=client-node-cache /client/node_modules ./node_modules
    COPY ./client/package.json ./client/.babelrc ./client/.sassrc ./client/tsconfig.json ./client/package-lock.json ./client/vite.config.ts ./
    COPY ./client/index.html ./index.html
    COPY ./client/stream/overlay/index.html ./stream/overlay/index.html
    COPY ./client/stream/remote/index.html ./stream/remote/index.html
    COPY ./client/public/ads.txt ./public/ads.txt
    COPY ./client/src/resources/EriMotionArrayDotCom.ogg ./src/resources/EriMotionArrayDotCom.ogg
    COPY ./client/src ./src
    COPY ./branding/surfshamb_logo_640.png ./src/resources/surfshamb_logo_640.png
    COPY ./branding/aql1.png ./src/resources/aql1.png
    COPY ./branding/logo_text.png ./src/resources/logo_text.png
    COPY ./branding/logo_text_whitebg.png ./src/logo_text_whitebg.png
    COPY ./branding/favicon.png ./src/favicon.png
    COPY ./branding-external/Digital-Patreon-Logo_White_64x64.png ./src/patreon_logo.png
    COPY ./branding-external/Discord-Logo-Color.png ./src/resources/discord_logo.png
    COPY ./branding-external/dazza256.png ./src/resources/dazza256.png
    COPY ./branding-external/redbubble.png ./src/resources/redbubble.png
    COPY ["./branding-external/Quake Champions", "./src/resources/qc"]
    COPY ["./branding-external/Quake Live", "./src/resources/ql"]
    RUN NODE_ENV=production npx vite build --outDir /var/www --base /static
    RUN cp ./public/ads.txt /var/www/ads.txt

FROM debian:bookworm-slim
    RUN apt-get update && apt-get -y install curl sqlite3 libssl3 libssl-dev && apt-get clean && rm -rf /var/lib/apt/lists/*
    COPY --from=server-builder /usr/local/cargo/bin/movine /bin/
    COPY --from=server-builder /server/target/release/kuachicups-server /bin/
    COPY --from=client-builder /var/www /var/www
    COPY ./server/db/migrations /db/migrations
    COPY ./server/db/movine-server.toml /db/movine.toml
